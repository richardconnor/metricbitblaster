#!/usr/bin/env bash

set -o errexit
set -o nounset

if [ $# -eq 2 ]; then
        JAVACLASSNAME=$1
        JAVAPARAMS="$2"

        MAVEN_OPTS="-XX:ParallelGCThreads=1 -Xmx16G" time mvn exec:java -q -Dexec.cleanupDaemonThreads=false -Dexec.mainClass=$JAVACLASSNAME -e -Dexec.args="$JAVAPARAMS"
        if [ $? -ne 0 ] ; then
                echo "Make sure you are in the repository root!!!"
        fi
elif [ $# -eq 1 ]; then
        JAVACLASSNAME=$1

        MAVEN_OPTS="-XX:ParallelGCThreads=1 -Xmx16G" time mvn exec:java -q -Dexec.cleanupDaemonThreads=false -Dexec.mainClass=$JAVACLASSNAME -e
        if [ $? -ne 0 ] ; then
                echo "Make sure you are in the repository root!!!"
        fi
else
        echo "Give me 2 arguments!"
        echo "First one is the name of the Java class, including package names"
        echo "Second is params in quotes"
        exit 1
fi
