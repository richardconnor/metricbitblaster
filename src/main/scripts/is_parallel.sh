#!/usr/bin/env bash

set -o errexit
set -o nounset

SCRIPTS_DIR=$PWD

NUM_REPEATS=10
MAX_PAR=24

cd ../../../

echo -e "code\tcontext\tpar\tdata\tquery\tthreshold\tnoResults\ttime"

for repeats in $(seq 1 1 $NUM_REPEATS)
do
        for par in $(seq 1 1 $MAX_PAR)
        do
            $SCRIPTS_DIR/runjava.sh uk.al_richard.metricbitblaster.experimental.mutiThreaded.forISPaper.MainRunnerFileMT "$par"
            $SCRIPTS_DIR/runjava.sh uk.al_richard.metricbitblaster.experimental.mutiThreaded.forISPaper.MainRunnerFileMTV4 "$par"
            $SCRIPTS_DIR/runjava.sh uk.al_richard.metricbitblaster.experimental.mutiThreaded.forISPaper.MainRunnerFileMTV7 "$par"
        done
done
