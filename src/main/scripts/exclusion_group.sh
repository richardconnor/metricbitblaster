#!/usr/bin/env bash

set -o errexit
set -o nounset

OUTPUT=ExclusionGroupresults.txt
datasize=1000000
queries=1000
fourpoint="false"
metric="euc"

SCRIPTS_DIR=$PWD

cd ../../../

degree_parallelism=1

echo -e "codebase\tmetric\tdata_size\tqueries_size\t threshold\tsepA\tnoOfRefPoints\tnum_groups\tnum_bitsets\tnum_balls\tnum_sheets\tdegree_parallel\tfraction_std_dev\tsepB\tcantbes\tmustbes\tpartitionsExcluded\tsepC\texclusion_time\tbit_time\tfilter_time\ttotal_time\tsepD\tdistance_filter_operations\tdistance_filter_operations/query\tno_results" >> $OUTPUT


#for repeats in {1..10}
#do
        for no_rings_sheets in $(seq 2 1 5)
        do
                for no_refs in $(seq 150 10 200)
                do
                        for fraction_std_dev in 0.25 0.5 0.75 1 1.25 1.5
                        do
                            $SCRIPTS_DIR/runJava.sh uk.al_richard.metricbitblaster.runners.ExclusionGroup "$no_rings_sheets $no_refs $fraction_std_dev $degree_parallelism" >> $OUTPUT
                        done
                done
        done
#done