#!/usr/bin/env bash

set -o errexit
set -o nounset

OUTPUT=ExponentQuartilesresults.txt
datasize=1000000
queries=1000
fourpoint="false"
metric="euc"

SCRIPTS_DIR=$PWD

echo sd: $PWD

cd ../../../

echo -e "codebase\tmetric\tdimensions\tdata_size\tqueries_size\tthreshold\tnoOfRefPoints\tnum_partitions\tnum_balls\tnum_sheets\tsep1\tcantbes\tmustbes\tpartitionsExcluded\tsep2\texclusion_time\tbit_time\tfilter_time\ttotal_time\tsep3\tdistance_filter_operations\tdistance_filter_operations/query\tno_results" > $OUTPUT

for repeats in {1..10}
do
        for dims in {5..20}
        do
                for refs in $(seq 10 10 200)
                do
                        #  Params: dimensions datasize  query_size  number_of_refs metricName four_point
                        $SCRIPTS_DIR/runJava.sh uk.al_richard.metricbitblaster.runners.MainRunnerFileDistanceExponentQuartilesMTInstrumentedParams "$dims $datasize $queries $refs $metric $fourpoint" >> $OUTPUT
                done
        done
done