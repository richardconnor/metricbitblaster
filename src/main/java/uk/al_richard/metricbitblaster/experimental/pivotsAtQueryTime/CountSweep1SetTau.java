package uk.al_richard.metricbitblaster.experimental.pivotsAtQueryTime;

public class CountSweep1SetTau {

    public static void main(String[] args) throws Exception {

        System.out.println( "mean bits\tmax bits\tmin bits\ttau\tdists/q\tmust bes/q\tcant bes/q\tnoOfResults\ttime" );
        for (int i = 0; i <= 100; i++) {
            float tau = 0.01f * i;
            CountBitsSet1SetTau instance = new CountBitsSet1SetTau();
            instance.runMain( tau );
        }

    }

}
