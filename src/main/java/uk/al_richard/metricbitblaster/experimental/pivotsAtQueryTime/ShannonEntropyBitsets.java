package uk.al_richard.metricbitblaster.experimental.pivotsAtQueryTime;

import dataPoints.cartesian.CartesianPoint;
import uk.al_richard.metricbitblaster.util.OpenBitSet;

import java.util.*;

/**
 * This class caclulates the Shannon Entropy for columns.
 *
 * @author al@st-andrews.ac.uk
 * 23/3/2022
 *
 */
public class ShannonEntropyBitsets {

    /**
     * This method initialises the most different list (the only point of this class).
     * Most
     * @param datarep - the bits representing inclusions and exclusions
     * @return the most different columns created by this class
     */
    public static List<Integer> getMostDifferent(OpenBitSet[] datarep, int datasize ) {

        // probability_denominators is a list of the columns with each entry the denominator of the probability of a 1 in that position
        List<Count<Double>> entropy = shanonEntropy(datarep, datasize);
        //moveZerosToEnd(entropy);
        return initialiseMostDifferent(entropy);
    }

    /**
     *
     * @param datarep
     * @param datasize
     * @return the entropy of each of the columns.
     */
    public static List<Count<Double>> shanonEntropy(OpenBitSet[] datarep, int datasize) {
        int columns = datarep.length;
        int rows = datasize;
        List<Count<Double>> entropy = new ArrayList<>(); // used to store the entropy of each column
        for(int i = 0; i < columns; i++ ) { entropy.add( new Count<Double>(i,0d) ); }

        for(int row = 0; row < rows; row++ ) {
            long count_1s = 0; // counts the set bits on a row
            // first count the bits set in a row
                for(int column = 0; column < columns; column++ ) {
                if( datarep[column].get(row) ) {
                    count_1s++;
                }
            }
            // now distribute the counts into the counts table
            for(int column = 0; column < columns; column++ ) {
                if (datarep[column].get(row)) { // counts the ones
                    entropy.get(column).data = entropy.get(column).data + bitEntropy(count_1s);
                }
            }
            for(int column = 0; column < columns; column++ ) {
                Double exp = entropy.get(column).data;
                entropy.get(column).data = Math.pow(10, -exp);
            }
        }
        return entropy;
    }

    /**
     *
     * @param count_1s - the inverse of the probablity of this being a 1.
     * @return the entropy of this bit = P(x_i).log( P(x_i) ) uses base 10 doesn't matter?
     */
    private static Double bitEntropy(long count_1s) {
        double p_count_1s = 1.0d / ((double) count_1s);
        return p_count_1s * Math.log( p_count_1s );
    }

    /**
     * This initialises the most_different list
     * The most different columns are at the start of the list.
     * @param entropies - an list of the column entropies
     */
    private static List<Integer> initialiseMostDifferent(List<Count<Double>> entropies) {

        Collections.sort(entropies,Collections.reverseOrder());
        return createMostDifferentFromEntropies(entropies);
    }

    /**
     * This method transforms a list of Counts into a list of Integers (identifying columns) by discarding the count values.
     * @param counts - an array of Counts: entropy and column ids
     * @return the most different columns in order by traversing the frequency list
     */
    private static List<Integer> createMostDifferentFromEntropies(List<Count<Double>> counts) {
        List<Integer> result = new ArrayList<>();
        for( Count<Double> c : counts ) {
            result.add( c.position);
        }
        return result;
    }

    /**
     * This method takes any zero count entries and moves them to the end of the list
     * Zero count entries are all columns of all 1s and as such not different from rest.
     * @param counts - a list of (column) Counts
     */
    private static void moveZerosToEnd(List<Count<Double>> counts) {
        List<Count<Double>> removed = new ArrayList<>();
        Iterator<Count<Double>> it = counts.iterator();
        while (it.hasNext()) {
            Count<Double> next = it.next();
            if (next.data == 0d) {
                it.remove();
                removed.add(next); // don't update whilst iterating
            }
            if(next.data > 0 ) {
                // found them all we can stop.
            }
        }
        counts.addAll(removed);
    }

    /**
     * For debugging - prints a bitmap
     * @param datarep - the exclusion table (usually the one supplied to the constructor)
     */
    private static void showBits(OpenBitSet[] datarep, int datasize) {

        int columns = datarep.length;
        int rows = datasize;

        for( int row = 0; row < rows; row++ ) {
            System.out.print( row + ": ");
            for (int column = 0; column < columns; column++) {
                if (datarep[column].get(row)) {
                    System.out.print("1");
                } else {
                    System.out.print("0");
                }
            }
            System.out.println();
        }
    }

    /**
     * Really a debugging method - shows the most_different list - strictly don't need this but handy.
     */
    public static void showMostDifferent(OpenBitSet[] datarep, int datasize) {
        List<Integer> most_different = getMostDifferent(datarep, datasize);
        List<Count<Double>> entropies = shanonEntropy(datarep, datasize);
        Collections.sort(entropies,Collections.reverseOrder());

        System.out.println( "Most different entropies:" );
        for( Count<Double> entropy : entropies ) {
            System.out.println( entropy.position + ": " + entropy.data );
        }

        System.out.println( "Most different columns:" );
        for( int i : most_different ) {
            System.out.println( i + ": " + printColumn( datarep[i]));
        }
    }

    private static String printColumn(OpenBitSet data) {
        int rows = data.size();

        StringBuilder sb = new StringBuilder();
        for (int row = 0; row < rows; row++) {
            if (data.get(row)) {
                sb.append("1");
            } else {
                sb.append("0");
            }
        }
        return sb.toString();
    }


    public static void printColumnSums(OpenBitSet[] datarep, int datasize) {
        List<Count<Double>> column_counts = shanonEntropy(datarep,datasize);
        System.out.println("Number of columns = " + column_counts.size());
        System.out.println("Column sums:");
        for (Count<Double> count : column_counts) {
            System.out.println(count.data); // let's look at the distribution
        }
    }

    public static void printColumBitCounts(OpenBitSet[] datarep, int datasize) {
        System.out.println("Number of columns = " + datarep.length);
        System.out.println("Number of rows = " + datarep[0].size());
        System.out.println("Number of bits set per column:");
        for (int i = 0; i < datarep.length; i++ ) {
            OpenBitSet column = datarep[i];
            System.out.println( column.cardinality() ); // print the distribution
        }
    }

    public static void printColumBitCountsByKind(OpenBitSet[] datarep, int datasize, List<ExclusionZone<CartesianPoint>> ezs) {
        System.out.println("Number of columns = " + datarep.length);
        System.out.println("Number of rows = " + datarep[0].size());
        System.out.println("Number of bits set per column:");
        System.out.println("Sheets:");
        for (int i = 0; i < datarep.length; i++ ) {
            if( ezs.get(i) instanceof SheetExclusion ) {
                OpenBitSet column = datarep[i];
                System.out.println(column.cardinality()); // let's look at the distribution
            }
        }
        System.out.println("Balls:");
        for (int i = 0; i < datarep.length; i++ ) {
            if( ezs.get(i) instanceof BallExclusion ) {
                OpenBitSet column = datarep[i];
                System.out.println(column.cardinality()); // let's look at the distribution
            }
        }
    }

}
