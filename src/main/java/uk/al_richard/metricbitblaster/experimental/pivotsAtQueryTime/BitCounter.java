package uk.al_richard.metricbitblaster.experimental.pivotsAtQueryTime;

import uk.al_richard.metricbitblaster.util.OpenBitSet;

import java.util.ArrayList;
import java.util.List;

/**
 * Bit Counting Utility class
 */
public class BitCounter {

	private static double std_dev;

	public static void doHorizontalCounting(OpenBitSet[] datarep, int rows) {

		System.out.println("Horiz counts");
		for( int i = 0; i < rows; i++ ) {  	 // the rows
			int count = 0;
			for (int j = 0; j < datarep.length; j++) {		 // the colummns
				if( datarep[j].get(i) ) {
					count = count + 1;
				}
			}
			System.out.println( count );
		}
	}

	public static void doVerticalCounting(OpenBitSet[] datarep) {

		System.out.println("Vert counts");
		for( int i = 0 ; i < datarep.length; i++ ) {   // the columns
			OpenBitSet column = datarep[i];            // the ith column
			System.out.println( column.cardinality() );
		}
	}

	public static int countDuplicatesSet(List<OpenBitSet> bits ) {
		int num_rows = bits.get(0).size();
		int total_count = 0;
		for (int i = 0; i < num_rows; i++) {    // the rows
			int row_count = 0;
			for (OpenBitSet column : bits ) {           // the columns
				if (column.get(i)) {                // the bit is set
					row_count++;
				}
			}
			if( row_count > 1 ) {		// only count if there is more than 1 bit set
				total_count += row_count;
			}
		}
		return total_count;
	}

	public static int countUniqueBits( List<OpenBitSet> bits ) {
		int num_rows = bits.get(0).size();
		int total_count = 0;
		for (int i = 0; i < num_rows; i++) {    // the rows
			int row_count = 0;
			for (OpenBitSet column : bits ) {           // the columns
				if (column.get(i)) {               		 // the bit is set
					row_count++;
				}
			}
			if( row_count == 1 ) {		// only count if there is exactly 1 bit set
				total_count ++;
			}
		}
		return total_count;
	}

	public static int countTotalSet( List<OpenBitSet> bits) {
		int num_rows = bits.get(0).size();
		int total_count = 0;
		for (int i = 0; i < num_rows; i++) {    // the rows
			for (OpenBitSet column : bits ) {           // the columns
				if (column.get(i)) {               		 // the bit is set
					total_count ++;
				}
			}
		}
		return total_count;
	}

	public static int countMaxSet(List<OpenBitSet> bits) {
		int max = Integer.MIN_VALUE;

		int num_rows = bits.get(0).size();

		for (int i = 0; i < num_rows; i++) {    // the rows
			int row_count = 0;
			for (OpenBitSet column : bits ) {           // the columns
				if (column.get(i)) {               		 // the bit is set
					row_count ++;
				}
			}
			max = Math.max( max,row_count );
		}
		return max;
	}

	public static int countMinSet(List<OpenBitSet> bits) {
		int min = Integer.MAX_VALUE;

		int num_rows = bits.get(0).size();

		for (int i = 0; i < num_rows; i++) {    // the rows
			int row_count = 0;
			for (OpenBitSet column : bits ) {           // the columns
				if (column.get(i)) {               		 // the bit is set
					row_count ++;
				}
			}
			min = Math.min( min,row_count );
		}
		return min;
	}

	public static int countEmptyRows(List<OpenBitSet> bits) {
		int empty_rows = 0;

		int num_rows = bits.get(0).size();

		for (int i = 0; i < num_rows; i++) {    // the rows
			int row_count = 0;
			for (OpenBitSet column : bits ) {           // the columns
				if (column.get(i)) {               		 // the bit is set
					row_count ++;
				}
			}
			if( row_count == 0 ) {
				empty_rows++;
			}
		}
		return empty_rows;
	}


	public static double countMeanSet(List<OpenBitSet> bits) {
		List<Integer> row_counts = new ArrayList<>();
		int num_rows = bits.get(0).size();
		double total_count = 0;
		for (int i = 0; i < num_rows; i++) {    // the rows
			int row_count = 0;
			for (OpenBitSet column : bits ) {           // the columns
				if (column.get(i)) {               		 // the bit is set
					row_count ++;
				}
			}
			total_count += row_count;
			row_counts.add( row_count );
		}
		double mean = total_count / num_rows;
		std_dev = stddev( row_counts,mean);
		return mean;
	}

	public static double getStdDev() {
		return std_dev;
	}

	private static double stddev(List<Integer> row_counts, double mean) {
		double sd = 0;
		for (int i : row_counts) {
			Double d = Double.valueOf(i);
			sd += Math.pow( d - mean, 2);
		}
		return Math.sqrt( sd / row_counts.size() );
	}
}
