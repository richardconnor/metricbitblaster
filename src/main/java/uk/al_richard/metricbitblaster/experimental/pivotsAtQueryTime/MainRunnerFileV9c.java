package uk.al_richard.metricbitblaster.experimental.pivotsAtQueryTime;

import coreConcepts.CountedMetric;
import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;
import testloads.TestContext;
import testloads.TestContext.Context;
import uk.al_richard.metricbitblaster.util.OpenBitSet;

import java.text.DecimalFormat;
import java.util.*;

/**
 * Ninth version of MainRunnerFile
 * Uses offsets from new Matlab code
 * Tries to find an optimal set of pivots based on combined scores of all columns.
 * Adds the contributions of each column and attributes them to ROs.
 */
public class MainRunnerFileV9c {

	/* Constants/config */

	public static final int NUMBER_OPERATIONAL_PIVOTS = 100; //140 // 3X normal size + 20
	public static final int NUMBER_CONFIG_PIVOTS = 2000; // 4000; // 2000 is 50X normal number for Euc20 (40)
	public static final int WITNESS_SET_SIZE = 500; // confirmed as 99% the same by WitnessSetSize.
	public static final int PARTS_SELECTED = 1000;

	int SE_WITNESS_SET_SIZE = 5001;   // TODO why? Like this in ref impl
	int BALL_WITNESS_SET_SIZE = 1000; // TODO why? Like this in ref impl

	protected static final double RADIUS_INCREMENT = 0.3;
	private static double MEAN_DIST = 1.81;

	// From Matlab
	// Balls:
	// xoffset_peak1 = 1.2403  // from dataset 10
	// xoffset_peak2 = 2.3092
	// Sheets:
	// java_mid_offset1 = -0.4782
	// java_mid_offset2 = 0.5877

	protected static double[] ball_radii =  new double[] {1.2403,2.3092}; // from Matlab trace //{0.794796,1.51698 }; //{ 1.44, 1.946 }; // old Matlab
	protected static double[] sheet_offsets =  new double[] {-0.4782,0.5877}; // from Matlab trace{-0.310454,0.40342}; // { -0.2878,0.3298 }; old matlab

	private Context context;
	private boolean fourPoint;
	private boolean balanced;
	private boolean rotationEnabled;
	private List<CartesianPoint> queries;
	private RefPointSet<CartesianPoint> rps;
	private List<ExclusionZone<CartesianPoint>> ezs;
	private int noOfBitSets;
	private OpenBitSet[] datarep;
	private List<CartesianPoint> dat;
	private int noOfRefPoints;
	private int querySize;
	private TestContext tc;
	private int partsSelected;
	private List<CartesianPoint> refs;
	private double threshold;
	private Metric<CartesianPoint> metric;
	private List<Integer> most_different;

	public MainRunnerFileV9c() throws Exception {
		// Only do this once or it goes wrong!
		context = Context.euc20;
		tc = new TestContext(context);
		querySize = (context == Context.colors || context == Context.nasa) ? tc.dataSize() / 10 : 1000;
		int noOfRefPoints = NUMBER_CONFIG_PIVOTS;
		tc.setSizes(querySize, noOfRefPoints); // only do this once.
		List<CartesianPoint> full_data = tc.getData();
		List<CartesianPoint> witness_set = full_data.subList(0, WITNESS_SET_SIZE);
		List<CartesianPoint> queries = tc.getQueries();
		initialise_state(true, tc.getRefPoints(), full_data, witness_set, tc.metric(), queries );
	}

	public static void main(String[] args) throws Exception {

		MainRunnerFileV9c mrf = new MainRunnerFileV9c();
		mrf.doQueries();
	}

	public void initialise_state(boolean first_time, List<CartesianPoint> refs, List<CartesianPoint> full_data, List<CartesianPoint> witness_set, Metric<CartesianPoint> metric, List<CartesianPoint> queries) throws Exception {

		this.fourPoint = true;
		this.balanced = false;
		this.rotationEnabled = false;

		this.most_different = null; // reset this to free up space - reinitialised below

		if( first_time ) { // adjust the data and the witness sizes used for balancing.
			this.dat = witness_set;
			SE_WITNESS_SET_SIZE = witness_set.size() - 1;
			BALL_WITNESS_SET_SIZE = witness_set.size() - 1;
		} else {
			this.dat = full_data;
		}
		this.refs = refs;
		this.noOfRefPoints = refs.size();
		int nChoose2 = ((noOfRefPoints - 1) * noOfRefPoints) / 2;
		noOfBitSets = (( sheet_offsets.length ) * nChoose2) + (noOfRefPoints * ball_radii.length);
		this.threshold = tc.getThresholds()[0];
		this.partsSelected = PARTS_SELECTED; // same as ref
		this.queries = queries;
		this.metric = metric;
		this.rps = new RefPointSet<>(this.refs, this.metric);
		this.ezs = new ArrayList<>();

		ezs.addAll(getSheetExclusions(this.dat, this.refs, this.rps, this.balanced, this.fourPoint, this.rotationEnabled, sheet_offsets));
		ezs.addAll(getBallExclusions(this.dat, this.refs, this.rps, this.balanced));

		System.out.println("Date/time\t" + new Date().toString());
		System.out.println("Class\t" + getFileName());
		System.out.println("context\t" + context);
		System.out.println("4point\t" + fourPoint);
		System.out.println("balanced\t" + balanced);
		System.out.println("rot enabled\t" + rotationEnabled);
		System.out.println("data size\t" + this.dat.size());
		System.out.println("query size\t" + querySize);
		System.out.println("threshold\t" + threshold);
		System.out.println("refs size\t" + refs.size());
		System.out.println("no of bitsets\t" + noOfBitSets);
		System.out.println("no of partitions selected\t" + partsSelected);
		System.out.println("ezs size:\t" + ezs.size());

		System.out.println("building bitsets");

		datarep = new OpenBitSet[this.noOfBitSets];
		long t_start_datasets = System.currentTimeMillis();
		buildBitSetData(this.dat, this.datarep, this.rps, this.ezs);

		System.out.println("Finished building bitsets after = " + (System.currentTimeMillis() - t_start_datasets));

		long t_start_build_tfidf = System.currentTimeMillis();
		most_different = TFIDF.getMostDifferent(this.datarep, this.dat.size()); // the most different columns in the set

		System.out.println("Finished building TFIDF after = " + (System.currentTimeMillis() - t_start_build_tfidf));


		if( first_time ) {
			// only run this if it is the first time round
			// rather than go straight to performing queries get the best pivots and reinitialise BB.
			// This will give more columns that standard BB from which to pick using TFIDF but should be far fewer distance calculations.

			List<CartesianPoint> new_refs = findBestRefs2(this.most_different, NUMBER_OPERATIONAL_PIVOTS, this.ezs, this.rps.refs);
			initialise_state( false,new_refs, full_data, witness_set, this.metric, this.queries);
		}
	}

	private void doQueries() {

		CountedMetric<CartesianPoint> cm = new CountedMetric<>(tc.metric());
		long t_start_query = System.currentTimeMillis();
		int noOfResults = 0;
		int partitionsExcluded = 0;

		for( CartesianPoint query : queries ) {
			CantBeMustBes cbimbi = getEzs(query, threshold, rps, ezs);
			CantBeMustBes selected = findNMostDifferentInQuery( cbimbi, most_different,partsSelected );
			// showSelected( selected, sheets_size, balls_size );
			partitionsExcluded += selected.size();
			List<CartesianPoint> res = querySubset(query, dat, cm, threshold, datarep, selected);
			noOfResults += res.size();
		}
		System.out.println("bitsets");
		System.out.println("dists per query\t" + (cm.reset() / queries.size() + noOfRefPoints));

		System.out.println("results\t" + noOfResults);
		System.out.println("time\t" + (System.currentTimeMillis() - t_start_query) / (float) queries.size());
		System.out.println("partitions excluded\t" + ((double) partitionsExcluded / queries.size()));
	}

	/**
	 * Reverse engineer the best reference points from the most different columns
	 * Do this by assigning counts to the EZs based on the position of the contributing columns.
	 * This is an economic optimisation of best utility rather than first come first served as in the previous version.
	 * @param most_different
	 * @param number_required
	 * @param ezs
	 * @param rps
	 * @return
	 */
	private List<CartesianPoint> findBestRefs2(List<Integer> most_different, int number_required, List<ExclusionZone<CartesianPoint>> ezs, List<CartesianPoint> rps) {
		long[] ro_contributions = new long[rps.size()]; // keep track of all the contributions of each RO.

		for( Integer i : most_different ) {   // TODO could only do the first half or quarter etc.
			if( i != 0 ) { // columns that are all zero are at end but these do not contribute so miss out.
				ExclusionZone zone = ezs.get(i);
				if (zone instanceof SheetExclusion || zone instanceof SheetExclusion4p) {
					int ro1_index = ((SheetExclusion) zone).getRef1();
					ro_contributions[ro1_index] = ro_contributions[ro1_index] + i; // could add some difference measure here
					int ro2_index = ((SheetExclusion) zone).getRef2();
					ro_contributions[ro2_index] = ro_contributions[ro2_index] + i; // could add some difference measure here
				} else if (zone instanceof BallExclusion) {
					int ro_index = ((BallExclusion) zone).getRef();
					ro_contributions[ro_index] = ro_contributions[ro_index] + i; // could add some difference measure here - have column count
				}
			}
		}

		// after this loop ez_contributions contains the ordered total contribution of all the EZs (lower is better)
		return filterBestRefs( ro_contributions,number_required, rps );
	}

	/**
	 *
	 * @param counted_contributions is the contribution of the RO in this position
	 * @param number_required - the number of ROs that are required
	 * @param rps - all the reference objects in order
	 * @return a list of Reference objects according to their counted_contributions
	 */
	private List<CartesianPoint> filterBestRefs(long[] counted_contributions, int number_required, List<CartesianPoint> rps) {

		List<CartesianPoint> result = new ArrayList<>();
		List<Count<Long>> ros_by_utility = new ArrayList<>(); // the ros and their utility (count)

		for( int i = 0; i < counted_contributions.length; i++ ) {
			ros_by_utility.add( new Count<>(i, counted_contributions[i]));
		}

		Collections.sort(ros_by_utility); // sort the ros into count order smallest first - by contribution to most difference

		for( Count<Long> c : ros_by_utility ) {
			result.add( rps.get( c.position) );
			if( result.size() >= number_required ) {
				return result;
			}
		}
		return result;
	}

	private void showSelected(CantBeMustBes selected, int sheets_size, int balls_size ) {
		System.out.println( "Can't bes size = " + selected.cant_bes.size() );
		System.out.println( "Must bes size = " + selected.must_bes.size() );
		int sheets = 0;
		int balls = 0;
		for( int i : selected.cant_bes ) {
			if( i < sheets_size ) {
				sheets++;
			} else {
				balls++;
			}
		}
		for( int i : selected.must_bes ) {
			if( i < sheets_size ) {
				sheets++;
			} else {
				balls++;
			}
		}
		System.out.println( "Most different includes " + sheets + " sheets from " + sheets_size + " and " + balls + " balls from " + balls_size );
	}

	/**
	 *
	 * @param query_selections - the can't beins and must be ins query_selections from the query
	 * @param most_different - the most different columns calcuated ahead of time
	 * @param partitions_required - the number of partitions required.
	 * @return the CantBeMustBes query_selections by filtering the most_different with the CantBeMustBes from the query
	 */
	private CantBeMustBes findNMostDifferentInQuery(CantBeMustBes query_selections, List<Integer> most_different, int partitions_required) {

		CantBeMustBes result = new CantBeMustBes(most_different);

		for( int column : most_different ) {
			if( query_selections.contains(column) ) {
				if(query_selections.cant_bes.contains(column)) {
					result.cant_bes.add(column);
				} else {
					result.must_bes.add(column);
				}
			}
			// be better to be able to work out when we have enough - could use different TFIDF interface for that? or measure somehow?
			if (result.size() >= partitions_required) break;
		}
		return result;
	}

	private <T> List<T> querySubset(T query, List<T> dat, CountedMetric<T> metric, double threshold, OpenBitSet[] datarep, CantBeMustBes selected ) {

		List<Integer> mustBeIn = selected.must_bes;
		List<Integer> cantBeIn = selected.cant_bes;

		List<T> res = new ArrayList<>();

		OpenBitSet inclusions = doExclusions(dat, threshold, datarep, metric, query, dat.size(), mustBeIn, cantBeIn);

		if (inclusions == null) {
			for (T d : dat) {
				if (metric.distance(query, d) <= threshold) {
					res.add(d);
				}
			}
		} else {
			filterContenders(dat, threshold, metric, query, res, dat.size(), inclusions);
		}

		return res;
	}

	public List<ExclusionZone<CartesianPoint>> getBallExclusions(List<CartesianPoint> dat,
			List<CartesianPoint> refs, RefPointSet<CartesianPoint> rps, boolean balanced) {
		List<ExclusionZone<CartesianPoint>> res = new ArrayList<>();
		for (int i = 0; i < refs.size(); i++) {
			List<BallExclusion<CartesianPoint>> balls = new ArrayList<>();
			for (double radius : ball_radii) {
				BallExclusion<CartesianPoint> be = new BallExclusion<>(rps, i, radius);
				balls.add(be);
			}
			if (balanced) {
				// TODO this doesn't work!
				balls.get(0).setWitnesses(dat.subList(0, BALL_WITNESS_SET_SIZE));
				double midRadius = balls.get(0).getRadius();
				double thisRadius = midRadius - ((balls.size() / 2) * RADIUS_INCREMENT);
				for (int ball = 0; ball < balls.size(); ball++) {
					balls.get(ball).setRadius(thisRadius);
					thisRadius += RADIUS_INCREMENT;
				}
			}
			res.addAll(balls);
		}
		return res;
	}

	public List<ExclusionZone<CartesianPoint>> getSheetExclusions(List<CartesianPoint> dat,
			List<CartesianPoint> refs, RefPointSet<CartesianPoint> rps, boolean balanced, boolean fourPoint,
			boolean rotationEnabled, double[] sheet_offsets ) {
		List<ExclusionZone<CartesianPoint>> res = new ArrayList<>();
		for (int i = 0; i < refs.size() - 1; i++) {
			for (int j = i + 1; j < refs.size(); j++) {
				for( int z = 0; z < sheet_offsets.length; z++ ) {
					if (fourPoint) {
						SheetExclusion4p<CartesianPoint> se = new SheetExclusion4p<>(rps, i, j, sheet_offsets[z]);
						if (balanced) {
							se.setRotationEnabled(rotationEnabled);
							se.setWitnesses(dat.subList(0, SE_WITNESS_SET_SIZE));
						}
						res.add(se);
					} else {
						SheetExclusion3p<CartesianPoint> se = new SheetExclusion3p<>(rps, i, j, sheet_offsets[z]);
						if (balanced) {
							se.setWitnesses(dat.subList(0, SE_WITNESS_SET_SIZE));
						}
						res.add(se);
					}
				}
			}
		}
		return res;
	}

	public <T> void buildBitSetData(List<T> data, OpenBitSet[] datarep, RefPointSet<T> rps,
			List<ExclusionZone<T>> ezs) {
		int dataSize = data.size();
		System.out.print( "Allocating " + datarep.length + " columns each of " + dataSize + " rows, size = ");
		double mbytes = (dataSize * 1.0) / (8 * 1024 * 1024);
		System.out.println( new DecimalFormat("#.#").format(datarep.length * mbytes ) + " Mbytes " );
		for (int i = 0; i < datarep.length; i++) {
			datarep[i] = new OpenBitSet(dataSize);
		}
		for (int n = 0; n < dataSize; n++) {
			T p = data.get(n);
			double[] dists = rps.extDists(p);
			for (int x = 0; x < datarep.length; x++) {
				boolean isIn = ezs.get(x).isIn(dists);
				if (isIn) {
					datarep[x].set(n);
				}
			}
		}
	}

	protected <T> OpenBitSet doExclusions(List<T> dat, double t, OpenBitSet[] datarep, CountedMetric<T> cm, T q,
			final int dataSize, List<Integer> mustBeIn, List<Integer> cantBeIn) {
		if (mustBeIn.size() != 0) {
			OpenBitSet ands = getAndOpenBitSets(datarep, dataSize, mustBeIn);
			if (cantBeIn.size() != 0) {
				/*
				 * hopefully the normal situation or we're in trouble!
				 */
				OpenBitSet nots = getOrOpenBitSets(datarep, dataSize, cantBeIn);
				nots.flip(0, dataSize);
				ands.and(nots);
				return ands;
				// filterContenders(dat, t, cm, q, res, dataSize, ands);
			} else {
				// there are no cantBeIn partitions
				return ands;
				// filterContenders(dat, t, cm, q, res, dataSize, ands);
			}
		} else {
			// there are no mustBeIn partitions
			if (cantBeIn.size() != 0) {
				OpenBitSet nots = getOrOpenBitSets(datarep, dataSize, cantBeIn);
				nots.flip(0, dataSize);
				return nots;
				// filterContenders(dat, t, cm, q, res, dataSize, nots);
			} else {
				// there are no exclusions at all...
				return null;
				// for (T d : dat) {
				// if (cm.distance(q, d) < t) {
				// res.add(d);
				// }
				// }
			}
		}
	}

	private <T> void filterContenders(List<T> dat, double t, CountedMetric<T> cm, T q, Collection<T> res,
			final int dataSize, OpenBitSet results) {
		for (int i = results.nextSetBit(0); i != -1 && i < dataSize; i = results.nextSetBit(i + 1)) {
			// added i < dataSize since Cuda code overruns datasize in the -
			// don't know case - must be conservative and include, easier than
			// complex bit masking and shifting.
			if (cm.distance(q, dat.get(i)) <= t) {
				res.add(dat.get(i));
			}
		}
	}

	private OpenBitSet getAndOpenBitSets(OpenBitSet[] datarep, final int dataSize, List<Integer> mustBeIn) {
		OpenBitSet ands = null;
		if (mustBeIn.size() != 0) {
			ands = datarep[mustBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < mustBeIn.size(); i++) {
				ands.and(datarep[mustBeIn.get(i)]);
			}
		}
		return ands;
	}

	private OpenBitSet getOrOpenBitSets(OpenBitSet[] datarep, final int dataSize, List<Integer> cantBeIn) {
		OpenBitSet nots = null;
		if (cantBeIn.size() != 0) {
			nots = datarep[cantBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < cantBeIn.size(); i++) {
				final OpenBitSet nextNot = datarep[cantBeIn.get(i)];
				nots.or(nextNot);
			}
		}
		return nots;
	}

	/**
	 * This is derived from first half of queryBitSetData
	 */
	private <T> CantBeMustBes getEzs(T query, double threshold, RefPointSet<T> rps, List<ExclusionZone<T>> ezs) {

		long t0 = System.currentTimeMillis();

		List<T> res = new ArrayList<>();
		double[] dists = rps.extDists(query, res, threshold);

		List<Integer> mustBeIn = new ArrayList<>();
		List<Integer> cantBeIn = new ArrayList<>();

		for (int i = 0; i < ezs.size(); i++) {
			ExclusionZone<T> ez = ezs.get(i);
			if (ez.mustBeIn(dists, threshold)) {
				mustBeIn.add(i);
			} else if (ez.mustBeOut(dists, threshold)) {
				cantBeIn.add(i);
			}
		}

		return new CantBeMustBes( cantBeIn, mustBeIn );
	}

	public String getFileName() {
		try {
			throw new RuntimeException();
		} catch (RuntimeException e) {
			return e.getStackTrace()[1].getClassName();
		}
	}
}
