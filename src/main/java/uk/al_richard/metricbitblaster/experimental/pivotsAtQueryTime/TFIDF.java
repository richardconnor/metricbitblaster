package uk.al_richard.metricbitblaster.experimental.pivotsAtQueryTime;

import dataPoints.cartesian.CartesianPoint;
import uk.al_richard.metricbitblaster.util.OpenBitSet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * This class is inspired by IR concept of TF-IDF
 * In information retrieval, tf–idf (also TF*IDF, TFIDF, TF-IDF, or Tf-idf),
 * short for term frequency–inverse document frequency,
 * is a numerical statistic that is intended to reflect how important
 * a word is to a document in a collection or corpus.
 * https://en.wikipedia.org/wiki/Tf%E2%80%93idf
 *
 * This class is an adaptation and counts the number of columns a set bit appears.
 * It then assigns that value to the corresponding position in a corresponding integer column.
 * The column sums are then taken and columns sorted by the sums, the most different are those with the lowest sums.
 * That's the theory!
 *
 * @author al@st-andrews.ac.uk
 * 20/1/2022
 *
 */
public class TFIDF {

    /**
     * This method initialises the most different list (the only point of this class).
     * It does this by counting the zero bits in each row and distributing the count amongst the columns containing the zero bits.
     * These column sums are then summed to give a column sum.
     * The most different columns are those with the lowest non-zero counts.
     * These are used to initialise the most_different list.
     *
     * @param datarep - the bits representing inclusions and exclusions
     * @return the most different columns created by this class
     */
    public static List<Integer> getMostDifferent(OpenBitSet[] datarep, int datasize ) {

        List<Count<Long>> column_counts = getColumnCounts(datarep, datasize);
        return initialiseMostDifferent(column_counts);
    }

    /**
     * @param datarep
     * @param datasize
     * @return a sorted list of Counts - most different first - int is index into Ezs.
     */
    public static List<Count<Long>> getSortedColumnCounts(OpenBitSet[] datarep, int datasize) {
        List<Count<Long>> column_counts = getColumnCounts(datarep, datasize);
        Collections.sort(column_counts);
        moveZerosToEnd(column_counts);

        return column_counts;

    }

    private static List<Count<Long>> getColumnCounts(OpenBitSet[] datarep, int datasize) {
        int columns = datarep.length;
        int rows = datasize;
        List<Count<Long>> column_counts = new ArrayList<>(); // used to store the bit counts to calculate the most different;
        for(int i = 0; i < columns; i++ ) { column_counts.add( new Count<Long>(i,0l) ); }

        for(int row = 0; row < rows; row++ ) {
            long count_0s = 0; // counts the unset bits on a row
            // first count the bits unset in a row
            for (int column = 0; column < columns; column++) {
                if (!datarep[column].get(row)) { // counts the zeros - don't want to make all zero columns the best
                    count_0s++;
                }
            }
            // now distribute the counts into the counts table
            for (int column = 0; column < columns; column++) {
                if (!datarep[column].get(row)) { // counts the zeros
                    column_counts.get(column).data = column_counts.get(column).data + count_0s;
                }
            }
        }
        return column_counts;
    }

    /**
     * This initialises the most_different list
     * The most different columns are at the start of the list.
     * @param column_sums - an list of counts initialised by initialiseMostDifferent
     */
    private static List<Integer> initialiseMostDifferent(List<Count<Long>> column_sums) {

        Collections.sort(column_sums);
        moveZerosToEnd(column_sums);
        return createMostDifferentFromCounts(column_sums);
    }

    /**
     * This method transforms a list of Counts into a list of Integers (identifying columns) by discarding the count values.
     * @param counts - an array of Counts: frequency and column ids
     * @return the most different columns in order by traversing the frequency list
     */
    private static List<Integer> createMostDifferentFromCounts(List<Count<Long>> counts) {
        List<Integer> result = new ArrayList<>();
        for( Count<Long> c : counts ) {
            result.add( c.position );
        }
        return result;
    }

    /**
     * This method takes any zero count entries and moves them to the end of the list
     * Zero count entries are all columns of all 1s and as such not different from rest.
     * @param counts - a list of (column) Counts
     */
    private static void moveZerosToEnd(List<Count<Long>> counts) {
        List<Count<Long>> removed = new ArrayList<>();
        Iterator<Count<Long>> it = counts.iterator();
        while (it.hasNext()) {
            Count<Long> next = it.next();
            if (next.data == 0l) {
                it.remove();
                removed.add(next); // don't update whilst iterating
            }
            if(next.data > 0 ) {
                // found them all we can stop.
            }
        }
        counts.addAll(removed);
    }

    /**
     * For debugging - prints a bitmap
     * @param datarep - the exclusion table (usually the one supplied to the constructor)
     */
    private static void showBits(OpenBitSet[] datarep, int datasize) {

        int columns = datarep.length;
        int rows = datasize;

        for( int row = 0; row < rows; row++ ) {
            System.out.print( row + ": ");
            for (int column = 0; column < columns; column++) {
                if (datarep[column].get(row)) {
                    System.out.print("1");
                } else {
                    System.out.print("0");
                }
            }
            System.out.println();
        }
    }

    /**
     * Really a debugging method - shows the most_different list - strictly don't need this but handy.
     */
    public static void showMostDifferent(OpenBitSet[] datarep, int datasize) {
        List<Integer> most_different = getMostDifferent(datarep, datasize);

        System.out.println( "Most different columns:" );
        for( int i : most_different ) {

            System.out.println( i + ": " + printColumn( datarep[i]));
        }
    }

    private static String printColumn(OpenBitSet data) {
        int rows = data.size();

        StringBuilder sb = new StringBuilder();
        for (int row = 0; row < rows; row++) {
            if (data.get(row)) {
                sb.append("1");
            } else {
                sb.append("0");
            }
        }
        return sb.toString();
    }


    public static void printColumnSums(OpenBitSet[] datarep, int datasize) {
        List<Count<Long>> column_counts = getColumnCounts(datarep,datasize);
        System.out.println("Number of columns = " + column_counts.size());
        System.out.println("Column sums:");
        for (Count<Long> count : column_counts) {
            System.out.println(count.data); // let's look at the distribution
        }
    }

    public static void printColumBitCounts(OpenBitSet[] datarep, int datasize) {
        System.out.println("Number of columns = " + datarep.length);
        System.out.println("Number of rows = " + datarep[0].size());
        System.out.println("Number of bits set per column:");
        for (int i = 0; i < datarep.length; i++ ) {
            OpenBitSet column = datarep[i];
            System.out.println( column.cardinality() ); // print the distribution
        }
    }

    public static void printColumBitCountsByKind(OpenBitSet[] datarep, int datasize, List<ExclusionZone<CartesianPoint>> ezs) {
        System.out.println("Number of columns = " + datarep.length);
        System.out.println("Number of rows = " + datarep[0].size());
        System.out.println("Number of bits set per column:");
        System.out.println("Sheets:");
        for (int i = 0; i < datarep.length; i++ ) {
            if( ezs.get(i) instanceof SheetExclusion ) {
                OpenBitSet column = datarep[i];
                System.out.println(column.cardinality()); // let's look at the distribution
            }
        }
        System.out.println("Balls:");
        for (int i = 0; i < datarep.length; i++ ) {
            if( ezs.get(i) instanceof BallExclusion ) {
                OpenBitSet column = datarep[i];
                System.out.println(column.cardinality()); // let's look at the distribution
            }
        }
    }

}
