package uk.al_richard.metricbitblaster.experimental.pivotsAtQueryTime;

import coreConcepts.Metric;
import org.apache.commons.math3.stat.regression.SimpleRegression;
import searchStructures.ObjectWithDistance;
import searchStructures.Quicksort;

import java.util.ArrayList;
import java.util.List;

public class Sheet4PPower<T> extends Power<T> {

    protected double rotationAngle;
    protected boolean rotationEnabled;
    protected double x_offset_for_rotation;
    private double max;
    private double min;

    public Sheet4PPower(Metric<T> metric, T ref1, T ref2, List<T> witnesses, double threshold, boolean rotationEnabled ) {
        super();
        dists = calcDists(witnesses, ref1, ref2, metric);
        cdfs = cdf();
        powers = powerGraph( getDists( cdfs ), getCDFs( cdfs ), threshold );
        this.rotationEnabled = rotationEnabled;
    }

    private ObjectWithDistance<Integer>[] calcDists(List<T> witnesses, T ref1, T ref2, Metric<T> metric) { // adapted from SheetExclusion4P
        List<double[]> twoDpoints = getCoordinateSet(witnesses,ref1,ref2,metric);
        if( rotationEnabled ) {
            calculateTransform(twoDpoints);
        }
        ObjectWithDistance<Integer>[] owds = new ObjectWithDistance[witnesses.size()];
        for (int i = 0; i < owds.length; i++) {
            double[] pt = twoDpoints.get(i);
            double xOffset = rotationEnabled ? getNewX(pt[0], pt[1]) : pt[0];
            owds[i] = new ObjectWithDistance<>(i, xOffset);
        }
        Quicksort.sort(owds);
        max = owds[owds.length-1].getDistance();
        min = owds[0].getDistance();
        return owds;

    }

    protected List<double[]> getCoordinateSet(List<T> values, T ref1, T ref2, Metric<T> metric) { // adapted from SheetExclusion4P
        List<double[]> res = new ArrayList<>();

        for (int i = 0; i < values.size(); i++) {
            double interPivotDistance = metric.distance(ref1,ref2);
            double d1 = metric.distance(values.get(i), ref1);
            double d2 = metric.distance(values.get(i), ref2);

            res.add(getTwoDPoint(d1, d2, interPivotDistance));
        }
        return res;
    }

    private double[] getTwoDPoint(double d1, double d2, double interPivotDistance) {

        if( interPivotDistance == 0 ) {
            throw new RuntimeException( "Zero inter pivot distance" );
        }
        final double x_offset = (d1 * d1 - d2 * d2) / (2 * interPivotDistance) + interPivotDistance / 2;
        final double y_offset = d1 * d1 - x_offset * x_offset;
        double[] pt = { x_offset, y_offset };
        return pt;
    }

    // Rotation code

    /**
     * inputs a list of (x,y) coordinates and calculates the gradient of the
     * best-fit straight line, from this sets x_offset and rotationAngle
     */
    protected void calculateTransform(List<double[]> points) {

        SimpleRegression reg = new SimpleRegression(true);

        for (double[] p : points) {
            reg.addData(p[0], p[1]);
        }
        double grad = reg.getSlope();
        double y_int = reg.getIntercept();

        if (grad != 0) {
            this.x_offset_for_rotation = -y_int / grad;
            this.rotationAngle = Math.atan(grad);
        }
    }

    protected double getNewX(double x, double y) {
        double newX = x - this.x_offset_for_rotation;

        if(Double.isNaN(x_offset_for_rotation)) {
            throw new RuntimeException("x_offset_for_rotation is a nan!" + x_offset_for_rotation);
        }

        if (!Double.isNaN(this.rotationAngle)) {
            newX = Math.cos(this.rotationAngle) * newX + Math.sin(this.rotationAngle) * y;
        }
        if (Double.isNaN(newX)) {
            throw new RuntimeException("newX is a nan!");
        }
        return newX;
    }

    public double getMax() { return max; }

    public double getMin() { return min; }
}
