package uk.al_richard.metricbitblaster.experimental.pivotsAtQueryTime;

import coreConcepts.CountedMetric;
import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;
import testloads.TestContext;
import testloads.TestContext.Context;
import uk.al_richard.metricbitblaster.util.OpenBitSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * This is the same code as CountBitsSet1 but permits a Tau value to be set
 *  This is based on the original impl - does not perform querying
 */
public class SetDoubleTau {

	protected static final double RADIUS_INCREMENT = 0.3;
	private static double MEAN_DIST = 1.81;
	protected static double[] ball_radii = new double[] { MEAN_DIST - 2 * RADIUS_INCREMENT,
			MEAN_DIST - RADIUS_INCREMENT, MEAN_DIST, MEAN_DIST - RADIUS_INCREMENT, MEAN_DIST - 2 * RADIUS_INCREMENT };

	static float tau1 = 0.07f;
	static float tau2 = 0.93f;

	public static void main(String[] args) throws Exception {

		Context context = Context.euc20;

		boolean fourPoint = true;
		boolean balanced = false;
		boolean rotationEnabled = false;
		int noOfRefPoints = 100; // 5X normal number for Euc20.

		int nChoose2 = ((noOfRefPoints - 1) * noOfRefPoints) / 2;
		int noOfBitSets = nChoose2 * 2 + (noOfRefPoints * ball_radii.length);

		TestContext tc = new TestContext(context);
		int querySize = (context == Context.colors || context == Context.nasa) ? tc.dataSize() / 10 : 1000;
		tc.setSizes(querySize, noOfRefPoints);
		List<CartesianPoint> dat = tc.getData();
		//dat = dat.subList(0,1000); // <<<<<<<<<<<<<<<<<<<<<<<<<<, TEMP!
		
		List<CartesianPoint> refs = tc.getRefPoints();
		double threshold = tc.getThresholds()[0];

		int partsSelected = 20; // log2( datasize )

		List<CartesianPoint> queries = tc.getQueries();
		System.out.println("Date/time\t" + new Date().toString());
		System.out.println("Class\t" + getFileName());
		System.out.println("context\t" + context);
		System.out.println("4point\t" + fourPoint);
		System.out.println("balanced\t" + balanced);
		System.out.println("rot enabled\t" + rotationEnabled);
		System.out.println("data size\t" + dat.size());
		System.out.println("query size\t" + querySize);
		System.out.println("threshold\t" + threshold);
		System.out.println("refs size\t" + refs.size());
		System.out.println("no of bitsets\t" + noOfBitSets);
		System.out.println("no of partitions selected\t" + partsSelected);

		RefPointSet<CartesianPoint> rps = new RefPointSet<>(refs, tc.metric());
		List<ExclusionZone<CartesianPoint>> ezs = new ArrayList<>();

		ezs.addAll(getSheetExclusions(dat, refs, rps, balanced, fourPoint, rotationEnabled, new float[] { tau1, tau2 } ));
		ezs.addAll(getBallExclusions(dat, refs, rps, balanced));

		OpenBitSet[] datarep = new OpenBitSet[noOfBitSets];

		buildBitSetData(dat, datarep, rps, ezs,tc.metric());

//		doVerticalCounting( datarep );
//		doHorizontalCounting( datarep,dat.size() );

		 doQueries(dat,datarep,noOfRefPoints,tc,queries,threshold,rps,ezs);

	}

	private static void doQueries(List<CartesianPoint> dat, OpenBitSet[] datarep, int noOfRefPoints, TestContext tc, List<CartesianPoint> queries, double threshold, RefPointSet<CartesianPoint> rps, List<ExclusionZone<CartesianPoint>> ezs) {
		int noOfResults = 0;

		CountedMetric<CartesianPoint> cm = new CountedMetric<>(tc.metric());
		long t0 = System.currentTimeMillis();
		int cantbes = 0;
		int mustbes = 0;

		for( CartesianPoint query : queries ) {
			CantBeMustBes cbimbi = getEzs(query, threshold, rps, ezs);
			mustbes += cbimbi.must_bes.size();
			cantbes += cbimbi.cant_bes.size();
			List<CartesianPoint> res = querySubset(query, dat, cm, threshold, datarep, cbimbi);
			noOfResults += res.size();
		}


		System.out.println("bitsets");
		System.out.println("dists per query\t" + (cm.reset() / queries.size() + noOfRefPoints));
		System.out.println("results\t" + noOfResults);
		System.out.println("must bes\t" + ((double) mustbes / queries.size()));
		System.out.println("cant bes\t" + ((double) cantbes / queries.size()));
		System.out.print( "tau1\t" + tau1 );
		System.out.print( "tau2\t" + tau2 );
		System.out.println("time\t" + (System.currentTimeMillis() - t0) / (float) queries.size() );
	}



	private static <T> List<T> querySubset(T query, List<T> dat, CountedMetric<T> metric, double threshold, OpenBitSet[] datarep,CantBeMustBes selected ) {

		List<Integer> mustBeIn = selected.must_bes;
		List<Integer> cantBeIn = selected.cant_bes;

		List<T> res = new ArrayList<>();

		OpenBitSet inclusions = doExclusions(dat, threshold, datarep, metric, query, dat.size(), mustBeIn, cantBeIn);

		if (inclusions == null) {
			for (T d : dat) {
				if (metric.distance(query, d) <= threshold) {
					res.add(d);
				}
			}
		} else {
			filterContenders(dat, threshold, metric, query, res, dat.size(), inclusions);
		}

		return res;
	}

	public static List<ExclusionZone<CartesianPoint>> getBallExclusions(List<CartesianPoint> dat,
			List<CartesianPoint> refs, RefPointSet<CartesianPoint> rps, boolean balanced) {
		List<ExclusionZone<CartesianPoint>> res = new ArrayList<>();
		for (int i = 0; i < refs.size(); i++) {
			List<BallExclusion<CartesianPoint>> balls = new ArrayList<>();
			for (double radius : ball_radii) {
				BallExclusion<CartesianPoint> be = new BallExclusion<>(rps, i, radius);
				balls.add(be);
			}
			if (balanced) {
				balls.get(0).setWitnesses(dat.subList(0, 1000));
				double midRadius = balls.get(0).getRadius();
				double thisRadius = midRadius - ((balls.size() / 2) * RADIUS_INCREMENT);
				for (int ball = 0; ball < balls.size(); ball++) {
					balls.get(ball).setRadius(thisRadius);
					thisRadius += RADIUS_INCREMENT;
				}
			}
			res.addAll(balls);
		}
		return res;
	}

	public static List<ExclusionZone<CartesianPoint>> getSheetExclusions(List<CartesianPoint> dat,
			List<CartesianPoint> refs, RefPointSet<CartesianPoint> rps, boolean balanced, boolean fourPoint,
			boolean rotationEnabled, float[] taus) {
		List<ExclusionZone<CartesianPoint>> res = new ArrayList<>();
		for (int t = 0; t < taus.length; t++) {
			for (int i = 0; i < refs.size() - 1; i++) {
				for (int j = i + 1; j < refs.size(); j++) {
					if (fourPoint) {
						SheetExclusion4p<CartesianPoint> se = new SheetExclusion4p<>(rps, i, j);
						if (balanced) {
							se.setRotationEnabled(rotationEnabled);
							se.setWitnesses(dat.subList(0, 5001));
						}

						se.adjustTau(dat.subList(0, 5001), taus[t]);

						res.add(se);
					} else {
						SheetExclusion3p<CartesianPoint> se = new SheetExclusion3p<>(rps, i, j);
						if (balanced) {
							se.setWitnesses(dat.subList(0, 5001));
						}
						res.add(se);
					}
				}
			}
		}
		return res;
	}

	public static <T> void buildBitSetData(List<T> data, OpenBitSet[] datarep, RefPointSet<T> rps,
										   List<ExclusionZone<T>> ezs, Metric<T> metric) {
		int dataSize = data.size();
		for (int i = 0; i < datarep.length; i++) {
			datarep[i] = new OpenBitSet(dataSize);
		}

		for (int data_index = 0; data_index < dataSize; data_index++) {
			T p = data.get(data_index);

			double[] dists = rps.extDists(p); // The distances from datum p to all the reference points
			for (int ez_index = 0; ez_index < datarep.length; ez_index++) {
				boolean isIn = ezs.get(ez_index).isIn(dists);
				if (isIn) {
					datarep[ez_index].set(data_index);
				}
			}
		}
		showColumns(data, datarep, rps, ezs, metric);
	}

	private static <T> void showColumns(List<T> data, OpenBitSet[] datarep, RefPointSet<T> rps, List<ExclusionZone<T>> ezs, Metric<T> metric) {
		double mean_bits_counts = 0d;
		int max_bit_counts = -1;
		int min_bit_counts = 1000000;

		for( int ez_index = 0; ez_index < datarep.length; ez_index++) {
//			System.out.println( bit_count[ez_index] + " " + datarep[ez_index].cardinality() );
			int cnt = datarep[ez_index].cardinality();
			mean_bits_counts = mean_bits_counts + cnt;
			if( cnt > max_bit_counts ) max_bit_counts = cnt;
			if( cnt < min_bit_counts ) min_bit_counts = cnt;
		}

		System.out.print(  mean_bits_counts / datarep.length );  // "Mean number of bits set = "
		System.out.print( "\t" );
		System.out.print( max_bit_counts); // "Max number of bits set = "
		System.out.print( "\t" );
		System.out.print( min_bit_counts); // "Min number of bits set = " +
		System.out.print( "\t" );

		printMeanIPD(datarep, ezs, metric);

		int[] pivot1_with_zero = new int[rps.refs.size()];
		int[] pivot2_with_zero = new int[rps.refs.size()];

		int empty_column_count = 0;

		for (int data_index = 0; data_index < datarep.length; data_index++) {
			if( datarep[data_index].cardinality() == 0 ) {

				empty_column_count = empty_column_count +1;
				ExclusionZone<T> ez = ezs.get(data_index);  // EZs and datarep in 1:1 correspondence sheets are first

				int indx1 = ((SheetExclusion4p) ez).getRef1() ;
				int indx2 = ((SheetExclusion4p) ez).getRef2() ;

				pivot1_with_zero[indx1]++;
				pivot2_with_zero[indx2]++;

				T rp1 = ez.getPointSet().refs.get(indx1);
				T rp2 = ez.getPointSet().refs.get(indx2);

//				System.out.println( "EZ zero column sum, index: " + data_index + " RO1,RO2 = " + indx1 + " " + indx2 + " ipd = " + metric.distance(rp1,rp2));
//				examineZone( (SheetExclusion4p) ez, data, metric);
			}
		}

//		System.out.println( "Number of empty columns = " + empty_column_count );
	}

	private static <T> void printMeanIPD(OpenBitSet[] datarep, List<ExclusionZone<T>> ezs, Metric<T> metric) {
		double total = 0d;
		for (int n = 0; n < datarep.length; n++) {
			ExclusionZone<T> ez = ezs.get(n);
			if( ez instanceof SheetExclusion4p) {
				int indx1 = ((SheetExclusion4p) ez).getRef1();
				int indx2 = ((SheetExclusion4p) ez).getRef2();

				T rp1 = ez.getPointSet().refs.get(indx1);
				T rp2 = ez.getPointSet().refs.get(indx2);
				total += metric.distance(rp1, rp2);
			}
		}
//		System.out.println( "Number of EZs = " + datarep.length );
//		System.out.println( "Mean ipd (for Sheets4P) = " + total / datarep.length );
	}

	private static <T> void examineZone( SheetExclusion4p<T> ez, List<T> data, Metric<T> metric) {

		int indx1 = ez.getRef1();
		int indx2 = ez.getRef2();

		System.out.println("Indx1 = " + indx1 + " Indx2 = " + indx2);

		T rp1 = ez.getPointSet().refs.get(indx1);
		T rp2 = ez.getPointSet().refs.get(indx2);

		int in = 0;
		int out = 0;
		double mean_d1 = 0d;
		double mean_d2 = 0d;
		double max_d1 = -1;
		double min_d1 = 1000000000;
		double max_d2 = -1;
		double min_d2 = 1000000000;

		int size = data.size();
		for (T datum : data) {
			double d1 = metric.distance(rp1, datum);
			double d2 = metric.distance(rp2, datum);
			double interPivotDistance = metric.distance(rp1, rp2);

			final double x_offset = (d1 * d1 - d2 * d2) / (2 * interPivotDistance) + interPivotDistance / 2;
			final double y_offset = d1 * d1 - x_offset * x_offset;
			double[] pt = {x_offset, y_offset};

			if (x_offset < 0) in++;
			else out++;

			mean_d1 += d1;
			mean_d2 += d2;
			if( d1 > max_d1 ) max_d1 = d1;
			if( d1 < min_d1 ) min_d1 = d1;
			if( d1 > max_d2 ) max_d2 = d2;
			if( d1 < min_d2 ) min_d2 = d2;
		}
		System.out.println("Ins: " + in + " Outs: " + out +
				" Mean_d1 = " + mean_d1 / size + " Max d1 = " + max_d1 + " Min_d1 = " + min_d1 + "\n" +
				" Mean_d2 = " + mean_d2 / size + " Max d2 = " + max_d2 + " Min_d2 = " + min_d2 );
	}


	protected static <T> OpenBitSet doExclusions(List<T> dat, double t, OpenBitSet[] datarep, CountedMetric<T> cm, T q,
												 final int dataSize, List<Integer> mustBeIn, List<Integer> cantBeIn) {
		if (mustBeIn.size() != 0) {
			OpenBitSet ands = getAndOpenBitSets(datarep, dataSize, mustBeIn);
			if (cantBeIn.size() != 0) {
				/*
				 * hopefully the normal situation or we're in trouble!
				 */
				OpenBitSet nots = getOrOpenBitSets(datarep, dataSize, cantBeIn);
				nots.flip(0, dataSize);
				ands.and(nots);
				return ands;
				// filterContenders(dat, t, cm, q, res, dataSize, ands);
			} else {
				// there are no cantBeIn partitions
				return ands;
				// filterContenders(dat, t, cm, q, res, dataSize, ands);
			}
		} else {
			// there are no mustBeIn partitions
			if (cantBeIn.size() != 0) {
				OpenBitSet nots = getOrOpenBitSets(datarep, dataSize, cantBeIn);
				nots.flip(0, dataSize);
				return nots;
				// filterContenders(dat, t, cm, q, res, dataSize, nots);
			} else {
				// there are no exclusions at all...
				return null;
				// for (T d : dat) {
				// if (cm.distance(q, d) < t) {
				// res.add(d);
				// }
				// }
			}
		}
	}

	static <T> void filterContenders(List<T> dat, double t, CountedMetric<T> cm, T q, Collection<T> res,
			final int dataSize, OpenBitSet results) {
		for (int i = results.nextSetBit(0); i != -1 && i < dataSize; i = results.nextSetBit(i + 1)) {
			// added i < dataSize since Cuda code overruns datasize in the -
			// don't know case - must be conservative and include, easier than
			// complex bit masking and shifting.
			if (cm.distance(q, dat.get(i)) <= t) {
				res.add(dat.get(i));
			}
		}
	}

	static OpenBitSet getAndOpenBitSets(OpenBitSet[] datarep, final int dataSize, List<Integer> mustBeIn) {
		OpenBitSet ands = null;
		if (mustBeIn.size() != 0) {
			ands = datarep[mustBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < mustBeIn.size(); i++) {
				ands.and(datarep[mustBeIn.get(i)]);
			}
		}
		return ands;
	}

	static OpenBitSet getOrOpenBitSets(OpenBitSet[] datarep, final int dataSize, List<Integer> cantBeIn) {
		OpenBitSet nots = null;
		if (cantBeIn.size() != 0) {
			nots = datarep[cantBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < cantBeIn.size(); i++) {
				final OpenBitSet nextNot = datarep[cantBeIn.get(i)];
				nots.or(nextNot);
			}
		}
		return nots;
	}

	/**
	 * This is derived from first half of queryBitSetData
	 */
	static <T> CantBeMustBes getEzs(T query, double threshold, RefPointSet<T> rps, List<ExclusionZone<T>> ezs) {

		long t0 = System.currentTimeMillis();

		List<T> res = new ArrayList<>();
		double[] dists = rps.extDists(query, res, threshold);

		List<Integer> mustBeIn = new ArrayList<>();
		List<Integer> cantBeIn = new ArrayList<>();

		for (int i = 0; i < ezs.size(); i++) {
			ExclusionZone<T> ez = ezs.get(i);
			if (ez.mustBeIn(dists, threshold)) {
				mustBeIn.add(i);
			} else if (ez.mustBeOut(dists, threshold)) {
				cantBeIn.add(i);
			}
		}

		return new CantBeMustBes( cantBeIn, mustBeIn );
	}

	public static String getFileName() {
		try {
			throw new RuntimeException();
		} catch (RuntimeException e) {
			return e.getStackTrace()[1].getClassName();
		}
	}
}
