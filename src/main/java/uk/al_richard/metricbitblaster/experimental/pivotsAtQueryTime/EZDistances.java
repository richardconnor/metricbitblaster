package uk.al_richard.metricbitblaster.experimental.pivotsAtQueryTime;

import uk.al_richard.metricbitblaster.util.HammingDistanceOB;
import uk.al_richard.metricbitblaster.util.OpenBitSet;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Maintains a list of pairs of exclusion zones indices and the Hamming distance between them most different first
 */
public class EZDistances {

    private final OpenBitSet[] datarep;
    List<Triple> ezs_by_distance = new ArrayList<>();
    DecimalFormat df = new DecimalFormat( "0.000000" );
    //Jaccard jaccard = new Jaccard();
    HammingDistanceOB hamming  = new HammingDistanceOB();

    public EZDistances(OpenBitSet[] datarep) {
        this.datarep = datarep; // temp
        for( int i = 0; i < datarep.length - 1; i++ ) {
            for( int j = i + 1; j < datarep.length; j++ ) {
                //double dist = jaccard.distance(datarep[i], datarep[j]);
                double dist = hamming.distance(datarep[i], datarep[j]);

                Triple triple = new Triple( i,j,dist );
                ezs_by_distance.add(triple);
            }
        }
        Collections.sort(ezs_by_distance);
    }

    /**
     * @param number_required - how many zones are wanted.
     * @return the index into the most different exclusion zones
     * TODO What to do about empty zones - anything?
     */
    public List<Integer> getNMostDifferentFrom( int number_required ) {
        ArrayList<Integer> result = new ArrayList<>();
        for( Triple t : ezs_by_distance ) {
            int zone1 = t.zone1_index;
            if( ! result.contains(zone1) ) {
                result.add(zone1);
                if (result.size() >= number_required) {
                    break;
                }
            }
            int zone2 = t.zone2_index;
            if( ! result.contains(zone2) ) {
                result.add(zone2);
            }
            if (result.size() >= number_required) {
                break;
            }
        }
        return result;
    }

    /**
     * @param selections - the list from which we are permitted to choose.
     * @param number_required - how many zones are wanted.
     * @return the index into the most different exclusion zones
     * TODO What to do about empty zones - anything?
     */
    public CantBeMustBes getNMostDifferentIn( CantBeMustBes selections, int number_required ) {

        CantBeMustBes result = new CantBeMustBes();
        List<Integer> cantBeIns = result.cant_bes;
        List<Integer> mustBeIns = result.must_bes;

        for( Triple t : ezs_by_distance ) {
            tryIncluding(t.zone1_index, result, selections);
            if (result.size() >= number_required) break;
            tryIncluding(t.zone2_index, result, selections);
            if (result.size() >= number_required) break;
        }
        return result;
    }

    public CantBeMustBes getNMostDifferentIn2( CantBeMustBes selections, int number_required ) {

        CantBeMustBes result = new CantBeMustBes();
        List<Integer> cantBeIns = result.cant_bes;
        List<Integer> mustBeIns = result.must_bes;

        // First find the  most different two points contained in the selections.
        for( Triple t : ezs_by_distance ) {
            if( selections.contains(t.zone1_index) && selections.contains(t.zone2_index) ) {
                tryIncluding(t.zone1_index, result, selections);
                tryIncluding(t.zone2_index, result, selections);
                break;
            }
        }
        // Now try and add the next most different from those two points.
        while( result.size() < number_required ) {
            addMinFrom(result, selections);
        }
        return result;
    }

    private void addMinFrom(CantBeMustBes result, CantBeMustBes selections) {
    }

    private void tryIncluding(int zone1, CantBeMustBes result, CantBeMustBes selections) {
        if( ! result.contains(zone1) && selections.contains(zone1) ) {
            if(selections.cant_bes.contains(zone1)) {
                result.cant_bes.add(zone1);
            } else {
                result.must_bes.add(zone1);
            }
        }
    }


    private void show(List<Triple> ezs_by_distance) {
        for( Triple t : ezs_by_distance ) {
            if( t.distance != 0.0d ) {
                System.out.println(t);
            }
        }
    }

    private class Triple implements Comparable<Triple> {
        public int zone1_index;
        public int zone2_index;
        public Double distance;

        public Triple( int index1, int index2, double distance ) {
            this.zone1_index = index1;
            this.zone2_index = index2;
            this.distance = distance;
        }

        public String toString() {
            return zone1_index + " " + zone2_index + " " + df.format(distance); // + "\n" + datarep[index1] + "\n" + datarep[index2];
        }

        @Override
        public int compareTo(Triple other) {
            return other.distance.compareTo(distance); // reverse order farthest first
        }
    }
}
