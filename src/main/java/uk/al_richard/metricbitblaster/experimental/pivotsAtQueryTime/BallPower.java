package uk.al_richard.metricbitblaster.experimental.pivotsAtQueryTime;

import coreConcepts.Metric;
import searchStructures.ObjectWithDistance;
import searchStructures.Quicksort;

import java.util.List;

public class BallPower<T> extends Power<T> {

    public BallPower(Metric<T> metric, T ref, List<T> witnesses, double threshold ) {
        super();
        dists = calcDists(witnesses, ref, metric);
        cdfs = cdf();
        powers = powerGraph( getDists( cdfs ), getCDFs( cdfs ), threshold );
    }

    private ObjectWithDistance<Integer>[] calcDists(List<T> witnesses, T ref, Metric<T> metric) {
        ObjectWithDistance<Integer>[] owds = new ObjectWithDistance[witnesses.size()];

        for (int i = 0; i < witnesses.size(); i++) {
            double d1 = metric.distance(witnesses.get(i), ref);
            owds[i] = new ObjectWithDistance<>(i, d1);
        }
        Quicksort.sort(owds);
        return owds;
    }
}
