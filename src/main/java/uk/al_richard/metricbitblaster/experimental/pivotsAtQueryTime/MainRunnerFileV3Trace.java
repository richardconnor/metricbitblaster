package uk.al_richard.metricbitblaster.experimental.pivotsAtQueryTime;

import coreConcepts.CountedMetric;
import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;
import testloads.TestContext;
import testloads.TestContext.Context;
import uk.al_richard.metricbitblaster.util.OpenBitSet;
import util.OrderedList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Third version of MainRunnerFileV5 - now OO encapsulated (not well!).
 */
public class MainRunnerFileV3Trace {

	protected static final double RADIUS_INCREMENT = 0.3;
	private static double MEAN_DIST = 1.81;
	protected static double[] ball_radii = new double[] { MEAN_DIST - 2 * RADIUS_INCREMENT,
			MEAN_DIST - RADIUS_INCREMENT, MEAN_DIST, MEAN_DIST - RADIUS_INCREMENT, MEAN_DIST - 2 * RADIUS_INCREMENT };

	private Context context;
	private boolean fourPoint;
	private boolean balanced;
	private boolean rotationEnabled;
	private List<CartesianPoint> queries;
	private RefPointSet<CartesianPoint> rps;
	private List<ExclusionZone<CartesianPoint>> ezs;
	private int noOfBitSets;
	private OpenBitSet[] datarep;
	private List<CartesianPoint> dat;
	private int noOfRefPoints;
	private int querySize;
	private TestContext tc;
	private List<Integer> most_different;
	private int partsSelected;
	private List<CartesianPoint> refs;
	private double threshold;
	private static int num_nns = 10;

	public MainRunnerFileV3Trace() throws Exception {
		initialise_state();
	}

	public static void main(String[] args) throws Exception {

		MainRunnerFileV3Trace mrf = new MainRunnerFileV3Trace();

	}

	public void initialise_state() throws Exception {
		context = Context.euc20;

		fourPoint = true;
		balanced = false;
		rotationEnabled = true;
		noOfRefPoints = 40;

		int nChoose2 = ((noOfRefPoints - 1) * noOfRefPoints) / 2;
		noOfBitSets = nChoose2 + (noOfRefPoints * ball_radii.length);

		tc = new TestContext(context);
		querySize = (context == Context.colors || context == Context.nasa) ? tc.dataSize() / 10 : 1000;
		tc.setSizes(querySize, noOfRefPoints);
		dat = tc.getData();
		//dat = dat.subList(0,1000); // <<<<<<<<<<<<<<<<<<<<<<<<<<, TEMP!

		refs = tc.getRefPoints();
		threshold = tc.getThresholds()[0];

		partsSelected = 400; // same as ref

		queries = tc.getQueries();
		System.out.println("Date/time\t" + new Date().toString());
		System.out.println("Class\t" + getFileName());
		System.out.println("context\t" + context);
		System.out.println("4point\t" + fourPoint);
		System.out.println("balanced\t" + balanced);
		System.out.println("rot enabled\t" + rotationEnabled);
		System.out.println("data size\t" + dat.size());
		System.out.println("query size\t" + querySize);
		System.out.println("threshold\t" + threshold);
		System.out.println("refs size\t" + refs.size());
		System.out.println("no of bitsets\t" + noOfBitSets);
		System.out.println("no of partitions selected\t" + partsSelected);

		rps = new RefPointSet<>(refs, tc.metric());
		ezs = new ArrayList<>();

		ezs.addAll(getSheetExclusions(dat, refs, rps, balanced, fourPoint, rotationEnabled));
		int sheets_size = ezs.size();
		ezs.addAll(getBallExclusions(dat, refs, rps, balanced));
		int balls_size = ezs.size() - sheets_size;

		System.out.println("ezs size:\t" + ezs.size());

		datarep = new OpenBitSet[noOfBitSets];

		System.out.println("building bitsets");

		long t_start_datasets = System.currentTimeMillis();

		buildBitSetData(dat, datarep, rps, ezs);

		System.out.println("Finished building bitsets after = " + (System.currentTimeMillis() - t_start_datasets));

		long t_start_build_tfidf = System.currentTimeMillis();

		List<Count<Long>> columns = TFIDF.getSortedColumnCounts(this.datarep, this.dat.size()); // the most different columns in the set
		examineLIDIM(columns, noOfRefPoints, this.ezs, this.rps.refs);

	}

	private void examineLIDIM(List<Count<Long>> columns, int number_required, List<ExclusionZone<CartesianPoint>> ezs, List<CartesianPoint> rps) throws Exception {

		writeColumHeaders();

		int order = 0; // indexes into results;t
		int ball_order = 0;
		int sheet_order = 0;

		for (Count<Long> c : columns) {
			int ez_index = c.position;
			long column_count = c.data;
			ExclusionZone zone = ezs.get(ez_index);
			if (zone instanceof SheetExclusion || zone instanceof SheetExclusion4p) {
				int ro1_index = ((SheetExclusion) zone).getRef1();
				int ro2_index = ((SheetExclusion) zone).getRef2();
				double ipd = tc.metric().distance(rps.get(ro1_index), rps.get(ro2_index));
				showLIDIM(order++,  sheet_order++, column_count, ro1_index, ez_index, rps, "Sheet", 1, ipd);
				showLIDIM(order++, sheet_order++, column_count, ro2_index, ez_index, rps, "Sheet", 2, ipd);
			} else if (zone instanceof BallExclusion) {
				int ro_index = ((BallExclusion) zone).getRef();
				showLIDIM(order++, ball_order++, column_count, ro_index, ez_index, rps, "Ball", 1, 0.0d);
			}
		}
	}

	private void writeColumHeaders() {
		System.out.print( "order\tkind_order\tcolumn_count\tez_index\tkind\twhich_ro\tlidim\tipd\tmean_dist\t");
		for( int i = 0 ; i < num_nns ; i++ ) {
			System.out.print( "Dist_" + i);
			if( i < num_nns ) {
				System.out.print("\t");
			}
		}
		System.out.println();
	}

	private void showLIDIM(int order, int kind_order, long column_count, int ro_index, int ez_index, List<CartesianPoint> rps, String kind, int which_ro, double ipd) throws Exception {
		CartesianPoint ro = rps.get(ro_index);
		List<Double> dists = getDists(ro, num_nns);
		double lidim = LIDimLevinaBickel(dists);
		System.out.print( order + "\t" + kind_order + "\t" + column_count + "\t" + ez_index + "\t" + kind + "\t" + which_ro + "\t" + lidim + "\t" + ipd + "\t" + mean(dists) + "\t");
		for( int i = 0 ; i < dists.size() ; i++ ) {
			System.out.print( dists.get(i) );
			if( i < num_nns ) {
				System.out.print("\t");
			}
		}
		System.out.println();
	}

	private double mean(List<Double> dists) {
		double d = 0;
		for( int i = 0 ; i < dists.size() ; i++ ) { d += dists.get(i); }
		return d / dists.size();
	}

	/**
	 * MLE IDIM Estimator by Levina and Bickel in paper "Maximum Likelihood
	 * Estimation of Intrinsic Dimension".
	 * @param dists - a list of distances.
	 * @return the MLE IDIM of the list
	 * @throws Exception if a zero length list is supplied.
	 */
	public double LIDimLevinaBickel(List<Double> dists) throws Exception {
		int count = dists.size();
		double one_over_k_minus_one = 1.0d / ((double) count - 1.0d);

		if (count > 0) {
			double max = max(dists);

			double sigma = 0; // sum

			for (int i = 0; i < count - 1; i++) {
				double d = dists.get(i);
				sigma += Math.log(max / d);
			}
			return 1.0d / (one_over_k_minus_one * sigma);
		} else {
			throw new Exception("zero length list");
		}
	}

	public static double max(List<Double> list) {
		if (list.size() == 0) {
			throw new RuntimeException("Empty list");
		}
		double max_so_far = Double.MIN_VALUE;
		for (double d : list) {

			max_so_far = Math.max( d,max_so_far );
		}
		return max_so_far;
	}


	private List<Double> getDists(CartesianPoint point, int count ) {
		OrderedList<CartesianPoint, Double> dists = new OrderedList(count);

		for( CartesianPoint datum : dat ) {
			double dist = tc.metric().distance( datum,point );
			if( dist != 0.0 ) {
				dists.add(datum, dist);
			}
		}
		return dists.getComparators();
	}


	private void doQueries() {

		CountedMetric<CartesianPoint> cm = new CountedMetric<>(tc.metric());
		long t_start_query = System.currentTimeMillis();
		int noOfResults = 0;
		int partitionsExcluded = 0;

		for( CartesianPoint query : queries ) {
			CantBeMustBes cbimbi = getEzs(query, threshold, rps, ezs);
			CantBeMustBes selected = findNMostDifferentInQuery( cbimbi, most_different,partsSelected );
			// showSelected( selected, sheets_size, balls_size );
			partitionsExcluded += selected.size();
			List<CartesianPoint> res = querySubset(query, dat, cm, threshold, datarep, selected);
			noOfResults += res.size();
		}
		System.out.println("bitsets");
		System.out.println("dists per query\t" + (cm.reset() / queries.size() + noOfRefPoints));
		System.out.println("results\t" + noOfResults);
		System.out.println("time\t" + (System.currentTimeMillis() - t_start_query) / (float) queries.size());
		System.out.println("partitions excluded\t" + ((double) partitionsExcluded / queries.size()));
	}

	/**
	 * Reverse engineer the best reference points from the most different columns
	 * @param most_different
	 * @param number_required
	 * @param ezs
	 * @return
	 */
	private static List<CartesianPoint> findBestRefs(List<Integer> most_different, int number_required, List<ExclusionZone<CartesianPoint>> ezs) {
		List<CartesianPoint> new_refs = new ArrayList<>();
		for( int i = 0; i < ( most_different.size() / 4 ); i++ ) {
			ExclusionZone zone = ezs.get(i);
			RefPointSet<CartesianPoint> points = ((SheetExclusion<CartesianPoint>) zone).getPointSet();
			List<CartesianPoint> ros = points.refs;
			if( ! new_refs.contains(ros.get(0))) {
				new_refs.add(ros.get(0));
			}
			if( zone instanceof SheetExclusion ) {
				if( ! new_refs.contains(ros.get(1))) {
					new_refs.add(ros.get(1));
				}
			}
			if( new_refs.size() >= number_required ) {
				return new_refs;
			}
		}
		return new_refs;
	}

	private static void showSelected(CantBeMustBes selected, int sheets_size, int balls_size ) {
		System.out.println( "Can't bes size = " + selected.cant_bes.size() );
		System.out.println( "Must bes size = " + selected.must_bes.size() );
		int sheets = 0;
		int balls = 0;
		for( int i : selected.cant_bes ) {
			if( i < sheets_size ) {
				sheets++;
			} else {
				balls++;
			}
		}
		for( int i : selected.must_bes ) {
			if( i < sheets_size ) {
				sheets++;
			} else {
				balls++;
			}
		}
		System.out.println( "Most different includes " + sheets + " sheets from " + sheets_size + " and " + balls + " balls from " + balls_size );
	}

	/**
	 *
	 * @param query_selections - the can't beins and must be ins query_selections from the query
	 * @param most_different - the most different columns calcuated ahead of time
	 * @param partitions_required - the number of partitions required.
	 * @return the CantBeMustBes query_selections by filtering the most_different with the CantBeMustBes from the query
	 */
	private static CantBeMustBes findNMostDifferentInQuery(CantBeMustBes query_selections, List<Integer> most_different, int partitions_required) {

		CantBeMustBes result = new CantBeMustBes(most_different);

		for( int column : most_different ) {
			if( query_selections.contains(column) ) {
				if(query_selections.cant_bes.contains(column)) {
					result.cant_bes.add(column);
				} else {
					result.must_bes.add(column);
				}
			}
			// be better to be able to work out when we have enough - could use different TFIDF interface for that? or measure somehow?
			if (result.size() >= partitions_required) break;
		}
		return result;
	}

	private static <T> List<T> querySubset(T query, List<T> dat, CountedMetric<T> metric, double threshold, OpenBitSet[] datarep,CantBeMustBes selected ) {

		List<Integer> mustBeIn = selected.must_bes;
		List<Integer> cantBeIn = selected.cant_bes;

		List<T> res = new ArrayList<>();

		OpenBitSet inclusions = doExclusions(dat, threshold, datarep, metric, query, dat.size(), mustBeIn, cantBeIn);

		if (inclusions == null) {
			for (T d : dat) {
				if (metric.distance(query, d) <= threshold) {
					res.add(d);
				}
			}
		} else {
			filterContenders(dat, threshold, metric, query, res, dat.size(), inclusions);
		}

		return res;
	}

	static <T> void queryBitSetData(List<T> queries, List<T> dat, Metric<T> metric, double threshold, OpenBitSet[] datarep,
									RefPointSet<T> rps, List<ExclusionZone<T>> ezs, int noOfRefPoints) {

		int noOfResults = 0;
		int partitionsExcluded = 0;
		CountedMetric<T> cm = new CountedMetric<>(metric);
		long t0 = System.currentTimeMillis();
		for (T q : queries) {
			List<T> res = new ArrayList<>();
			double[] dists = rps.extDists(q, res, threshold);

			List<Integer> mustBeIn = new ArrayList<>();
			List<Integer> cantBeIn = new ArrayList<>();

			for (int i = 0; i < ezs.size(); i++) {
				ExclusionZone<T> ez = ezs.get(i);
				if (ez.mustBeIn(dists, threshold)) {
					mustBeIn.add(i);
				} else if (ez.mustBeOut(dists, threshold)) {
					cantBeIn.add(i);
				}
			}

			partitionsExcluded += cantBeIn.size() + mustBeIn.size();

			OpenBitSet inclusions = doExclusions(dat, threshold, datarep, cm, q, dat.size(), mustBeIn, cantBeIn);

			if (inclusions == null) {
				for (T d : dat) {
					if (cm.distance(q, d) <= threshold) {
						res.add(d);
					}
				}
			} else {
				filterContenders(dat, threshold, cm, q, res, dat.size(), inclusions);
			}

			noOfResults += res.size();
		}

		System.out.println("bitsets");
		System.out.println("dists per query\t" + (cm.reset() / queries.size() + noOfRefPoints));
		System.out.println("results\t" + noOfResults);
		System.out.println("time\t" + (System.currentTimeMillis() - t0) / (float) queries.size());
		System.out.println("partitions excluded\t" + ((double) partitionsExcluded / queries.size()));
	}

	public static List<ExclusionZone<CartesianPoint>> getBallExclusions(List<CartesianPoint> dat,
			List<CartesianPoint> refs, RefPointSet<CartesianPoint> rps, boolean balanced) {
		List<ExclusionZone<CartesianPoint>> res = new ArrayList<>();
		for (int i = 0; i < refs.size(); i++) {
			List<BallExclusion<CartesianPoint>> balls = new ArrayList<>();
			for (double radius : ball_radii) {
				BallExclusion<CartesianPoint> be = new BallExclusion<>(rps, i, radius);
				balls.add(be);
			}
			if (balanced) {
				balls.get(0).setWitnesses(dat.subList(0, 1000));
				double midRadius = balls.get(0).getRadius();
				double thisRadius = midRadius - ((balls.size() / 2) * RADIUS_INCREMENT);
				for (int ball = 0; ball < balls.size(); ball++) {
					balls.get(ball).setRadius(thisRadius);
					thisRadius += RADIUS_INCREMENT;
				}
			}
			res.addAll(balls);
		}
		return res;
	}

	public static List<ExclusionZone<CartesianPoint>> getSheetExclusions(List<CartesianPoint> dat,
			List<CartesianPoint> refs, RefPointSet<CartesianPoint> rps, boolean balanced, boolean fourPoint,
			boolean rotationEnabled) {
		List<ExclusionZone<CartesianPoint>> res = new ArrayList<>();
		for (int i = 0; i < refs.size() - 1; i++) {
			for (int j = i + 1; j < refs.size(); j++) {
				if (fourPoint) {
					SheetExclusion4p<CartesianPoint> se = new SheetExclusion4p<>(rps, i, j);
					if (balanced) {
						se.setRotationEnabled(rotationEnabled);
						se.setWitnesses(dat.subList(0, 5001));
					}
					res.add(se);
				} else {
					SheetExclusion3p<CartesianPoint> se = new SheetExclusion3p<>(rps, i, j);
					if (balanced) {
						se.setWitnesses(dat.subList(0, 5001));
					}
					res.add(se);
				}
			}
		}
		return res;
	}

	public static <T> void buildBitSetData(List<T> data, OpenBitSet[] datarep, RefPointSet<T> rps,
			List<ExclusionZone<T>> ezs) {
		int dataSize = data.size();
		for (int i = 0; i < datarep.length; i++) {
			datarep[i] = new OpenBitSet(dataSize);
		}
		for (int n = 0; n < dataSize; n++) {
			T p = data.get(n);
			double[] dists = rps.extDists(p);
			for (int x = 0; x < datarep.length; x++) {
				boolean isIn = ezs.get(x).isIn(dists);
				if (isIn) {
					datarep[x].set(n);
				}
			}
		}
	}

	protected static <T> OpenBitSet doExclusions(List<T> dat, double t, OpenBitSet[] datarep, CountedMetric<T> cm, T q,
			final int dataSize, List<Integer> mustBeIn, List<Integer> cantBeIn) {
		if (mustBeIn.size() != 0) {
			OpenBitSet ands = getAndOpenBitSets(datarep, dataSize, mustBeIn);
			if (cantBeIn.size() != 0) {
				/*
				 * hopefully the normal situation or we're in trouble!
				 */
				OpenBitSet nots = getOrOpenBitSets(datarep, dataSize, cantBeIn);
				nots.flip(0, dataSize);
				ands.and(nots);
				return ands;
				// filterContenders(dat, t, cm, q, res, dataSize, ands);
			} else {
				// there are no cantBeIn partitions
				return ands;
				// filterContenders(dat, t, cm, q, res, dataSize, ands);
			}
		} else {
			// there are no mustBeIn partitions
			if (cantBeIn.size() != 0) {
				OpenBitSet nots = getOrOpenBitSets(datarep, dataSize, cantBeIn);
				nots.flip(0, dataSize);
				return nots;
				// filterContenders(dat, t, cm, q, res, dataSize, nots);
			} else {
				// there are no exclusions at all...
				return null;
				// for (T d : dat) {
				// if (cm.distance(q, d) < t) {
				// res.add(d);
				// }
				// }
			}
		}
	}

	static <T> void filterContenders(List<T> dat, double t, CountedMetric<T> cm, T q, Collection<T> res,
			final int dataSize, OpenBitSet results) {
		for (int i = results.nextSetBit(0); i != -1 && i < dataSize; i = results.nextSetBit(i + 1)) {
			// added i < dataSize since Cuda code overruns datasize in the -
			// don't know case - must be conservative and include, easier than
			// complex bit masking and shifting.
			if (cm.distance(q, dat.get(i)) <= t) {
				res.add(dat.get(i));
			}
		}
	}

	static OpenBitSet getAndOpenBitSets(OpenBitSet[] datarep, final int dataSize, List<Integer> mustBeIn) {
		OpenBitSet ands = null;
		if (mustBeIn.size() != 0) {
			ands = datarep[mustBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < mustBeIn.size(); i++) {
				ands.and(datarep[mustBeIn.get(i)]);
			}
		}
		return ands;
	}

	static OpenBitSet getOrOpenBitSets(OpenBitSet[] datarep, final int dataSize, List<Integer> cantBeIn) {
		OpenBitSet nots = null;
		if (cantBeIn.size() != 0) {
			nots = datarep[cantBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < cantBeIn.size(); i++) {
				final OpenBitSet nextNot = datarep[cantBeIn.get(i)];
				nots.or(nextNot);
			}
		}
		return nots;
	}

	/**
	 * This is derived from first half of queryBitSetData
	 */
	static <T> CantBeMustBes getEzs(T query, double threshold, RefPointSet<T> rps, List<ExclusionZone<T>> ezs) {

		long t0 = System.currentTimeMillis();

		List<T> res = new ArrayList<>();
		double[] dists = rps.extDists(query, res, threshold);

		List<Integer> mustBeIn = new ArrayList<>();
		List<Integer> cantBeIn = new ArrayList<>();

		for (int i = 0; i < ezs.size(); i++) {
			ExclusionZone<T> ez = ezs.get(i);
			if (ez.mustBeIn(dists, threshold)) {
				mustBeIn.add(i);
			} else if (ez.mustBeOut(dists, threshold)) {
				cantBeIn.add(i);
			}
		}

		return new CantBeMustBes( cantBeIn, mustBeIn );
	}

	public static String getFileName() {
		try {
			throw new RuntimeException();
		} catch (RuntimeException e) {
			return e.getStackTrace()[1].getClassName();
		}
	}
}
