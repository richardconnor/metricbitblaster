package uk.al_richard.metricbitblaster.experimental.pivotsAtQueryTime;

import searchStructures.ObjectWithDistance;

public abstract class Power<T> {

    protected double[] powers;
    protected ObjectWithDistance<Integer>[] dists;
    protected ObjectWithDistance<Double>[] cdfs;

    protected ObjectWithDistance<Double>[] cdf() {
        ObjectWithDistance<Double>[] cdf_array = new ObjectWithDistance[dists.length];
        double sum = 0;
        for( ObjectWithDistance owd : dists ) {
            sum = sum + owd.getDistance();
        }
        double cumulator = 0;
        for( int i = 0; i < dists.length; i++ ) {
            ObjectWithDistance owd = dists[i];
            double dist = owd.getDistance();
            cumulator += dist;
            cdf_array[i] = new ObjectWithDistance<Double>( cumulator/sum,dist );
        }
        return cdf_array;
    }

    public double[] powerGraph(double[] dists, double[] cdfs, double threshold ) {
        double[] powers = new double[ cdfs.length ];
        int leftPointer = 0;
        int rightPointer = 0;

        for( int i = 0; i < cdfs.length; i++ ) {
            while( dists[leftPointer] <= dists[i] - threshold ) {
                leftPointer = leftPointer + 1;
            }
            while( dists[rightPointer] < dists[i] + threshold && rightPointer < dists.length - 1 ) {
                rightPointer = rightPointer + 1;
            }
            powers[i] = cdfs[i] * (1 - cdfs[rightPointer]) + (1 - cdfs[i]) * cdfs[leftPointer];
        }
        return powers;
    }

    public ObjectWithDistance<Double> getPeak1() {
        double max = Double.MIN_VALUE;
        int index = 0;
        for( int i = 0; i < powers.length /2; i++ ) {
            if( powers[i] > max ) {
                max = powers[i];
                index = i;
            }
        }
        return new ObjectWithDistance<Double>(powers[index],dists[index].getDistance());
    }

    public ObjectWithDistance<Double> getPeak2() {
        double max = Double.MIN_VALUE;
        int index = 0;
        for( int i = powers.length - 1; i >= powers.length/2; i-- ) {
            if( powers[i] > max ) {
                max = powers[i];
                index = i;
            }
        }
        return new ObjectWithDistance<Double>(powers[index],dists[index].getDistance());
    }

    public void show() {
        System.out.println( "Powers:" );
        for( int i = 0; i < powers.length; i++ ) {
            System.out.println( dists[i].getDistance() + "\t" + powers[i] );
        }
    }

    protected double[] getCDFs(ObjectWithDistance<Double>[] cdfs) {
        double[] result = new double[cdfs.length];
        for( int i = 0; i < cdfs.length; i++) {
            result[i] = cdfs[i].getValue();
        }
        return result;
    }

    protected double[] getDists(ObjectWithDistance<Double>[] cdfs) {
        double[] result = new double[cdfs.length];
        for( int i = 0; i < cdfs.length; i++) {
            result[i] = cdfs[i].getDistance();
        }
        return result;
    }
}
