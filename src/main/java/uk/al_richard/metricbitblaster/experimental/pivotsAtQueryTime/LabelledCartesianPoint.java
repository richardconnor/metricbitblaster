package uk.al_richard.metricbitblaster.experimental.pivotsAtQueryTime;

import dataPoints.cartesian.CartesianPoint;

public class LabelledCartesianPoint extends CartesianPoint {

    public static int counter = 0;

    public final int label;

    public LabelledCartesianPoint(CartesianPoint p) {
        super(p.getPoint());
        this.label = counter++;
    }

    public LabelledCartesianPoint(int label, CartesianPoint p) {
        super(p.getPoint());
        this.label = label;
    }


}
