package uk.al_richard.metricbitblaster.experimental.pivotsAtQueryTime;

/**
 * Private class used to keep track of columns and their corresponding bit sums
 */
public class Count<T> implements Comparable<Count<T>> {
    public T data;
    public int position;

    public Count(int position, T data) {
        this.data = data;
        this.position = position;
    }

    @Override
    public int compareTo(Count<T> o) {
        return ((Comparable) data).compareTo( (Comparable) o.data);
    }
}
