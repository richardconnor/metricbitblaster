package uk.al_richard.metricbitblaster.experimental.pivotsAtQueryTime;

import uk.al_richard.metricbitblaster.util.OpenBitSet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * This class is inspired by IR concept of TF-IDF
 * In information retrieval, tf–idf (also TF*IDF, TFIDF, TF-IDF, or Tf-idf),
 * short for term frequency–inverse document frequency,
 * is a numerical statistic that is intended to reflect how important
 * a word is to a document in a collection or corpus.
 * https://en.wikipedia.org/wiki/Tf%E2%80%93idf
 *
 * This class is an adaptation and counts the number of columns a set bit appears.
 * It then assigns that value to the corresponding position in a corresponding integer column.
 * The column sums are then taken and columns sorted by the sums, the most different are those with the lowest sums.
 * That's the theory!
 *
 * @author al@st-andrews.ac.uk
 * 19/1/2022
 *
 */
public class TFIDFReferenceImpl {
    private final OpenBitSet[] datarep;
    private final int columns;
    private final int rows;
    List<Integer> most_different = new ArrayList<>(); // the most different columns in the datarep

    public TFIDFReferenceImpl(OpenBitSet[] datarep, int datasize ) {
        this.rows = datasize;
        this.columns = datarep.length;
        this.datarep = datarep;
        initialiseMostDifferent();
    }

    /**
     * This method initialises the modt different list (the only point of this class).
     * It does this by counting the zero bits in each row and distributing the count amongst the columns containing the zero bits.
     * These column sums are then summed to give a column sum.
     * The most different columns are those with the lowest non-zero counts.
     * These are used to initialise the most_different list.
     */
    private void initialiseMostDifferent() {
//        showBits(datarep);

        short[][] counts = new short[rows][columns]; // used to store the bit counts to calculate the most different - short will do for this.

        for( int row = 0; row < rows; row++ ) {
            short count_0s = 0; // counts the unset bits on a row
            // first count the bits unset in a row
                for( int column = 0; column < columns; column++ ) {
                if( ! datarep[column].get(row) ) { // counts the zeros - don't want to make all zero columns the best
                    count_0s++;
                }
            }
            // now distribute the counts into the counts list
                for( int column = 0; column < columns; column++ ) {
                if ( ! datarep[column].get(row) ) { // counts the zeros
                    counts[row][column] = count_0s;
                }
            }
        }
//        showCountTable(counts);
        initialiseMostDifferent(counts);
    }

    /**
     * This initialises the most_different list
     * The most different columns are at the start of the list.
     * @param counts - an array of counts that is created by  initialiseMostDifferent()
     */
    private void initialiseMostDifferent(short[][] counts) {
        List<Count> column_sums = new ArrayList<>();

        for(int column = 0; column < counts[column].length ; column++ ) {
            int sum = 0;
            for( int row = 0; row < rows; row++ ) {
                sum = sum + counts[row][column];
            }
            column_sums.add( new Count(sum,column) );
        }
        Collections.sort(column_sums);
        moveZerosToEnd(column_sums);
//        showSums(column_sums);
        most_different = createMostDifferentFromCounts(column_sums);
    }

    /**
     * This method transforms a list of Counts into a list of Integers (identifying columns) by discarding the count values.
     * @param counts - a list of Counts: frequency and column ids
     * @return the most different columns in order by traversing the frequency list
     */
    private List<Integer> createMostDifferentFromCounts(List<Count> counts) {
        List<Integer> result = new ArrayList<>();
        for( Count c : counts ) {
            result.add( c.col );
        }
        return result;
    }

    /**
     * This method takes any zero count entries and moves them to the end of the list
     * Zero count entries are all columns of all 1s and as such not different from rest.
     * @param counts - a list of (column) Counts
     */
    private void moveZerosToEnd(List<Count> counts) {
        List<Count> removed = new ArrayList<>();
        Iterator<Count> it = counts.iterator();
        while (it.hasNext()) {
            Count next = it.next();
            if (next.sum == 0) {
                it.remove();
                removed.add(next); // don't update whilst iterating
            }
            if(next.sum > 0 ) {
                // found them all we can stop.
            }
        }
        counts.addAll(removed);
    }

    /**
     * For debugging - prints a bitmap
     * @param datarep - the exclusion table (usually the one supplied to the constructor)
     */
    private void showBits(OpenBitSet[] datarep) {
        for( int row = 0; row < rows; row++ ) {
            System.out.print( row + ": ");
            for (int column = 0; column < columns; column++) {
                if (datarep[column].get(row)) {
                    System.out.print("1");
                } else {
                    System.out.print("0");
                }
            }
            System.out.println();
        }
    }

    /**
     * For debugging - prints the frequency table
     * @param counts - an array of all the
     */
    private void showCountTable(short[][] counts) {
        for(int row = 0; row < counts.length ; row++ ) {
            for(int column = 0; column < counts[column].length ; column++ ) {
                System.out.print(counts[row][column] + "  " );
            }
            System.out.println();
        }
    }

    /**
     * Debugging method - shows the columns sums.
     * @param sums - the sums collected
     */
    private void showSums(List<Count> sums) {
        System.out.println( "Column sums:" );
        for( Count c : sums ) {
            System.out.println( c.col + ":" + c.sum);
        }
    }

    /**
     * Really a debugging method - shows the most_different list - strictly don't need this but handy.
     */
    public void showMostDifferent() {
        System.out.println( "Most different columns:" );
        for( Integer i : most_different ) {
            System.out.println( i );
        }
    }

    /**
     * @return the most diffent columns created by this class
     */
    public List<Integer> getMostDifferent() { return most_different; }

    /**
     * Private class used to keep track of columns and their corresponding bit sums
     */
    private class Count implements Comparable<Count> {
        public Integer sum;
        public int col;

        public Count(int sum, int col) {
            this.sum = sum;
            this.col = col;
        }

        @Override
        public int compareTo(Count o) {
            return sum.compareTo(o.sum);
        }
    }
}
