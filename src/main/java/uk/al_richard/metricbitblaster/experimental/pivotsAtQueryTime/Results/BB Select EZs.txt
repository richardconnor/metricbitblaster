
Hypotheses

** A ** Prefiltering vs non prefiltering

Impl		options				data	conf ro	ros		d/q			ez size		parts selec	parts exc
V7			4pt,power,br2		euc20	100		100		214287		10100		100		100		1490.4 Mbytes 
V7			4pt,power,br2		euc20	2000	100		213882		10100		100		100		1200.4 Mbytes 

V7			4pt,power,br2		euc20	100		100		159181		10100		400		400		1202.7 Mbytes 
V7			4pt,power,br2		euc20	2000	100		158882		10100		400		400		1202.7 Mbytes 	
V7			4pt,power,br2		euc20	5000	100		158401		10100		400		400		1202.7 Mbytes 

Not a huge difference but some.

<<<<<<<<<<<<<<<<<<<<<<<<<<


** B ** Choosing at run time has an effect - 634185 vs 158882

V7			4pt,power,br2		euc20	2000	100		158882		10100		400		400		1202.7 Mbytes 	
Reference	4pt,re				euc20	-		40		634185		980		-			404			18.141


** C ** Can do well with high numbers of pivots (160) but can do better with pre-filtering and dynamic filtering.

reference	4pt,re				euc20	-		160		178581		13520	-			6940.9

V5			4pt,re				euc20	2000	160		184324		13520	1000		1000 
V5			4pt,re				euc20	2000	160		179380		13520	2000		2000
V5			4pt,re				euc20	2000	160		178518		13250	3000		3000
V5			4pt,re				euc20	2000	160		178253		13250	6000		6000

V7			4pt,power,br2		euc20	2000	160		84143		25760	1000		1000
V7			4pt,power,br2		euc20	2000	160		82776		25760	2000		2000
V7			4pt,power,br2		euc20	2000	160		82593		25760	6000		6000



** E ** Measuring the contribution made by EZs better than simple choosing of pivots FCFS

V6			4pt,power			euc20	2000	100		165052		10100	400			400
V7			4pt,power,br2		euc20	2000	100		158882		10100	400			400		

Even the best of these requires examining a residual list of about 1/8 of the total data set size


========================


Impl		options				data	conf ro	ros		d/q			ez size	parts selec	parts exc	time			
Reference	4pt,re				euc20	-		40		634185		980		-			404			18.141			

V2			4pt,balanced,re		euc20	-		100		467862		5450	1000		1000		14.80			
V2			4pt,balanced,re		euc20	-		140		376182		10430	400			400			15.444, 15.53	
V2			4pt,re				euc20	-		100		305646		5450	1000		1000		26.633			
V2			4pt,re				euc20	-		100		316526		10430	400			400			24.532			
V2			4pt,re				euc20	-		140		230916		10430	400			400			26.897, 27.281	
Reference	4pt,re				euc20	-		160		178581		13520	-			6940.901	42.386			
 
V7 different ROs:

V7			4pt,power,br2		euc20	2000	40			459737		1640	1000		1000	194.9 Mbytes 
V7			4pt,power,br2		euc20	2000	60			304914		3660	1000		1000	435 Mbytes
V7			4pt,power,br2		euc20	2000	80			211838		6480	1000		1000 
V7			4pt,power,br2		euc20	2000	100			155697		10100	1000		1000
V7			4pt,power,br2		euc20	2000	120			122218		14520	1000		1000
V7			4pt,power,br2		euc20	2000	160			84143		25760	1000		1000

More Ros is better but up against memory usage.

Increase partitions selected:

V7			4pt,power,br2		euc20	2000	100			997200		10100		100		100
V7			4pt,power,br2		euc20	2000	100			171179		10100		200		200		
V7			4pt,power,br2		euc20	2000	100			158882		10100		400		400	 
V7			4pt,power,br2		euc20	2000	100			156577		10100		600		600	 
V7			4pt,power,br2		euc20	2000	100			155929		10100		800		800
	
Increasing partitions selected helps but tails off.
	  
** F ** Can weights help improve the performance?          
	            
V8 with weights does worse with i * i or

Impl		options				data	conf ro	ros		d/q			ez size	parts selec	parts exc			
V8			i*i weight			euc20	2000	100		164040		10100		400		400
V8			thresh /200 filt	 euc20	2000	100		158882		10100		400		400

Aggressively penalising the last columns helps (a little)
 
----------

V9 different ROs:

ball_radii =  new double[] {0.7923,1.5170};
sheet_offsets =  new double[] {-0.3836,0.3401};

V9			4pt,power,br2		euc20	2000	40			463014	1640	1000		748.0 
V9			4pt,power,br2		euc20	2000	60			306484	3660	1000		1000	
V9			4pt,power,br2		euc20	2000	80			214131	6480	1000		1000 
V9			4pt,power,br2		euc20	2000	100			157207	10100	1000		1000 

worse than e.g.:
V7			4pt,power,br2		euc20	2000	100			155697		10100	1000		1000

Have I got something wrong in Matlab?
		
------

V10 & V11 - matlab plotting.

Try to select only useful columns at runtime.

V12 vs V9

Impl		options				data	conf ro	ros		d/q			ez size	parts selec	parts exc	time
V9			4pt,power,br2		euc20	2000	60			306484	3660	1000		1000		25.674
V12			4pt,power,br2		euc20	2000	60			306869	3660	700			350			24.5

V9			4pt,power,br2		euc20	2000	100			157207	10100	1000		100		 35.1		
V12			350 use_cols		euc20	2000	100			160621	10100	700			350			32.4
V12			500 use_cols		euc20	2000	100			158613	10100	1000		500			34.0	

This may make a significant difference if dynamically loading columns (from disk).

------

Can you drop the number of pivots if you use less columns?
What columns are selected are some (always) unused (at end of list?)		
	                        		

------
V9d

Impl		options				data	conf ro	ros		d/q			ez size	parts selec	parts exc	time


V9d			4pt,power,br2		euc20	2000	100		158765		10100	1000		1000		 	
V9			4pt,power,br2		euc20	2000	100		157207		10100	1000		1000		 35.1		
reference	4pt,rot				euc20	-		100		304039		5450	??		2750.974     52.506

V9 Shannon	4pt,rot				euc20	-		100		228320		10100	1000		1000

		
V9d:

	Balls:
		xoffset_peak1 = 0.9176
		xoffset_peak2 = 1.6355

	Sheets:
		java_mid_offset1 = -0.3601
		java_mid_offset2 = 0.3655
	 */
	
V9:

	// From Matlab				// from ???
	// Balls:
	// xoffset_peak1 = 0.7923
	// xoffset_peak2 = 1.5170
	// Sheets:
	// java_offset1 = -0.3836
	// java_offset2 = 0.3401


Shifts
Impl		options				data	conf ro	ros		d/q			ez size	parts selec	parts exc	time
reference	4pt,rot				euc20	-		100		304039		5450	??		2750.974     52.506
1SetTau		1%					euc20			100		245421		5450	??		1218.124	 23.354
1SetTau		4%					euc20			100		141144		4550	??		541.204		 15.013
1SetTau		5%					euc20			100		135021		5450	??		456.548		 14.571
1SetTau		7%					euc20			100		131977		5450	??		344.386		 12.899
1SetTau		8%					euc20			100		133308		5450	??		305.434		 13.453
1SetTau		10%					euc20			100		139017		5450	??		247.287		 12.227


Impl		options				data	conf ro	ros		d/q			ez size		must be	cant be		time
SetDoubleTau 0.07,0.93			euc20	-		100		88450		10400		341.788	291.836		11.374    <<<<<<<<<<<
reference	4pt,rot				euc20	-		100		304039		5450							52.506

Power tuned: ExamineOrthogonalityDoublePower

								euc20	-		100		93460		10100		773.7	188.22		13.776    <<<<<<<<<<<

Filtering columns: dynamic and static - 							
	
ExamineOrthogonalityV9Power		euc20	2000	100		93529		10100		754.81	308.275		19.183


ExamineOrthogonalityDoublePowerMultipleSheets	
+/- 1/10 dist1,dist2	 						100		57556		29900							21.545
+/1 1/5  dist1,dist2							100		58857		29900							25.269

double dist1 = pow.getPeak1().getDistance() / 10;
double dist2 = pow.getPeak2().getDistance() / 10;

Cut down number of n choose 2?

+/- 1/10 dist1,dist2	25/100 choose 2 sheets 	100		355296		2000				21.501
+/- 1/10 dist1,dist2	50/100 choose 2			100		157393		7550

(as above - wrong)

ExamineOrthogonalityDoublePowerSubsetMultipleSheets

+/1 1/20 dist1			50/100 choose 2			100		183841		7550				18.37

5 EZs per power zone = 10 each /20 /40			25		393120		3050				20.496			
5 EZs per power zone = 10 each /20 /40			50		165181		12350				18.849	
5 EZs per power zone = 10 each /10 /20			50		144694		12350				18.993
5 EZs per power zone = 10 each /5 /10			50		136689		12350				21.265

Try some intervals:
pow.getPeak1().getDistance(), pow.getPeak2().getDistance(),
pow.getMax(), pow.getMin(),
pow.getMin() + fivetenpcmin, pow.getMax() - fivetenpcmin,
pow.getPeak1().getDistance() - fivetenpcmin, pow.getPeak1().getDistance() + fivetenpcmin,
pow.getPeak2().getDistance() - fivetenpcmin, pow.getPeak2().getDistance() + fivetenpcmin

												50		234311		12350							36.624
												100		89418		49700	9330.4		7592.9		84.988  <<<<<<<
6 zones										  100		89457		39800	5821		4078		54.05

MainRunnerFileV9PickPivotsLIDIM

																				exclusions
Impl		options				data	conf ro	ros		d/q			ez size		must be	cant be		time		results
SetDoubleTau 0.07,0.93			euc20	-		100		88450		10400		341.788	291.836		11.374    <<<<<<<<<<<
reference	4pt,rot				euc20	-		100		304039		5450							52.506
PickPivotsLIDIM	(low)			euc20	-		100		160140		10100		4910				20.7		131052	
PickPivotsLIDIM	(high)			euc20	-		100		165238		10100		5520.6
PickPivotsLIDIM	(high-low)		euc20	-		100		159840		10100		5287.8				20.682		131323
                                                   
ExamineOrthogonalityDoubleTauLIDIM	(low)			100		90938		10000	288.4 293.5 T:581.994 	10.938	131052
ExamineOrthogonalityDoubleTauLIDIM	(high)			100		91571		10000	288.81 288.387 T:577.1 	12.017	129633
ExamineOrthogonalityDoubleTauLIDIM	(high-low)		100		91193		10000	290.317 287.604 T:577.921 11.819	131323
---------------

Versions: 

Reference
V1 Incremental - not interesting.
V2 TFIDF
V3 Just OO encapsulated
V4 Uses whole dataset to determine which columns - no optimisation of pre search
V5 Powers
V6 sorted columns FCFS - FIRST ONE TO USE OFFSETS
V7 - contributions
V8 weights and cut off
V9 new offsets calculated from EucJava Matlab
V10 Creates tables of LIDIM and distances for use in matlab plots.
V11 Generates efficacy of columns for use in matlab plots.
V12 limits the humber of columns used based on V11.
1SetTau Sets an offset in the static partitions to permit some % of bits to be set.
SetDoubleTau Uses tau offsets from Matlab to set Tau values for balls and sheets.
ExamineOrthogonalityDoublePower Tunes the individual balls and sheets using Power calculations
ExamineOrthogonalityV9Power - uses tuned balls and sheets and pre filtering and dynamic filtering (which has no effect)
MainRunnerFileV9PickPivotsLIDIM - picks pivots based on LIDIMS.
ExamineOrthogonalityDoubleTauLIDIM 
