package uk.al_richard.metricbitblaster.experimental.pivotsAtQueryTime;

import java.util.ArrayList;
import java.util.List;

public class CantBeMustBes {
    public final List<Integer> cant_bes;
    public final List<Integer> must_bes;
    public final List<Integer> most_different;

    public CantBeMustBes() {
        this(null);
    }

    public CantBeMustBes(List<Integer> most_different) {
        this.most_different = most_different;
        this.cant_bes = new ArrayList<>();
        this.must_bes = new ArrayList<>();
    }

    public CantBeMustBes(List<Integer> most_different, List<Integer> cant_bes, List<Integer> must_bes) {
        this.most_different = most_different;
        this.cant_bes = cant_bes;
        this.must_bes = must_bes;
    }

    public CantBeMustBes(List<Integer> cant_bes, List<Integer> must_bes) {
        this.most_different = null;
        this.cant_bes = cant_bes;
        this.must_bes = must_bes;
    }

    public boolean contains(int zone) {
        return cant_bes.contains(zone) || must_bes.contains(zone);
    }

    public int size() {
        return cant_bes.size() + must_bes.size();
    }
}
