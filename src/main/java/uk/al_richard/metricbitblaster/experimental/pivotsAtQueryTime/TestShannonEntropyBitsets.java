package uk.al_richard.metricbitblaster.experimental.pivotsAtQueryTime;

import org.junit.Before;
import org.junit.Test;
import uk.al_richard.metricbitblaster.util.OpenBitSet;

import java.util.List;

public class TestShannonEntropyBitsets {

    long all1s = 0xFFFF_FFFF_FFFF_FFFFl;
    long all0s = 0x0000_0000_0000_0000l;
    long almost_all0s = 0x000_0000_0000_0001l;
    long firsthalf = 0xFFFF_FFFF_0000_0001l;
    long secondhalf = 0x0000_0000_FFFF_FFFFl;
    long mix = 0xF0F0_F0F0_F0F0_F0FFl;
    long last_two = 0x0000_0000_0000_00FFl;

    OpenBitSet[] data0 = new OpenBitSet[7];
    OpenBitSet[] data1 = new OpenBitSet[7];


    @Before
    public void init() {

        data1[0] = new OpenBitSet(new long[]{all1s, all1s});
        data1[1] = new OpenBitSet(new long[]{all0s, almost_all0s});
        data1[2] = new OpenBitSet(new long[]{firsthalf, firsthalf});
        data1[3] = new OpenBitSet(new long[]{secondhalf, secondhalf});
        data1[4] = new OpenBitSet(new long[]{mix, mix});
        data1[5] = new OpenBitSet(new long[]{secondhalf, secondhalf}); // copy of 3!
        data1[6] = new OpenBitSet(new long[]{all0s, last_two});
    }

    /*
     * Not really a test!!!
     * Really used as a debug/sanity checking framework
     */
    @Test
    public void Test1() {
        ShannonEntropyBitsets.showMostDifferent(data1, Long.SIZE * 2);
    }
    
    @Test
    public void Test2() {
        List<Count<Double>> entropies = ShannonEntropyBitsets.shanonEntropy(data1, Long.SIZE * 2);
        for( int i = 0; i < entropies.size(); i++ ) {

        }
    }


}
