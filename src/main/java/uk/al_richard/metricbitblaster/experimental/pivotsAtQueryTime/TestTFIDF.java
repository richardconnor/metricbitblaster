package uk.al_richard.metricbitblaster.experimental.pivotsAtQueryTime;

import org.junit.Test;
import uk.al_richard.metricbitblaster.util.OpenBitSet;

public class TestTFIDF {

    /*
     * Not really a test!!!
     * Really used as a debug/sanity checking framework
     */
    @Test
    public void Test1() {
        long all1s = 0xFFFF_FFFF_FFFF_FFFFl;
        long all0s = 0x0000_0000_0000_0000l;
        long almost_all0s = 0x000_0000_0000_0001l;
        long firsthalf = 0xFFFF_FFFF_0000_0001l;
        long secondhalf = 0x0000_0000_FFFF_FFFFl;
        long mix = 0xF0F0_F0F0_F0F0_F0FFl;
        long last_two = 0x0000_0000_0000_00FFl;

        OpenBitSet[] data = new OpenBitSet[7];
        data[0] = new OpenBitSet(new long[]{all1s,all1s});
        data[1] = new OpenBitSet(new long[]{all0s,almost_all0s});
        data[2] = new OpenBitSet(new long[]{firsthalf,firsthalf});
        data[3] = new OpenBitSet(new long[]{secondhalf,secondhalf});
        data[4] = new OpenBitSet(new long[]{mix,mix});
        data[5] = new OpenBitSet(new long[]{secondhalf,secondhalf}); // copy of 3!
        data[6] = new OpenBitSet(new long[]{all0s,last_two});

        // List<Integer> most_different = TFIDF.getMostDifferent(data, Long.SIZE * 4);
        TFIDF.showMostDifferent(data, Long.SIZE * 2);
    }
}
