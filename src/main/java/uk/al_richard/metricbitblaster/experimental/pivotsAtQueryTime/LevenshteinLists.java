package uk.al_richard.metricbitblaster.experimental.pivotsAtQueryTime;

import coreConcepts.Metric;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Returns the Levenshtein distance of the Lists of Integers
 *
 * @author Richard Connor (actually I think Robert Moss wrote the distance
 *         function, after Wikipedia!)
 *
 * Hacked by al to work over Lists of Integers
 * 26/1/2022
 */
public class LevenshteinLists implements Metric<List<Integer>> {

    public LevenshteinLists() {} // for unit testing

    private static int minimum(int a, int b, int c) {
        return Math.min(Math.min(a, b), c);
    }

    @Override
    public double distance(List<Integer> x, List<Integer> y) {

        if( x.size() != y.size() ) {
            throw new RuntimeException( "distance: Lists must be same size" );
        }

        int[][] distanceAcc = new int[x.size() + 1][y.size() + 1];

        for (int i = 1; i <= x.size(); i++) {
            distanceAcc[i][0] = i;
        }
        for (int j = 1; j <= y.size(); j++) {
            distanceAcc[0][j] = j;
        }

        for (int i = 1; i <= x.size(); i++) {
            for (int j = 1; j <= y.size(); j++) {
                distanceAcc[i][j] = minimum(
                        distanceAcc[i - 1][j] + 1,
                        distanceAcc[i][j - 1] + 1,
                        distanceAcc[i - 1][j - 1] + ((x.get(i - 1).equals(y.get(j - 1))) ? 0 : 1));
            }
        }

        return distanceAcc[x.size()][y.size()] * 1.0 / x.size();
    }

    @Override
    public String getMetricName() { return "Levenshtein-Lists"; }

    @Test
    public void sameIdentical() {
        List<Integer> list = Arrays.asList( new Integer[]{ 1,4,5,6,7 } );
        assertEquals(distance(list, list),0d, 0.0001 );
    }

    @Test
    public void sameNonIdentical() {
        List<Integer> list1 = Arrays.asList( new Integer[]{ 1,4,5,6,7 } );
        List<Integer> list2 = Arrays.asList( new Integer[]{ 1,4,5,6,7 } );
        assertEquals(distance(list1, list2),0d, 0.0001 );
    }

    @Test
    public void different1() {
        List<Integer> list1 = Arrays.asList( new Integer[]{ 1,4,5,6,7 } );
        List<Integer> list2 = Arrays.asList( new Integer[]{ 7,6,5,4,1 } );
        assertEquals(distance(list1, list2),0.8d, 0.0001 );
    }

    @Test
    public void different2() {
        List<Integer> list1 = Arrays.asList( new Integer[]{ 7,6,5,4,1 } );
        List<Integer> list2 = Arrays.asList( new Integer[]{ 7,6,5,4,0 } );
        assertEquals(distance(list1, list2),0.2d, 0.0001 );
    }
}