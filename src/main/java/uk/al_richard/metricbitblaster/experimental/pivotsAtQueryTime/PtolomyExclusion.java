package uk.al_richard.metricbitblaster.experimental.pivotsAtQueryTime;

import java.util.List;

/**
 * @author al
 * 
 *  Implemnents Ptolomeic exclusion as in:
 *  @article{hetland_ptolemaic_2013,
 *  title = {Ptolemaic access methods: Challenging the reign of the metric space model},
 * 	         volume = {38},
 * 	         issn = {03064379},
 * 	         url = {https://linkinghub.elsevier.com/retrieve/pii/S0306437912000786},
 * 	         doi = {10.1016/j.is.2012.05.011},
 * 	         shorttitle = {Ptolemaic access methods},
 * 	         pages = {989--1006},
 * 	         number = {7},
 * 	         journaltitle = {Information Systems},
 * 	         author = {Hetland, Magnus Lie and Skopal, Tomáš and Lokoč, Jakub and Beecks, Christian},
 * 	         date = {2013-10},
 *  }
 *
 * @param <T> the type of the values
 */
public class PtolomyExclusion<T> extends ExclusionZone<T> {

	protected double interPivotDistance;

	private final RefPointSet<T> pointSet;
	private final int ref1;
	private final int ref2;

	public PtolomyExclusion(RefPointSet<T> pointSet, int ref1, int ref2) {
		this.pointSet = pointSet;
		this.ref1 = ref1;
		this.ref2 = ref2;
		this.setInterPivotDistance(pointSet.intDist(ref1, ref2));
	}



	/**
	 * @return true if
	 */
	@Override
	public boolean isIn(double[] dists) {
		double dp0s = dists[this.getRef1()];
		double dp1s = dists[this.getRef2()];

		// TODO Al is here this class unfinished!

		return false;

		// dists[this.ref1] < this.radius;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ExclusionZone#mustBeIn(java.lang.Object, double[], double)
	 * 
	 * return true if the query is a long way to the left of the defined
	 * partition...
	 */
	@Override
	public boolean mustBeIn(double[] dists, double t) {
		// TODO Al is here this class unfinished!
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ExclusionZone #mustBeIn(java.lang.Object, double[], double)
	 *
	 */
	@Override
	public boolean mustBeOut(double[] dists, double t) {
		// TODO Al is here this class unfinished!
		return false;

	}

	public void setWitnesses(List<T> witnesses) {
		// TODO Al is here this class unfinished! - maybe delete this?
//		List<double[]> twoDpoints = getCoordinateSet(witnesses);
//
//		if (this.rotationEnabled) {
//			this.calculateTransform(twoDpoints);
//		}
//		/*
//		 * now rotationAngle and x_offset are set, we need to get rotated xs
//		 */
//		@SuppressWarnings("unchecked")
//		ObjectWithDistance<Object>[] owds = new ObjectWithDistance[witnesses.size()];
//		for (int i = 0; i < owds.length; i++) {
//			double[] pt = twoDpoints.get(i);
//			double xOffset = this.rotationEnabled ? getNewX(pt[0], pt[1]) : pt[0];
//			owds[i] = new ObjectWithDistance<>(null, xOffset);
//		}
//		Quicksort.placeMedian(owds);
//		// offset is now the median x value for the rotated pointset
//		this.offset = owds[owds.length / 2].getDistance();
	}

	/**
	 * Adjusts the x offset to include some proportion of the witness in the region
	 * @param witnesses - a set of witness objects
	 * @param proportion - the proportion of objects to include in the region, e.g. 0.1 for 10% of them
	 */
	public void adjustTau(List<T> witnesses, float proportion ) {
		// TODO Al is here this class unfinished!
	}

	@Override
	public RefPointSet<T> getPointSet() {
		return pointSet;
	}

	public int getRef1() {
		return ref1;
	}

	public int getRef2() {
		return ref2;
	}

	public double getInterPivotDistance() {
		return interPivotDistance;
	}

	private void setInterPivotDistance(double intDist) {
		this.interPivotDistance = intDist;
	}
}
