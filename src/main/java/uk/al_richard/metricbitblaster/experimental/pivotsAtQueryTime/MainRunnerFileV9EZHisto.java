package uk.al_richard.metricbitblaster.experimental.pivotsAtQueryTime;

import coreConcepts.CountedMetric;
import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;
import testloads.TestContext;
import testloads.TestContext.Context;
import uk.al_richard.metricbitblaster.util.OpenBitSet;

import java.util.*;

/**
 * Eighth version of MainRunnerFile
 * Uses offsets from new Matlab code
 * Prints out histogram of difference table from phase 2.
 */
public class MainRunnerFileV9EZHisto {

	/* Constants/config */

	public static final int NUMBER_OPERATIONAL_PIVOTS = 100; //140 // 3X normal size + 20
	public static final int NUMBER_CONFIG_PIVOTS = 500; // 4000; // 2000 is 50X normal number for Euc20 (40)
	public static final int WITNESS_SET_SIZE = 500; // confirmed as 99% the same by WitnessSetSize.
	public static final int PARTS_SELECTED = 600;

	int SE_WITNESS_SET_SIZE = 5001;   // TODO why? Like this in ref impl
	int BALL_WITNESS_SET_SIZE = 1000; // TODO why? Like this in ref impl

	protected static final double RADIUS_INCREMENT = 0.3;

	protected static double[] ball_radii =  new double[] {0.7923,1.5170}; // from Matlab trace //{0.794796,1.51698 }; //{ 1.44, 1.946 }; // old Matlab
	protected static double[] sheet_offsets =  new double[] {-0.1498,0.5749}; // from Matlab trace{-0.310454,0.40342}; // { -0.2878,0.3298 }; old matlab

	private Context context;
	private boolean fourPoint;
	private boolean balanced;
	private boolean rotationEnabled;
	private List<CartesianPoint> queries;
	private RefPointSet<CartesianPoint> rps;
	private List<ExclusionZone<CartesianPoint>> ezs;
	private int noOfBitSets;
	private OpenBitSet[] datarep;
	private List<CartesianPoint> dat;
	private int noOfRefPoints;
	private int querySize;
	private TestContext tc;
	private int partsSelected;
	private List<CartesianPoint> refs;
	private double threshold;
	private Metric<CartesianPoint> metric;
	private List<Integer> most_different;

	public MainRunnerFileV9EZHisto() throws Exception {
		// Only do this once or it goes wrong!
		context = Context.euc20;
		tc = new TestContext(context);
		querySize = (context == Context.colors || context == Context.nasa) ? tc.dataSize() / 10 : 1000;
		int noOfRefPoints = NUMBER_CONFIG_PIVOTS;
		tc.setSizes(querySize, noOfRefPoints); // only do this once.
		List<CartesianPoint> full_data = tc.getData();
		List<CartesianPoint> witness_set = full_data.subList(0, WITNESS_SET_SIZE);
		List<CartesianPoint> queries = tc.getQueries();
		initialise_state(true, tc.getRefPoints(), full_data, witness_set, tc.metric(), queries );
	}

	public static void main(String[] args) throws Exception {

		MainRunnerFileV9EZHisto mrf = new MainRunnerFileV9EZHisto();
	}

	public void initialise_state(boolean first_time, List<CartesianPoint> refs, List<CartesianPoint> full_data, List<CartesianPoint> witness_set, Metric<CartesianPoint> metric, List<CartesianPoint> queries) throws Exception {

		this.fourPoint = true;
		this.balanced = false;
		this.rotationEnabled = false;

		if( first_time ) { // adjust the data and the witness sizes used for balancing.
			this.dat = witness_set;
			SE_WITNESS_SET_SIZE = witness_set.size() - 1;
			BALL_WITNESS_SET_SIZE = witness_set.size() - 1;
		} else {
			this.dat = full_data;
		}
		this.refs = refs;
		this.noOfRefPoints = refs.size();
		int nChoose2 = ((noOfRefPoints - 1) * noOfRefPoints) / 2;
		noOfBitSets = (( sheet_offsets.length ) * nChoose2) + (noOfRefPoints * ball_radii.length);
		this.threshold = tc.getThresholds()[0];
		this.partsSelected = PARTS_SELECTED; // same as ref
		this.queries = queries;
		this.metric = metric;
		this.rps = new RefPointSet<>(this.refs, this.metric);
		this.ezs = new ArrayList<>();

		ezs.addAll(getSheetExclusions(this.dat, this.refs, this.rps, this.balanced, this.fourPoint, this.rotationEnabled, sheet_offsets));
		ezs.addAll(getBallExclusions(this.dat, this.refs, this.rps, this.balanced));

		System.out.println("Date/time\t" + new Date().toString());
		System.out.println("Class\t" + getFileName());
		System.out.println("context\t" + context);
		System.out.println("4point\t" + fourPoint);
		System.out.println("balanced\t" + balanced);
		System.out.println("rot enabled\t" + rotationEnabled);
		System.out.println("data size\t" + this.dat.size());
		System.out.println("query size\t" + querySize);
		System.out.println("threshold\t" + threshold);
		System.out.println("refs size\t" + refs.size());
		System.out.println("no of bitsets\t" + noOfBitSets);
		System.out.println("no of partitions selected\t" + partsSelected);
		System.out.println("ezs size:\t" + ezs.size());

		System.out.println("building bitsets");

		datarep = new OpenBitSet[this.noOfBitSets];
		long t_start_datasets = System.currentTimeMillis();
		buildBitSetData(this.dat, this.datarep, this.rps, this.ezs);

		System.out.println("Finished building bitsets after = " + (System.currentTimeMillis() - t_start_datasets));

		TFIDF.printColumnSums(this.datarep, this.dat.size());
		//TFIDF.printColumBitCounts(this.datarep, this.dat.size());
		//TFIDF.printColumBitCountsByKind(this.datarep, this.dat.size(),ezs);
		return;

//		if( first_time ) {
//			// only run this if it is the first time round
//			// rather than go straight to performing queries get the best pivots and reinitialise BB.
//			// This will give more columns that standard BB from which to pick using TFIDF but should be far fewer distance calculations.
//
//			List<CartesianPoint> new_refs = findBestRefsWeighted(this.most_different, NUMBER_OPERATIONAL_PIVOTS, this.ezs, this.rps.refs);
//			initialise_state( false,new_refs, full_data, witness_set, this.metric, this.queries);
//		} else {
//			TFIDF.printColumnSums(this.datarep, this.dat.size());
//			TFIDF.printColumBitCounts(this.datarep, this.dat.size());
//		    TFIDF.printColumBitCountsByKind(this.datarep, this.dat.size(),ezs);
//		}
	}

	public List<ExclusionZone<CartesianPoint>> getBallExclusions(List<CartesianPoint> dat,
			List<CartesianPoint> refs, RefPointSet<CartesianPoint> rps, boolean balanced) {
		List<ExclusionZone<CartesianPoint>> res = new ArrayList<>();
		for (int i = 0; i < refs.size(); i++) {
			List<BallExclusion<CartesianPoint>> balls = new ArrayList<>();
			for (double radius : ball_radii) {
				BallExclusion<CartesianPoint> be = new BallExclusion<>(rps, i, radius);
				balls.add(be);
			}
			if (balanced) {
				// TODO this doesn't work!
				balls.get(0).setWitnesses(dat.subList(0, BALL_WITNESS_SET_SIZE));
				double midRadius = balls.get(0).getRadius();
				double thisRadius = midRadius - ((balls.size() / 2) * RADIUS_INCREMENT);
				for (int ball = 0; ball < balls.size(); ball++) {
					balls.get(ball).setRadius(thisRadius);
					thisRadius += RADIUS_INCREMENT;
				}
			}
			res.addAll(balls);
		}
		return res;
	}

	public List<ExclusionZone<CartesianPoint>> getSheetExclusions(List<CartesianPoint> dat,
			List<CartesianPoint> refs, RefPointSet<CartesianPoint> rps, boolean balanced, boolean fourPoint,
			boolean rotationEnabled, double[] sheet_offsets ) {
		List<ExclusionZone<CartesianPoint>> res = new ArrayList<>();
		for (int i = 0; i < refs.size() - 1; i++) {
			for (int j = i + 1; j < refs.size(); j++) {
				for( int z = 0; z < sheet_offsets.length; z++ ) {
					if (fourPoint) {
						SheetExclusion4p<CartesianPoint> se = new SheetExclusion4p<>(rps, i, j, sheet_offsets[z]);
						if (balanced) {
							se.setRotationEnabled(rotationEnabled);
							se.setWitnesses(dat.subList(0, SE_WITNESS_SET_SIZE));
						}
						res.add(se);
					} else {
						SheetExclusion3p<CartesianPoint> se = new SheetExclusion3p<>(rps, i, j, sheet_offsets[z]);
						if (balanced) {
							se.setWitnesses(dat.subList(0, SE_WITNESS_SET_SIZE));
						}
						res.add(se);
					}
				}
			}
		}
		return res;
	}

	public <T> void buildBitSetData(List<T> data, OpenBitSet[] datarep, RefPointSet<T> rps,
			List<ExclusionZone<T>> ezs) {
		int dataSize = data.size();
		for (int i = 0; i < datarep.length; i++) {
			datarep[i] = new OpenBitSet(dataSize);
		}
		for (int n = 0; n < dataSize; n++) {
			T p = data.get(n);
			double[] dists = rps.extDists(p);
			for (int x = 0; x < datarep.length; x++) {
				boolean isIn = ezs.get(x).isIn(dists);
				if (isIn) {
					datarep[x].set(n);
				}
			}
		}
	}

	protected <T> OpenBitSet doExclusions(List<T> dat, double t, OpenBitSet[] datarep, CountedMetric<T> cm, T q,
			final int dataSize, List<Integer> mustBeIn, List<Integer> cantBeIn) {
		if (mustBeIn.size() != 0) {
			OpenBitSet ands = getAndOpenBitSets(datarep, dataSize, mustBeIn);
			if (cantBeIn.size() != 0) {
				/*
				 * hopefully the normal situation or we're in trouble!
				 */
				OpenBitSet nots = getOrOpenBitSets(datarep, dataSize, cantBeIn);
				nots.flip(0, dataSize);
				ands.and(nots);
				return ands;
				// filterContenders(dat, t, cm, q, res, dataSize, ands);
			} else {
				// there are no cantBeIn partitions
				return ands;
				// filterContenders(dat, t, cm, q, res, dataSize, ands);
			}
		} else {
			// there are no mustBeIn partitions
			if (cantBeIn.size() != 0) {
				OpenBitSet nots = getOrOpenBitSets(datarep, dataSize, cantBeIn);
				nots.flip(0, dataSize);
				return nots;
				// filterContenders(dat, t, cm, q, res, dataSize, nots);
			} else {
				// there are no exclusions at all...
				return null;
				// for (T d : dat) {
				// if (cm.distance(q, d) < t) {
				// res.add(d);
				// }
				// }
			}
		}
	}

	private OpenBitSet getAndOpenBitSets(OpenBitSet[] datarep, final int dataSize, List<Integer> mustBeIn) {
		OpenBitSet ands = null;
		if (mustBeIn.size() != 0) {
			ands = datarep[mustBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < mustBeIn.size(); i++) {
				ands.and(datarep[mustBeIn.get(i)]);
			}
		}
		return ands;
	}

	private OpenBitSet getOrOpenBitSets(OpenBitSet[] datarep, final int dataSize, List<Integer> cantBeIn) {
		OpenBitSet nots = null;
		if (cantBeIn.size() != 0) {
			nots = datarep[cantBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < cantBeIn.size(); i++) {
				final OpenBitSet nextNot = datarep[cantBeIn.get(i)];
				nots.or(nextNot);
			}
		}
		return nots;
	}

	public String getFileName() {
		try {
			throw new RuntimeException();
		} catch (RuntimeException e) {
			return e.getStackTrace()[1].getClassName();
		}
	}
}
