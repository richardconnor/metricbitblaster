package uk.al_richard.metricbitblaster.experimental.pivotsAtQueryTime;

import coreConcepts.CountedMetric;
import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;
import testloads.TestContext;
import testloads.TestContext.Context;
import uk.al_richard.metricbitblaster.util.OpenBitSet;

import java.util.ArrayList;
import java.util.List;

/**
 * Examines different sized data sets to see how small a witness set you can use to
 * establish the most different EZs.
 **/
public class WitnessSetSize {

	protected static final double RADIUS_INCREMENT = 0.3;
	private static double MEAN_DIST = 1.81;
	protected static double[] ball_radii = new double[] { MEAN_DIST - 2 * RADIUS_INCREMENT,
			MEAN_DIST - RADIUS_INCREMENT, MEAN_DIST, MEAN_DIST - RADIUS_INCREMENT, MEAN_DIST - 2 * RADIUS_INCREMENT };

	private Context context;
	private boolean fourPoint;
	private boolean balanced;
	private boolean rotationEnabled;
	private List<CartesianPoint> queries;
	private RefPointSet<CartesianPoint> rps;
	private List<ExclusionZone<CartesianPoint>> ezs;
	private int noOfBitSets;
	private OpenBitSet[] datarep;
	private List<CartesianPoint> dat;
	private int noOfRefPoints;
	private int querySize;
	private TestContext tc;
	private int partsSelected;
	private List<CartesianPoint> refs;
	private double threshold;
	private Metric<CartesianPoint> metric;
	private List<Integer> most_different;

	public WitnessSetSize() throws Exception {
		// Only do this once or it goes wrong!
		context = Context.euc20;
		tc = new TestContext(context);
		querySize = (context == Context.colors || context == Context.nasa) ? tc.dataSize() / 10 : 1000;
		int noOfRefPoints = 200;  // 200 is 10X normal number for Euc20.
		tc.setSizes(querySize, noOfRefPoints); // only do this once.
		this.dat = tc.getData();

	}

	public static void main(String[] args) throws Exception {

		WitnessSetSize mrf = new WitnessSetSize();
		List<List<Integer>> lists = new ArrayList<>();
		int largest = mrf.dat.size();
		for( int data_size = largest; data_size > 400; data_size = data_size / 2 ) {
			System.out.println( "Adding list for data of size " + data_size );
			lists.add( mrf.compareExclusionSizes(data_size) );
		}

		LevenshteinLists ll = new LevenshteinLists();

		int data_size = largest;
		for( int i = 0; i < lists.size() - 1; i++ ) {
			for( int j = i+1; j < lists.size(); j++ ) {
				System.out.print("Comparing lists of size " + data_size / ((int)Math.pow(2,i)) + " and " + data_size / ((int)Math.pow(2,j)) );
				System.out.println(", Distance = " + ll.distance(lists.get(i), lists.get(j)));
			}
		}
	}



	public List<Integer> compareExclusionSizes(int subset_size) {
		this.fourPoint = true;
		this.balanced = false;
		this.rotationEnabled = false;
		this.dat = dat.subList(0,subset_size);
		this.refs = tc.getRefPoints();
		this.noOfRefPoints = refs.size();
		int nChoose2 = ((noOfRefPoints - 1) * noOfRefPoints) / 2;
		this.noOfBitSets = nChoose2 + (noOfRefPoints * ball_radii.length);
		this.threshold = tc.getThresholds()[0];
		this.partsSelected = 400; // same as ref
		this.metric = tc.metric();
		this.rps = new RefPointSet<>(this.refs, this.metric);
		this.ezs = new ArrayList<>();

		ezs.addAll(getSheetExclusions(this.dat, this.refs, this.rps, this.balanced, this.fourPoint, this.rotationEnabled));
		ezs.addAll(getBallExclusions(this.dat, this.refs, this.rps, this.balanced));

		datarep = new OpenBitSet[this.noOfBitSets];
		buildBitSetData(this.dat, this.datarep, this.rps, this.ezs);
		return TFIDF.getMostDifferent(this.datarep, this.dat.size()); // the most different columns in the set
	}

	public List<ExclusionZone<CartesianPoint>> getBallExclusions(List<CartesianPoint> dat,
			List<CartesianPoint> refs, RefPointSet<CartesianPoint> rps, boolean balanced) {
		List<ExclusionZone<CartesianPoint>> res = new ArrayList<>();
		for (int i = 0; i < refs.size(); i++) {
			List<BallExclusion<CartesianPoint>> balls = new ArrayList<>();
			for (double radius : ball_radii) {
				BallExclusion<CartesianPoint> be = new BallExclusion<>(rps, i, radius);
				balls.add(be);
			}
			if (balanced) {
				balls.get(0).setWitnesses(dat.subList(0, 1000));
				double midRadius = balls.get(0).getRadius();
				double thisRadius = midRadius - ((balls.size() / 2) * RADIUS_INCREMENT);
				for (int ball = 0; ball < balls.size(); ball++) {
					balls.get(ball).setRadius(thisRadius);
					thisRadius += RADIUS_INCREMENT;
				}
			}
			res.addAll(balls);
		}
		return res;
	}

	public List<ExclusionZone<CartesianPoint>> getSheetExclusions(List<CartesianPoint> dat,
			List<CartesianPoint> refs, RefPointSet<CartesianPoint> rps, boolean balanced, boolean fourPoint,
			boolean rotationEnabled) {
		List<ExclusionZone<CartesianPoint>> res = new ArrayList<>();
		for (int i = 0; i < refs.size() - 1; i++) {
			for (int j = i + 1; j < refs.size(); j++) {
				if (fourPoint) {
					SheetExclusion4p<CartesianPoint> se = new SheetExclusion4p<>(rps, i, j);
					if (balanced) {
						se.setRotationEnabled(rotationEnabled);
						se.setWitnesses(dat.subList(0, 5001));
					}
					res.add(se);
				} else {
					SheetExclusion3p<CartesianPoint> se = new SheetExclusion3p<>(rps, i, j);
					if (balanced) {
						se.setWitnesses(dat.subList(0, 5001));
					}
					res.add(se);
				}
			}
		}
		return res;
	}

	public <T> void buildBitSetData(List<T> data, OpenBitSet[] datarep, RefPointSet<T> rps,
			List<ExclusionZone<T>> ezs) {
		int dataSize = data.size();
		for (int i = 0; i < datarep.length; i++) {
			datarep[i] = new OpenBitSet(dataSize);
		}
		for (int n = 0; n < dataSize; n++) {
			T p = data.get(n);
			double[] dists = rps.extDists(p);
			for (int x = 0; x < datarep.length; x++) {
				boolean isIn = ezs.get(x).isIn(dists);
				if (isIn) {
					datarep[x].set(n);
				}
			}
		}
	}

	protected <T> OpenBitSet doExclusions(List<T> dat, double t, OpenBitSet[] datarep, CountedMetric<T> cm, T q,
			final int dataSize, List<Integer> mustBeIn, List<Integer> cantBeIn) {
		if (mustBeIn.size() != 0) {
			OpenBitSet ands = getAndOpenBitSets(datarep, dataSize, mustBeIn);
			if (cantBeIn.size() != 0) {
				/*
				 * hopefully the normal situation or we're in trouble!
				 */
				OpenBitSet nots = getOrOpenBitSets(datarep, dataSize, cantBeIn);
				nots.flip(0, dataSize);
				ands.and(nots);
				return ands;
				// filterContenders(dat, t, cm, q, res, dataSize, ands);
			} else {
				// there are no cantBeIn partitions
				return ands;
				// filterContenders(dat, t, cm, q, res, dataSize, ands);
			}
		} else {
			// there are no mustBeIn partitions
			if (cantBeIn.size() != 0) {
				OpenBitSet nots = getOrOpenBitSets(datarep, dataSize, cantBeIn);
				nots.flip(0, dataSize);
				return nots;
				// filterContenders(dat, t, cm, q, res, dataSize, nots);
			} else {
				// there are no exclusions at all...
				return null;
				// for (T d : dat) {
				// if (cm.distance(q, d) < t) {
				// res.add(d);
				// }
				// }
			}
		}
	}

	private OpenBitSet getAndOpenBitSets(OpenBitSet[] datarep, final int dataSize, List<Integer> mustBeIn) {
		OpenBitSet ands = null;
		if (mustBeIn.size() != 0) {
			ands = datarep[mustBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < mustBeIn.size(); i++) {
				ands.and(datarep[mustBeIn.get(i)]);
			}
		}
		return ands;
	}

	private OpenBitSet getOrOpenBitSets(OpenBitSet[] datarep, final int dataSize, List<Integer> cantBeIn) {
		OpenBitSet nots = null;
		if (cantBeIn.size() != 0) {
			nots = datarep[cantBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < cantBeIn.size(); i++) {
				final OpenBitSet nextNot = datarep[cantBeIn.get(i)];
				nots.or(nextNot);
			}
		}
		return nots;
	}

	public String getFileName() {
		try {
			throw new RuntimeException();
		} catch (RuntimeException e) {
			return e.getStackTrace()[1].getClassName();
		}
	}
}
