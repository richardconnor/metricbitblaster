package uk.al_richard.metricbitblaster.experimental.alRichardLucia.vectorSpace.stringVectorisation;

import java.util.List;
import java.util.TreeSet;

public class ErrorValues {
	double[] correct;
	double[] mapped;
	double minRatio;
	double maxRatio;
	double meanRatio;
	int n;

	ErrorValues(double[] correct, double[] mapped) {
		this.n = correct.length;
		assert this.n == mapped.length;
		this.correct = correct;
		this.mapped = mapped;
		this.minRatio = Double.MAX_VALUE;
		this.maxRatio = Double.MIN_VALUE;
		double acc = 0;
		for (int i = 0; i < this.n; i++) {
			double ratio = correct[i] / mapped[i];
			// assert mapped[i] < correct[i] * 10;
			if (!Double.isNaN(ratio) && Double.isFinite(ratio)) {
				this.minRatio = Math.min(this.minRatio, ratio);
				this.maxRatio = Math.max(this.maxRatio, ratio);
				acc += ratio;
			} else {
				assert !Double.isNaN(mapped[i]) : "mapping is nan";
				// assert mapped[i] != 0 : "mapped ratio is infinite";
				assert mapped[i] == 0 : "mapped distance is zero";
			}
		}
		this.meanRatio = acc / this.n;
	}

	public double getRelativeError() {
		double acc = 0;
		for (int i = 0; i < n; i++) {
			double err = (this.correct[i] - (this.mapped[i] * this.meanRatio)) / this.correct[i];
			acc += Math.abs(err);
		}
		return acc / this.n;
	}

	public double getAbsoluteSquaredError() {
		double acc = 0;
		for (int i = 0; i < n; i++) {
			double err = (this.correct[i] - (this.mapped[i] * this.meanRatio));
			acc += err * err;
		}
		return acc / this.n;
	}

	public double getDistortion() {
		return (this.maxRatio / this.minRatio);
	}

	public static double mean(List<Double> array) {
		double acc = 0;
		for (double d : array) {
			acc += d;
		}
		return acc / array.size();
	}

	public static double median(List<Double> array) {
		TreeSet<Double> t = new TreeSet<>();
		for (double d : array) {
			while( t.contains(d)) {
				d = d + 1e-10;
			}
			t.add(d);
		}
		//assert (t.size() == array.size());
		for (int i = 0; i < t.size() / 2; i++) {
			t.remove(t.first());
		}
		return t.first();
	}
}
