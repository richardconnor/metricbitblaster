package uk.al_richard.metricbitblaster.experimental.alRichardLucia.vectorSpace;

import dataPoints.cartesian.CartesianPoint;
import dataPoints.cartesian.Euclidean;
import uk.al_richard.metricbitblaster.experimental.alRichardLucia.Pair;
import uk.al_richard.metricbitblaster.experimental.alRichardLucia.vectorSpace.stringVectorisation.Metric;
import uk.al_richard.metricbitblaster.experimental.alRichardLucia.vectorSpace.stringVectorisation.NdimSimplex;
import util.OrderedList;

import java.util.ArrayList;
import java.util.List;

public class Medians<T> {

    private final double max_distance;
    private final double almost_max_distance;
    private final Euclidean<CartesianPoint> euclidian;
    private NdimSimplex<T> reference_simplex;
    private Metric<T> metric;

    public Medians(Metric<T> metric, List<T> reference_points_in_original_space) throws NdimSimplex.SimplexError {

        this.metric = metric;
        euclidian = new Euclidean<>();
        reference_simplex = new NdimSimplex<T>(metric, reference_points_in_original_space);

        max_distance = findMaxInterpointDistanceFromSample( reference_points_in_original_space );
        almost_max_distance = max_distance * 0.98;
    }

    private double findMaxInterpointDistanceFromSample(List<T> reference_points_in_original_space) {
        double max = Double.MIN_VALUE;

        for( int i = 0; i < reference_points_in_original_space.size() - 1; i++ ) {
            for( int j = i+1; j < reference_points_in_original_space.size(); j++ ) {
                double d = metric.distance(reference_points_in_original_space.get(i),reference_points_in_original_space.get(j));
                if( d > max ) {
                    max = d;
                }
            }
        }
        return max;
    }


    List<Pair<T>> getReferenceObjects(List<T> data_points_in_original_space, boolean trace ) {

        if( trace ) {
            System.out.println("Building apices for " + data_points_in_original_space.size() + " points");
        }
        List<double[]> euclidian_points = dataPointsInOriginalSpaceToApices(data_points_in_original_space);

        List<double[]> euclidian_pivot_pairs = new ArrayList<>();

        for( int dimension = 0; dimension < euclidian_points.get(0).length; dimension++ ) {

            addpair( dimension,euclidian_points,euclidian_pivot_pairs );
        }

        List<Pair<T>> ro_pairs = getROPairs(euclidian_pivot_pairs, euclidian_points, data_points_in_original_space, false);
        return ro_pairs;
    }

    /**
     * for each dimension find the median points in that dimension and add a pair that is far apart.
     * this is finding the plane: ø across 1 dimension of the annulus at a time.
     * @param dimension
     * @param mapped_points
     * @return a pair far across the space in that dim.
     */
    private void addpair(int dimension, List<double[]> mapped_points, List<double[]> vectors ) {
        OrderedList<double[],Double> points_ordered_by_dimension = new OrderedList<>( mapped_points.size() );

        for( double[] mapped_point : mapped_points ) {
             points_ordered_by_dimension.add( mapped_point, mapped_point[dimension] ); // sort points by the value of the particular dimension e.g. for dimension D order by [.....,D....]
        }
        List<double[]> points = points_ordered_by_dimension.getList();
        int median_index = points.size() / 2;
        // Now move out from the middle index and try and find a pair that is greater than almost_max_distance
        // if we can't find one take the biggest from 20 either side of the middle_index
        double max = Double.MIN_VALUE;
        int best_i = 0;
        int best_j = 0;

//        for( int i = 0 ; i < points.size() - 1; i++ ) {
//                      for (int j = i+1; j < points.size(); j++) {

        for( int i = median_index - 10 ; i < median_index + 9; i++ ) {
            for (int j = i+1; j < median_index + 10; j++) {

                double[] d1 = points.get(i);
                double[] d2 = points.get(j);

                double distance = euclidian.distance(new CartesianPoint(d1), new CartesianPoint(d2));
            if( distance > almost_max_distance ) {
                if( !vectors.contains(d1) && !vectors.contains(d2) ) {
                    System.out.println( "Found point better than almost_max_distance - dim  " + dimension );
                    vectors.add(d1);
                    vectors.add(d2);
                    return;
                }
            } else
                if (distance > max && ! isIn(vectors,d1) && ! isIn(vectors,d2) ) {
                        best_i = i;
                        best_j = j;
                        max = distance;
                }
            }
        }
        if( max > almost_max_distance ) {
            System.out.println( "Found point further than almost_max_distance - dim  " + dimension + " distance = " + max );
        } else {
            System.out.println("DID NOT find point better than almost_max_distance - dim  " + dimension + " distance = " + max );
        }
        vectors.add( points.get( best_i ) );
        vectors.add( points.get( best_j ) );
       // vectors.add( points.get( 0 ) );
       // vectors.add( points.get( points.size() - 1 ) );
    }

    private boolean isIn(List<double[]> vectors, double[] vector ) {
        for( double[] next : vectors ) {
            if( equal( next, vector) ) {
                return true;
            }
        }
        return false;
    }

    private boolean equal(double[] first, double[] second) {
        if( first.length !=  second.length ) {
            return false;
        }
        for( int i = 0; i < first.length; i++ ) {
            if( first[i] != second[i] ) {
                return false;
            }
        }
        return true;
    }


    private void printVectors(List<double[]> vectors) {
        for( int index = 0; index < vectors.size(); index++ ) {
            System.out.println( VectorOperations.vecToString(vectors.get(index)) );
        }
    }


    private List<double[]> dataPointsInOriginalSpaceToApices(List<T> data_points_in_original_space) {
        List<double[]> apices = new ArrayList<>();
        for( T input : data_points_in_original_space ) {
            double[] new_apex = reference_simplex.getApex(input);
            apices.add( new_apex );
        }
        return apices;
    }

    private List<Pair<T>> getROPairs(List<double[]> data_points_in_euclidian_space, List<double[]> vectors, List<T> data_points_in_original_space, boolean trace) {

        List<Pair<T>>  ro_pairs = new ArrayList<>();

        for( int i = 0; i < data_points_in_euclidian_space.size(); i+=2 ) {

            double[] euclidian_point1 = data_points_in_euclidian_space.get(i);
            double[] euclidian_point2 = data_points_in_euclidian_space.get(i+1);

            int euc_index1 = vectors.indexOf(euclidian_point1);
            int euc_index2 = vectors.indexOf(euclidian_point2);

            ro_pairs.add( new Pair<T>( data_points_in_original_space.get( euc_index1 ),data_points_in_original_space.get( euc_index2 ) ) );
            if( trace ) {
                System.out.println("Pair:");
                System.out.println("\t index:" + euc_index1 + "," + euc_index2 );
                System.out.println("\t" + data_points_in_original_space.get(euc_index1) + " , " + data_points_in_original_space.get(euc_index2));
            }
        }

        return ro_pairs;
    }
}
