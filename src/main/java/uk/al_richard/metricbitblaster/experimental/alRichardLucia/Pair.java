package uk.al_richard.metricbitblaster.experimental.alRichardLucia;

import java.util.Objects;

public class Pair<K> {
    private final K first;
    private final K second;

    public Pair( K first, K second ) {
        this.first = first;
        this.second = second;
    }

    public K getFirst() {
        return this.first;
    }

    public K getSecond() {
        return this.second;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Pair)) return false;
        Pair<?> pair = (Pair<?>) o;
        return Objects.equals(first, pair.first) &&
                Objects.equals(second, pair.second);
    }

    @Override
    public int hashCode() {
        return Objects.hash(first, second);
    }
}
