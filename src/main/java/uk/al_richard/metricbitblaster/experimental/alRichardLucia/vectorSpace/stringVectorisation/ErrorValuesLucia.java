package uk.al_richard.metricbitblaster.experimental.alRichardLucia.vectorSpace.stringVectorisation;

import java.util.List;

public class ErrorValuesLucia {
	double[] correct;
	double[] mapped;
	double minRatio;
	double maxRatio;
	double meanRatio;
	int n;
	int dims;

	ErrorValuesLucia(double[] correct, double[] mapped, int projDims) {
		this.n = correct.length;
		assert this.n == mapped.length;
		this.correct = correct;
		this.mapped = mapped;
		this.minRatio = Double.MAX_VALUE;
		this.maxRatio = Math.pow(10, -16); // max ratio is r*D, it should be always greather than 0
		this.dims=projDims;
		double acc = 0;
		int count=0;
		int skipped=0;
		for (int i = 0; i < this.n; i++) {
			double ratio = correct[i] / mapped[i];
//			assert mapped[i] < correct[i] * 10;
		
			if (mapped[i]>Math.pow(10, -10)&&!Double.isNaN(ratio) && Double.isFinite(ratio)) {//I've observed strange results when mapped is too close to zero..
				this.minRatio = Math.min(this.minRatio, ratio);
				this.maxRatio = Math.max(this.maxRatio, ratio);
				acc += ratio;
				count ++;
			} else {
//				assert !Double.isNaN(mapped[i]) : "mapping is nan";
////				assert mapped[i] != 0 : "mapped ratio is infinite";
//				assert mapped[i] == 0 : "mapped distance is zero";
//				assert mapped[i] <=Math.pow(10, -10): "mapped distance is almost zero";
				skipped++;
			}
		}
		if(skipped>0) System.out.print(" --dist ratio skipped:"+skipped);
		if(skipped>count) 
			this.minRatio=0;// setting to zero to exclude this run
		this.meanRatio = acc / count;
	}

	public double getRelativeError() {
		double acc = 0;
		for (int i = 0; i < n; i++) {
			double err = (this.correct[i] - (this.mapped[i] * this.meanRatio)) / this.correct[i];
			acc += Math.abs(err);
		}
		return acc / this.n;
	}
	
	public double getRelativeErrorNoScaling() {
		double acc = 0;
		for (int i = 0; i < n; i++) {
			double err = (this.correct[i] - this.mapped[i]) / this.correct[i];
			acc += Math.abs(err);
		}
		return acc / this.n;
	}

	public double getAbsoluteSquaredError() {
		double acc = 0;
		for (int i = 0; i < n; i++) {
			double err = (this.correct[i] - (this.mapped[i] * this.meanRatio));
			acc += err * err;
		}
		return acc / this.n;
	}
	
	public double getAbsoluteSquaredErrorNoScaling() {
		double acc = 0;
		for (int i = 0; i < n; i++) {
			double err = (this.correct[i] - (this.mapped[i]));
			acc += err * err;
		}
		return acc / this.n;
	}

	public double getDistortion() {
		return (this.maxRatio / this.minRatio);
	}

	public static double mean(List<Double> array) {
		double acc = 0;
		for (double d : array) {
			acc += d;
		}
		return acc / array.size();
	}
}
