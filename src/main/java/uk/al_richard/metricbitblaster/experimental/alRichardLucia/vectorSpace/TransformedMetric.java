package uk.al_richard.metricbitblaster.experimental.alRichardLucia.vectorSpace;

import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;

public class TransformedMetric implements Metric<CartesianPoint> {
    private final Orthogonal<CartesianPoint> orthog;
    private final Euclidean euc = new Euclidean();
    private final Metric<CartesianPoint> original_metric;

    public TransformedMetric(Orthogonal<CartesianPoint> orthog, Metric<CartesianPoint> metric) {
        this.orthog = orthog;
        this.original_metric = metric;
    }

    @Override
    public double distance(CartesianPoint t1, CartesianPoint t2) {

//        System.out.print( "original distance = " + original_metric.distance(t1,t2) );
        double[] d1 = orthog.createApex(t1);
        CartesianPoint p1 = new CartesianPoint( d1 );

        double[] d2 = orthog.createApex(t2);
        CartesianPoint p2 = new CartesianPoint( d2 );

        double distance = euc.distance(p1,p2);

//        System.out.println( " transformed distance = " + distance );
        return distance;
    }

    @Override
    public String getMetricName() {
        return "TransformedMetric(Euc)";
    }
}
