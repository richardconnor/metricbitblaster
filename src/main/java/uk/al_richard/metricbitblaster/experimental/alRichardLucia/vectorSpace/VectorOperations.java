package uk.al_richard.metricbitblaster.experimental.alRichardLucia.vectorSpace;

public class VectorOperations {

    static double[] toVector(double[] point1, double[] point2) {
        if( point1.length != point2.length ) {
            throw new RuntimeException( "toVector: arrays of different lengths" );
        }
        double[] result = new double[point1.length];
        for( int i = 0; i < point1.length; i++ ) {
            result[i] = point1[i] - point2[i];
        }
        return result;
    }

    static double magnitude(double[] vector) {
        double result = 0;
        for( int i = 0; i < vector.length; i++ ) {
            result = result + vector[i] * vector[i];
        }
        return Math.sqrt( result );
    }

    static <T> double[] toUnitVector(Orthogonal<T> orthogonal, double[] vector) throws Exception {

        double mag = magnitude(vector);
        if( mag == 0 ) {
            throw new Exception( "Got a nan - vector magnitude = 0" );
        }
        double[] r = scaleDown(vector, mag);
        return r;
    }

    static double innerProduct(double[] vector1, double[] vector2) {
        if( vector1.length != vector2.length ) {
            throw new RuntimeException( "toVector: arrays of different lengths" );
        }
        double result = 0;
        for( int i = 0; i < vector1.length; i++ ) {
            result = result + ( vector1[i] * vector2[i] );
        }
        return result;
    }

    static String vecToString(double[] vector) {
        StringBuilder sb = new StringBuilder();
        sb.append( "[" );
        for( int i = 0; i < vector.length - 1; i++ ) {
            sb.append( vector[i] + ", " );
        }
        sb.append( vector[vector.length - 1 ] + " " );
        sb.append( "]" );
        return sb.toString();
    }

    private static double[] scaleDown(double[] vector, double by) {

        double[] result = new double[vector.length];
        for( int i = 0; i < vector.length; i++ ) {
            result[i] = vector[i] / by;
            if( Double.isNaN(result[i]) ) {
                System.out.println( "nan produced from " + vector[i] );
            }
        }
        return result;
    }
}
