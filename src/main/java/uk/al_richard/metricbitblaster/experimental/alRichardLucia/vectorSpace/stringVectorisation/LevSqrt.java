package uk.al_richard.metricbitblaster.experimental.alRichardLucia.vectorSpace.stringVectorisation;

public class LevSqrt implements Metric<String> {

	private static Metric<String> lev = new Levenshtein();

	@Override
	public double distance(String x, String y) {
		return Math.sqrt(lev.distance(x, y));
	}

	@Override
	public String getMetricName() {
		return "levSqrt";
	}

}
