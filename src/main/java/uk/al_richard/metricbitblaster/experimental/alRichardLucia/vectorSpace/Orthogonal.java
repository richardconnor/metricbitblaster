package uk.al_richard.metricbitblaster.experimental.alRichardLucia.vectorSpace;

import uk.al_richard.metricbitblaster.experimental.alRichardLucia.Pair;
import uk.al_richard.metricbitblaster.experimental.alRichardLucia.vectorSpace.stringVectorisation.Metric;
import uk.al_richard.metricbitblaster.experimental.alRichardLucia.vectorSpace.stringVectorisation.NdimSimplex;
import util.OrderedList;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Orthogonal<T> {

    public static final double threshold = 0.;
    static long SEED = 87626L;
    private NdimSimplex<T> reference_simplex;
    private Metric<T> metric;
    private int max_len = 0;
    private double[] zero_vector;

    public Orthogonal(Metric<T> metric, List<T> reference_points_in_original_space) throws NdimSimplex.SimplexError {

        this.metric = metric;
        reference_simplex = new NdimSimplex<T>(metric, reference_points_in_original_space);
        zero_vector = new double[ reference_points_in_original_space.size() ];
    }

    List<Pair<T>> getReferenceObjects(List<T> data_points_in_original_space, boolean trace ) {

        if( trace ) {
            System.out.println("Orthogonality threshold\t" +  threshold );
        }
        if( trace ) {
            System.out.println("Building apices for " + data_points_in_original_space.size() + " points");
        }
        List<double[]> euclidian_points = dataPointsInOriginalSpaceToApices(data_points_in_original_space);
        if( trace ) {
            System.out.println("Creating unit vectors from apices");
        }
        List<double[]> euclidian_vectors = apicesToUnitVectors(euclidian_points); // data_points_in_original_space.size() choose 2 of them
        if( trace ) {
            System.out.println("Created " + euclidian_vectors.size() + " vectors.. searching.. ");
        }
        List<double[]> orthogonal_vectors = elminateVectorPairsThatAreScalarProducts(euclidian_vectors);
        if( trace ) {
            System.out.println("Left with " + orthogonal_vectors.size() + " vectors (pairs of points)" );
        }
        List<Pair<T>> ro_pairs = getROPairs(orthogonal_vectors, euclidian_vectors, data_points_in_original_space, trace);
        return ro_pairs;
    }

    List<T> getReferenceObjects2(List<T> data_points_in_original_space, boolean trace, int required) {

        if( trace ) {
            System.out.println("Orthogonality threshold\t" +  threshold );
        }
        if( trace ) {
            System.out.println("Building apices for " + data_points_in_original_space.size() + " points");
        }
        List<double[]> euclidian_points = dataPointsInOriginalSpaceToApices(data_points_in_original_space);
        if( trace ) {
            System.out.println("Creating unit vectors from apices");
        }
        List<double[]> euclidian_vectors = apicesToUnitVectors(euclidian_points); // data_points_in_original_space.size() choose 2 of them
        if( trace ) {
            System.out.println("Created " + euclidian_vectors.size() + " vectors.. searching.. ");
        }
        List<Integer> orthogonal_indices = filterleastOrthogonal( euclidian_vectors,required );
        if( trace ) {
            System.out.println("Left with " + orthogonal_indices.size() + " indices " );
        }

        List<T> result = new ArrayList<>();
        for( int i : orthogonal_indices ) {
            result.add( data_points_in_original_space.get( i ) );
        }
        return result;
    }


    /*
     * This method takes the orthogonal vectors (post elimination)
     * Looks up their position in euclidian_vectors to find the index in euclidian_vectors.
     * This is the index of a pair of points that comprise the vector.
     * Next use that index to find the n choose 2 pair that maps to that index.
     * This gives the 2 indices into the original data
     * use these indexes to find the original data items in the unmapped space.
     * This is complex but I think it is right.
     */
    private List<Pair<T>> getROPairs(List<double[]> orthogonal_vectors, List<double[]> euclidian_vectors, List<T> data_points_in_original_space, boolean trace) {

        List<Pair<T>>  ro_pairs = new ArrayList<>();

        System.out.println( "picking from " + orthogonal_vectors.size() );
        System.out.println( "original n choose 2 from: " + data_points_in_original_space.size());

        List<int[]> original_index_pairs = pairsFromNChoose2( data_points_in_original_space.size() );

        for( double[] orthogonal_vector : orthogonal_vectors ) {
            int vector_index = euclidian_vectors.indexOf(orthogonal_vector);
            int[] original_indices = original_index_pairs.get( vector_index );
            ro_pairs.add( new Pair<T>( data_points_in_original_space.get( original_indices[0] ),data_points_in_original_space.get( original_indices[1] ) ) );
            if( trace ) {
                System.out.println("Pair:");
                System.out.println("\t" + data_points_in_original_space.get(original_indices[0]) + " , " + data_points_in_original_space.get(original_indices[1]));
            }
        }

        return ro_pairs;
    }

    private List< int[] > pairsFromNChoose2( int n ) {

        List<int[]> pairs = new ArrayList<>();

        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j < n; j++) {
                int[] pair = new int[]{i, j};
                pairs.add(pair);
            }
        }

        return pairs;
    }

    /**
     *
     * @param vectors_to_search
     * @return
     */
    private List<Integer> filterleastOrthogonal(List<double[]> vectors_to_search, int required ) {

        OrderedList<Integer, Double> correlations = new OrderedList<>( required ); // pairs of pivot pairs and their sums of absolute correlations, least correlated first.

        for (int i = 0; i < vectors_to_search.size(); i++) {

            double sigma_correlation = 0;

            for (int j = 0; j<  vectors_to_search.size(); j++){

                if( i != j ) {

                    double[] point1 = vectors_to_search.get(i);
                    double[] point2 = vectors_to_search.get(j);

                    sigma_correlation += Math.abs( VectorOperations.innerProduct( point1, point2 ) );
                }
            }
            correlations.add(i,sigma_correlation);
        }

        return correlations.getList();
    }


    /**
     *
     * @param vectors_to_search
     * @return
     */
    private List<double[]> elminateVectorPairsThatAreScalarProducts(List<double[]> vectors_to_search) {

        List<double[]> vectors_with_product_below_threshold = new ArrayList<>( vectors_to_search ); // making a copy of all and eliminate

        for( int index1 = 0; index1 < vectors_with_product_below_threshold.size() - 1; index1++ ) {
            if( vectors_with_product_below_threshold.get(index1) == zero_vector ) {
                // remove zero vectors introduced earlier
                vectors_with_product_below_threshold.remove(index1);
                index1--; // all elements have dropped by 1 position so must move index1 back
            } else {
                for (int index2 = index1 + 1; index2 < vectors_with_product_below_threshold.size(); index2++) {
                    if( vectors_with_product_below_threshold.get(index2) == zero_vector ) {
                        // remove zero vectors introduced earlier
                        vectors_with_product_below_threshold.remove(index2);
                        index2--; // all elements have dropped by 1 position so must move on index2 back
                    } else {
                        if ( Math.abs(VectorOperations.innerProduct( vectors_with_product_below_threshold.get(index1), vectors_with_product_below_threshold.get(index2) ) ) > threshold) {
                            // vectors not orthogonal so remove vector at index - this element is (almost) a scalar factor of element at index1
                            vectors_with_product_below_threshold.remove(index2);
                            index2--; // all elements have dropped by 1 position so must move index2 back
                        }
                    }
                }
            }
        }
        return vectors_with_product_below_threshold;
    }

    private List<double[]> removeFlagged(List<double[]> vectors_to_search, List<Integer> remove_list) {
        List<double[]> result = new ArrayList<>();
        for( int i = 0; i < vectors_to_search.size(); i++ ) {
            if( ! remove_list.contains(i) ) {
                result.add( vectors_to_search.get(i));
            }
        }
        return result;
    }


    private List<double[]> apicesToUnitVectors(List<double[]> apices) {
        List<double[]> unit_vectors = new ArrayList<>();
        for (int i = 0; i < apices.size() - 1; i++) {
            for (int j = i + 1; j < apices.size(); j++) {
                try {
                    double[] unit_vector = VectorOperations.toUnitVector(this, VectorOperations.toVector(apices.get(i), apices.get(j)));
                    unit_vectors.add(unit_vector);
                } catch (Exception e) {
                    unit_vectors.add(zero_vector); // add these in to keep indexing correct and reversible.
                }
            }
        }
        return unit_vectors;
    }

    public List<double[]> dataPointsInOriginalSpaceToApices(List<T> data_points_in_original_space) {
        List<double[]> apices = new ArrayList<>();
        for( T input : data_points_in_original_space ) {
            double[] new_apex = createApex(input);
            apices.add( new_apex );
        }
        return apices;
    }

    public double[] createApex(T input) {
        return reference_simplex.getApex(input);
    }


    static <T> List<T> permute(final Iterable<T> records, long seed) {

        Random random = new Random(seed);

        List<T> record_list = new ArrayList<>();
        for (T record : records) {
            record_list.add(record);
        }

        int number_of_records = record_list.size();

        for (int i = 0; i < number_of_records; i++) {
            int swap_index = random.nextInt(number_of_records);
            T temp = record_list.get(i);
            record_list.set(i, record_list.get(swap_index));
            record_list.set(swap_index, temp);
        }
        return record_list;
    }
}
