package uk.al_richard.metricbitblaster.experimental.alRichardLucia.vectorSpace;

import dataPoints.cartesian.CartesianPoint;
import uk.al_richard.metricbitblaster.experimental.alRichardLucia.vectorSpace.stringVectorisation.Metric;

public class Euclidean implements Metric<CartesianPoint> {
    public Euclidean() {
    }

    public double distance(CartesianPoint x, CartesianPoint y) {
        double[] ys = y.getPoint();
        double acc = 0.0D;
        int ptr = 0;
        double[] point = x.getPoint();
        int len = point.length;

        for(int i = 0; i < len; ++i) {
            double xVal = point[i];
            double diff = xVal - ys[ptr++];
            acc += diff * diff;
        }

        return Math.sqrt(acc);
    }

    public String getMetricName() {
        return "euc";
    }
}
