package uk.al_richard.metricbitblaster.experimental.alRichardLucia;

import coreConcepts.Metric;
import uk.al_richard.metricbitblaster.referenceImplementation.BallExclusion;
import uk.al_richard.metricbitblaster.referenceImplementation.ExclusionZone;
import uk.al_richard.metricbitblaster.referenceImplementation.SheetExclusion;
import uk.ac.standrews.cs.utilities.metrics.coreConcepts.DataDistance;
import uk.al_richard.metricbitblaster.production.MetricBitBlaster;
import uk.al_richard.metricbitblaster.util.PointDistance;
import util.OrderedList;

import java.util.*;

public class FFTBB <T> {

    private final List<T> dat;
    private final Metric<T> similarity_metric;
    private final Metric<T> distance_metric;
    private MetricBitBlaster<T> bitblaster;

    private static final int numpivots = 40;

    private Iterator<Integer> randoms;

    public FFTBB(List<T> dat, Metric<T> distance_metric, Metric<T> similarity_metric ) {

        this.dat = dat;
        this.similarity_metric = similarity_metric;
        this.distance_metric = distance_metric;
        randoms = new Random().ints( 0, dat.size() ).iterator();

        // Similarity is used since we are trying to find the furthest points in the set
        // When rangeSearch is used we get the lowest similarity (what we find in a search) that the most distant apart.

        List<T> pivots = dat.subList(0, numpivots);
        List<T> data = dat.subList(numpivots,dat.size());


        bitblaster = null; //new MetricBitBlaster<T>(similarity_metric::distance, pivots, data, false, false);
    }

    public ReverseOrderedList<Pair<Integer>, Double> getBestHeuristic(int desiredNumber) {

        ReverseOrderedList<Pair<Integer>,Double> ordered_ez_indices_by_distance = new ReverseOrderedList(desiredNumber);  // twice as many as needed since list of pairs but with duplicate ezs in the pairs

        List<T> remove_list = new ArrayList<>(dat); // copy of data - going to do select and remove from it.

        Random random = new Random();

        for( int i = 0; i < 1000; i++ ) {
            int random_seed = random.nextInt(remove_list.size());
            PointDistance<Integer> pd = maxDistance(remove_list, distance_metric, random_seed);
            Pair<Integer> new_pair = new Pair<>(pd.point1, pd.point2);
            if (!ordered_ez_indices_by_distance.getList().contains(new_pair)) {
                ordered_ez_indices_by_distance.add(new_pair, pd.distance);                  // OrderedList only keeps the best desiredNumber of entries.
            }
            remove_list.remove(random_seed);
        }

        show( ordered_ez_indices_by_distance );
        return ordered_ez_indices_by_distance; // results;

    }

    public void getBestEZsSimplexMaxMax(int desiredNumber, List<ExclusionZone<T>> ezs, List<Pair<T>> new_ezs) {

        List<Integer> ez_indices = new ArrayList<>();
        List<Integer> exclude_indices = new ArrayList<>();

        PointDistance<Integer> pd = maxDistance(dat, distance_metric, new Random().nextInt(dat.size()));    // pd has 2 values - each an index into data - exclusionZone<BitSet> (T)

        addEZPairToNewEZs(ezs, new_ezs, pd.point1);  // add the pair of ROs from the two ezs that are furthest apart.   // new_ros is a list of ROs of type <BitSet> (T)
        addEZPairToNewEZs(ezs, new_ezs, pd.point2);

        ez_indices.add( pd.point1 );                // each an index into data - remember these as ezs that we have picked.
        ez_indices.add( pd.point2 );

        for( int i = 0; i < ezs.size() && new_ezs.size() < desiredNumber ; i++ ) {
            long t1 = System.currentTimeMillis();
            Integer point = maxDistanceSimplexMaxMax(dat, ez_indices, exclude_indices);

            addEZPairToNewEZs(ezs, new_ezs, point);

            ez_indices.add( point );
            int to_search = dat.size() - exclude_indices.size() - ez_indices.size();
            long percent = Math.round( to_search * 0.02 );
            exclude_indices.addAll( closestN( point, dat, percent , ez_indices, exclude_indices ) );
            System.out.println( "Found " + new_ezs.size() + " Pairs (corresponding to sheet exclusions) / " + desiredNumber + " excluded = " + exclude_indices.size() + " this time = " + percent + " to search = " + ( dat.size() - exclude_indices.size() - ez_indices.size() ) + " in " + ( System.currentTimeMillis() - t1 ) );
        }
    }

    private Collection<Integer> closestN(Integer point, List<T> dat, long required, List<Integer> ez_indices, List<Integer> exclude_indices) {
        OrderedList<Integer,Double> closest = new OrderedList((int) required);

        for( int index = 0; index < dat.size(); index++ ) {
            if( ! ez_indices.contains(index) && ! exclude_indices.contains(index) ) {

                closest.add( index, distance_metric.distance( dat.get(point),dat.get( index ) ) );
            }
        }
        return  closest.getList();
    }


    void addEZPairToNewEZs(List<ExclusionZone<T>> ezs, List<Pair<T>> new_ezs, int ez_index) {
        ExclusionZone<T> next_ez = ezs.get(ez_index);
        if (next_ez instanceof BallExclusion) {
            System.out.println( "Found a ball ignoring it.");
        }
        else if (next_ez instanceof SheetExclusion) {

            addSheetNewSheetPair((SheetExclusion<T>) next_ez, new_ezs, ez_index);
        }
    }

    private void addSheetNewSheetPair(SheetExclusion<T> next_ez, List<Pair<T>> new_ezs, int ez_index) {

        Pair<T> new_pair = new Pair<>( next_ez.getPointSet().refs.get(next_ez.getRef1()),next_ez.getPointSet().refs.get(next_ez.getRef2()));

        System.out.println( "\tSheet: " + ez_index + " " );

        if ( ! new_ezs.contains(new_pair) ) {
            System.out.println( "\tnew pair added: new_pair( " + next_ez.getRef1() + "," + next_ez.getRef2() + ")" );
            new_ezs.add(new_pair);
        } else {
            System.out.println( "\tEZ already in set: " + next_ez.getRef1());
        }
    }

    public void getBestHeuristicSimplexMaxMax(int desiredNumber, List<ExclusionZone<T>> ezs, List<T> new_ros) {

        List<Integer> ez_indices = new ArrayList<>();
        List<Integer> exclude_indices = new ArrayList<>(); // not used here

        PointDistance<Integer> pd = maxDistance(dat, distance_metric, new Random().nextInt(dat.size()));    // pd has 2 values - each an index into data - exclusionZone<BitSet> (T)

        addROsToNewROs(ezs, new_ros, pd.point1 );  // add the ROs from the two ezs that are furthest apart.   // new_ros is a list of ROs of type <BitSet> (T)
        addROsToNewROs(ezs, new_ros, pd.point2 );

        ez_indices.add( pd.point1 );                // each an index into data - remember these as ezs that we have picked.
        ez_indices.add( pd.point2 );


        for( int i = 0; i < ezs.size() && new_ros.size() < desiredNumber ; i++ ) {
            Integer point = maxDistanceSimplexMaxMax(dat, ez_indices,exclude_indices);
            addROsToNewROs(ezs, new_ros, point );
            ez_indices.add( point );
            System.out.println( "Found " + new_ros.size() + " cartesian points " );
        }
    }

    void addROsToNewROs(List<ExclusionZone<T>> ezs, List<T> new_ros, int ez_index) {
        ExclusionZone<T> next_ez = ezs.get(ez_index);
        if (next_ez instanceof BallExclusion) {
            addBallRTToNewROs((BallExclusion<T>)next_ez, new_ros, ez_index );
        }
        else if (next_ez instanceof SheetExclusion) {

            addSheetROsToNewROs((SheetExclusion<T>) next_ez, new_ros, ez_index);
        }
    }

    private void addSheetROsToNewROs(SheetExclusion<T> next_ez, List<T> new_ros, int ez_index) {

        T next_ref1 = next_ez.getPointSet().refs.get(next_ez.getRef1());
        T next_ref2 = next_ez.getPointSet().refs.get(next_ez.getRef2());

        System.out.println( "\tSheet: " + ez_index + " " );

        if ( ! new_ros.contains(next_ref1) ) {
            System.out.println( "\tro1 added: " + next_ez.getRef1() );
            new_ros.add(next_ref1);
        } else {
            System.out.println( "\tro1 already in set: " + next_ez.getRef1());
        }

        if ( ! new_ros.contains(next_ref2) ) {
            System.out.println( "\tro2 added: " + next_ez.getRef2());
            new_ros.add(next_ref2);
        } else {
            System.out.println( "\tro2 already in set: " + next_ez.getRef2());
        }
    }

    public void addBallRTToNewROs(BallExclusion<T> next_ez, List<T> new_ros, int ez_index)  {

        T next_ref = next_ez.getPointSet().refs.get(next_ez.getIndex());
        System.out.print("\tBall: " + ez_index + " ");

        if (!new_ros.contains(next_ref)) {
            System.out.println("\tro added: " + next_ez.getIndex());
            new_ros.add(next_ref);
        } else {
            System.out.println("\tro already in set: " + next_ez.getIndex());
        }
    }


    public void getBestHeuristicSimplexSigma(int desiredNumber, List<ExclusionZone<T>> ezs, List<Pair<T>> new_ezs) {

        List<Integer> ez_indices = new ArrayList<>();
        List<Integer> exclude_indices = new ArrayList<>();

        PointDistance<Integer> pd = maxDistance(dat, distance_metric, new Random().nextInt(dat.size()));    // pd has 2 values - each an index into data - exclusionZone<BitSet> (T)

        addEZPairToNewEZs(ezs, new_ezs, pd.point1);  // add the pair of ROs from the two ezs that are furthest apart.   // new_ros is a list of ROs of type <BitSet> (T)
        addEZPairToNewEZs(ezs, new_ezs, pd.point2);

        ez_indices.add(pd.point1);                // each an index into data - remember these as ezs that we have picked.
        ez_indices.add(pd.point2);

        for (int i = 0; i < ezs.size() && new_ezs.size() < desiredNumber; i++) {
            long t1 = System.currentTimeMillis();
            Integer point = maxDistanceSimplexSigma(dat, ez_indices, exclude_indices);

            addEZPairToNewEZs(ezs, new_ezs, point);

            ez_indices.add(point);
            int to_search = dat.size() - exclude_indices.size() - ez_indices.size();
            long percent = Math.round(to_search * 0.02);
            exclude_indices.addAll(closestN(point, dat, percent, ez_indices, exclude_indices));
            System.out.println("Found " + new_ezs.size() + " Pairs (corresponding to sheet exclusions) / " + desiredNumber + " excluded = " + exclude_indices.size() + " this time = " + percent + " to search = " + (dat.size() - exclude_indices.size() - ez_indices.size()) + " in " + (System.currentTimeMillis() - t1));
        }
    }

    /*
     * Starts with a list containing at least 2 points
     * Finds the point that whose sum of distances is maximally far from those already picked.
     */
    private int maxDistanceSimplexSigma(List<T> dat, List<Integer> ez_indices, List<Integer> exclude_indices) {

        double max = Double.MIN_VALUE;
        int result = -1;

        for( int i = 0; i < dat.size(); i++ ) {
            if( ! ez_indices.contains(i) && ! exclude_indices.contains(i) ) {                // we have chosen this point already
                double sum = sumDistances(dat.get(i), ez_indices);
                if (sum > max) {
                    max = sum;
                    result = i;

                }
            }
        }
        return result;

    }

    /*
     * Starts with a list containing at least 2 points
     * Finds the point that whose whose minimum distance is is maximally far from those already picked.
     */
    private int maxDistanceSimplexMaxMax(List<T> remaining_dat, List<Integer> ez_indices, List<Integer> exclude_indices) {

        double save = Double.MIN_VALUE;
        int result = -1;

        for( int i = 0; i < remaining_dat.size(); i++ ) {
            if( ! ez_indices.contains(i) && ! exclude_indices.contains(i) ) {                // we have chosen this point already
                double max_dist = maxDistance( remaining_dat.get(i),ez_indices);
                if( max_dist > save ) {
                    save = max_dist;
                    result = i;
                }
            }
        }
        System.out.println( "max dist = " + save);
        return result;

    }

    private double maxDistance(T point, List<Integer> ez_indices) {
        double max_dist = Double.MIN_VALUE;
        for( int index : ez_indices ) {
            double this_dist = distance_metric.distance(point,dat.get( index ));
            if( this_dist > max_dist ) {
                max_dist = this_dist;
            }
        }
        return max_dist;
    }

    private double sumDistances(T point, List<Integer> ez_indices) {

        double sum = 0;
        for( int index : ez_indices ) {
            sum += distance_metric.distance(point,dat.get(index));
        }
        return sum;
    }

    public ReverseOrderedList<Pair<Integer>, Double> getBestBF(int desiredNumber) {

        double query_distance_reduce_by = 0.999999;    // how much to reduce the distance by after failing lookups

        ReverseOrderedList<Pair<Integer>,Double> ordered_ez_indices_by_distance = new ReverseOrderedList(desiredNumber);  // twice as many as needed since list of pairs but with duplicate ezs in the pairs
        List<Integer> to_process = new ArrayList<>();

        PointDistance<Integer> furthest_pair = maxDistanceBF( dat, distance_metric );
        ordered_ez_indices_by_distance.add( new Pair<>( furthest_pair.point1, furthest_pair.point2 ),furthest_pair.distance );
        to_process.add(furthest_pair.point1);
        to_process.add(furthest_pair.point2);

        furthest_pair.distance = furthest_pair.distance;

        for( int i = 0; i < desiredNumber; i++ ) { // This loop is not really the best - not sure what it should be!

            if( to_process.size() == 0 ) {
                break;
            }
            int next = to_process.get(0); // take the first of to_process.
            double query_distance = furthest_pair.distance * query_distance_reduce_by;
            List<DataDistance<T>> query_results;

            do {
                query_results = bitblaster.rangeSearch(dat.get(next), query_distance);  // Use bitblaster to find the furthest points from the random point.
                query_distance = query_distance * query_distance_reduce_by;
            }
            while( query_results.size() == 0 ); // didn't get any results

            to_process.remove(0); // if we get some results remove that item

            // loop says - got more results from the search and either the results isn't full or the results are worse than those we have found already.
            for (int index = 0; index < query_results.size() && ( ordered_ez_indices_by_distance.getList().size() < desiredNumber || query_results.get(index).distance > getMinDistance(ordered_ez_indices_by_distance) ); index++) {

                int point = dat.indexOf(query_results.get(index).value);
                Pair<Integer> new_pair = new Pair<>(next, point);
                if( ! ordered_ez_indices_by_distance.getList().contains( new_pair ) ) {
                    ordered_ez_indices_by_distance.add(new_pair, query_results.get(index).distance); // OrderedList only keeps the best desiredNumber of entries.
                    to_process.add(point);
                }
            }
        }

        return ordered_ez_indices_by_distance;
    }

    private void show(ReverseOrderedList<Pair<Integer>, Double> ordered_ez_indices_by_distance) {
        System.out.println( "max = " + getMaxDistance( ordered_ez_indices_by_distance ) );
        System.out.println( "min = " + getMinDistance( ordered_ez_indices_by_distance ) );
        double mean = mean( ordered_ez_indices_by_distance.getComparators() );
        System.out.println( "mean = " + mean );
        System.out.println( "std_dev = " + stdDev( ordered_ez_indices_by_distance.getComparators(),mean ) );

    }


    private double getMinDistance(ReverseOrderedList<Pair<Integer>, Double> list) {
        List<Double> comparitors = list.getComparators();
        return comparitors.get( comparitors.size() - 1 ); //ReverseOrderedList - last is smallest
    }

    private double getMaxDistance(ReverseOrderedList<Pair<Integer>, Double> list) {
        List<Double> comparitors = list.getComparators();
        return comparitors.get( 0 ); //ReverseOrderedList - first is biggest
    }

    private double getMinDistance(OrderedList<Integer, Double> list) {
        List<Double> comparitors = list.getComparators();
        return comparitors.get( 0 ); //OrderedList - first is smallest
    }

    private double getMaxDistance(OrderedList<Integer, Double> list) {
        List<Double> comparitors = list.getComparators();
        return comparitors.get( comparitors.size() - 1 ); // OrderedList - last is biggest
    }


    /*
     * Heuristic for finding furthest pair.
     * Start with a random seed and find furthest from it.
     * Then find furthest from that point.
     * Keep doing this until we are bouncing between the same two points.
     */
    public PointDistance<Integer> maxDistance(List<T> data, Metric<T> metric, int seed ) {

        double greatest = Double.MIN_VALUE;

        int index1 = seed;
        int point = seed;
        int previous = -1;
        int previous_previous = -1;

        int furthest_from_point = -1;

        while ( index1 != previous_previous ) {

            // loop finds max distance from point at index1, and saves the other coord in furthest_from_point
            for (int index2 = 0; index2 < data.size(); index2++) {

                if (!data.get(index1).equals(data.get(index2))) {
                    double dist = metric.distance(data.get(index1), data.get(index2));
                    if (dist > greatest) {
                        greatest = dist;
                        point = index1;
                        furthest_from_point = index2;
                    }
                }
            }
            previous_previous = previous;
            previous = index1;
            index1 = furthest_from_point; // furthest2 is the furthest point from the point at index1, take this and look for more distant points
        }

        return new PointDistance( point, furthest_from_point, greatest );
    }


    double max_dist = Double.MIN_VALUE;
    double min_dist = Double.MAX_VALUE;

    double s0 = 0; //s0 = sum(1 for x in samples)
    double s1 = 0; //s1 = sum(x for x in samples)
    double ss2 = 0; //s2 = sum(x*x for x in samples)

    public PointDistance<Integer> maxDistanceBF(List<T> data, Metric<T> metric ) {

        int save_i = -1;
        int save_j = -1;

        for( int i = 0; i < data.size() - 1; i++ ) {

            for (int j = i + 1; j < data.size(); j++) {

                double dist = metric.distance(data.get(i), data.get(j));
                s0 += 1;
                s1 += dist;
                ss2 += dist * dist;

                if (dist > max_dist) {
                    max_dist = dist;
                    save_i = i;
                    save_j = j;
                }
                if (dist < min_dist) {
                    min_dist = dist;
                }

            }
        }
        
        System.out.println( "Max distance = " + max_dist );
        System.out.println( "Min distance = " + min_dist );
        System.out.println( "mean distance = " + s1 / s0 );
        System.out.println( "Std dev = " + stdDev( s0, s1, ss2 ) );
        
        return new PointDistance( save_i, save_j, max_dist );
    }

    // see https://stackoverflow.com/questions/5543651/computing-standard-deviation-in-a-stream
    // may be ill-conditioned if your samples are floating point numbers and the standard deviation is small compared to the mean - could be the case here
    private double stdDev(double s0, double s1, double s2) {
        return  Math.sqrt((s0 * s2 - s1 * s1)/(s0 * (s0 - 1)));
    }

    private double stdDev(List<Double> distances, double mean_dist) {
        double sq_dists = 0;

        for( double d : distances ) {
            sq_dists += Math.pow( d - mean_dist, 2 );
        }
        return Math.sqrt( sq_dists / distances.size() );
    }

    private double mean(List<Double> distances) {
        double total_dist = 0;
        for( double d : distances ) {
            total_dist += d;
        }
        return total_dist / distances.size();
    }
}
