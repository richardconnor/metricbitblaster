package uk.al_richard.metricbitblaster.experimental.alRichardLucia;

import coreConcepts.CountedMetric;
import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;
import testloads.TestContext;
import testloads.TestContext.Context;
import uk.al_richard.metricbitblaster.referenceImplementation.*;
import uk.al_richard.metricbitblaster.util.HammingDistance;
import uk.al_richard.metricbitblaster.util.HammingSimilarity;
import uk.al_richard.metricbitblaster.util.PointDistance;

import java.util.*;

/**
 * Attempts combine the column optimisation from:
 * 		MainRunnerFileFFHammingPivotCompetition
 * with the Matlab predictions from:
 * 		MainRunnerFileSelectOffsetsBasedOnMatLabPredictions
 */

public class MainRunnerFileApproxFFSimplexSigmaJustSelected {

	protected static double[] ball_radii =  new double[] { 0.94, 1.535 }; // from Matlab for Euc20
	protected static double[] sheet_offsets =  new double[] { 0.3,-0.3 };
	private static int nChoose2;
	private static Random rand = new Random();

	public static void main(String[] args) throws Exception {

		Context context = Context.euc20;

		boolean fourPoint = true;
		boolean balanced = false;
		int noOfSampleRefPoints = 800;
		int num_refs = 40;

		TestContext tc = new TestContext(context);
		int querySize = (context == Context.colors || context == Context.nasa) ? tc.dataSize() / 10 : 1000;
		tc.setSizes(querySize, noOfSampleRefPoints);
		List<CartesianPoint> dat = tc.getData();
		List<CartesianPoint> queries = tc.getQueries();
		Metric<CartesianPoint> metric = tc.metric();

		List<CartesianPoint> refs = tc.getRefPoints();

		double threshold = tc.getThresholds()[0];

		runExperiment(context, fourPoint, balanced, queries, metric, querySize, dat, refs.subList(0,num_refs), threshold);

		//--------------------

		choosePivots(context, fourPoint, balanced, queries, metric, querySize, dat, num_refs, noOfSampleRefPoints, refs, threshold);
	}


	private static void runExperiment(Context context, boolean fourPoint, boolean balanced, List<CartesianPoint> queries, Metric<CartesianPoint> metric, int querySize, List<CartesianPoint> dat, List<CartesianPoint> refs, double threshold) {
		int num_refs = refs.size();
		nChoose2 = ((num_refs - 1) * num_refs) / 2;
		int noOfBitSets = ( sheet_offsets.length * nChoose2 ) + (num_refs * ball_radii.length);

		System.out.println("----------- Running experiment ------------" );
		System.out.println("codebase\t" + "findPivots" );
		System.out.println("context\t" + context);
		System.out.println("data size\t" + dat.size());
		System.out.println("query size\t" + querySize);
		System.out.println("threshold\t" + threshold);
		System.out.println("sample refs size\t" + num_refs);
		System.out.println("sample no of bitsets\t" + noOfBitSets);

		RefPointSet<CartesianPoint> rps = new RefPointSet<>(refs, metric);

		List<ExclusionZone<CartesianPoint>> ezs = new ArrayList<>();

		addSheetExclusions( dat, refs, rps, ezs, fourPoint,sheet_offsets );
		System.out.println( "added sheets " + ezs.size() );
		addBallExclusions( dat, refs, rps, ezs );
		System.out.println( "added balls " + ezs.size() );

		System.out.println("Sample ezs size:\t" + ezs.size());

		BitSet[] datarep = new BitSet[noOfBitSets];
		buildBitSetData( dat, datarep, rps, ezs );

		// Run the queries with a random set of EZs
		queryBitSetData( queries, dat, metric, threshold, datarep, rps, ezs, 70 );

	}

	private static void runExperimentSpecial(Context context, boolean fourPoint, boolean balanced, List<CartesianPoint> queries, Metric<CartesianPoint> metric, int querySize, List<CartesianPoint> dat, List<Pair<CartesianPoint>> sheets, double threshold) {
		List<CartesianPoint> refs = getROsFromSheets( sheets );
		int num_refs = refs.size();

		int noOfBitSets = ( 2 * sheets.size() ) + (num_refs * ball_radii.length);

		System.out.println("----------- Running Supplied Sheets experiment ------------" );
		System.out.println("codebase\t" + "MainRunnerFileApproxFFSimplexMaxMax" );
		System.out.println("context\t" + context);
		System.out.println("data size\t" + dat.size());
		System.out.println("query size\t" + querySize);
		System.out.println("threshold\t" + threshold);
		System.out.println("sample refs size\t" + num_refs);
		System.out.println("sample no of bitsets\t" + noOfBitSets);

		RefPointSet<CartesianPoint> rps = new RefPointSet<>(refs, metric);

		List<ExclusionZone<CartesianPoint>> ezs = new ArrayList<>();

		addExtentSheets( dat, refs, rps, ezs, fourPoint,sheet_offsets, sheets );
		System.out.println( "added sheets " + ezs.size() );
		addBallExclusions( dat, refs, rps, ezs );
		System.out.println( "added balls " + ezs.size() );

		System.out.println("Ezs size:\t" + ezs.size());

		BitSet[] datarep = new BitSet[noOfBitSets];
		buildBitSetData( dat, datarep, rps, ezs );

		// Run the queries with a random set of EZs
		queryBitSetData( queries, dat, metric, threshold, datarep, rps, ezs, 70 );
	}

	private static void addExtentSheets(List<CartesianPoint> dat, List<CartesianPoint> refs, RefPointSet<CartesianPoint> rps, List<ExclusionZone<CartesianPoint>> ezs, boolean fourPoint, double[] sheet_offsets, List<Pair<CartesianPoint>> sheets) {
		for( Pair<CartesianPoint> sheet : sheets ) {
				for( int z = 0; z < sheet_offsets.length; z++ ) {
					SheetExclusion<CartesianPoint> se;
					if( fourPoint ) {
						se = new SheetExclusion4p<>(rps, refs.indexOf(sheet.getFirst()), refs.indexOf(sheet.getSecond()), sheet_offsets[z]);
					} else {
						se = new SheetExclusion3p<>(rps, refs.indexOf(sheet.getFirst()), refs.indexOf(sheet.getSecond()), sheet_offsets[z]);
					}
					ezs.add(se);
				}
		}
	}

	private static List<CartesianPoint> getROsFromSheets(List<Pair<CartesianPoint>> sheets) {
		List<CartesianPoint> ros = new ArrayList<>();
		for( Pair<CartesianPoint> sheet : sheets ) {
			ros.add( sheet.getFirst() );
			ros.add( sheet.getSecond() );
		}
		return ros;
	}




	private static void choosePivots(Context context, boolean fourPoint, boolean balanced, List<CartesianPoint> queries, Metric<CartesianPoint> metric, int querySize, List<CartesianPoint> dat, int num_refs, int noOfSampleRefPoints, List<CartesianPoint> refs, double threshold) {

		nChoose2 = ((noOfSampleRefPoints - 1) * noOfSampleRefPoints) / 2;
		int noOfBitSets = nChoose2 ;

		List<CartesianPoint> sample_dat = dat.subList(0, 1000);

		System.out.println("----------- Picking pivots ------------" );
		System.out.println("context\t" + context);
		System.out.println("data size\t" + dat.size());
		System.out.println("sample data size\t" + sample_dat.size());
		System.out.println("query size\t" + querySize);
		System.out.println("threshold\t" + threshold);
		System.out.println("sample refs size\t" + refs.size());
		System.out.println("sample no of bitsets\t" + noOfBitSets);

		RefPointSet<CartesianPoint> rps = new RefPointSet<>(refs, metric);

		List<ExclusionZone<CartesianPoint>> ezs = new ArrayList<>();

		addSheetExclusions(sample_dat, refs, rps, ezs, fourPoint, sheet_offsets);
		System.out.println( "added sheets " + ezs.size() );
//		exploreRegions(sample_dat, refs, rps, ezs );
//		System.out.println( "added balls " + ezs.size() );

		System.out.println("Sample ezs size:\t" + ezs.size());

		BitSet[] datarep = new BitSet[noOfBitSets];
		buildBitSetData( sample_dat, datarep, rps, ezs );

		List<Pair<CartesianPoint>> new_sheets = selectBestEZs(ezs, datarep, 80 ); // 180); <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

		runExperimentSpecial(context, fourPoint, balanced, queries, metric, querySize, dat, new_sheets, threshold ); // do it again with chosen pivots

	}

	private static List<Pair<CartesianPoint>> selectBestEZs(List<ExclusionZone<CartesianPoint>> ezs, BitSet[] datarep, int desiredNumber_of_Ezs ) {

		List<BitSet> dat = new ArrayList<>();
		for( int i = 0; i < datarep.length; i++ ) {
			dat.add(datarep[i]);
		}

		System.out.println( "Building FFT MinMax" );
		FFTBB fftbb = new FFTBB( dat, new HammingDistance(), new HammingSimilarity() );
		System.out.println( "Querying looking for " + desiredNumber_of_Ezs + " EZS " );
		List<Pair<CartesianPoint>>  new_ezs = new ArrayList<>();
		fftbb.getBestHeuristicSimplexSigma( desiredNumber_of_Ezs,ezs,new_ezs ); // look for half the number of ezs since we need (desiredNumber_of_Ezs / 2 pivots ) then add some for duplicates.
		System.out.println( "Found " + new_ezs.size() + " sheets / " + desiredNumber_of_Ezs );

		return new_ezs;
	}

	/**
	 * @return a new list of refs
	 */
	protected static  List<CartesianPoint> selectRefs(List<ExclusionZone<CartesianPoint>> ezs, ReverseOrderedList best_ezs, int desiredNumber) {

		List<CartesianPoint> new_refs = new ArrayList<>();

		List<Pair<Integer>> pairs = best_ezs.getList(); // List of pair of EZ indices
		List<Double> distances = best_ezs.getComparators();     // List of distances that EZ indices are apart

		for( int index = 0; new_refs.size() < desiredNumber && index < pairs.size(); index++ ) {

			Pair<Integer> pair = pairs.get(index);
			double distance = distances.get(index);

			System.out.println( "*** Distance: " + distance + " " );

			selectEZ(ezs, new_refs, pair.getFirst() );
			selectEZ(ezs, new_refs, pair.getSecond() );
		}

		return new_refs;
	}

	static void selectEZ(List<ExclusionZone<CartesianPoint>> ezs, List<CartesianPoint> new_refs, int ez_index) {
		ExclusionZone<CartesianPoint> next_ez = ezs.get(ez_index);
		if (next_ez instanceof BallExclusion) {
			selectBall( new_refs, next_ez,ez_index );
		}
		else if (next_ez instanceof SheetExclusion) {

			selectSheet(new_refs, (SheetExclusion<CartesianPoint>) next_ez, ez_index);
		}
	}

	private static void selectSheet(List<CartesianPoint> new_refs, SheetExclusion<CartesianPoint> next_ez, int ez_index) {
		SheetExclusion<CartesianPoint> sheet = next_ez;
		CartesianPoint next_ref1 = sheet.getPointSet().refs.get(sheet.getRef1());
		CartesianPoint next_ref2 = sheet.getPointSet().refs.get(sheet.getRef2());

		System.out.println( "\tSheet: " + ez_index + " " );

		if ( ! new_refs.contains(next_ref1) ) {
			System.out.println( "\tro1 added: " + sheet.getRef1() );
			new_refs.add(next_ref1);
		} else {
			System.out.println( "\tro1 already in set: " + sheet.getRef1());
		}

		if ( ! new_refs.contains(next_ref2) ) {
			System.out.println( "\tro2 added: " + sheet.getRef2());
			new_refs.add(next_ref2);
		} else {
			System.out.println( "\tro2 already in set: " + sheet.getRef2());
		}
	}

	public static void selectBall(List<CartesianPoint> new_refs,ExclusionZone<CartesianPoint> next_ez, int ez_index  )  {

		BallExclusion<CartesianPoint> ball = (BallExclusion<CartesianPoint>) next_ez;
		CartesianPoint next_ref = ball.getPointSet().refs.get(ball.getIndex());
		System.out.print("\tBall: " + ez_index + " ");

		if (!new_refs.contains(next_ref)) {
			System.out.println("\tro added: " + ball.getIndex());
			new_refs.add(next_ref);
		} else {
			System.out.println("\tro already in set: " + ball.getIndex());
		}
	}

	/*
	 * Copied from Richard's util package and class Util_ISpaper
	 */
	public static <T> List<Integer> getFFT(List<T> data, Metric<T> metric, int noOfPoints) {
		if (noOfPoints >= data.size()) {
			throw new RuntimeException( "Size of required > data.size()");
			// was return data;
		} else {
			List<Integer> pivotSet = new ArrayList<>(); // was List<T>
			pivotSet.add( rand.nextInt( data.size() ) ); // was  data.get(rand.nextInt(data.size())));
			while (pivotSet.size() < noOfPoints) {
				double max = 0;
				int furthest = -1;
				for (int i : new Range(0, data.size())) {
					T d = data.get(i);
					if (!pivotSet.contains(d)) {
						double min = Double.MAX_VALUE;
						for (int n : pivotSet) {					// was T n : pivotSet
							double dist = metric.distance(d, data.get(n) );	// was metric.distance(d, n);
							if (dist < min) {
								min = dist;
							}
						}
						if (min >= max) {
							max = min;
							furthest = i;
						}
					}
				}
				pivotSet.add( furthest ); // was data.get(furthest));
			}
			assert pivotSet.size() == noOfPoints : "required:  " + noOfPoints
					+ "; got " + pivotSet.size();
			return pivotSet;
		}

	}


	/**
	 *
	 * @param data - the dataset from which to choose the furthest points
	 * @param metric - the metric being used to measure distance
	 * @return - required datapoints that are the furthest apart from each other
	 */
	public static  <T> PointDistance<Integer> maxDistance(List<T> data, Metric<T> metric ) {

		double greatest_span = Double.MIN_VALUE;

		int index1 = 0;
		T point1 = data.get(index1);

		int previous_index = -1;
		int previous_previous_index = -1;

		int furthest_index_1 = -1;
		int furthest_index_2 = -1;

		while ( index1 != previous_previous_index ) {

			for (int i = 0; i < data.size(); i++) {
				int index2 = i;
				T point2 = data.get(index2);
				if (!point1.equals(point2)) {
					double dist = metric.distance(point1, point2);
					if (dist > greatest_span) {
						greatest_span = dist;
						furthest_index_1 = index1;
						furthest_index_2 = index2;
					}
				}
			}
			previous_previous_index = previous_index;
			previous_index = index1;
			index1 = furthest_index_2;
			point1 = data.get(index1);
		}

		return new PointDistance( furthest_index_1, furthest_index_2, greatest_span );
	}



	public static void addBallExclusions(List<CartesianPoint> dat, List<CartesianPoint> refs,
										 RefPointSet<CartesianPoint> rps, List<ExclusionZone<CartesianPoint>> ezs) {
		for (int i = 0; i < refs.size(); i++) {
			List<BallExclusion<CartesianPoint>> balls = new ArrayList<>();
			for (double radius : ball_radii) {
				BallExclusion<CartesianPoint> be = new BallExclusion<>(rps, i, radius);
				balls.add(be);
			}
			ezs.addAll(balls);
		}
	}

	public static void addSheetExclusions(List<CartesianPoint> dat,
										  List<CartesianPoint> refs, RefPointSet<CartesianPoint> rps,
										  List<ExclusionZone<CartesianPoint>> ezs, boolean fourPoint, double[] sheet_offsets) {

		for (int i = 0; i < refs.size() - 1; i++) {
			for (int j = i + 1; j < refs.size(); j++) {
				for( int z = 0; z < sheet_offsets.length; z++ ) {
					SheetExclusion<CartesianPoint> se;
					if( fourPoint ) {
						se = new SheetExclusion4p<CartesianPoint>(rps, i, j,sheet_offsets[z]);
					} else {
						se = new SheetExclusion3p<CartesianPoint>(rps, i, j,sheet_offsets[z]);
					}
					ezs.add(se);
				}
			}
		}
	}

	public static <T> void buildBitSetData(List<T> data, BitSet[] datarep,
										   RefPointSet<T> rps, List<ExclusionZone<T>> ezs) {
		int dataSize = data.size();
		for (int i = 0; i < datarep.length; i++) {
			datarep[i] = new BitSet(dataSize);
		}
		for (int n = 0; n < dataSize; n++) {
			T p = data.get(n);
			double[] dists = rps.extDists(p);
			for (int x = 0; x < datarep.length; x++) {
				boolean isIn = ezs.get(x).isIn(dists);
				if (isIn) {
					datarep[x].set(n);
				}
			}
		}
	}

	protected static <T> BitSet doExclusions(List<T> dat, double t,
			BitSet[] datarep, CountedMetric<T> cm, T q, final int dataSize,
			List<Integer> mustBeIn, List<Integer> cantBeIn) {
		if (mustBeIn.size() != 0) {
			BitSet ands = getAndBitSets(datarep, dataSize, mustBeIn);
			if (cantBeIn.size() != 0) {
				/*
				 * hopefully the normal situation or we're in trouble!
				 */
				BitSet nots = getOrBitSets(datarep, dataSize, cantBeIn);
				nots.flip(0, dataSize);
				ands.and(nots);
				return ands;
				// filterContenders(dat, t, cm, q, res, dataSize, ands);
			} else {
				// there are no cantBeIn partitions
				return ands;
				// filterContenders(dat, t, cm, q, res, dataSize, ands);
			}
		} else {
			// there are no mustBeIn partitions
			if (cantBeIn.size() != 0) {
				BitSet nots = getOrBitSets(datarep, dataSize, cantBeIn);
				nots.flip(0, dataSize);
				return nots;
				// filterContenders(dat, t, cm, q, res, dataSize, nots);
			} else {
				// there are no exclusions at all...
				return null;
				// for (T d : dat) {
				// if (cm.distance(q, d) < t) {
				// res.add(d);
				// }
				// }
			}
		}
	}

	public static <T> void filterContenders(List<T> dat, double t,
											CountedMetric<T> cm, T q, Collection<T> res, final int dataSize,
											BitSet results) {

		// System.out.println( "Contenders size = " + results.cardinality());

		for (int i = results.nextSetBit(0); i != -1 && i < dataSize; i = results.nextSetBit(i + 1)) {
			// added i < dataSize since Cuda code overruns datasize in the -
			// don't know case - must be conservative and include, easier than
			// complex bit masking and shifting.
			if (results.get(i)) {
				if (cm.distance(q, dat.get(i)) <= t) {
					res.add(dat.get(i));
				}
			}
		}
	}

	@SuppressWarnings("boxing")
	static BitSet getAndBitSets(BitSet[] datarep,
			final int dataSize, List<Integer> mustBeIn) {
		BitSet ands = null;
		if (mustBeIn.size() != 0) {
			ands = datarep[mustBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < mustBeIn.size(); i++) {
				ands.and(datarep[mustBeIn.get(i)]);
			}
		}
		return ands;
	}

	@SuppressWarnings("boxing")
	static BitSet getOrBitSets(BitSet[] datarep,
			final int dataSize, List<Integer> cantBeIn) {
		BitSet nots = null;
		if (cantBeIn.size() != 0) {
			nots = datarep[cantBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < cantBeIn.size(); i++) {
				final BitSet nextNot = datarep[cantBeIn.get(i)];
				nots.or(nextNot);
			}
		}
		return nots;
	}

	@SuppressWarnings("boxing")
	public static <T> void queryBitSetData(List<T> queries, List<T> dat,
										   Metric<T> metric, double threshold, BitSet[] datarep,
										   RefPointSet<T> rps, List<ExclusionZone<T>> ezs, int noOfRefPoints) {

		int noOfResults = 0;
		int partitionsExcluded = 0;
		CountedMetric<T> cm = new CountedMetric<>(metric);
		long t0 = System.currentTimeMillis();
		for (T q : queries) {
			List<T> res = new ArrayList<>();
			double[] dists = rps.extDists(q, res, threshold);

			List<Integer> mustBeIn = new ArrayList<>();
			List<Integer> cantBeIn = new ArrayList<>();

			for (int i = 0; i < ezs.size(); i++) {
				ExclusionZone<T> ez = ezs.get(i);
				if (ez.mustBeIn( dists, threshold)) {
					mustBeIn.add(i);
				} else if (ez.mustBeOut( dists, threshold)) {
					cantBeIn.add(i);
				}
			}

			partitionsExcluded += cantBeIn.size() + mustBeIn.size();

			BitSet inclusions = doExclusions(dat, threshold, datarep, cm,
					q, dat.size(), mustBeIn, cantBeIn);

			// System.out.println("Inclusions = " + inclusions.size());

			if (inclusions == null) {
				for (T d : dat) {
					if (cm.distance(q, d) <= threshold) {
						res.add(d);
					}
				}
			} else {
				filterContenders(dat, threshold, cm, q, res, dat.size(),
						inclusions);
			}

			noOfResults += res.size();
			// System.out.println( "results = " + res.size() );
		}

		System.out.println("bitsets");
		System.out.println("dists per query\t"
				+ (cm.reset() / queries.size() + noOfRefPoints));
		System.out.println("results\t" + noOfResults);
		System.out.println("time\t" + (System.currentTimeMillis() - t0)
				/ (float) queries.size());
		System.out.println("partitions excluded\t"
				+ ((double) partitionsExcluded / queries.size()));
	}
}
