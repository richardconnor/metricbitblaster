package uk.al_richard.metricbitblaster.experimental.alRichardLucia.vectorSpace;

import coreConcepts.CountedMetric;
import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;
import testloads.TestContext;
import testloads.TestContext.Context;
import uk.al_richard.metricbitblaster.experimental.alRichardLucia.Pair;
import uk.al_richard.metricbitblaster.referenceImplementation.BallExclusion;
import uk.al_richard.metricbitblaster.referenceImplementation.ExclusionZone;
import uk.al_richard.metricbitblaster.referenceImplementation.RefPointSet;
import uk.al_richard.metricbitblaster.referenceImplementation.SheetExclusion4p;

import java.util.*;

/**
 * Idea is to transform from metric space into vector space.
 * Create orthogonal basis in vector space
 * Do queries in vector space.
 */

public class MainRunnerFileQueryInTransformedSpace {

	protected static double[] ball_radii =  new double[] { 0.94, 1.535 }; // from Matlab for Euc20
	protected static double[] sheet_offsets =  new double[] { 0.3,-0.3 };
	private static int nChoose2;
	private static Random rand = new Random();
	private static List<Double> inter_pivot_distances = new ArrayList<>();

	final static int projected_dimensions = 20;   	// this is the dimension of the vector space into which we are projecting

	public static void main(String[] args) throws Exception {

		Context context = Context.euc20;

		int ref_count = 60;

		TestContext tc = new TestContext(context);
		int querySize = (context == Context.colors || context == Context.nasa) ? tc.dataSize() / 10 : 1000;
		tc.setSizes(querySize, ref_count);
		List<CartesianPoint> dat = tc.getData().subList(0,10000); //<<<<<<<<<<<<<<<<<<<<<<<<<<< made the data set a bit smaller!!
		List<CartesianPoint> queries = tc.getQueries();
		Metric<CartesianPoint> metric = tc.metric();

		List<CartesianPoint> refs = tc.getRefPoints();

		double threshold = tc.getThresholds()[0];

		runExperiment(context, queries, metric, querySize, dat, refs, threshold, true, true );

		changeCoordinateSystems(context, queries, metric, querySize, dat, threshold  );
	}


	private static void runExperiment(Context context, List<CartesianPoint> queries, Metric<CartesianPoint> metric, int querySize, List<CartesianPoint> dat, List<CartesianPoint> refs, double threshold, boolean balls, boolean sheets ) {
		int num_refs = refs.size();
		int noOfBitSets = 0;

		nChoose2 = ((num_refs - 1) * num_refs) / 2;
		if( ! sheets && ! balls ) {
			throw new RuntimeException( "One of sheets and balls must be true" );
		}
		if( sheets ) {
			noOfBitSets = ( 2 * sheet_offsets.length * nChoose2 );	// <<<<<<<<<<<<<<<<<<<<<<<<<<< 2 *
		}
		if( balls ) {
			noOfBitSets = noOfBitSets + (num_refs * ball_radii.length );
		}

		System.out.println("----------- Running experiment ------------" );
		System.out.println("balls\t" + balls );
		System.out.println("sheets\t" + sheets );
		System.out.println("codebase\t" + "MainRunnerFileQueryInTransformedSpace" );
		System.out.println("context\t" + context);
		System.out.println("data size\t" + dat.size());
		System.out.println("query size\t" + querySize);
		System.out.println("threshold\t" + threshold);
		System.out.println("sample refs size\t" + num_refs);
		System.out.println("sample no of bitsets\t" + noOfBitSets);

		RefPointSet<CartesianPoint> rps = new RefPointSet<>(refs, metric);

		List<ExclusionZone<CartesianPoint>> ezs = new ArrayList<>();

		if( sheets ) {
			addSheetExclusions(dat, refs, rps, ezs, sheet_offsets);
			System.out.println( "added sheets " + ezs.size() );
			printInterPivotDistances();
		}
		if( balls ) {
			addBallExclusions(dat, refs, rps, ezs, ball_radii);
			System.out.println("added balls " + ezs.size());
		}

		System.out.println("Sample ezs size:\t" + ezs.size());

		BitSet[] datarep = new BitSet[noOfBitSets];
		buildBitSetData( dat, datarep, rps, ezs );

		queryBitSetData( queries, dat, metric, threshold, datarep, rps, ezs, 70, threshold );
	}

	private static void printInterPivotDistances() {
		System.out.println( "Max inter pivot distance: " + Collections.max(inter_pivot_distances) );
		System.out.println( "Min inter pivot distance: " + Collections.min(inter_pivot_distances) );
		System.out.println( "Average inter pivot distance: " + average( inter_pivot_distances ) );

	}

	private static double average(List<Double> inter_pivot_distances) {
		double total = 0;
		for( double ipd : inter_pivot_distances ) {
			total = total + ipd;
		}
		return total / inter_pivot_distances.size();
	}

	private static void runExperimentSpecial(Context context, List<CartesianPoint> queries, Orthogonal<CartesianPoint> orthog, Metric<CartesianPoint> metric, int querySize, List<CartesianPoint> dat, List<Pair<CartesianPoint>> sheets, List<CartesianPoint> ball_pivots, double transformed_threshold, int original_dimension) {

		int noOfBitSets = ( sheets.size() * sheet_offsets.length ) + (ball_pivots.size() * ball_radii.length);

		System.out.println("----------- Running JUST Supplied Sheets experiment ------------" );
		System.out.println("codebase\t" + "MainRunnerFileQueryInTransformedSpace" );
		System.out.println("context\t" + context);
		System.out.println("data size\t" + dat.size());
		System.out.println("query size\t" + querySize);
		System.out.println("threshold\t" + transformed_threshold);
		System.out.println("num sheets\t" + sheets.size());
		System.out.println("num balls\t" + ball_pivots.size());
		System.out.println("sample no of bitsets\t" + noOfBitSets);

		List<CartesianPoint> refs = new ArrayList<>();
		for( Pair<CartesianPoint> pair : sheets ) {
			refs.add( pair.getFirst() );
			refs.add( pair.getSecond() );
		}
		for( CartesianPoint point : ball_pivots ) {
			refs.add( point );
		}

		System.out.println("refs size\t" + refs.size());

		RefPointSet<CartesianPoint> rps = new RefPointSet<>(refs, metric);

		List<ExclusionZone<CartesianPoint>> ezs = new ArrayList<>();

		double[] transformed_sheet_offsets = MapTransform( sheet_offsets, metric, ball_pivots.get(0).getPoint().length);
		addExtentSheets( dat, refs, rps, ezs, transformed_sheet_offsets, sheets );
		int sheet_count = ezs.size();
		System.out.println( "added sheets " + ezs.size() );
		double[] transformed_radii = MapTransform( ball_radii, metric, original_dimension  );
		addBallExclusions( dat, ball_pivots, rps, ezs, transformed_radii );
		System.out.println( "added balls " + ( ezs.size() - sheet_count ) );

		System.out.println("Ezs size:\t" + ezs.size());

		BitSet[] datarep = new BitSet[noOfBitSets];
		buildBitSetData( dat, datarep, rps, ezs );

		// Run the queries with a random set of EZs
		queryBitSetData( transform( queries,orthog ), dat, metric, transformed_threshold, datarep, rps, ezs, 70, transformed_threshold);
	}

	private static List<CartesianPoint> transform(List<CartesianPoint> queries, Orthogonal<CartesianPoint> orthog ) {
		List<CartesianPoint> result = new ArrayList<>();
		for( CartesianPoint query : queries ) {
			result.add( new CartesianPoint( orthog.createApex( query ) ) );
		}
		return result;
	}

	private static double[] MapTransform(double[] distances, Metric<CartesianPoint> metric, int original_dimension) {
		double[] result = new double[ distances.length ];
		for( int i = 0 ; i < distances.length ; i++ ) {

			CartesianPoint zero = new CartesianPoint(new double[original_dimension] );

			result[i] = metric.distance( zero, pointDistanceFromZero( distances[i],original_dimension ) );
		}
		return result;
	}

	private static CartesianPoint pointDistanceFromZero(double distance, int original_dimension) {
		double[] ds = new double[original_dimension]; // all zeros
		ds[original_dimension-1] = distance;
		return  new CartesianPoint( ds );
	}

	private static void show(CartesianPoint point) {
		double[] ds = point.getPoint();
		System.out.print( "coord: " );
		for( double d : ds ) {
			System.out.print( d + " " );
		}
		System.out.println( );
	}

	private static void addExtentSheets(List<CartesianPoint> dat, List<CartesianPoint> refs, RefPointSet<CartesianPoint> rps, List<ExclusionZone<CartesianPoint>> ezs, double[] sheet_offsets, List<Pair<CartesianPoint>> sheets) {
		for( Pair<CartesianPoint> sheet : sheets ) {
				for( int z = 0; z < sheet_offsets.length; z++ ) {
					SheetExclusion4p<CartesianPoint> se = new SheetExclusion4p<CartesianPoint>(rps, refs.indexOf(sheet.getFirst()), refs.indexOf(sheet.getSecond()), sheet_offsets[z]);
					ezs.add(se);
				}
		}
	}


	private static void changeCoordinateSystems(Context context, List<CartesianPoint> queries, Metric<CartesianPoint> metric, int querySize, List<CartesianPoint> dat, double threshold ) throws Exception {

		TestContext tc = new TestContext(context);

		int num_queries_not_used = 10;

		tc.setSizes(num_queries_not_used, projected_dimensions);

		List<Pair<CartesianPoint>> new_sheets = createBasis( projected_dimensions );

		List<CartesianPoint> reference_points_in_original_space = tc.getRefPoints();

		Orthogonal<CartesianPoint> orthog = new Orthogonal<>(new Euclidean(), reference_points_in_original_space);

		Metric<CartesianPoint> transformed_metric = new TransformedMetric( orthog, metric );

		List<CartesianPoint> ball_pivots = transformPivots( orthog, reference_points_in_original_space );

		List<CartesianPoint>  refs = new ArrayList<>();
		for( Pair<CartesianPoint> sheet : new_sheets ) {
			refs.add( sheet.getFirst() );
			refs.add( sheet.getSecond() );
		}
		refs.addAll(ball_pivots);


		int original_dimension = ball_pivots.get(0).getPoint().length;
		CartesianPoint zero = new CartesianPoint(new double[original_dimension] );
		double transformed_threshold = metric.distance( zero, pointDistanceFromZero(threshold, original_dimension));
		System.out.println("transformed threshold\t" + transformed_threshold);

		runExperimentSpecial(context, queries, orthog, transformed_metric, querySize, dat, new_sheets, ball_pivots, transformed_threshold, original_dimension); // do it again with just the chosen pivots
		runExperiment(context, queries, transformed_metric, querySize, dat, refs, transformed_threshold, true, true );
	}

	private static List<CartesianPoint> transformPivots(Orthogonal<CartesianPoint> orthog, List<CartesianPoint> reference_points_in_original_space) {
		List<CartesianPoint> result = new ArrayList<>();
		for( CartesianPoint point : reference_points_in_original_space ) {

			double[] d1 = orthog.createApex(point);
			CartesianPoint p1 = new CartesianPoint( d1 );

			result.add( p1 );
		}
		return result;
	}

	private static List<Pair<CartesianPoint>> createBasis(int projected_dimensions) {
		List<Pair<CartesianPoint>> result = new ArrayList<>();

		CartesianPoint origin = new CartesianPoint( new double[projected_dimensions] ); // all zeros

		for( int i = 0; i < projected_dimensions; i++ ) {

			double[] ds = new double[projected_dimensions]; // all zeros
			ds[i] = 1;	                                    // apart from this 1
			CartesianPoint p1 = new CartesianPoint( ds );

			result.add( new Pair<>( origin,p1 ) );
		}
		return result;
	}


	static void selectEZ(List<ExclusionZone<CartesianPoint>> ezs, List<CartesianPoint> new_refs, int ez_index) {
		ExclusionZone<CartesianPoint> next_ez = ezs.get(ez_index);
		if (next_ez instanceof BallExclusion) {
			selectBall( new_refs, next_ez,ez_index );
		}
		else if (next_ez instanceof SheetExclusion4p) {

			selectSheet(new_refs, (SheetExclusion4p<CartesianPoint>) next_ez, ez_index);
		}
	}

	private static void selectSheet(List<CartesianPoint> new_refs, SheetExclusion4p<CartesianPoint> next_ez, int ez_index) {
		SheetExclusion4p<CartesianPoint> sheet = next_ez;
		CartesianPoint next_ref1 = sheet.getPointSet().refs.get(sheet.getRef1());
		CartesianPoint next_ref2 = sheet.getPointSet().refs.get(sheet.getRef2());

		System.out.println( "\tSheet: " + ez_index + " " );

		if ( ! new_refs.contains(next_ref1) ) {
			System.out.println( "\tro1 added: " + sheet.getRef1() );
			new_refs.add(next_ref1);
		} else {
			System.out.println( "\tro1 already in set: " + sheet.getRef1());
		}

		if ( ! new_refs.contains(next_ref2) ) {
			System.out.println( "\tro2 added: " + sheet.getRef2());
			new_refs.add(next_ref2);
		} else {
			System.out.println( "\tro2 already in set: " + sheet.getRef2());
		}
	}

	public static void selectBall(List<CartesianPoint> new_refs,ExclusionZone<CartesianPoint> next_ez, int ez_index  )  {

		BallExclusion<CartesianPoint> ball = (BallExclusion<CartesianPoint>) next_ez;
		CartesianPoint next_ref = ball.getPointSet().refs.get(ball.getIndex());
		System.out.print("\tBall: " + ez_index + " ");

		if (!new_refs.contains(next_ref)) {
			System.out.println("\tro added: " + ball.getIndex());
			new_refs.add(next_ref);
		} else {
			System.out.println("\tro already in set: " + ball.getIndex());
		}
	}



	public static void addBallExclusions(List<CartesianPoint> dat, List<CartesianPoint> ball_refs,
										 RefPointSet<CartesianPoint> rps, List<ExclusionZone<CartesianPoint>> ezs, double[] ball_radii) {
		for (int i = 0; i < ball_refs.size(); i++) {
			List<BallExclusion<CartesianPoint>> balls = new ArrayList<>();
			for (double radius : ball_radii) {
				BallExclusion<CartesianPoint> be = new BallExclusion<>(rps, i, radius);
				balls.add(be);
			}
			ezs.addAll(balls);
		}
	}

	public static void addSheetExclusions(List<CartesianPoint> dat,
										  List<CartesianPoint> refs, RefPointSet<CartesianPoint> rps,
										  List<ExclusionZone<CartesianPoint>> ezs, double[] sheet_offsets) {

		for (int i = 0; i < refs.size() - 1; i++) {
			for (int j = i + 1; j < refs.size(); j++) {
				for( int z = 0; z < sheet_offsets.length; z++ ) {
					SheetExclusion4p<CartesianPoint> se = new SheetExclusion4p<CartesianPoint>(rps, i, j,sheet_offsets[z]);
					SheetExclusion4p<CartesianPoint> se2 = new SheetExclusion4p<CartesianPoint>(rps, j, i,sheet_offsets[z]); //<<<<<<<<<<<<<<<<<<<<<<<
					double inter_pivot_distance = se.getInterPivotDistance();
					if( inter_pivot_distance == 0 ) {
						System.out.println( "Zero IPD") ;
						System.out.println( "ref1: " + se.getRef1() );
						System.out.println( "ref2: " + se.getRef2() );
					}
					inter_pivot_distances.add( inter_pivot_distance );
					ezs.add(se);
					ezs.add(se2);																					//<<<<<<<<<<<<<<<<<<<<<<<
				}
			}
		}
	}

	public static <T> void buildBitSetData(List<T> data, BitSet[] datarep,
										   RefPointSet<T> rps, List<ExclusionZone<T>> ezs) {
		int dataSize = data.size();
		for (int i = 0; i < datarep.length; i++) {
			datarep[i] = new BitSet(dataSize);
		}
		for (int n = 0; n < dataSize; n++) {
			T p = data.get(n);
			double[] dists = rps.extDists(p);
			for (int x = 0; x < datarep.length; x++) {
				try {
					boolean isIn = ezs.get(x).isIn(dists);
					if (isIn) {
						datarep[x].set(n);
					}
				} catch(  RuntimeException e ) {
					// AL ***0986***
					// eat the runtime exception that is thrown if we get a nan
					// can't make a judgement.
				}
			}
		}
	}

	protected static <T> BitSet doExclusions(List<T> dat, double t,
			BitSet[] datarep, CountedMetric<T> cm, T q, final int dataSize,
			List<Integer> mustBeIn, List<Integer> cantBeIn) {
		if (mustBeIn.size() != 0) {
			BitSet ands = getAndBitSets(datarep, dataSize, mustBeIn);
			if (cantBeIn.size() != 0) {
				/*
				 * hopefully the normal situation or we're in trouble!
				 */
				BitSet nots = getOrBitSets(datarep, dataSize, cantBeIn);
				nots.flip(0, dataSize);
				ands.and(nots);
				return ands;
				// filterContenders(dat, t, cm, q, res, dataSize, ands);
			} else {
				// there are no cantBeIn partitions
				return ands;
				// filterContenders(dat, t, cm, q, res, dataSize, ands);
			}
		} else {
			// there are no mustBeIn partitions
			if (cantBeIn.size() != 0) {
				BitSet nots = getOrBitSets(datarep, dataSize, cantBeIn);
				nots.flip(0, dataSize);
				return nots;
				// filterContenders(dat, t, cm, q, res, dataSize, nots);
			} else {
				// there are no exclusions at all...
				return null;
				// for (T d : dat) {
				// if (cm.distance(q, d) < t) {
				// res.add(d);
				// }
				// }
			}
		}
	}

	public static <T> void filterContenders(List<T> dat, double t,
											CountedMetric<T> cm, T q, Collection<T> res, final int dataSize,
											BitSet results) {

		// System.out.println( "Contenders size = " + results.cardinality());

		for (int i = results.nextSetBit(0); i != -1 && i < dataSize; i = results.nextSetBit(i + 1)) {
			// added i < dataSize since Cuda code overruns datasize in the -
			// don't know case - must be conservative and include, easier than
			// complex bit masking and shifting.
			if (results.get(i)) {
				if (cm.distance(q, dat.get(i)) <= t) {
					res.add(dat.get(i));
				}
			}
		}
	}

	@SuppressWarnings("boxing")
	static BitSet getAndBitSets(BitSet[] datarep,
			final int dataSize, List<Integer> mustBeIn) {
		BitSet ands = null;
		if (mustBeIn.size() != 0) {
			ands = datarep[mustBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < mustBeIn.size(); i++) {
				ands.and(datarep[mustBeIn.get(i)]);
			}
		}
		return ands;
	}

	@SuppressWarnings("boxing")
	static BitSet getOrBitSets(BitSet[] datarep,
			final int dataSize, List<Integer> cantBeIn) {
		BitSet nots = null;
		if (cantBeIn.size() != 0) {
			nots = datarep[cantBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < cantBeIn.size(); i++) {
				final BitSet nextNot = datarep[cantBeIn.get(i)];
				nots.or(nextNot);
			}
		}
		return nots;
	}

	@SuppressWarnings("boxing")
	public static <T> void queryBitSetData(List<T> queries, List<T> dat,
										   Metric<T> metric, double threshold, BitSet[] datarep,
										   RefPointSet<T> rps, List<ExclusionZone<T>> ezs, int noOfRefPoints, double thresh) {

		int noOfResults = 0;
		int partitionsExcluded = 0;
		CountedMetric<T> cm = new CountedMetric<>(metric);
		long t0 = System.currentTimeMillis();
		for (T q : queries) {
//			show( (CartesianPoint) q );
//			System.out.println( "d to zero = " + metric.distance( q, (T) new CartesianPoint( new double[20]) ));
			List<T> res = new ArrayList<>();
			double[] dists = rps.extDists(q, res, thresh);

			List<Integer> mustBeIn = new ArrayList<>();
			List<Integer> cantBeIn = new ArrayList<>();

			for (int i = 0; i < ezs.size(); i++) {
				ExclusionZone<T> ez = ezs.get(i);
				try {
					if (ez.mustBeIn(dists, thresh)) {
						mustBeIn.add(i);
//						if( ez instanceof SheetExclusion4p ) {
//							System.out.println( "sheet in" + i );
//						} else {
//							System.out.println( "Ball in " + i );
//						}
					} else if (ez.mustBeOut(dists, thresh)) {
//						if( ez instanceof SheetExclusion4p ) {
//							System.out.println( "sheet out" + i );
//						} else {
//							System.out.println( "Ball out" + i );
//						}
						cantBeIn.add(i);
					}
				} catch(  RuntimeException e ) {
					// AL ***0986***
					// eat the runtime exception that is thrown if we get a nan
					// can't make a judgement.
				}
			}

			partitionsExcluded += cantBeIn.size() + mustBeIn.size();

			BitSet inclusions = doExclusions(dat, thresh, datarep, cm, q, dat.size(), mustBeIn, cantBeIn);

			// System.out.println("Inclusions = " + inclusions.size());

			if (inclusions == null) {
				for (T d : dat) {
					if (cm.distance(q, d) <= thresh) {
						res.add(d);
					}
				}
			} else {
				filterContenders(dat, thresh, cm, q, res, dat.size(), inclusions);
			}

			noOfResults += res.size();
			// System.out.println( "results = " + res.size() );
		}

		System.out.println("bitsets");
		System.out.println("dists per query\t"
				+ (cm.reset() / queries.size() + noOfRefPoints));
		System.out.println("results\t" + noOfResults);
		System.out.println("time\t" + (System.currentTimeMillis() - t0)
				/ (float) queries.size());
		System.out.println("partitions excluded per query\t"
				+ ((double) partitionsExcluded / queries.size()));
	}
}
