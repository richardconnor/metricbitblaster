package uk.al_richard.metricbitblaster.experimental.alRichardLucia;

import java.util.ArrayList;
import java.util.List;


public class ReverseOrderedList<E, I extends Comparable<I>> {
    private List<E> elements = new ArrayList();
    private List<I> comparators = new ArrayList();
    private int limit;
    private I threshold;

    public ReverseOrderedList(int limit) {
        this.limit = limit;
        this.threshold = null;
    }

    public void add(E element, I comparator) {
        if (this.threshold == null || comparator.compareTo(this.threshold) >= 0) {
            if (this.comparators.size() == 0) {
                this.comparators.add(comparator);
                this.elements.add(element);
            } else {
                int ptr = 0;
                /*
                 * advance ptr until the node it's pointing at is greater than
                 * or equal to the comparator, or past the end of the list
                 */
                while (ptr < this.comparators.size()
                        && this.comparators.get(ptr).compareTo(comparator) >= 0) {
                    ptr++;
                }

                if (ptr < this.limit) {
                    this.comparators.add(ptr, comparator);
                    this.elements.add(ptr, element);
                }

                if (this.comparators.size() == this.limit) {
                    this.threshold = this.comparators.get(this.limit - 1);
                }

                if (this.comparators.size() > this.limit) {
                    this.threshold = this.comparators.get(this.limit - 1);
                    this.comparators.remove(this.limit); // remove the lowest
                    this.elements.remove(this.limit); // remove the lowest
                }
            }
        }

    }

    public I getThreshold() {
        return this.threshold;
    }

    public List<E> getList() {
        return this.elements;
    }

    public List<I> getComparators() {
        return this.comparators;
    }

    @Override
    public String toString() {
        StringBuffer res = new StringBuffer("[");
        for (E e : this.elements) {
            res.append(e.toString() + ",");
        }
        res.setCharAt(res.length() - 1, ']');
        res.append(" [");
        for (I c : this.comparators) {
            res.append(c.toString() + ",");
        }
        res.setCharAt(res.length() - 1, ']');

        return res.toString();
    }
}

