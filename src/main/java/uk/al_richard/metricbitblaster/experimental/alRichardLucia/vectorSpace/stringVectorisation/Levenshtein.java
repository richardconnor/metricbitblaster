package uk.al_richard.metricbitblaster.experimental.alRichardLucia.vectorSpace.stringVectorisation;

/**
 * Returns the Levenshtein distance of the strings
 * 
 * @author Richard Connor (actually I think Robert Moss wrote the distance
 *         function, after Wikipedia!)
 * 
 * @param <T>
 */
public class Levenshtein implements Metric<String> {

	private static int minimum(int a, int b, int c) {
		return Math.min(Math.min(a, b), c);
	}

	@Override
	public double distance(String x, String y) {

		int[][] distanceAcc = new int[x.length() + 1][y.length() + 1];

		for (int i = 0; i <= x.length(); i++) {
			distanceAcc[i][0] = i;
		}
		for (int j = 0; j <= y.length(); j++) {
			distanceAcc[0][j] = j;
		}

		for (int i = 1; i <= x.length(); i++) {
			for (int j = 1; j <= y.length(); j++) {
				distanceAcc[i][j] = minimum(distanceAcc[i - 1][j] + 1, distanceAcc[i][j - 1] + 1,
						distanceAcc[i - 1][j - 1] + ((x.charAt(i - 1) == y.charAt(j - 1)) ? 0 : 1));
			}
		}

		return (distanceAcc[x.length()][y.length()]);
	}

	@Override
	public String getMetricName() {
		return "lev";
	}

}
