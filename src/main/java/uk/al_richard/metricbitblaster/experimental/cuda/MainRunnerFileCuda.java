package uk.al_richard.metricbitblaster.experimental.cuda;

import coreConcepts.CountedMetric;
import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;
import jcuda.Pointer;
import jcuda.driver.CUdeviceptr;
import jcuda.driver.CUfunction;
import jcuda.driver.CUmodule;
import testloads.TestContext;
import testloads.TestContext.Context;
import uk.al_richard.metricbitblaster.referenceImplementation.ExclusionZone;
import uk.al_richard.metricbitblaster.referenceImplementation.RefPointSet;
import uk.al_richard.metricbitblaster.experimental.al.MainRunnerFile;
import uk.al_richard.metricbitblaster.util.FastPerms;
import uk.al_richard.metricbitblaster.util.OpenBitSet;

import java.util.ArrayList;
import java.util.List;

import static jcuda.driver.JCudaDriver.cuCtxSynchronize;
import static jcuda.driver.JCudaDriver.cuLaunchKernel;


public class MainRunnerFileCuda extends MainRunnerFile {

    private static boolean cuda = false;         // Global flag - set to true to run on GPU, false to run as pure Java

    private static boolean cuda_template = false; // a template for cuda exclusions.

    //********* GPU data structures *********

    private static CUdeviceptr datarep_gpu;

    private static CUdeviceptr threshold_gpu;
    private static CUdeviceptr ball_radii_gpu;
    private static CUdeviceptr dists_gpu;
    private static CUdeviceptr mustBeIn_gpu;
    private static CUdeviceptr cantBeIn_gpu;
    private static CUdeviceptr results_gpu;

    private static CUfunction mustBeInOrOut_function;
    private static CUfunction do_exclusions_function;
    private static Pointer mustbein_kernel_parameters;
    private static Pointer do_exclusions_kernel_parameters;

    //********* end of GPU data structures *********

    public static void main(String[] args) {

        Context context = Context.euc20;

        int noOfRefPoints = 50;
        int nChoose2 = ((noOfRefPoints - 1) * noOfRefPoints) / 2;
        int noOfBitSets = nChoose2 + (noOfRefPoints * ball_radii.length);

        TestContext tc = null;
        try {
            tc = new TestContext(context);
        } catch (Exception e) {
            System.out.println("Error initialising TestContext");
            System.exit(-1);
        }
        int querySize = (context == Context.colors || context == Context.nasa) ? tc
                .dataSize() / 10 : 1000;
        tc.setSizes(querySize, noOfRefPoints);
        List<CartesianPoint> data = tc.getData();

        List<CartesianPoint> refPool = tc.getRefPoints();
        List<CartesianPoint> refs = refPool;
        double threshold = tc.getThresholds()[0];

        List<CartesianPoint> queries = tc.getQueries();
        System.out.println("cuda\t" + cuda);
        System.out.println("context\t" + context);
        System.out.println("data size\t" + data.size());
        System.out.println("query size\t" + querySize);
        System.out.println("threshold\t" + threshold);

        System.out.println("refs size\t" + refs.size());
        System.out.println("no of bitsets\t" + noOfBitSets);

        // buildAndQueryVpt(tc);

        RefPointSet<CartesianPoint> rps = new RefPointSet<>(refs, tc.metric());
        List<ExclusionZone<CartesianPoint>> ezs = new ArrayList<>();

        MainRunnerFile.addSheetExclusions(data, refs, rps, ezs, false, false); // No 4 point or balance impl
        int num_ses = ezs.size();
        System.out.println("sheet exclusions size:\t" + ezs.size());
        MainRunnerFile.addBallExclusions(data, refs, rps, ezs, false); // no balance impl
        System.out.println("ball exclusions size:\t" + (ezs.size() - num_ses));
        System.out.println("exclusion zones size:\t" + ezs.size());

        OpenBitSet[] datarep = new OpenBitSet[noOfBitSets];
        MainRunnerFile.buildBitSetData(data, datarep, rps, ezs);

        if (cuda) {
            initialiseCuda(data.size(), datarep, noOfBitSets, noOfRefPoints, ezs.size());
        }

        System.out.println("Querying ");

        queryBitSetData(queries, data, tc.metric(), threshold, datarep, rps, ezs, noOfRefPoints);

    }

    private static void initialiseCuda(int datasize, OpenBitSet[] datarep, int noOfBitSets, int noOfRefPoints, int noOfSheets) {

        CudaUtils.initialiseGPU();
        initialiseCudaCode();
        initialiseCudaDataStructures( datarep,noOfBitSets,noOfRefPoints );
        initialiseCudaKernelParams( datasize,noOfBitSets,noOfSheets);
    }

    private static void initialiseCudaCode() {

        CUmodule module;

        try {
            module = CudaUtils.loadModule("src/main/CU/exclusions.cu");
        } catch (Exception e) {
            // if this failed we are hozed
            System.err.println("Fatal error loading Cuda functions");
            System.exit(-1);
            return;
        }
        try {
            mustBeInOrOut_function = CudaUtils.loadPTXFunction(module, "mustBeInOrOut");
        } catch (Exception e) {
            // if this failed we are hozed
            System.err.println("Fatal error initalising Cuda function sheet_mustBeInOrOut ");
            System.exit(-1);
        }
        try {
            do_exclusions_function = CudaUtils.loadPTXFunction(module, "do_exclusions");
        } catch (Exception e) {
            // if this failed we are hozed
            System.err.println("Fatal error initalising Cuda function sheet_mustBeInOrOut ");
            System.exit(-1);
        }
    }

    /**
     * Initialises the following data structures for the CUDA computation:
     * <p>
     * int number_sheets;
     * int number_of_balls
     * double[noOfRefPoints] dists;    // from ros to query
     * bit[noOfBitSets][dataSize] datarep;
     * double[num_radii] ball_radii;
     * bit[noOfBitSets] mustBeIn;
     * bit[noOfBitSets] cantBeIn;
     * bit[dataSize] nots
     * bit[dataSize] ands
     *
     * @param datarep
     */
    private static void initialiseCudaDataStructures(OpenBitSet[] datarep, int noOfBitSets, int noOfRefPoints) {

        // do allocation

        long[] bits = datarep[0].getBits();
        int size_of_data_in_longs = bits.length;


        threshold_gpu = CudaUtils.allocateGPUMemoryDoubles(1);
        mustBeIn_gpu = CudaUtils.allocateGPUMemoryInts(noOfBitSets);
        cantBeIn_gpu = CudaUtils.allocateGPUMemoryInts(noOfBitSets);
        dists_gpu = CudaUtils.allocateGPUMemoryDoubles(noOfRefPoints);
        ball_radii_gpu = CudaUtils.allocateGPUMemoryDoubles(MainRunnerFile.ball_radii.length);

        datarep_gpu = CudaUtils.copyAndAllocGPUMemory( datarep );
        results_gpu = CudaUtils.allocateGPUMemoryLongs(size_of_data_in_longs);

        // then do copies

        CudaUtils.copyToGPUMemory( ball_radii_gpu, MainRunnerFile.ball_radii );

    }


    private static void initialiseCudaKernelParams(int datasize, int noOfBitSets, int numberOfSheets ) {

        // Set up the kernel parameters: A pointer to an array of pointers which point to the actual values.

        int[] number_of_bitsets_param = new int[]{noOfBitSets};  // CPU data structures used in CUDA calls
        int[] n_sheets_param = new int[]{numberOfSheets};             // CPU data structures used in CUDA calls
        int[] n_radii_param = new int[]{MainRunnerFile.ball_radii.length};         // CPU data structures used in CUDA calls
        int[] dataSize_in_longs_param = new int[]{CudaUtils.bits_to_longs(datasize)};        // CPU data structures used in CUDA calls

         // Java: mustBeInOrOut(int n_ezs, double threshold, int number_sheets, int n_radii, double[] ball_radii, double[] dists, List<Integer> mustBeIn, List<Integer> cantBeIn)
        // Cuda: extern "C" __global__ void mustBeInOrOut( int n_ezs, double *threshold, int number_sheets, int num_radii, double *ball_radii, double *dists, int *mustBeIn, int *cantBeIn )

        mustbein_kernel_parameters =
                Pointer.to(                                        // mustBeInOrOut:
                        Pointer.to(number_of_bitsets_param),       //      int ref
                        Pointer.to(threshold_gpu),                 //      double *threshold
                        Pointer.to(n_sheets_param),                //      int number_sheets
                        Pointer.to(n_radii_param),                 //      int num_radii
                        Pointer.to(ball_radii_gpu),                //      double[] radii
                        Pointer.to(dists_gpu),                     //      double[] dists
                        Pointer.to(mustBeIn_gpu),                  //      int[] mustBeIn
                        Pointer.to(cantBeIn_gpu)                   //      int[] cantBeIn )
                );

        do_exclusions_kernel_parameters =
                Pointer.to(                                        // do_exclusions:
                        Pointer.to(dataSize_in_longs_param),       // final int dataSize_param,
                        Pointer.to(number_of_bitsets_param),       // n_ezs
                        Pointer.to(datarep_gpu),                   // OpenBitSet[] datarep,
                        Pointer.to(mustBeIn_gpu),                  // int[] mustBeIn
                        Pointer.to(cantBeIn_gpu),                  // int[] cantBeIn )
                        Pointer.to(results_gpu)                    // long[] results
                );

    }

    private static void initialiseMustbeInOrOutDynamicParams(double threshold, double[] dists, int noOfBitSets) {

        CudaUtils.zeroGPUMemoryInts(mustBeIn_gpu, noOfBitSets);
        CudaUtils.zeroGPUMemoryInts(cantBeIn_gpu, noOfBitSets);

        CudaUtils.copyToGPUMemory(dists_gpu, dists);
        CudaUtils.copyToGPUMemory(threshold_gpu, new double[]{threshold});
    }


    @SuppressWarnings("boxing")
    static <T> void queryBitSetData(List<T> queries, List<T> data,
                                    Metric<T> metric, double threshold, OpenBitSet[] datarep,
                                    RefPointSet<T> rps, List<ExclusionZone<T>> ezs, int noOfRefPoints, int noOfBitSets, int numberOfSheets) {

        int noOfResults = 0;
        int partitionsExcluded = 0;
        CountedMetric<T> cm = new CountedMetric<>(metric);
        long t0 = System.currentTimeMillis();

        int count = 0;

        for (T q : queries) {
            List<T> res = new ArrayList<>();
            double[] dists = rps.extDists(q, res, threshold);

            List<Integer> mustBeIn = new ArrayList<>();
            List<Integer> cantBeIn = new ArrayList<>();

            if (cuda) {
                findExclusionsCuda(dists, threshold, mustBeIn, cantBeIn, noOfBitSets);
            } else {
                findExclusions(dists, threshold, mustBeIn, cantBeIn, ezs, numberOfSheets );
                partitionsExcluded += cantBeIn.size() + mustBeIn.size();
            }


            if( cuda ) {
                long[] result = doExclusionsCuda(data, threshold, datarep, cm, q, res, mustBeIn, cantBeIn);
                OpenBitSet result_converted = new OpenBitSet(result);
                MainRunnerFile.filterContenders(data, threshold, cm, q, res, data.size(), result_converted);
            } else {
                OpenBitSet inclusions = MainRunnerFile.doExclusions(data, threshold, datarep, cm, q, data.size(), mustBeIn, cantBeIn);
                if( inclusions == null ) {
                    for (T d : data) {
                        if (cm.distance(q, d) < threshold) {
                            res.add(d);
                        }
                    }
                } else {
                    MainRunnerFile.filterContenders(data, threshold, cm, q, res, data.size(), inclusions);
                }
            }
            noOfResults += res.size();
        }

        System.out.println("bitsets");
        System.out.println("dists per query\t"
                + (cm.reset() / queries.size() + noOfRefPoints));
        System.out.println("results\t" + noOfResults);
        System.out.println("time\t" + (System.currentTimeMillis() - t0)
                / (float) queries.size());
        System.out.println("partitions excluded\t"
                + ((double) partitionsExcluded / queries.size()));
    }

    private static void compare(List<Integer> mustBeIn, List<Integer> mustBeIn_java, List<Integer> cantBeIn, List<Integer> cantBeIn_java) {
        cmp(mustBeIn, mustBeIn_java, "mustbe");
        cmp(cantBeIn, cantBeIn_java, "cantbe");
    }

    private static void cmp(List<Integer> gpu_list, List<Integer> java_list, String listname) {
        if (gpu_list.size() != java_list.size()) {
            System.out.println(listname + " Sizes differ: gpu=" + gpu_list.size() + " java=" + java_list.size());
        }
        System.out.println(listname + " Java: " );
        for (int i = 0; i < java_list.size(); i++) {
            System.out.print( java_list.get(i) + " , " );
        }
        System.out.println();
        System.out.println(listname + " GPU: " );
        for (int i = 0; i < gpu_list.size(); i++) {
                System.out.print( gpu_list.get(i) + " , " );
        }
        System.out.println();

    }

    private static void mustBeInOrOut(int index, double threshold, int number_sheets, double[] ball_radii, double[] dists, List<Integer> mustBeIn, List<Integer> cantBeIn) {

        if( index < number_sheets ) { // sheet exclusion
            int[] coords = FastPerms.getNthPair(index);
            int i = coords[0];
            int j = coords[1];
            if (dists[i] - (dists[j]) < -(2 * threshold)) {  // was sheet mustBeIn - only 3 point - return  (dists[this.ref1] - dists[this.ref2]) - this.offset < -(2 * t);
                mustBeIn.add(index);
            } else if (dists[i] - (dists[j]) >= 2 * threshold) { // was sheet mustBeOut - only 3 point - return (dists[this.ref1] - dists[this.ref2]) - this.offset >= (2 * t);
                cantBeIn.add(index);
            }
        } else { // ball exclusion

            int ro_index = (index - number_sheets) / ball_radii.length;         // which ro we are looking at
            int radius_index = (index - number_sheets) % ball_radii.length;     // ref of the radius relative to the current ro.

            double radius = ball_radii[radius_index];

            if (dists[ro_index] < radius - threshold) {   // was ball mustBeIn - return dists[this.ref] < this.radius - t;
                mustBeIn.add(index);
            } else if (dists[ro_index] >= radius + threshold) { // was ball mustBeOut - return dists[this.ref] >= this.radius + t;
                cantBeIn.add(index);
            }
        }
    }

    // Template for doExclusionsCuda:
    protected static <T> boolean[] doExclusionsCudaTemplate(OpenBitSet[] datarep, final int dataSize, List<Integer> mustBeIn, List<Integer> cantBeIn, int noOfBitSets) {

        //   int[] mustbein_selections_from_gpu = new int[noOfBitSets]; // in Cuda these are in GPU memory
        //   int[] cantbein_selections_from_gpu = new int[noOfBitSets];

        boolean[] result = new boolean[dataSize];

        for( int row = 0; row < dataSize; row++ ) {

            boolean first = true;

            for (int column_id = 0; column_id < noOfBitSets ; column_id++ ) { //  for( int column_id : mustbein_selections_from_gpu ) {

                if( mustBeIn.contains(column_id) ) {   //if (mustbein_selections_from_gpu[column_id] == 1) {
                    if( first ) {
                        result[row] = datarep[column_id].get(row);
                        first = false;
                    } else {
                        result[row] = result[row] && datarep[column_id].get(row);
                    }
                }
                if( cantBeIn.contains(column_id) ) {// if (cantbein_selections_from_gpu[column_id] == 1) {
                    if( first ) {
                        result[row] = !datarep[column_id].get(row);
                        first = false;
                    } else {
                        result[row] = result[row] && !datarep[column_id].get(row);
                    }
                }
            }
            if( first ) {
                result[row] = false;
            }
        }
        return result;
    }

    protected static <T> long[] doExclusionsCuda(List<T> data, double t, OpenBitSet[] datarep, CountedMetric<T> cm, T q, List<T> res,
                                           List<Integer> mustBeIn, List<Integer> cantBeIn) {

        long[] bits = datarep[0].getBits();
        int size_of_data_in_longs = bits.length;

        int cuda_block_size = 256;
        int numBlocks = (int) Math.ceil((size_of_data_in_longs + cuda_block_size - 1) / cuda_block_size);

        CudaUtils.checkResult(
                cuLaunchKernel(
                        do_exclusions_function,
                        numBlocks, 1, 1,                 // grid X,Y,Z of blocks
                        cuda_block_size, 1, 1,        // each block contains blockDimX x blockDimY x blockDimZ threads.
                        0, null,                   // Shared memory size and stream
                        do_exclusions_kernel_parameters, null             // Kernel and extra parameters
                ),
                "cuLaunchKernel (do_exclusions_function) in doExclusionsCuda");

        CudaUtils.checkResult( cuCtxSynchronize(), "cuCtxSynchronize in doExclusionsCuda" );

        long[] selections_from_gpu = CudaUtils.copyFromGPULongs(results_gpu, size_of_data_in_longs);

        return selections_from_gpu;
    }



    private static void findExclusionsCuda(double[] dists, double threshold, List<Integer> mustBeIn, List<Integer> cantBeIn, int noOfBitSets) {

        initialiseMustbeInOrOutDynamicParams(threshold,dists,noOfBitSets);

        int cuda_block_size = 256;
        int numBlocks = (int) Math.ceil((noOfBitSets + cuda_block_size - 1) / cuda_block_size);

        CudaUtils.checkResult(
                cuLaunchKernel(
                        mustBeInOrOut_function,
                        numBlocks, 1, 1,                 // grid X,Y,Z of blocks
                        cuda_block_size, 1, 1,        // each block contains blockDimX x blockDimY x blockDimZ threads.
                        0, null,                   // Shared memory size and stream
                        mustbein_kernel_parameters, null             // Kernel and extra parameters
                ),
                "cuLaunchKernel (mustBeInOrOut_function) in findExclusionsCuda");

        CudaUtils.checkResult( cuCtxSynchronize(), "cuCtxSynchronize in findExclusionsCuda" );

        int[] mustbein_selections_from_gpu = CudaUtils.copyFromGPUInts(mustBeIn_gpu, noOfBitSets);  // copy bits from GPU memory.
        int[] cantBeIn_selections_from_gpu = CudaUtils.copyFromGPUInts(cantBeIn_gpu, noOfBitSets);      // copy bits from GPU memory.

//		show(mustbein_selections_from_gpu,"mustbein");
//		show(cantBeIn_selections_from_gpu,"cantbein");
//        checkallDS(threshold,dists);

//        for (int i = 0; i < mustbein_selections_from_gpu.length; i++) {
//            if (mustbein_selections_from_gpu[i] == 1) {
//                mustBeIn.add(i);
//            }
//        }
//        for (int i = 0; i < cantBeIn_selections_from_gpu.length; i++) {
//            if (cantBeIn_selections_from_gpu[i] == 1) {
//                cantBeIn.add(i);
//            }
//        }
    }

    private static <T> void findExclusions(double[] dists, double threshold, List<Integer> mustBeIn, List<Integer> cantBeIn, List<ExclusionZone<T>> ezs, int numberOfSheets ) {

        for (int i = 0; i < ezs.size(); i++) {

            mustBeInOrOut(i, threshold, numberOfSheets, MainRunnerFile.ball_radii, dists, mustBeIn, cantBeIn);  // is a CUDA kernel in doQueryCuda

        }
    }

    //********************************* Debug code below here *********************************//

    private static void show(int[] array, String title) {
        System.out.print(title + "length: " + array.length + " = ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    private static void show(double[] array, String title) {
        System.out.print(title + "length: " + array.length + " = ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }


    private static void checkallDS(double threshold, double[] dists) {

        double[] copy_radii = CudaUtils.copyFromGPUDoubles(ball_radii_gpu, MainRunnerFile.ball_radii.length);
        double[] copy_dists = CudaUtils.copyFromGPUDoubles(dists_gpu, dists.length);
        double[] copy_threshold = CudaUtils.copyFromGPUDoubles(threshold_gpu, 1);

     //   System.out.println( "Copy of threshold = " + copy_threshold[0] + " -2*copy= " + (-2 * copy_threshold[0]) + " threshold = " + threshold ) ;

        check_same(copy_threshold, threshold, "threshold");
        check_same(copy_radii, MainRunnerFile.ball_radii, "radii");
        check_same(copy_dists, dists, "dists");
    }

    private static void check_same(double[] copy, double orig, String msg) {
        if (copy[0] != orig) {
            System.err.println("Difference (double) : " + copy + " and " + orig + " in field: " + msg);
        }
    }

    private static void check_same(double[] copy, double[] orig, String msg) {
        if (copy.length != orig.length) {
            System.err.println("Difference in lengths (double[]) : " + copy + " and " + orig + " in field: " + msg);
            return;
        }
        for (int i = 0; i < copy.length; i++) {
            if (copy[i] != orig[i]) {
                System.err.println("Difference (double[]) : element " + i + " : " + copy + " and " + orig + " in field: " + msg);
            }
        }

    }

    private static void check_same(int[] copy, int orig, String msg) {
        if (copy[0] != orig) {
            System.err.println("Difference (int) : " + copy + " and " + orig + " in field: " + msg);
        }
    }

}
