package uk.al_richard.metricbitblaster.experimental.cuda;

import jcuda.Pointer;
import jcuda.Sizeof;
import jcuda.driver.*;
import uk.al_richard.metricbitblaster.util.OpenBitSet;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import static jcuda.driver.CUresult.CUDA_SUCCESS;
import static jcuda.driver.JCudaDriver.*;
import static jcuda.runtime.JCuda.cudaMemset;

public class CudaUtils {

    /**
     * The extension of the given file name is replaced with "ptx".
     * If the file with the resulting name does not exist, it is
     * compiled from the given file using NVCC. The name of the
     * PTX file is returned.
     *
     * @param cuFileName The name of the .CU file
     * @return The name of the PTX file
     * @throws IOException If an I/O error occurs
     */
    private static String preparePtxFile(String cuFileName) throws IOException {
        int endIndex = cuFileName.lastIndexOf('.');
        if (endIndex == -1) {
            endIndex = cuFileName.length() - 1;
        }
        String ptxFileName = cuFileName.substring(0, endIndex + 1) + "ptx";
        File ptxFile = new File(ptxFileName);
        if (ptxFile.exists()) {
            return ptxFileName;
        }

        File cuFile = new File(cuFileName);
        if (!cuFile.exists()) {
            throw new IOException("Input file not found: " + cuFileName);
        }
        String modelString = "-m" + System.getProperty("sun.arch.data.model");
        String command =
                "nvcc " + modelString + " -ptx " +
                        cuFile.getPath() + " -o " + ptxFileName;

        System.out.println("Executing\n" + command);
        Process process = Runtime.getRuntime().exec(command);

        String errorMessage =
                new String(toByteArray(process.getErrorStream()));
        String outputMessage =
                new String(toByteArray(process.getInputStream()));
        int exitValue = 0;
        try {
            exitValue = process.waitFor();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new IOException(
                    "Interrupted while waiting for nvcc output", e);
        }

        if (exitValue != 0) {
            System.err.println("nvcc process exitValue " + exitValue);
            System.err.println("errorMessage:\n" + errorMessage);
            System.err.println("outputMessage:\n" + outputMessage);
            throw new IOException(
                    "Could not create .ptx file: " + errorMessage);
        }

        System.out.println("Finished creating PTX file");
        return ptxFileName;
    }

    public static void initialiseGPU() {
        // Enable exceptions and omit all subsequent error checks
        JCudaDriver.setExceptionsEnabled(true);
        // Initialize the driver and create a context for the first GPU (GPU 0).
        // (it is possible to have more than one GPUs in high end systems.)
        checkResult( cuInit(0),"cuInit in initialiseGPU" ); ;
        CUdevice device = new CUdevice();
        checkResult( cuDeviceGet(device, 0), "cuDeviceGet in initialiseGPU" );
        CUcontext context = new CUcontext();
        checkResult( cuCtxCreate(context, 0, device), "cuCtxCreate in initialiseGPU" );
    }

    /**
     * Fully reads the given InputStream and returns it as a byte array
     *
     * @param inputStream The input stream to read
     * @return The byte array containing the data from the input stream
     * @throws IOException If an I/O error occurs
     */
    private static byte[] toByteArray(InputStream inputStream)
            throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte buffer[] = new byte[8192];
        while (true) {
            int read = inputStream.read(buffer);
            if (read == -1) {
                break;
            }
            baos.write(buffer, 0, read);
        }
        return baos.toByteArray();
    }

    public static int[] copyFromGPUInts( CUdeviceptr gpu_pntr, int num_ints ) {
        int length_bytes = num_ints * Sizeof.INT;
        int[] result = new int[num_ints];
        checkResult( cuMemcpyDtoH(Pointer.to(result), gpu_pntr, length_bytes),"cuMemcpyDtoH in copyFromGPUBytes" );

        return result;
    }


    public static long[] copyFromGPULongs( CUdeviceptr gpu_pntr, int num_longs ) {

        int length_bytes = num_longs * Sizeof.LONG;
        long[] result = new long[num_longs];
        checkResult( cuMemcpyDtoH(Pointer.to(result), gpu_pntr, length_bytes),"cuMemcpyDtoH in copyFromGPUBytes" );

        return result;
    }

    public static double[] copyFromGPUDoubles( CUdeviceptr gpu_pntr, int num_doubles ) {

        int length_bytes = num_doubles * Sizeof.DOUBLE;
        double[] result = new double[num_doubles];
        checkResult( cuMemcpyDtoH(Pointer.to(result), gpu_pntr, length_bytes),"cuMemcpyDtoH in copyFromGPUBytes" );

        return result;
    }


    public static long[] copyFromGPUBytes(CUdeviceptr gpu_pntr, int length_bytes) {
        int num_elems = bytes_to_longs( length_bytes );
        long[] result = new long[num_elems];
        checkResult( cuMemcpyDtoH(Pointer.to(result), gpu_pntr, length_bytes), "cuMemcpyDtoH in copyFromGPUBytes" );
        return result;
    }

    public static long[] copyFromGPUBits(CUdeviceptr gpu_pntr, int length_in_bits_) {
        int length_bytes = bits_to_bytes( length_in_bits_);
        int num_elems = bytes_to_longs( length_bytes );
        long[] result = new long[num_elems];
        checkResult( cuMemcpyDtoH(Pointer.to(result), gpu_pntr, length_bytes),"cuMemcpyDtoH in copyFromGPUBytes" );

        return result;
    }

//    public static void copyToGPUMemory(CUdeviceptr device_memory, double data) {
//        double[] vector = new double[]{data};
//        checkResult( cuMemcpyHtoD(device_memory, Pointer.to(vector), (long) Sizeof.DOUBLE),"cuMemcpyHtoD in copyToGPUMemory" );
//    }

    public static void copyToGPUMemory(CUdeviceptr device_memory, int[] vector) {
        checkResult( cuMemcpyHtoD(device_memory, Pointer.to(vector), (long) vector.length * Sizeof.INT),"cuMemcpyHtoD in copyToGPUMemory( int[] )" );
    }

    public static void copyToGPUMemory(CUdeviceptr device_memory, long[] vector) {
        checkResult(cuMemcpyHtoD(device_memory, Pointer.to(vector), (long) vector.length * Sizeof.LONG), "cuMemcpyHtoD in copyToGPUMemory( long[] )");
    }


    public static void copyToGPUMemory(CUdeviceptr device_memory, double[] vector) {
        checkResult( cuMemcpyHtoD(device_memory, Pointer.to(vector), (long) vector.length * Sizeof.DOUBLE),"cuMemcpyHtoD in copyToGPUMemory( double[] )" );
    }


    public static CUdeviceptr allocateGPUMemoryBitsRoundupToLong(int size_in_bits) {
        int bytes_size = bits_to_bytes( size_in_bits );
        int long_size = bytes_to_longs( bytes_size );
        return allocateGPUMemoryBytes(long_size * Sizeof.LONG );
    }

    public static CUdeviceptr allocateGPUMemoryBitsRoundupToint(int size_in_bits) {
        int bytes_size = bits_to_bytes( size_in_bits );
        int int_size = bytes_to_ints( bytes_size );
        return allocateGPUMemoryBytes(int_size * Sizeof.INT );
    }

    public static CUdeviceptr allocateGPUMemoryBits(int size_in_bits) {
        int bytes_size = bits_to_bytes( size_in_bits );
        return allocateGPUMemoryBytes(bytes_size);
    }

    public static CUdeviceptr allocateGPUMemoryBytes(int size_in_bytes) {
        CUdeviceptr device_memory = new CUdeviceptr();
        checkResult( cuMemAlloc(device_memory, size_in_bytes),"allocateGPUMemoryBytes" );
        checkResult( cudaMemset(device_memory, 0, size_in_bytes ), "cudaMemset in allocateGPUMemoryBytes" );

        return device_memory;
    }

    public static CUdeviceptr allocateGPUMemoryInts(int size_in_ints) {
        CUdeviceptr device_memory = new CUdeviceptr();

        long size = size_in_ints * Sizeof.INT;
        checkResult( cuMemAlloc(device_memory, size),"cuMemAlloc in allocateGPUMemoryInts" );


        return device_memory;
    }

    public static CUdeviceptr allocateGPUMemoryLongs(int size_in_longs) {
        CUdeviceptr device_memory = new CUdeviceptr();

        long length_bytes = size_in_longs * Sizeof.LONG;
        checkResult( cuMemAlloc(device_memory, length_bytes),"cuMemAlloc in allocateGPUMemoryDoubles" );
        checkResult( cudaMemset(device_memory, 0, length_bytes ), "cudaMemset in allocateGPUMemoryDoubles" );

        return device_memory;
    }

    public static CUdeviceptr allocateGPUMemoryDoubles(int size_in_doubles) {
        CUdeviceptr device_memory = new CUdeviceptr();

        long length_bytes = size_in_doubles * Sizeof.DOUBLE;
        checkResult( cuMemAlloc(device_memory, length_bytes),"cuMemAlloc in allocateGPUMemoryDoubles" );
        checkResult( cudaMemset(device_memory, 0, length_bytes ), "cudaMemset in allocateGPUMemoryDoubles" );

        return device_memory;
    }

    public static void zeroGPUMemoryInts(CUdeviceptr device_memory, int size_in_ints) {
        long size = size_in_ints * Sizeof.INT;
        checkResult(cudaMemset(device_memory, 0, size), "cudaMemset in zeroGPUMemoryInts");
    }

    public static CUdeviceptr copyAndAllocGPUMemory(OpenBitSet vector) {
        long[] bits = vector.getBits();
        return copyToGPUMemoryLong(bits);
    }

    public static CUdeviceptr copyAndAllocGPUMemory(OpenBitSet[] datarep) {

        long[] bits = datarep[0].getBits();
        int each_array_size_bytes = bits.length * Sizeof.LONG;

        CUdeviceptr device_memory = allocateGPUMemoryBytes(each_array_size_bytes * datarep.length ); // space for the actual bits
        CUdeviceptr next_bitset_pntr = device_memory;

        CUdeviceptr[] host_result_pointers_array = new CUdeviceptr[datarep.length]; // an array of pointers to each of the bitsets in cpu memory.

        // copy the values fro datarep into gpu memory and assign the pointers to the slot of host_result_pointers_array
        for( int i = 0; i < datarep.length; i++ ) {
            host_result_pointers_array[i] = next_bitset_pntr;
            long[] next_bitset = datarep[i].getBits();
            copyToGPUMemory( next_bitset_pntr,next_bitset );
            next_bitset_pntr =  next_bitset_pntr.withByteOffset( each_array_size_bytes );
        }

        // copy host_result_pointers_array vector to the GPU

        CUdeviceptr deviceResultArray = new CUdeviceptr();
        cuMemAlloc(deviceResultArray, datarep.length * Sizeof.POINTER);
        cuMemcpyHtoD(deviceResultArray, Pointer.to(host_result_pointers_array),datarep.length * Sizeof.POINTER);

        return deviceResultArray;
    }

    public static CUdeviceptr copyAndAllocGPUMemory(int[] vector) {
        CUdeviceptr device_memory = allocateGPUMemoryBytes(vector.length * Sizeof.INT);
        copyToGPUMemory(device_memory, vector);
        return device_memory;
    }

    public static CUdeviceptr copyToGPUMemoryLong(long[] vector) {
        CUdeviceptr device_memory = allocateGPUMemoryBytes(vector.length * Sizeof.LONG);
        copyToGPUMemory(device_memory, vector);
        return device_memory;
    }

    public static CUmodule loadModule( String cu_file_name ) throws IOException {

        // Create the PTX file by calling the NVCC
        String ptx_file = preparePtxFile(cu_file_name);

        CUmodule module = new CUmodule();
        checkResult( cuModuleLoad(module, ptx_file),"cuModuleLoad in loadModule" );
        return module;
    }

    public static CUfunction loadPTXFunction(CUmodule module, String function_name) throws IOException {


        // Obtain a function pointer to the "and" function.
        CUfunction function = new CUfunction();
        checkResult( cuModuleGetFunction(function, module, function_name),"cuModuleGetFunction in loadPTXFunction" );

        return function;

    }

    public static int bits_to_longs(int size_in_bits) {

        int bytes_size = size_in_bits / 8;
        if(size_in_bits %8!=0) {
            bytes_size += 1;
        }
        return bytes_to_longs( bytes_size );
    }

    public static int bits_to_bytes(int size_in_bits) {

        int bytes_size = size_in_bits / 8;
        if(size_in_bits %8!=0) {
            bytes_size += 1;
        }
        return bytes_size;
    }


    private static int bytes_to_ints(int bytes_size) {
        int n_ints = bytes_size / Sizeof.INT;
        if(bytes_size %4!=0) {
            n_ints += 1;
        }
        return n_ints;
    }

    private static int bytes_to_longs(int bytes_size) {
        int n_longs = bytes_size / Sizeof.LONG;
        if(bytes_size %8!=0) {
            n_longs += 1;
        }
        return n_longs;
    }

    public static void checkResult( int result, String method_name ) {
        if( result != CUDA_SUCCESS ) {
            System.err.println( "Error in " + method_name + ", result is " + CUresult.stringFor( result ) );
            System.exit(-1);
        }
    }
}
