package uk.al_richard.metricbitblaster.experimental.al;

import coreConcepts.CountedMetric;
import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;
import dataPoints.cartesian.Euclidean;
import testloads.CartesianThresholds;
import testloads.TestLoad;
import uk.al_richard.metricbitblaster.referenceImplementation.ExclusionZone;
import uk.al_richard.metricbitblaster.referenceImplementation.RefPointSet;
import uk.al_richard.metricbitblaster.util.OpenBitSet;

import java.util.ArrayList;
import java.util.List;

public class MainRunnerSISAPDImensionsExplore extends MainRunnerFile {

	private static final double RADIUS_INCREMENT = 0.3;
	private static final double MEAN_DIST = 1.81;
    private static final double[] ball_radii = new double[] { MEAN_DIST - 2 * RADIUS_INCREMENT, MEAN_DIST- RADIUS_INCREMENT, MEAN_DIST, MEAN_DIST + RADIUS_INCREMENT, MEAN_DIST + 2 * RADIUS_INCREMENT };

    private static final Metric<CartesianPoint> metric = new Euclidean<>();

	public static void main(String[] args) throws Exception {

        boolean fourPoint = true;
        boolean balanced = true;

	    int a_hundred = 100;
        int two_hundred = 200;
	    int one_thousand = 1000;
        int ten_thousand = 10 * 1000;
        int one_hundred_thousand = 100 * 1000;
        int a_million = 1 * 1000 * 1000;

        int data_size = ten_thousand; // was one_hundred_thousand;
        int number_of_queries = one_thousand;

        System.out.println( "Dimensions\tData size\tthreshold    \tno. queries\tno ros\tno parts\tparts exluded\tdistances/q\tms/query\tphase1/q\tphase2/q\tphase3/q\tno unfiltered results\tno results" );


        for (int dimensions = 6; dimensions <= 20; dimensions++) {

            for( int refs_size = 20; refs_size <= 400; refs_size+= 20 ) {

                doExperiment(dimensions, data_size, number_of_queries, refs_size, balanced, fourPoint);
            }
        }
    }

    private static void doExperiment( int dimensions, int data_size, int number_of_queries, int number_of_refs, boolean balanced, boolean fourPoint ) {

        TestLoad tl = new TestLoad(dimensions, data_size + number_of_queries + number_of_refs, false );

        List<CartesianPoint> refs = tl.getQueries(number_of_refs);
        List<CartesianPoint> queries = tl.getQueries( number_of_queries );
		List<CartesianPoint> data = tl.getDataCopy();

        double threshold = CartesianThresholds.getThreshold( metric.getMetricName(), dimensions, 1 ); // 1 is ppm

        int nChoose2 = ((number_of_refs - 1) * number_of_refs) / 2;
        int noOfBitSets = nChoose2 + ( number_of_refs * ball_radii.length );

        System.out.print( dimensions + "\t" +  data.size() + "\t" + threshold + "\t" + queries.size() + "\t" +  refs.size() + "\t" + noOfBitSets);

		RefPointSet<CartesianPoint> rps = new RefPointSet<>(refs, metric);
		List<ExclusionZone<CartesianPoint>> ezs = new ArrayList<>();

		addSheetExclusions(data, refs, rps, ezs, balanced, fourPoint);
		addBallExclusions(data, refs, rps, ezs, balanced);

		OpenBitSet[] datarep = new OpenBitSet[noOfBitSets];
		buildBitSetData(data, datarep, rps, ezs);
		queryBitSetData(queries, data, metric, threshold, datarep, rps, ezs, number_of_refs);

	}

    @SuppressWarnings("boxing")
    public static <T> void queryBitSetData(List<T> queries, List<T> dat,
                                           Metric<T> metric, double threshold, OpenBitSet[] datarep,
                                           RefPointSet<T> rps, List<ExclusionZone<T>> ezs, int noOfRefPoints) {

        int noOfResults = 0;
        int noOfUnfliteredResults = 0;
        int partitionsExcluded = 0;
        int distance_calcs = 0;
        long t0 = System.currentTimeMillis();
        long phase1_time = 0;
        long phase2_time = 0;
        long phase3_time = 0;

        for (T q : queries) {
            CountedMetric<T> cm = new CountedMetric<>(metric);
            List<T> res = new ArrayList<>();

            long start = System.currentTimeMillis();
            double[] dists = rps.extDists(q, res, threshold);
            long end = System.currentTimeMillis();
            phase1_time += end - start;
            start = end;

            List<Integer> mustBeIn = new ArrayList<>();
            List<Integer> cantBeIn = new ArrayList<>();

            for (int i = 0; i < ezs.size(); i++) {
                ExclusionZone<T> ez = ezs.get(i);
                if (ez.mustBeOut( dists, threshold)) {
                    mustBeIn.add(i);
                } else if (ez.mustBeOut( dists, threshold)) {
                    cantBeIn.add(i);
                }
            }

            partitionsExcluded += cantBeIn.size() + mustBeIn.size();

            OpenBitSet inclusions = doExclusions(dat, threshold, datarep, cm, q, dat.size(), mustBeIn, cantBeIn);

            end = System.currentTimeMillis();
            phase2_time += end - start;
            start = end;

            if( inclusions == null ) {
                noOfUnfliteredResults += dat.size();
                for (T d : dat) {
                    if (cm.distance(q, d) < threshold) {
                        res.add(d);
                    }
                }
            } else {
                noOfUnfliteredResults += inclusions.cardinality();
                filterContenders(dat, threshold, cm, q, res, dat.size(), inclusions);
            }

            end = System.currentTimeMillis();
            phase3_time += end - start;

            noOfResults += res.size();
            distance_calcs += cm.reset(); // + noOfRefPoints; // only report residual distances - if totals are wanted add ref points.
        }

        System.out.println("\t"
                + ((double) partitionsExcluded / queries.size()) + "\t"
                + (distance_calcs / (float) queries.size()) + "\t"
                + ((System.currentTimeMillis() - t0) / (float) queries.size()) + "\t" +
                + (phase1_time / (float) queries.size()) + "\t" +
                + (phase2_time / (float) queries.size()) + "\t" +
                + (phase3_time / (float) queries.size()) + "\t" +
                + noOfUnfliteredResults + "\t"
                + noOfResults);

    }
}
