package uk.al_richard.metricbitblaster.experimental.al.density;

import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;
import testloads.TestContext;
import testloads.TestContext.Context;
import uk.al_richard.metricbitblaster.referenceImplementation.BallExclusion;
import uk.al_richard.metricbitblaster.referenceImplementation.ExclusionZone;
import uk.al_richard.metricbitblaster.referenceImplementation.RefPointSet;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static uk.al_richard.metricbitblaster.util.Filename.getFileName;

/**
 * Tries to explore the local density of objects around points
 */
public class ExploreDensity {

	public static void main(String[] args) throws Exception {

		Context context = Context.euc20;

		int noOfRefPoints = 1000;

		TestContext tc = new TestContext(context);
		int querySize = 0;
		tc.setSizes(querySize, noOfRefPoints);
		List<CartesianPoint> dat = tc.getData();
		Metric<CartesianPoint> metric = tc.metric();

		List<CartesianPoint> refs = tc.getRefPoints();

		System.out.println("Date/time\t" + new Date().toString());
		System.out.println("Class\t" + getFileName());
		System.out.println("context\t" + context);
		System.out.println("refs size\t" + refs.size());

		RefPointSet<CartesianPoint> rps = new RefPointSet<>(refs, tc.metric());
		List<ExclusionZone<CartesianPoint>> ezs = new ArrayList<>();

		exploreRegions(metric, dat, refs, rps);

	}

	public static void exploreRegions(Metric<CartesianPoint> metric, List<CartesianPoint> dat, List<CartesianPoint> refs, RefPointSet<CartesianPoint> rps) {
		for (int i = 0; i < refs.size(); i++) {
			BallExclusion<CartesianPoint> be = new BallExclusion<>(rps, i, 2);

			List<CartesianPoint> witnesses = dat.subList(0, 1000);

			for (CartesianPoint witness : witnesses) {

				double d = metric.distance(be.getPointSet().refs.get(0), witness);
			}

		}
	}
}
