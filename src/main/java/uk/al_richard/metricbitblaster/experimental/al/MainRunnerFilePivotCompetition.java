package uk.al_richard.metricbitblaster.experimental.al;

import coreConcepts.CountedMetric;
import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;
import searchStructures.VPTree;
import testloads.TestContext;
import testloads.TestContext.Context;
import uk.al_richard.metricbitblaster.referenceImplementation.*;
import uk.al_richard.metricbitblaster.dataStructures.EZInfo;
import uk.al_richard.metricbitblaster.util.Jaccard;
import uk.al_richard.metricbitblaster.util.OpenBitSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static java.util.Arrays.sort;

/**
 * Attempts to measure the contribution of the EZs
 * to get a handle on optimising the EZs.
 * This was an attempt pre reading Pavel and Vlad's paper using Pearson.
 * This does Jaccard comparison on the columns and picks the best.
 */

public class MainRunnerFilePivotCompetition {

	protected static final double RADIUS_INCREMENT = 0.3;
	private static double MEAN_DIST = 1.81;
	protected static double[] ball_radii = new double[]{
			MEAN_DIST - 2 * RADIUS_INCREMENT, MEAN_DIST - RADIUS_INCREMENT,
			MEAN_DIST, MEAN_DIST - RADIUS_INCREMENT,
			MEAN_DIST - 2 * RADIUS_INCREMENT};
	private static int nChoose2;

	// public static double[] ball_radii = new double[] { 0.168, 0.268, 0.318,
	// 0.418, 0.518, 0.618 };

	public static void main(String[] args) throws Exception {

		Context context = Context.colors;

		boolean fourPoint = true;
		boolean balanced = true;
		int noOfSampleRefPoints = 200;
		int noOfRefPoints = 20;
		nChoose2 = ((noOfSampleRefPoints - 1) * noOfSampleRefPoints) / 2;
		int noOfBitSets = nChoose2 + ( noOfSampleRefPoints * ball_radii.length );

		TestContext tc = new TestContext(context);
		int querySize = (context == Context.colors || context == Context.nasa) ? tc
				.dataSize() / 10 : 1000;
		tc.setSizes(querySize, noOfSampleRefPoints);
		List<CartesianPoint> dat = tc.getData();

		int a_hundredth = dat.size() / 100;

		List<CartesianPoint> sample_dat = dat.subList(0,a_hundredth);

		List<CartesianPoint> refs = tc.getRefPoints();
		double threshold = tc.getThresholds()[0];

		List<CartesianPoint> queries = tc.getQueries();
		System.out.println("context\t" + context);
		System.out.println("data size\t" + dat.size());
		System.out.println("query size\t" + querySize);
		System.out.println("threshold\t" + threshold);
		System.out.println("sample refs size\t" + refs.size());
		System.out.println("sample no of bitsets\t" + noOfBitSets);

		// buildAndQueryVpt(tc);

		RefPointSet<CartesianPoint> rps = new RefPointSet<>(refs, tc.metric());

		List<ExclusionZone<CartesianPoint>> ezs = new ArrayList<>();

		addSheetExclusions(sample_dat, refs, rps, ezs, balanced, fourPoint);
		addBallExclusions(sample_dat, refs, rps, ezs, balanced);

		System.out.println("Sample ezs size:\t" + ezs.size());

		OpenBitSet[] datarep = new OpenBitSet[noOfBitSets];
		buildBitSetData(sample_dat, datarep, rps, ezs);

		EZInfo[] distance_sorted_ezs = analyseEZs( datarep, ezs );

		ezs = selectBestPivots( distance_sorted_ezs, refs, noOfRefPoints );

		// Just do it all again to see if it works - none of below works!

		nChoose2 = ((noOfRefPoints - 1) * noOfRefPoints) / 2;
		noOfBitSets = nChoose2 + ( noOfRefPoints * ball_radii.length );
		ezs = new ArrayList<>();
		rps = new RefPointSet<>(refs, tc.metric());

		addSheetExclusions(dat, refs, rps, ezs, balanced, fourPoint);
		addBallExclusions(dat, refs, rps, ezs, balanced);

		System.out.println("-------------------------------");
		System.out.println("ezs size:\t" + ezs.size());
		System.out.println("refs size\t" + refs.size());
		System.out.println("no of bitsets\t" + noOfBitSets);

		datarep = new OpenBitSet[noOfBitSets];
		buildBitSetData(dat, datarep, rps, ezs);

		// End of re-initialisation following pivot selection


//		rps = new RefPointSet<>(refs, tc.metric()); // rebuild rps after pivot selection
//		noOfBitSets = ezs.size();
//		datarep = new OpenBitSet[noOfBitSets];
//		buildBitSetData(dat, datarep, rps, ezs);

		queryBitSetData(queries, dat, tc.metric(), threshold, datarep, rps, ezs, noOfRefPoints);

	}

	/**
	 *
	 * @param distance_sorted_ezs
	 * @param refs - the old refs - gets cleared by this method
	 * @param desiredNumber - the number of refs required
	 * @return a new EZs list
	 * The complexity of this is linear with the number of
	 */
	private static  List<ExclusionZone<CartesianPoint>> selectBestPivots(EZInfo[] distance_sorted_ezs, List<CartesianPoint> refs, int desiredNumber) {
		List<ExclusionZone<CartesianPoint>> new_ezs = new ArrayList<>();
		List<CartesianPoint> new_refs = new ArrayList<>();

		int index = 0;
		while( new_refs.size() < desiredNumber && index < distance_sorted_ezs.length ) {

			ExclusionZone<CartesianPoint> next_ez = distance_sorted_ezs[index++].ez;

			if (next_ez instanceof BallExclusion) {

				BallExclusion<CartesianPoint> ball = (BallExclusion<CartesianPoint>) next_ez;
				CartesianPoint next_ref = ball.getPointSet().refs.get(ball.getIndex());

				if( ! new_refs.contains( next_ref ) ) {
					new_refs.add(next_ref);
				}
				new_ezs.add(next_ez);

			} else if (next_ez instanceof SheetExclusion) {

				SheetExclusion<CartesianPoint> sheet = (SheetExclusion<CartesianPoint>) next_ez;
				CartesianPoint next_ref1 = sheet.getPointSet().refs.get(sheet.getRef1());
				if (!new_refs.contains(next_ref1)) {
					refs.add(next_ref1);
				}
				CartesianPoint next_ref2 = sheet.getPointSet().refs.get(sheet.getRef2());
				if (!new_refs.contains(next_ref2)) {
					new_refs.add(next_ref2);
				}
				new_ezs.add(next_ez);
			}
		}

		// We have enough refs - now add all the EZs that make reference to those refs in the refs list.

		while( index < distance_sorted_ezs.length ) {
			ExclusionZone<CartesianPoint> next_ez = distance_sorted_ezs[index++].ez;
			if (next_ez instanceof BallExclusion) {

				BallExclusion<CartesianPoint> ball = (BallExclusion<CartesianPoint>) next_ez;
				CartesianPoint next_ref = ball.getPointSet().refs.get(ball.getIndex());

				if (new_refs.contains(next_ref)) { // already added this reference point
					new_ezs.add(next_ez);            // the reference point is already chosen - get this ball for free - add it if not already in
				}
			} else if (next_ez instanceof SheetExclusion) {

				SheetExclusion<CartesianPoint> sheet = (SheetExclusion<CartesianPoint>) next_ez;
				CartesianPoint next_ref1 = sheet.getPointSet().refs.get(sheet.getRef1());
				CartesianPoint next_ref2 = sheet.getPointSet().refs.get(sheet.getRef2());

				if ( new_refs.contains( next_ref1 ) && new_refs.contains( next_ref2 ) ) { // already added these reference points
					new_ezs.add(next_ez);   // both the reference points are already chosen - get this sheet for free - add it if not already in
				}
			}
		}

		refs.clear();			// hacky but easiest way...
		refs.addAll(new_refs);
		return new_ezs;
	}

	public static void addBallExclusions(List<CartesianPoint> dat,
                                         List<CartesianPoint> refs, RefPointSet<CartesianPoint> rps,
                                         List<ExclusionZone<CartesianPoint>> ezs, boolean balanced) {
		for (int i = 0; i < refs.size(); i++) {
			List<BallExclusion<CartesianPoint>> balls = new ArrayList<>();
			for (double radius : ball_radii) {
				BallExclusion<CartesianPoint> be = new BallExclusion<>(rps, i,
						radius);
				balls.add(be);
			}
			if (balanced) {
				balls.get(0).setWitnesses(dat.subList(0, 1000));
				double midRadius = balls.get(0).getRadius();
				double thisRadius = midRadius
						- ((balls.size() / 2) * RADIUS_INCREMENT);
				for (int ball = 0; ball < balls.size(); ball++) {
					balls.get(ball).setRadius(thisRadius);
					thisRadius += RADIUS_INCREMENT;
				}
			}
			ezs.addAll(balls);
		}
	}

	public static void addSheetExclusions(List<CartesianPoint> dat,
                                          List<CartesianPoint> refs, RefPointSet<CartesianPoint> rps,
                                          List<ExclusionZone<CartesianPoint>> ezs, boolean balanced,
                                          boolean fourPoint) {
		for (int i = 0; i < refs.size() - 1; i++) {
			for (int j = i + 1; j < refs.size(); j++) {
				SheetExclusion<CartesianPoint> se;
				if( fourPoint ) {
					se = new SheetExclusion4p<>(rps, i, j);
				}  else {
					se = new SheetExclusion3p<>(rps, i, j);
				}
				if (balanced) {
					se.setWitnesses(dat.subList(0, 1000));
				}
				ezs.add(se);
			}
		}
	}

	private static void buildAndQueryVpt(TestContext tc) {

		List<CartesianPoint> data = tc.getData();
		data.addAll(tc.getRefPoints());

		CountedMetric<CartesianPoint> cm = new CountedMetric<>(tc.metric());
		VPTree<CartesianPoint> vpt = new VPTree<>(data, cm);
		cm.reset();
		final List<CartesianPoint> queries = tc.getQueries();
		final double t = tc.getThreshold();

		long t0 = System.currentTimeMillis();
		int noOfRes = 0;
		for (CartesianPoint q : queries) {
			List<CartesianPoint> res = vpt.thresholdSearch(q, t);
			noOfRes += res.size();
		}

		System.out.println("vpt");
		System.out.println("dists per query\t" + cm.reset() / queries.size());
		System.out.println("printResults\t" + noOfRes);
		System.out.println("time\t" + (System.currentTimeMillis() - t0)
				/ (float) queries.size());
	}

	public static <T> void buildBitSetData(List<T> data, OpenBitSet[] datarep,
										   RefPointSet<T> rps, List<ExclusionZone<T>> ezs) {
		int dataSize = data.size();
		for (int i = 0; i < datarep.length; i++) {
			datarep[i] = new OpenBitSet(dataSize);
		}
		for (int n = 0; n < dataSize; n++) {
			T p = data.get(n);
			double[] dists = rps.extDists(p);
			for (int x = 0; x < datarep.length; x++) {
				boolean isIn = ezs.get(x).isIn(dists);
				if (isIn) {
					datarep[x].set(n);
				}
			}
		}
	}

	protected static <T> OpenBitSet doExclusions(List<T> dat, double t,
												 OpenBitSet[] datarep, CountedMetric<T> cm, T q, final int dataSize,
												 List<Integer> mustBeIn, List<Integer> cantBeIn) {
		if (mustBeIn.size() != 0) {
			OpenBitSet ands = getAndOpenBitSets(datarep, dataSize, mustBeIn);
			if (cantBeIn.size() != 0) {
				/*
				 * hopefully the normal situation or we're in trouble!
				 */
				OpenBitSet nots = getOrOpenBitSets(datarep, dataSize, cantBeIn);
				nots.flip(0, dataSize);
				ands.and(nots);
				return ands;
				// filterContenders(dat, t, cm, q, res, dataSize, ands);
			} else {
				// there are no cantBeIn partitions
				return ands;
				// filterContenders(dat, t, cm, q, res, dataSize, ands);
			}
		} else {
			// there are no mustBeIn partitions
			if (cantBeIn.size() != 0) {
				OpenBitSet nots = getOrOpenBitSets(datarep, dataSize, cantBeIn);
				nots.flip(0, dataSize);
				return nots;
				// filterContenders(dat, t, cm, q, res, dataSize, nots);
			} else {
				// there are no exclusions at all...
				return null;
				// for (T d : dat) {
				// if (cm.distance(q, d) < t) {
				// res.add(d);
				// }
				// }
			}
		}
	}

	static <T> void filterContenders(List<T> dat, double t,
									 CountedMetric<T> cm, T q, Collection<T> res, final int dataSize,
									 OpenBitSet results) {

		// System.out.println( "Contenders size = " + printResults.cardinality());

		for (int i = results.nextSetBit(0); i != -1 && i < dataSize; i = results.nextSetBit(i + 1)) {
			// added i < dataSize since Cuda code overruns datasize in the -
			// don't know case - must be conservative and include, easier than
			// complex bit masking and shifting.
			if (results.get(i)) {
				if (cm.distance(q, dat.get(i)) <= t) {
					res.add(dat.get(i));
				}
			}
		}
	}

	@SuppressWarnings("boxing")
	static OpenBitSet getAndOpenBitSets(OpenBitSet[] datarep,
										final int dataSize, List<Integer> mustBeIn) {
		OpenBitSet ands = null;
		if (mustBeIn.size() != 0) {
			ands = datarep[mustBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < mustBeIn.size(); i++) {
				ands.and(datarep[mustBeIn.get(i)]);
			}
		}
		return ands;
	}

	@SuppressWarnings("boxing")
	static OpenBitSet getOrOpenBitSets(OpenBitSet[] datarep,
									   final int dataSize, List<Integer> cantBeIn) {
		OpenBitSet nots = null;
		if (cantBeIn.size() != 0) {
			nots = datarep[cantBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < cantBeIn.size(); i++) {
				final OpenBitSet nextNot = datarep[cantBeIn.get(i)];
				nots.or(nextNot);
			}
		}
		return nots;
	}


	@SuppressWarnings("boxing")
	static <T> void queryBitSetData(List<T> queries, List<T> dat,
									Metric<T> metric, double threshold, OpenBitSet[] datarep,
									RefPointSet<T> rps, List<ExclusionZone<T>> ezs, int noOfRefPoints) {

		List<List<Integer>> combined_mustBeIns = new ArrayList<>();
		List<List<Integer>> combined_cantBeIns = new ArrayList<>();

		int noOfResults = 0;
		int partitionsExcluded = 0;
		CountedMetric<T> cm = new CountedMetric<>(metric);
		long t0 = System.currentTimeMillis();
		for (T q : queries) {
			List<T> res = new ArrayList<>();
			double[] dists = rps.extDists(q, res, threshold);

			List<Integer> mustBeIn = new ArrayList<>();
			List<Integer> cantBeIn = new ArrayList<>();

			combined_mustBeIns.add(mustBeIn);
			combined_cantBeIns.add(cantBeIn);

			for (int i = 0; i < ezs.size(); i++) {
				ExclusionZone<T> ez = ezs.get(i);
				if (ez.mustBeOut( dists, threshold)) {
					mustBeIn.add(i);
				} else if (ez.mustBeOut( dists, threshold)) {
					cantBeIn.add(i);
				}
			}

			partitionsExcluded += cantBeIn.size() + mustBeIn.size();

			OpenBitSet inclusions = doExclusions(dat, threshold, datarep, cm,
					q, dat.size(), mustBeIn, cantBeIn);

			if (inclusions == null) {
				for (T d : dat) {
					if (cm.distance(q, d) <= threshold) {
						res.add(d);
					}
				}
			} else {
				filterContenders(dat, threshold, cm, q, res, dat.size(),
						inclusions);
			}

			noOfResults += res.size();
		}

		analyseInclusions(combined_mustBeIns, combined_cantBeIns, ezs.size());

		System.out.println("bitsets");
		System.out.println("dists per query\t"
				+ (cm.reset() / queries.size() + noOfRefPoints));
		System.out.println("printResults\t" + noOfResults);
		System.out.println("time\t" + (System.currentTimeMillis() - t0)
				/ (float) queries.size());
		System.out.println("partitions excluded\t"
				+ ((double) partitionsExcluded / queries.size()));
	}

	//*************** Analysis code below here ***************//

	private static EZInfo[] analyseEZs(OpenBitSet[] datarep, List<ExclusionZone<CartesianPoint>> ezs) {
		findInclusions( datarep );
		return findEzsSortedByInterEZDistance( datarep, ezs );
	}

	private static void findInclusions(OpenBitSet[] datarep) {
		List<Integer>[] subsets = new List[datarep.length];
		for( int i = 0; i < subsets.length; i++ ) {
			subsets[i] = new ArrayList<>();
		}

		for( int i = 0; i < datarep.length - 1; i++ ) {
			for( int j = i+1 ; j < datarep.length; j++ ) {
				checkOr( i,j,datarep,subsets );
				checkOr( j,i,datarep,subsets );
			}
		}
		// showSubsets( subsets );
	}

	// Run a competition to find the best Pivots
	private static void compete(OpenBitSet[] datarep, List<ExclusionZone<CartesianPoint>> ezs) {

		Jaccard jac = new Jaccard();

		List<Integer>[] subsets = new List[datarep.length];

		for( int i = 0; i < subsets.length; i++ ) {
			subsets[i] = new ArrayList<>();
		}

		for( int i = 0; i < datarep.length - 1; i++ ) {
			for( int j = i+1 ; j < datarep.length; j++ ) {

				double d = jac.distance(datarep[i], datarep[j]);

				if( firstSubsetOfSecond( i,j,datarep ) ) {

				} else if( firstSubsetOfSecond( j,i,datarep ) ) {

				} else if( d == 0 ) {     // No overlap between EZs

				} else { // there is overlap - biggest difference wins

				}


			}
		}



	}

	private static void showSubsets(List<Integer>[] subsets) {
		for( int i = 0; i < subsets.length; i++ ) {
			if( subsets[i].size() != 0 ) {
				System.out.print( ballOrSheet(i) + i + " is a subset " + subsets[i].size() + " times of: " );
				for( Integer ii : subsets[i] ) {
					if( ii - i < 5 && ii / 5 == i / 5 ) {
						System.out.print(" * ");
					} else {
						System.out.print(ballOrSheet(ii) + ii + " ");
					}
				}
				System.out.println();
			}
		}
		System.out.println();
	}

	/**
	 *
	 * @param datarep - a (subset) of the data to analyse
	 * @param ezs - a set of exclusion zones from which to find the most different.
	 * @return the EZs distance sorted by Jaccard distance of the columns.
	 * This is n2 in terms of the number of data items in the set.
	 */
	private static EZInfo[] findEzsSortedByInterEZDistance(OpenBitSet[] datarep, List<ExclusionZone<CartesianPoint>> ezs) {
		double[] average_distances = new double[datarep.length];
		Jaccard jac = new Jaccard();

		for( int i = 0; i < datarep.length; i++ ) {
			double average = 0.0d;
			for (int j = 0; j < datarep.length; j++) {
				double d = jac.distance(datarep[i], datarep[j]);
				average += d;
			}
			average_distances[i] = average / datarep.length;
		}
		EZInfo[] sorted = sortEzsByAverageDistance(average_distances, ezs);
		// showSortedEZs( sorted );
		return sorted;
	}

	private static void showSortedEZs(EZInfo[] sorted) {
		for( int i = 0; i < sorted.length; i++ ) {
			ExclusionZone<CartesianPoint> ez = sorted[i].ez;
			double distance = sorted[i].average_distance;
			int position = sorted[i].position;

			System.out.print( "Average Distance = " + distance + " ez ref = " + ballOrSheet(position) + " " + position ) ;

			if( ez instanceof BallExclusion) {
				BallExclusion ball = (BallExclusion) ez;
				System.out.println( " ref = " + ball.getIndex() + " Ro = " + ball.getPointSet().refs.get( ball.getIndex()) );
			}
			if( ez instanceof SheetExclusion ) {
				SheetExclusion sheet = (SheetExclusion) ez;
				System.out.println( " ref1 = " + sheet.getRef1() + " ref2 = " + sheet.getRef2() + " Ro1= " + sheet.getPointSet().refs.get( sheet.getRef1() ) +
						" Ro2= " + sheet.getPointSet().refs.get( sheet.getRef2() ));
			}

		}
	}

	private static EZInfo[] sortEzsByAverageDistance(double[] average_distances, List<ExclusionZone<CartesianPoint>> ezs) {
		double[] unsorted = average_distances.clone();
		EZInfo[] sorted_by_distance = new EZInfo[average_distances.length];
		sort(average_distances);

		for( int i = 0; i < average_distances.length; i++ ) {
			double value = average_distances[i];
			int position = firstIndexOf(value, unsorted);
			ExclusionZone<CartesianPoint> ez = ezs.get(position);
			sorted_by_distance[i] = new EZInfo( position,value,ez );
		}
		return sorted_by_distance;
	}

	public static int firstIndexOf(double target, double[] araii ) {
		for (int i=0; i<araii.length; i++) {
			if (araii[i] == target ) {
				return i;
			}
		}
		return -1;
	}

	private static boolean firstSubsetOfSecond(int i, int j, OpenBitSet[] datarep) {
		OpenBitSet copy = datarep[i].get(0, datarep[i].length());
		copy.or(datarep[j]);
		return copy.cardinality() == datarep[i].cardinality();
	}

	private static void checkOr(int i, int j, OpenBitSet[] datarep, List<Integer>[] subsets) {
		OpenBitSet copy = datarep[i].get(0, datarep[i].length());
		copy.or(datarep[j]);
		if( copy.cardinality() == datarep[i].cardinality() ) {
			subsets[j].add(i);
		}
	}

	private static String ballOrSheet(int i) {
		return i < nChoose2 ? " sheet " : " ball ";
	}

	private static void analyseInclusions(List<List<Integer>> combined_mustBeIns, List<List<Integer>> combined_cantBeIns, int size) {

		printList("Mustbeins", combined_mustBeIns, size);
		printList("Catbeins", combined_cantBeIns, size);
		showCombined(combined_mustBeIns, combined_cantBeIns, size);
	}

	private static void showCombined(List<List<Integer>> combined_mustBeIns, List<List<Integer>> combined_cantBeIns, int size) {
		int[] hits = new int[size];
		calculate_hits(combined_mustBeIns, 0, size -1 , hits);
		calculate_hits(combined_cantBeIns, 0, size -1, hits);
		printResults("Combined contribution",hits);
	}

	private static void printList(String name, List<List<Integer>> combined_lists, int size) {

		printList(name + " (all)", combined_lists, size, 0, size - 1);
		printList(name + " (sheets)", combined_lists, nChoose2, 0, nChoose2 - 1);
		printList(name + " (balls)", combined_lists, size - nChoose2, nChoose2, size - 1);

	}

	private static void printList(String name, List<List<Integer>> combined_lists, int size, int range_start, int range_end) {

		int[] hits = new int[size];
		calculate_hits(combined_lists, range_start, range_end, hits);
		printResults(name,hits);
	}

	private static void calculate_hits(List<List<Integer>> combined_lists, int range_start, int range_end, int[] hits) {
		for (List<Integer> list : combined_lists) {
			for (Integer inclusion : list) {
				if (inclusion >= range_start && inclusion <= range_end) {
					hits[inclusion - range_start]++;
				}
			}
		}
	}

	private static void printResults(String name, int[] hits ) {

		System.out.println( "Number of hits for " + name + " per exclusion zone:");
		for (int i = 0; i < hits.length; i++) {
			System.out.print(i + ":" + hits[i] + " ");
			if (i != 0 && i % 20 == 0) {
				System.out.println();
			}
		}
		System.out.println();
		double mean = mean( hits );
		double sd = stddev( hits, mean );
		System.out.println( "min = " + min( hits) + " max = " + max( hits) + " mean = " + mean + " sd = " + sd );
		System.out.println();
	}

	public static int max( int[] arrai ) {
		int result = Integer.MIN_VALUE;
		for( int i : arrai ) {
			if( i > result ) {
				result = i;
			}
		}
		return result;
	}

	public static int min( int[] arrai ) {
		int result = Integer.MAX_VALUE;
		for( int i : arrai ) {
			if( i < result ) {
				result = i;
			}
		}
		return result;
	}

	public static double mean( int[] arrai ) {
		int result = 0;
		for( int i : arrai ) {
				result = result + i;
		}
		return ((double) result) / arrai.length;
	}

	public static double stddev( int[] arrai, double mean ) {
		double sd = 0;
		for( int i : arrai ) {
			sd += Math.pow(i - mean, 2);
		}
		return Math.sqrt( sd / arrai.length );
	}
}
