package uk.al_richard.metricbitblaster.experimental.al;

import coreConcepts.DataSet;
import coreConcepts.MetricSpace;
import dataPoints.cartesian.CartesianPoint;
import histogram.MetricHistogram;
import testloads.TestContext;
import testloads.TestContext.Context;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import static uk.al_richard.metricbitblaster.util.Filename.getFileName;

public class MainRunnerFileReduceIdim extends MainRunnerFilePavelPivot {


    public static void main(String[] args) throws Exception {
        Context context = Context.euc30;

        boolean fourPoint = true;
        boolean balanced = true;

        int querySize = 100;

        doExperiments(context, querySize, fourPoint, balanced);
    }

    public static void doExperiments(Context context, int querySize, boolean fourPoint, boolean balanced) throws Exception {

        System.out.println("Date/time\t" + new Date().toString() );
        System.out.println("Class\t" + getFileName() );

        TestContext tc = new TestContext(context);

        tc.setSizes(0, 0);

        List<CartesianPoint> dat = tc.getData().subList(0,1000);

        int reduced_by = 10;

        lowerDimensions( dat, 10 );

        Random r = new Random(1234567);

        DataSet ds = new DataSet() {
            @Override
            public boolean isFinite() {
                return true;
            }

            @Override
            public Object randomValue() {
                return dat.get( r.nextInt(dat.size()));
            }

            @Override
            public String getDataSetName() {
                return tc.toString();
            }

            @Override
            public String getDataSetShortName() {
                return tc.toString();
            }

            @Override
            public int size() {
                return dat.size();
            }

            @Override
            public Iterator iterator() {
                return dat.iterator();
            }
        };

        MetricSpace<CartesianPoint> ms = new MetricSpace<CartesianPoint>(ds, tc.metric());

        MetricHistogram<CartesianPoint> mhist = new MetricHistogram<CartesianPoint>( ms, 100, 10000, true, false, 2.0 );

        System.out.println( "Reduced by " + reduced_by +" averageIDIM of " + context.name() + " is " + mhist.getHistogramInfo().idim );

    }

    public static void lowerDimensions(List<CartesianPoint> dat, int i) {
        for( CartesianPoint point : dat ) {
            double[] data = point.getPoint();
            for( int index = 0; index < i; index++ ) {
                data[index] = 0;
            }
        }
    }
}
