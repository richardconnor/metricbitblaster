package uk.al_richard.metricbitblaster.experimental.al;

import coreConcepts.CountedMetric;
import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;
import dataPoints.cartesian.Euclidean;
import testloads.TestContext;
import testloads.TestContext.Context;
import uk.al_richard.metricbitblaster.referenceImplementation.ExclusionZone;
import uk.al_richard.metricbitblaster.referenceImplementation.RefPointSet;
import uk.al_richard.metricbitblaster.util.OpenBitSet;

import java.util.ArrayList;
import java.util.List;

public class MainRunnerFileSISAPColorsAndNasaForPaper extends MainRunnerFile {

	private static final Metric<CartesianPoint> metric = new Euclidean<>();

	static final Context nasa = Context.nasa;
	static final Context colors = Context.colors;

	public static void main(String[] args) throws Exception {

		do4Combinations();

	}

	private static void do4Combinations() throws Exception {

		do1Combination(true, true);
		do1Combination(false, true);
		do1Combination(true, false);
		do1Combination(false, false);
	}

	private static void do1Combination(boolean balanced, boolean fourPoint) throws Exception {
		System.out
				.println("Context\tData size\tthreshold    \tno. queries\tno ros\tno parts\tparts exluded\tdistances/q\tms/query\tphase1/q\tphase2/q\tphase3/q\tno unfiltered results\tno results\tfour_point="+fourPoint+"\tbalanced="+balanced);

		doExperiments(nasa, "nasa", balanced, fourPoint);
		doExperiments(colors, "colors", balanced, fourPoint);
	}

	private static void doExperiments(Context context, String context_name, boolean balanced, boolean fourPoint)
			throws Exception {

		for (int noOfRefPoints = 10; noOfRefPoints < 65; noOfRefPoints += 5) {
			int nChoose2 = ((noOfRefPoints - 1) * noOfRefPoints) / 2;
			int noOfBitSets = nChoose2 + (noOfRefPoints * MainRunnerFile.ball_radii.length);

            TestContext tc = new TestContext(context);
            int query_size = tc.dataSize() / 10;
            tc.setSizes(query_size, noOfRefPoints);
            List<CartesianPoint> queries = tc.getQueries();
            List<CartesianPoint> data = tc.getData();
            double[] thresholds = tc.getThresholds();
			List<CartesianPoint> refs = tc.getRefPoints();

			String print = context_name + "\t" + data.size() + "\t" + "XXX"
					+ "\t" + queries.size() + "\t" + refs.size() + "\t"
					+ noOfBitSets;

			doExperiment(queries, thresholds, noOfRefPoints, refs, data,
					noOfBitSets, print, balanced, fourPoint);

		}
	}

	private static void doExperiment(List<CartesianPoint> queries,
			double[] thresholds, int noOfRefPoints, List<CartesianPoint> refs,
			List<CartesianPoint> data, int noOfBitSets, String print, boolean balanced, boolean fourPoint) {

		RefPointSet<CartesianPoint> rps = new RefPointSet<>(refs, metric);
		List<ExclusionZone<CartesianPoint>> ezs = new ArrayList<>();

		MainRunnerFile.addSheetExclusions(data, refs, rps, ezs, balanced, fourPoint);
		MainRunnerFile.addBallExclusions(data, refs, rps, ezs, balanced);

		OpenBitSet[] datarep = new OpenBitSet[noOfBitSets];
		MainRunnerFile.buildBitSetData(data, datarep, rps, ezs);
		for (double t : thresholds) {
			System.out.print(print.replace("XXX", "" + t));
			queryBitSetData(queries, data, metric, t, datarep, rps, ezs,
					noOfRefPoints);
		}

	}

	@SuppressWarnings("boxing")
	public static <T> void queryBitSetData(List<T> queries, List<T> dat,
                                           Metric<T> metric, double threshold, OpenBitSet[] datarep,
                                           RefPointSet<T> rps, List<ExclusionZone<T>> ezs, int noOfRefPoints) {

		int noOfResults = 0;
		int noOfUnfliteredResults = 0;
		int partitionsExcluded = 0;
		int distance_calcs = 0;
		long t0 = System.currentTimeMillis();
		long phase1_time = 0;
		long phase2_time = 0;
		long phase3_time = 0;

        CountedMetric<T> cm = new CountedMetric<>(metric);

		for (T q : queries) {
			List<T> res = new ArrayList<>();

			long start = System.currentTimeMillis();
			double[] dists = rps.extDists(q, res, threshold);
			long end = System.currentTimeMillis();
			phase1_time += end - start;
			start = end;

			List<Integer> mustBeIn = new ArrayList<>();
			List<Integer> cantBeIn = new ArrayList<>();

			for (int i = 0; i < ezs.size(); i++) {
				ExclusionZone<T> ez = ezs.get(i);
				if (ez.mustBeOut( dists, threshold)) {
					mustBeIn.add(i);
				} else if (ez.mustBeOut( dists, threshold)) {
					cantBeIn.add(i);
				}
			}

			partitionsExcluded += cantBeIn.size() + mustBeIn.size();
			OpenBitSet inclusions = MainRunnerFile.doExclusions(dat, threshold, datarep, cm, q, dat.size(), mustBeIn, cantBeIn);

			end = System.currentTimeMillis();
			phase2_time += end - start;
			start = end;

            if( inclusions == null ) {
                noOfUnfliteredResults += dat.size();
                for (T d : dat) {
                    if (cm.distance(q, d) <= threshold) {
                        res.add(d);
                    }
                }
            } else {
                noOfUnfliteredResults += inclusions.cardinality();
                filterContenders(dat, threshold, cm, q, res, dat.size(), inclusions);
            }

			end = System.currentTimeMillis();
			phase3_time += end - start;

			noOfResults += res.size();
			distance_calcs += cm.reset() + noOfRefPoints;
		}

		System.out.println("\t"
				+ ((double) partitionsExcluded / queries.size()) + "\t"
				+ (distance_calcs / (float) queries.size()) + "\t"
				+ ((System.currentTimeMillis() - t0) / (float) queries.size()) + "\t"
				+ phase1_time / (float) queries.size() + "\t"
				+ phase2_time / (float) queries.size() + "\t"
				+ phase3_time / (float) queries.size() + "\t"
				+ noOfUnfliteredResults + "\t"
                + noOfResults
        );

	}
}
