package uk.al_richard.metricbitblaster.experimental.al;

import coreConcepts.CountedMetric;
import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;
import testloads.TestContext;
import testloads.TestContext.Context;
import uk.al_richard.metricbitblaster.referenceImplementation.ExclusionZone;
import uk.al_richard.metricbitblaster.referenceImplementation.RefPointSet;
import uk.al_richard.metricbitblaster.util.HeuristicCorrelationSubsetSearch;
import uk.al_richard.metricbitblaster.util.OpenBitSet;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static uk.al_richard.metricbitblaster.experimental.al.IDIM.IDIM;
import static uk.al_richard.metricbitblaster.experimental.al.MainRunnerFileReduceIdim.lowerDimensions;
import static uk.al_richard.metricbitblaster.util.Filename.getFileName;

/**
 * This code takes the Euc10 data set and knocks out columns of it to (hopefully) reduce the dimension of teh data set.
 * This does reduce the dimension of the data sets (measured using u2 / 2 sigma2 ).
 * It is unclear if this is the best way of doing this.
 */
public class MainRunnerFilePavelPivotExploreReducedDim extends MainRunnerFilePavelPivot {


    public static void main(String[] args) throws Exception {
        Context context = Context.euc10;

        boolean fourPoint = true;
        boolean balanced = true;

        int querySize = 100;

        doExperiments(context, querySize, fourPoint, balanced);
    }

    public static void doExperiments(Context context, int querySize, boolean fourPoint, boolean balanced) throws Exception {

        System.out.println("Date/time\t" + new Date().toString() );
        System.out.println("Class\t" + getFileName() );

        System.out.println("context\tidim\t4point\tbalanced\tdata_size\tquery_size\tthreshold\tsample_refs_size\tdesired_refs_size\tactual_refs_size\tdistances_per_query\t_partitions_excluded" );


        int no_sample_ref_points = 170;

        for( int reduce_by = 0; reduce_by < 5; reduce_by++ ) {
            for( int no_desired_ref_points = 10; no_desired_ref_points < 100 ; no_desired_ref_points+= 10 ) {

                if( no_desired_ref_points <= no_sample_ref_points ) {
                    doExperiment(context, reduce_by, querySize, no_sample_ref_points, no_desired_ref_points, fourPoint, balanced);
                }
            }
        }
    }


    public static void doExperiment(Context context, int reduced_by, int querySize, int no_sample_ref_points, int no_desired_ref_points, boolean fourPoint, boolean balanced) throws Exception {

        TestContext tc = new TestContext(context);

        int nChoose2 = ((no_sample_ref_points - 1) * no_sample_ref_points) / 2;
        int noOfBitSets = nChoose2 + (no_sample_ref_points * ball_radii.length);

        tc.setSizes(querySize, no_sample_ref_points);

        List<CartesianPoint> dat = tc.getData();

        int sample_size = ( dat.size() / 100 ) < 1000 ? 1000 : dat.size() / 100;

        List<CartesianPoint> sample_dat = dat.subList(0,sample_size);

        lowerDimensions( sample_dat, reduced_by );

        String name = context + "-" + reduced_by;

        double idim = IDIM( sample_dat, context, tc.metric() );

        List<CartesianPoint> refs = tc.getRefPoints();

        double threshold = tc.getThresholds()[0];

        List<CartesianPoint> queries = tc.getQueries();

        System.out.print( name + "\t" + String.format( "%.2f", idim ) + "\t"  + fourPoint + "\t" + balanced + "\t" + dat.size() + "\t" + querySize + "\t" + threshold + "\t" + no_sample_ref_points + "\t" + no_desired_ref_points + "\t" );

        RefPointSet<CartesianPoint> rps;

        if( no_desired_ref_points < no_sample_ref_points ) {

            rps = new RefPointSet<>(refs, tc.metric());

            List<ExclusionZone<CartesianPoint>> sheet_ezs = new ArrayList<>();
            List<ExclusionZone<CartesianPoint>> ball_ezs = new ArrayList<>();

            addSheetExclusions(sample_dat, refs, rps, sheet_ezs, balanced, fourPoint);
            addBallExclusions(sample_dat, refs, rps, ball_ezs, balanced);

            OpenBitSet[] sheet_datarep = new OpenBitSet[nChoose2];
            OpenBitSet[] ball_datarep = new OpenBitSet[no_sample_ref_points * ball_radii.length];

            buildBitSetData(sample_dat, sheet_datarep, rps, sheet_ezs);
            buildBitSetData(sample_dat, ball_datarep, rps, ball_ezs);

            HeuristicCorrelationSubsetSearch select_algorithm = new HeuristicCorrelationSubsetSearch();

            List<Integer> uncorrelated_ball_ez_indices = select_algorithm.selectLowCorrelatedSubset(ball_datarep, sample_size, no_desired_ref_points);  // TODO How to determine size?
            List<Integer> uncorrelated_sheet_ez_indices = select_algorithm.selectLowCorrelatedSubset(sheet_datarep, sample_size, no_desired_ref_points);  // TODO How to determine size?

            List<CartesianPoint> sheet_refs = selectRefs(sheet_ezs, uncorrelated_sheet_ez_indices, no_desired_ref_points);
            List<CartesianPoint> ball_refs = selectRefs(ball_ezs, uncorrelated_ball_ez_indices, no_desired_ref_points);

            refs = collectRefs(sheet_refs, ball_refs);
        }

        no_desired_ref_points = refs.size();

        System.out.print( no_desired_ref_points + "\t" ); // actual_refs_size

        // just do it all again to see if it works

        nChoose2 = ((no_desired_ref_points - 1) * no_desired_ref_points) / 2;
        noOfBitSets = nChoose2 + ( no_desired_ref_points * ball_radii.length );
        ArrayList<ExclusionZone<CartesianPoint>> ezs = new ArrayList<>();
        rps = new RefPointSet<>(refs, tc.metric());

        addSheetExclusions(dat, refs, rps, ezs, balanced, fourPoint);
        addBallExclusions(dat, refs, rps, ezs, balanced);

        OpenBitSet[] datarep = new OpenBitSet[noOfBitSets];
        buildBitSetData(dat, datarep, rps, ezs);

        // End of re-initialisation following pivot selection

        queryBitSetData(queries, dat, tc.metric(), threshold, datarep, rps, ezs, no_desired_ref_points);

    }

    // This is the same as the code in the file from which this inherits apart from the diagnostic
    // (true at time of writing this).
    @SuppressWarnings("boxing")
    static <T> void queryBitSetData(List<T> queries, List<T> dat,
                                    Metric<T> metric, double threshold, OpenBitSet[] datarep,
                                    RefPointSet<T> rps, List<ExclusionZone<T>> ezs, int noOfRefPoints) {

        List<List<Integer>> combined_mustBeIns = new ArrayList<>();
        List<List<Integer>> combined_cantBeIns = new ArrayList<>();

        int noOfResults = 0;
        int partitionsExcluded = 0;
        CountedMetric<T> cm = new CountedMetric<>(metric);
        long t0 = System.currentTimeMillis();
        for (T q : queries) {
            List<T> res = new ArrayList<>();
            double[] dists = rps.extDists(q, res, threshold);

            List<Integer> mustBeIn = new ArrayList<>();
            List<Integer> cantBeIn = new ArrayList<>();

            combined_mustBeIns.add(mustBeIn);
            combined_cantBeIns.add(cantBeIn);

            for (int i = 0; i < ezs.size(); i++) {
                ExclusionZone<T> ez = ezs.get(i);
                if (ez.mustBeOut( dists, threshold)) {
                    mustBeIn.add(i);
                } else if (ez.mustBeOut( dists, threshold)) {
                    cantBeIn.add(i);
                }
            }

            partitionsExcluded += cantBeIn.size() + mustBeIn.size();

            OpenBitSet inclusions = doExclusions(dat, threshold, datarep, cm,
                    q, dat.size(), mustBeIn, cantBeIn);

            if (inclusions == null) {
                for (T d : dat) {
                    if (cm.distance(q, d) <= threshold) {
                        res.add(d);
                    }
                }
            } else {
                filterContenders(dat, threshold, cm, q, res, dat.size(),
                        inclusions);
            }

            noOfResults += res.size();
        }

        System.out.print( (cm.reset() / queries.size() + noOfRefPoints) + "\t" );   // distances per query
        System.out.println( ((double) partitionsExcluded / queries.size()) );         // partitions excluded
    }


}
