package uk.al_richard.metricbitblaster.experimental.al;

import coreConcepts.DataSet;
import coreConcepts.Metric;
import coreConcepts.MetricSpace;
import dataPoints.cartesian.CartesianPoint;
import histogram.MetricHistogram;
import testloads.TestContext;
import testloads.TestContext.Context;
import uk.al_richard.metricbitblaster.production.DistanceExponent;

import java.util.*;

import static uk.al_richard.metricbitblaster.util.Filename.getFileName;

public class IDIM extends MainRunnerFilePavelPivot {

    public static void main(String[] args) throws Exception {
        Context[] contexts = new Context[]{ Context.colors, Context.nasa, Context.euc10, Context.euc20, Context.euc30 };

        System.out.println("Date/time\t" + new Date().toString() );
        System.out.println("Class\t" + getFileName() );

        for( Context context : contexts ) {

            TestContext tc = new TestContext(context);

            List<CartesianPoint> dat = tc.getData().subList(0, 5000);

            ChavezIDIM(dat, context, tc.metric());
            double idim = IDIM(dat,context,tc.metric());
            System.out.println("Simple Chavez IDIM of " + context.name() + " is " + idim );
            DEidim(dat,tc.metric(),context, 10);
        }

    }

    public static void DEidim(List<CartesianPoint> dat, Metric<CartesianPoint> metric, Context context, int num_samples) throws Exception {

        List<Double> de_data = new ArrayList<>();

        for( int i = 0; i < num_samples; i++ ) {
            DistanceExponent<CartesianPoint> de = new DistanceExponent<>(metric::distance, dat);  // each has random pivots

            de_data.add( de.IDIM() );
        }
        System.out.println("(Distance exponent) IDIM of " + context.name() + " is " + calculateMean(de_data) );

    }

    private static double calculateMean(List<Double> de_data) {
        double sum = 0;
        for( int i = 0; i < de_data.size(); i++ ) {
            sum = sum + de_data.get(i);
        }
        return sum / de_data.size();
    }


    public static void ChavezIDIM(List<CartesianPoint> dat, Context context, Metric<CartesianPoint> metric) throws Exception {

        Random r = new Random(1234567);

        DataSet ds = new DataSet() {
            @Override
            public boolean isFinite() {
                return true;
            }

            @Override
            public Object randomValue() {
                return dat.get( r.nextInt(dat.size()));
            }

            @Override
            public String getDataSetName() {
                return context.name();
            }

            @Override
            public String getDataSetShortName() {
                return context.name();
            }

            @Override
            public int size() {
                return dat.size();
            }

            @Override
            public Iterator iterator() {
                return dat.iterator();
            }
        };

        MetricSpace<CartesianPoint> ms = new MetricSpace<CartesianPoint>(ds, metric);

        MetricHistogram<CartesianPoint> mhist = new MetricHistogram<CartesianPoint>( ms, 5000, 10000, true, false, 5.0 );
        System.out.println("Mean of " + context.name() + " is " + mhist.getMeanDistance() );
        System.out.println("Variance of " + context.name() + " is " + mhist.getVariance() );
        System.out.println("(Chavez/Metric Histogram code) IDIM of " + context.name() + " is " + mhist.getHistogramInfo().idim);
    }

    public static double IDIM(List<CartesianPoint> dat, Context context, Metric<CartesianPoint> metric) throws Exception {
        if( dat.size() > 5000 ) {
            dat = dat.subList(0,5000);
        }
        ArrayList<Double> dists = new ArrayList<>();
        for( CartesianPoint p1 : dat ) {
            for( CartesianPoint p2 : dat ) {
                if( ! p1.equals(p2) ) {
                    dists.add( metric.distance(p1,p2) );
                }
            }
        }
        Double[] array_dists = dists.toArray(new Double[dists.size()]);
        double mn = mean( array_dists );
        double sd = stddev( array_dists, mn );

        double idim = (mn * mn) / ( 2 * sd * sd );

        return idim;
    }

    public static double mean(Double[] arrai) {
        double result = 0;
        for (double i : arrai) {
            result = result + i;
        }
        return result / arrai.length;
    }

    public static double stddev(Double[] arrai, double mean) {
        double sd = 0;
        for (double i : arrai) {
            sd += Math.pow(i - mean, 2);
        }
        return Math.sqrt( sd / arrai.length );
    }
}
