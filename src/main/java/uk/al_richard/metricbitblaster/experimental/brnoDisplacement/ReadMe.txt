Files here

1. MainRunnerDisplacement28May and findPivots28May

    These versions try and find uncorrelated pivots following discussion after Richard's Seminar in Brno.
    They try and find first point clouds with high SD of X displacement off the hyperplane.
    Then they find the least correlated pivot pairs by comparing the sum of Pearson correlation of:
           pivot pair 1 against pivot pair 2.. max_pivots
           pivot pair 2 against pivot pair 1,3..max_pivots etc.

    This version was pulled back from Git history following failure of attempt 2 below.


2. MainRunnerDisplacement and findPivots:

    These try to do the same as above but use multiple correlations (see definitions in Wikipedia and in files).
    These do not work - the matrix has a determinant of zero.

3. MainRunnerDisplacementLuciaPivots and findPivotsLuciaPivots

    This version follows a suggestion by Lucia:
        Try and find balls that have a high SD in radius and also are dissimilar from each other.
        Use similar measures to 1 to try and find good balls.

    Q. Use the balls for the sheet exclusions too or keep them separate?

