package uk.al_richard.metricbitblaster.experimental.brnoDisplacement;

import coreConcepts.CountedMetric;
import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;
import testloads.TestContext;
import testloads.TestContext.Context;
import uk.al_richard.metricbitblaster.experimental.alRichardLucia.Pair;
import uk.al_richard.metricbitblaster.referenceImplementation.BallExclusion;
import uk.al_richard.metricbitblaster.referenceImplementation.ExclusionZone;
import uk.al_richard.metricbitblaster.referenceImplementation.RefPointSet;
import uk.al_richard.metricbitblaster.referenceImplementation.SheetExclusion4p;

import java.util.*;

import static uk.al_richard.metricbitblaster.experimental.brnoDisplacement.findPivots.choosePivots;
import static uk.al_richard.metricbitblaster.experimental.brnoDisplacement.findPivots.choosePivotsCoefDet;
import static uk.al_richard.metricbitblaster.experimental.brnoDisplacement.findPivots.choosePivotsNChoose2;

public class MainRunnerFileDisplacementPivots {

	protected static double[] ball_radii =  new double[] { 0.94, 1.535 }; // from Matlab for Euc20
	protected static double[] sheet_offsets =  new double[] { 0.3,-0.3 };
	private static int nChoose2;
	private static Random rand = new Random();
	private static List<Double> inter_pivot_distances = new ArrayList<>();

	public static void main(String[] args) throws Exception {

		Context context = Context.euc20;

		int num_refs =  120; // was 800

		final int num_points_to_choose_pivots_from = 800; //40000;  // the number of candidate points from which we choose pivots

		TestContext tc = new TestContext(context);
		int querySize = (context == Context.colors || context == Context.nasa) ? tc.dataSize() / 10 : 1000;
		tc.setSizes(querySize, num_refs);
		List<CartesianPoint> dat = tc.getData();
		List<CartesianPoint> queries = tc.getQueries();
		Metric<CartesianPoint> metric = tc.metric();

		List<CartesianPoint> refs = tc.getRefPoints();

		double threshold = tc.getThresholds()[0];

		int num_witnesses = 100;
		int num_sample_refs = 1000;
		int required_refs = 120;

		runExperiment(context, queries, metric, querySize, dat, refs, threshold, true, true );
		//pivotExperimentChoosePivotsSpecial( context, queries, metric, querySize, dat, num_witnesses, num_sample_refs, required_refs, threshold);
		pivotExperimentNChoose2( context, queries, metric, querySize, dat, num_witnesses, num_sample_refs, required_refs, threshold);  // Got 10% improvement with this one.
		//pivotExperimentCoeffDep( context, queries, metric, querySize, dat, num_witnesses, num_sample_refs, required_refs, threshold);
	}

	private static void runExperiment(Context context, List<CartesianPoint> queries, Metric<CartesianPoint> metric, int querySize, List<CartesianPoint> dat, List<CartesianPoint> refs, double threshold, boolean balls, boolean sheets ) {

		int num_refs = refs.size();
		int noOfBitSets = 0;

		nChoose2 = ((num_refs - 1) * num_refs) / 2;
		if( ! sheets && ! balls ) {
			throw new RuntimeException( "One of sheets and balls must be true" );
		}
		if( sheets ) {
			noOfBitSets = (sheet_offsets.length * nChoose2);
		}
		if( balls ) {
			noOfBitSets = noOfBitSets + (num_refs * ball_radii.length );
		}

		System.out.println("----------- Running experiment ------------" );
		System.out.println("balls\t" + balls );
		System.out.println("sheets\t" + sheets );
		System.out.println("codebase\t" + "MainRunnerFileDisplacementPivots" );
		System.out.println("context\t" + context);
		System.out.println("data size\t" + dat.size());
		System.out.println("query size\t" + querySize);
		System.out.println("threshold\t" + threshold);
		System.out.println("num refs\t" + num_refs);
		System.out.println("sample no of bitsets\t" + noOfBitSets);

		RefPointSet<CartesianPoint> rps = new RefPointSet<>(refs, metric);

		List<ExclusionZone<CartesianPoint>> ezs = new ArrayList<>();

		if( sheets ) {
			addSheetExclusions(dat, refs, rps, ezs, sheet_offsets);
			System.out.println( "added sheets " + ezs.size() );
		}
		if( balls ) {
			addBallExclusions(dat, refs, rps, ezs);
			System.out.println("added balls " + ezs.size());
		}

		System.out.println("Sample ezs size:\t" + ezs.size());

		BitSet[] datarep = new BitSet[noOfBitSets];
		buildBitSetData( dat, datarep, rps, ezs );

		queryBitSetData( queries, dat, metric, threshold, datarep, rps, ezs, num_refs );
	}

	private static void runExperimentSpecial(Context context, List<CartesianPoint> queries, Metric<CartesianPoint> metric, int querySize, List<CartesianPoint> dat, List<Pair<CartesianPoint>> sheets, double threshold) {
		List<CartesianPoint> refs = getROsFromSheets( sheets );
		int num_refs = refs.size();

		int noOfBitSets = ( sheets.size() * sheet_offsets.length ) + ( num_refs * ball_radii.length );

		System.out.println("----------- Running JUST Supplied Sheets experiment ------------" );
		System.out.println("codebase\t" + "MainRunnerFileVectorHyperplanes" );
		System.out.println("context\t" + context);
		System.out.println("data size\t" + dat.size());
		System.out.println("query size\t" + querySize);
		System.out.println("threshold\t" + threshold);
		System.out.println("sample refs size\t" + num_refs);
		System.out.println("sample no of bitsets\t" + noOfBitSets);

		RefPointSet<CartesianPoint> rps = new RefPointSet<>(refs, metric);

		List<ExclusionZone<CartesianPoint>> ezs = new ArrayList<>();

		addExtentSheets( dat, refs, rps, ezs, sheet_offsets, sheets );
		System.out.println( "added sheets " + ezs.size() );
		addBallExclusions( dat, refs, rps, ezs );
		System.out.println( "added balls " + ezs.size() );

		System.out.println("Ezs size:\t" + ezs.size());

		BitSet[] datarep = new BitSet[noOfBitSets];
		buildBitSetData( dat, datarep, rps, ezs );

		// Run the queries with a random set of EZs
		queryBitSetData( queries, dat, metric, threshold, datarep, rps, ezs, num_refs );
	}

	private static void addExtentSheets(List<CartesianPoint> dat, List<CartesianPoint> refs, RefPointSet<CartesianPoint> rps, List<ExclusionZone<CartesianPoint>> ezs, double[] sheet_offsets, List<Pair<CartesianPoint>> sheets) {
		for( Pair<CartesianPoint> sheet : sheets ) {
				for( int z = 0; z < sheet_offsets.length; z++ ) {
					SheetExclusion4p<CartesianPoint> se = new SheetExclusion4p<CartesianPoint>(rps, refs.indexOf(sheet.getFirst()), refs.indexOf(sheet.getSecond()), sheet_offsets[z]);
					ezs.add(se);
				}
		}
	}

	private static List<CartesianPoint> getROsFromSheets(List<Pair<CartesianPoint>> sheets) {
		List<CartesianPoint> ros = new ArrayList<>();
		for( Pair<CartesianPoint> sheet : sheets ) {
			ros.add( sheet.getFirst() );
			ros.add( sheet.getSecond() );
		}
		return ros;
	}

	private static void pivotExperimentChoosePivotsSpecial(Context context, List<CartesianPoint> queries, Metric<CartesianPoint> metric, int querySize,
														   List<CartesianPoint> dat, int num_witnesses, int num_sample_refs, int required_refs, double threshold) throws Exception {


		TestContext tc = new TestContext(context);
		int queries_unused = 100;
		tc.setSizes(queries_unused, num_sample_refs);
		List<CartesianPoint> witnesses = tc.getData().subList(0,num_witnesses);
		List<CartesianPoint> refPoints = tc.getRefPoints();

		List<Pair<Pair<Integer>>> pivot_indices = choosePivots(witnesses, refPoints, metric, required_refs);

		List<Pair<CartesianPoint>> sheets = getSheets(pivot_indices, refPoints);

		runExperimentSpecial(context, queries, metric, querySize, dat, sheets, threshold);

	}


	private static void pivotExperimentNChoose2(Context context, List<CartesianPoint> queries, Metric<CartesianPoint> metric, int querySize,
												List<CartesianPoint> dat, int num_witnesses, int num_sample_refs, int required_refs, double threshold) throws Exception {

		System.out.println("----------- Running pivot experiment ------------" );
		System.out.println("num_sample_refs\t" + num_sample_refs );
		System.out.println("required_refs\t" + required_refs );
		System.out.println("witnesses\t" + num_witnesses );

		TestContext tc = new TestContext(context);
		int queries_unused = 100;
		tc.setSizes(queries_unused, num_sample_refs);
		List<CartesianPoint> witnesses = tc.getData().subList(0,num_witnesses);
		List<CartesianPoint> refPoints = tc.getRefPoints();

		List<CartesianPoint> pivots = choosePivotsNChoose2(witnesses, refPoints, metric, required_refs);

		runExperiment( context, queries, metric, querySize, dat, pivots, threshold, true, true );

	}


	private static void pivotExperimentCoeffDep(Context context, List<CartesianPoint> queries, Metric<CartesianPoint> metric, int querySize,
												List<CartesianPoint> dat, int num_witnesses, int num_sample_refs, int required_refs, double threshold) throws Exception {

		System.out.println("----------- Running Cooef of Det experiment ------------" );
		System.out.println("num_sample_refs\t" + num_sample_refs );
		System.out.println("required_refs\t" + required_refs );
		System.out.println("witnesses\t" + num_witnesses );

		TestContext tc = new TestContext(context);
		int queries_unused = 100;
		tc.setSizes(queries_unused, num_sample_refs);
		List<CartesianPoint> witnesses = tc.getData().subList(0,num_witnesses);
		List<CartesianPoint> refPoints = tc.getRefPoints();
		System.out.println("refPoints\t" + refPoints.size() );

		List<Pair<CartesianPoint>> sheets =  choosePivotsCoefDet(witnesses, refPoints, metric, num_sample_refs, required_refs);
		runExperimentSpecial( context, queries, metric, querySize, dat, sheets, threshold );
	}


		private static List<Pair<CartesianPoint>> getSheets(List<Pair<Pair<Integer>>> pivot_indices, List<CartesianPoint> refPoints) {
		List<Pair<CartesianPoint>> sheets = new ArrayList<>();
		for( Pair<Pair<Integer>> pair_indices : pivot_indices ) {
			sheets.add( new Pair<>( refPoints.get( pair_indices.getFirst().getFirst() ), refPoints.get( pair_indices.getFirst().getSecond() ) ) );
			sheets.add( new Pair<>( refPoints.get( pair_indices.getSecond().getFirst() ), refPoints.get( pair_indices.getSecond().getSecond() ) ) );
		}
		return sheets;
	}

	public static void addBallExclusions(List<CartesianPoint> dat, List<CartesianPoint> refs,
										 RefPointSet<CartesianPoint> rps, List<ExclusionZone<CartesianPoint>> ezs) {
		for (int i = 0; i < refs.size(); i++) {
			List<BallExclusion<CartesianPoint>> balls = new ArrayList<>();
			for (double radius : ball_radii) {
				BallExclusion<CartesianPoint> be = new BallExclusion<>(rps, i, radius);
				balls.add(be);
			}
			ezs.addAll(balls);
		}
	}

	public static void addSheetExclusions(List<CartesianPoint> dat,
										  List<CartesianPoint> refs, RefPointSet<CartesianPoint> rps,
										  List<ExclusionZone<CartesianPoint>> ezs, double[] sheet_offsets) {

		int count = 0;

		for (int i = 0; i < refs.size() - 1; i++) {
			for (int j = i + 1; j < refs.size(); j++) {
				for( int z = 0; z < sheet_offsets.length; z++ ) {
					SheetExclusion4p<CartesianPoint> se = new SheetExclusion4p<>(rps, i, j,sheet_offsets[z]);
					double inter_pivot_distance = se.getInterPivotDistance();
					if( inter_pivot_distance == 0 ) {
						System.out.println( "Zero IPD") ;
						System.out.println( "ref1: " + se.getRef1() );
						System.out.println( "ref2: " + se.getRef2() );
					}
					inter_pivot_distances.add( inter_pivot_distance );
					ezs.add(se);
					count += 2;
				}
			}
		}
		System.out.println( "Actually added:" + count + " sheets" );
	}

	public static <T> void buildBitSetData(List<T> data, BitSet[] datarep,
										   RefPointSet<T> rps, List<ExclusionZone<T>> ezs) {
		int dataSize = data.size();
		for (int i = 0; i < datarep.length; i++) {
			datarep[i] = new BitSet(dataSize);
		}
		for (int n = 0; n < dataSize; n++) {
			T p = data.get(n);
			double[] dists = rps.extDists(p);
			for (int x = 0; x < datarep.length; x++) {
				try {
					boolean isIn = ezs.get(x).isIn(dists);
					if (isIn) {
						datarep[x].set(n);
					}
				} catch(  RuntimeException e ) {
					// AL ***0986***
					// eat the runtime exception that is thrown if we get a nan
					// can't make a judgement.
				}
			}
		}
	}

	protected static <T> BitSet doExclusions(List<T> dat, double t,
			BitSet[] datarep, CountedMetric<T> cm, T q, final int dataSize,
			List<Integer> mustBeIn, List<Integer> cantBeIn) {
		if (mustBeIn.size() != 0) {
			BitSet ands = getAndBitSets(datarep, dataSize, mustBeIn);
			if (cantBeIn.size() != 0) {
				/*
				 * hopefully the normal situation or we're in trouble!
				 */
				BitSet nots = getOrBitSets(datarep, dataSize, cantBeIn);
				nots.flip(0, dataSize);
				ands.and(nots);
				return ands;
				// filterContenders(dat, t, cm, q, res, dataSize, ands);
			} else {
				// there are no cantBeIn partitions
				return ands;
				// filterContenders(dat, t, cm, q, res, dataSize, ands);
			}
		} else {
			// there are no mustBeIn partitions
			if (cantBeIn.size() != 0) {
				BitSet nots = getOrBitSets(datarep, dataSize, cantBeIn);
				nots.flip(0, dataSize);
				return nots;
				// filterContenders(dat, t, cm, q, res, dataSize, nots);
			} else {
				// there are no exclusions at all...
				return null;
				// for (T d : dat) {
				// if (cm.distance(q, d) < t) {
				// res.add(d);
				// }
				// }
			}
		}
	}

	public static <T> void filterContenders(List<T> dat, double t,
											CountedMetric<T> cm, T q, Collection<T> res, final int dataSize,
											BitSet results) { // }, ArrayList<Double> hits, ArrayList<Double> misses) {

		for (int i = results.nextSetBit(0); i != -1 && i < dataSize; i = results.nextSetBit(i + 1)) {
			// added i < dataSize since Cuda code overruns datasize in the -
			// don't know case - must be conservative and include, easier than
			// complex bit masking and shifting.
			if (results.get(i)) {
				double distance = cm.distance(q, dat.get(i));
				if( distance <= t) {
					res.add(dat.get(i));
					//hits.add(distance);
				} else {
					//misses.add(distance);
				}
			}
		}
	}

	@SuppressWarnings("boxing")
	static BitSet getAndBitSets(BitSet[] datarep,
			final int dataSize, List<Integer> mustBeIn) {
		BitSet ands = null;
		if (mustBeIn.size() != 0) {
			ands = datarep[mustBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < mustBeIn.size(); i++) {
				ands.and(datarep[mustBeIn.get(i)]);
			}
		}
		return ands;
	}

	@SuppressWarnings("boxing")
	static BitSet getOrBitSets(BitSet[] datarep,
			final int dataSize, List<Integer> cantBeIn) {
		BitSet nots = null;
		if (cantBeIn.size() != 0) {
			nots = datarep[cantBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < cantBeIn.size(); i++) {
				final BitSet nextNot = datarep[cantBeIn.get(i)];
				nots.or(nextNot);
			}
		}
		return nots;
	}

	@SuppressWarnings("boxing")
	public static <T> void queryBitSetData(List<T> queries, List<T> dat,
										   Metric<T> metric, double threshold, BitSet[] datarep,
										   RefPointSet<T> rps, List<ExclusionZone<T>> ezs, int noOfRefPoints) {

		int noOfResults = 0;
		int partitionsExcluded = 0;
		CountedMetric<T> cm = new CountedMetric<>(metric);
		long t0 = System.currentTimeMillis();

		//ArrayList<Double> misses = new ArrayList<>();
		//ArrayList<Double> hits = new ArrayList<>();

//		long filter_time = 0;
//		long inclusion_time = 0;
//		long distance_time = 0;

		int inclusion_count = 0;

		for (T q : queries) {
			List<T> res = new ArrayList<>();
			double[] dists = rps.extDists(q, res, threshold);

			List<Integer> mustBeIn = new ArrayList<>();
			List<Integer> cantBeIn = new ArrayList<>();

			//long td = System.currentTimeMillis();

			for (int i = 0; i < ezs.size(); i++) {
				ExclusionZone<T> ez = ezs.get(i);
				try {
					if (ez.mustBeIn(dists, threshold)) {
						mustBeIn.add(i);
					} else if (ez.mustBeOut(dists, threshold)) {
						cantBeIn.add(i);
					}
				} catch(  RuntimeException e ) {
					// AL *********
					// eat the runtime exception that is thrown if we get a nan
					// can't make a judgement.
				}
			}

			//distance_time = System.currentTimeMillis() - td;

			partitionsExcluded += cantBeIn.size() + mustBeIn.size();

			//long ti = System.currentTimeMillis();
			BitSet inclusions = doExclusions(dat, threshold, datarep, cm, q, dat.size(), mustBeIn, cantBeIn);

			//inclusion_time += System.currentTimeMillis() - ti;
			// System.out.println("Inclusions = " + inclusions.size());

			inclusion_count += inclusions.cardinality();

			if (inclusions == null) {
				for (T d : dat) {
					if (cm.distance(q, d) <= threshold) {
						res.add(d);
					}
				}
			} else {
				//long tf = System.currentTimeMillis();
				filterContenders(dat, threshold, cm, q, res, dat.size(), inclusions); //, hits, misses);
				//filter_time += System.currentTimeMillis() - tf;
			}

			noOfResults += res.size();
			// System.out.println( "results = " + res.size() );
		}

//		System.out.println("Distance time\t" + distance_time );
//		System.out.println("Inclusion time\t" + inclusion_time );
//		System.out.println("filter time\t" + filter_time );
//
//		System.out.println( "Contenders size\t" + ( hits.size() + misses.size() ));
//		System.out.println( "Threshold\t" + threshold );
//		displayResults( "hits", hits);
//		displayResults( "misses", misses);

		System.out.println("bitsets");
		System.out.println("dists per query\t"
				+ (cm.reset() / queries.size() + noOfRefPoints));
		System.out.println("Pre filtered results\t" + inclusion_count);
		System.out.println("Misses\t" + ( inclusion_count - noOfResults ) );
		System.out.println("results\t" + noOfResults);
		System.out.println("time\t" + (System.currentTimeMillis() - t0)
				/ (float) queries.size());
		System.out.println("partitions excluded\t"
				+ ((double) partitionsExcluded / queries.size()));
	}

}
