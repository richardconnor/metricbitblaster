package uk.al_richard.metricbitblaster.experimental.brnoDisplacement;

import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;
import testloads.TestContext;
import testloads.TestContext.Context;
import uk.al_richard.metricbitblaster.experimental.alRichardLucia.Pair;
import uk.al_richard.metricbitblaster.experimental.alRichardLucia.ReverseOrderedList;
import util.OrderedList;

import java.util.ArrayList;
import java.util.List;

    /**
     * Attempts find pivots based on displacement - as per notebook p23-26
     */

    public class findPivots28May {

        public static void main(String[] args) throws Exception {

            Context context = Context.euc20;

            int num_refPoints = 100;
            int witnessSize = 100;

            int required_refs = 20;

            TestContext tc = new TestContext(context);
            tc.setSizes(witnessSize, num_refPoints);
            List<CartesianPoint> witnesses = tc.getData();
            List<CartesianPoint> refPoints = tc.getRefPoints();
            Metric<CartesianPoint> metric = tc.metric();

            // double threshold = tc.getThresholds()[0];

            long t1 = System.currentTimeMillis();

            choosePivots(witnesses, refPoints, metric, required_refs);

            System.out.println( ( ( System.currentTimeMillis() - t1 ) / 1000 ) + " seconds to choose 20 pivots from 500 with 100 witnesses" );
        }

        public static List<Pair<Pair<Integer>>> choosePivots2(List<CartesianPoint> witnesses, List<CartesianPoint> refPoints,
                                                              Metric<CartesianPoint> metric, int required_refs) {
            List<Pair<Pair<Integer>>> pivots =  new ArrayList<>();
            int count = 0;
            for (int i = 0; i < 100 - 1; i++) {
                for (int j = i + 1; j < 100; j++) {
                    pivots.add(new Pair<>(new Pair<Integer>(i, j), new Pair<Integer>(i + 1, j + 1)));
                    if( pivots.size() >= required_refs ) {
                        return pivots;
                    }
                }
            }
            return pivots;
        }

        public static List<Pair<Pair<Integer>>> choosePivots(List<CartesianPoint> witnesses, List<CartesianPoint> refPoints,
                                                             Metric<CartesianPoint> metric, int required_refs) {

            long t1 = System.currentTimeMillis();

            System.out.println( "Getting pivot pairs"  );
            List<Pair<Integer>> pivot_pairs = getPivotPairs(refPoints);
            System.out.println( "Got " + pivot_pairs.size() + " pivot pairs in " +  ( ( System.currentTimeMillis() - t1 ) / 1000 ) + " seconds" );

            t1 = System.currentTimeMillis();

            System.out.println( "Finding pivot pairs with highest sd" );
            // displacement_map is a map from pivot pairs (sheets) to a map of x displacements from the pivot median line to the witnesses - both -ve and +ve values for right and left of line.
            ReverseOrderedList<Pair<Integer>, Double> pivotsWithHighestSDinXlist = pivotsWithHighestSDinX( pivot_pairs, witnesses, refPoints, metric, 100);
            System.out.println( "Got " + pivotsWithHighestSDinXlist.getList().size() + " high sd pivots in " +  ( ( System.currentTimeMillis() - t1 ) / 1000 ) + " seconds" );
            List<Double> sds = pivotsWithHighestSDinXlist.getComparators();
            System.out.println( "Max sd = " + sds.get(0) + " lowest sd = " + sds.get(sds.size()-1) );

            t1 = System.currentTimeMillis();

            System.out.println( "Finding uncorrelated pairs" );
            OrderedList<Pair<Pair<Integer>>, Double> un_correlated = mostUncorrelatedPivots(metric, pivotsWithHighestSDinXlist, required_refs, witnesses, refPoints);
            System.out.println( "Got " + un_correlated.getList().size() + " uncorrelated Pairs of pairs of pivots in " +  ( ( System.currentTimeMillis() - t1 ) / 1000 ) + " seconds" );


            List<Pair<Pair<Integer>>> uncorrelated_pivots = un_correlated.getList();
            List<Double> correlations = un_correlated.getComparators();
            for( int i = 0; i < uncorrelated_pivots.size(); i++ ) {
                Pair<Pair<Integer>> pair = uncorrelated_pivots.get(i);
                System.out.println( pair.getFirst().getFirst() + " " + pair.getFirst().getSecond() +
                        " "  + pair.getSecond().getFirst() + " " + pair.getSecond().getSecond() + " " + correlations.get(i) );
            }
            return uncorrelated_pivots;
        }

        public static List<CartesianPoint> choosePivotsNChoose2(List<CartesianPoint> witnesses, List<CartesianPoint> refPoints,
                                                                Metric<CartesianPoint> metric, int required_refs) {

            long t1 = System.currentTimeMillis();

            System.out.println( "Getting pivot pairs"  );
            List<Pair<Integer>> pivot_pairs = getPivotPairs(refPoints);
            System.out.println( "Got " + pivot_pairs.size() + " pivot pairs in " +  ( ( System.currentTimeMillis() - t1 ) / 1000 ) + " seconds" );

            t1 = System.currentTimeMillis();

            System.out.println( "Finding pivot pairs with highest sd" );

            ReverseOrderedList<Pair<Integer>, Double> pivotsWithHighestSDinXlist = pivotsWithHighestSDinX( pivot_pairs, witnesses, refPoints, metric, required_refs);
            System.out.println( "Got " + pivotsWithHighestSDinXlist.getList().size() + " high sd pivots in " +  ( ( System.currentTimeMillis() - t1 ) / 1000 ) + " seconds" );
            List<Double> sds = pivotsWithHighestSDinXlist.getComparators();

            System.out.println( "Max sd = " + sds.get(0) + " lowest sd = " + sds.get(sds.size()-1) );

            t1 = System.currentTimeMillis();

            System.out.println( "Finding uncorrelated pairs" );

            ReverseOrderedList<Integer, Double> un_correlated = mostUncorrelatedPivotPairs(metric, pivotsWithHighestSDinXlist, required_refs, witnesses, refPoints);

            List<CartesianPoint> result = new ArrayList<>();
            for( int i : un_correlated.getList() ) {
                result.add( refPoints.get( i ) );
            }

            List<Double> comparitors = un_correlated.getComparators();

            System.out.println( "Found " + result.size() + " uncorrelated pairs,  min correlation = " + comparitors.get(0) + " max = " + comparitors.get(comparitors.size()-1) + " in " + ( ( System.currentTimeMillis() - t1 ) / 1000 ) + " seconds" );

            return result;
        }

        private static ReverseOrderedList<Integer, Double> mostUncorrelatedPivotPairs(Metric<CartesianPoint> metric, ReverseOrderedList<Pair<Integer>, Double> pivotsWithHighestSDinXlist, int required, List<CartesianPoint> witnesses, List<CartesianPoint> refPoints) {

            ReverseOrderedList<Integer, Double> correlations = new ReverseOrderedList<>( required ); // pairs of pivot pairs and their sums of absolute correlations, least correlated first.

            List<Pair<Integer>> pivots = pivotsWithHighestSDinXlist.getList();

            for (int i = 0; i < pivots.size(); i++) {

                double sigma_correlation = 0;

                for (int j = 0; j < pivots.size(); j++) {

                    if( i != j ) {

                        Pair<Integer> pair1 = pivots.get(i);
                        Pair<Integer> pair2 = pivots.get(j);

                        sigma_correlation += Math.abs( correlation(getDisplacements(witnesses, refPoints, metric, pair1), getDisplacements(witnesses, refPoints, metric, pair2)) );
                    }
                }
                correlations.add(i,sigma_correlation);
            }
            // return the least correlated pairs
            return correlations;
        }


        private static OrderedList<Pair<Pair<Integer>>, Double> mostUncorrelatedPivots(Metric<CartesianPoint> metric, ReverseOrderedList<Pair<Integer>, Double> pivotsWithHighestSDinXlist, int required, List<CartesianPoint> witnesses, List<CartesianPoint> refPoints) {

            OrderedList<Pair<Pair<Integer>>,Double> correlations = new OrderedList<>( required ); // pairs of pivot pairs and their correlations, least correlated first.

            List<Pair<Integer>> pivots = pivotsWithHighestSDinXlist.getList();

            for (int i = 0; i < pivots.size() - 1; i++) {
                for (int j = i + 1; j < pivots.size(); j++) {

                    Pair<Integer> pair1 = pivots.get( i );
                    Pair<Integer> pair2 = pivots.get( j );

                    double correlation = correlation( getDisplacements(witnesses, refPoints, metric, pair1),getDisplacements(witnesses, refPoints, metric, pair2) );

                    correlations.add( new Pair<>(pair1,pair2), correlation );
                }
            }
            // return the least correlated pairs
            return correlations;
        }

        private static double correlation(double[] doubles1, double[] doubles2) {
            double mean1 = mean(doubles1);
            double mean2 = mean(doubles1);
            double covariance = covariance( doubles1, mean1, doubles2, mean2 );
            return covariance / ( stddev(doubles1,mean1) * stddev(doubles2,mean2) );
        }


        private static double covariance(double[] doubles1, double mean1, double[] doubles2, double mean2) {
            double product = 0;
            int len =  doubles1.length;
            for( int i = 0; i < len; i++ ) {
                product += (doubles1[i] - mean1 ) * (doubles2[i] - mean2 );
            }
            return product / ( len -1 );
        }


        private static ReverseOrderedList<Pair<Integer>, Double> pivotsWithHighestSDinX(List<Pair<Integer>> pivot_pairs, List<CartesianPoint> witnesses, List<CartesianPoint> refPoints, Metric<CartesianPoint> metric, int required) {

            ReverseOrderedList<Pair<Integer>,Double> standard_deviations = new ReverseOrderedList<>( required ); // list of pivot pairs and the std dev of x displacement from points, highest std dev first

            for( Pair<Integer> pair : pivot_pairs ) {

                double[] displacements = getDisplacements(witnesses, refPoints, metric, pair);
                standard_deviations.add( pair,stddev( displacements ) );

            }
            return standard_deviations;
        }

        private static double[] getDisplacements(List<CartesianPoint> witnesses, List<CartesianPoint> refPoints, Metric<CartesianPoint> metric, Pair<Integer> pair) {
            double[] displacements = new double[witnesses.size()];

            CartesianPoint p1 = refPoints.get( pair.getFirst() );
            CartesianPoint p2 = refPoints.get( pair.getSecond() );
            double base = metric.distance(  p1,p2 );

            for( int i = 0; i < witnesses.size(); i++ ) {

                CartesianPoint p = witnesses.get(i);

                double d_pivot1 = metric.distance( p, p1 );
                double d_pivot2 = metric.distance( p, p2 );

                displacements[i] = calculateXDisplacement( base, d_pivot1, d_pivot2 );
            }
            return displacements;
        }

        private static double calculateXDisplacement(double base, double d_pivot1, double d_pivot2) {
            // See diagram in Resources/triangles.png
            double half_perimeter = ( base + d_pivot1 + d_pivot2 ) / 2.0;
            double height = ( 2 * Math.sqrt( half_perimeter * ( half_perimeter - base ) * ( half_perimeter - d_pivot1 )  * ( half_perimeter - d_pivot2 ) ) ) / base; // Heron's formula
            // height is the height of the triangle formed between the three supplied sides.

            if( Double.isNaN( height ) ) {
                System.out.println( "nan1: " + base + " " + d_pivot1 + " " +  d_pivot2 );
            }
            double result;

            if( d_pivot1 < d_pivot2 ) { // point is closer to p1 than p2 - call that -ve
                double base_triangle_p1 = Math.sqrt( ( d_pivot1 * d_pivot1 )  - ( height * height )  );
                if( Double.isNaN( base_triangle_p1 ) ) { // can happen if height is very close to d_pivot1
                    base_triangle_p1 = 0;
                }
                // base_triangle_p1 is the base of the right angled triangle formed with the d_pivot1 (hypotenuse) and the height
                // result is the x distance to midline of pivots: 1/2 base of interpivots - base_triangle_p1
                return -1 * (( base * 0.5 ) - base_triangle_p1);
            } else {
                double base_triangle_p2 = Math.sqrt( ( d_pivot2 * d_pivot2 )  - ( height * height )  );
                if( Double.isNaN( base_triangle_p2 ) ) { // can happen if height is very close to d_pivot2
                    base_triangle_p2 = 0;
                }
                return 1 * (( base * 0.5 ) - base_triangle_p2);
            }
        }

        private static List<Pair<Integer>> getPivotPairs(List<CartesianPoint> refs) {
            List<Pair<Integer>> pivot_pairs = new ArrayList<>();

            for (int i = 0; i < refs.size() - 1; i++) {
                for (int j = i + 1; j < refs.size(); j++) {
                    pivot_pairs.add(new Pair<Integer>(i,j));
                }
            }
            return pivot_pairs;
        }

        public static double mean( double[] arrai ) {
            double result = 0;
            for( double i : arrai ) {
                result = result + i;
            }
            return result / arrai.length;
        }

        public static double stddev( double[] arrai, double mean ) {
            double sd = 0;
            for( double i : arrai ) {
                sd += Math.pow(i - mean, 2);
            }
            return Math.sqrt( sd / arrai.length );
        }

        public static double stddev( double[] arrai ) {
            double mean = mean( arrai );
            double sd = 0;
            for( double i : arrai ) {
                sd += Math.pow(i - mean, 2);
            }
            return Math.sqrt( sd / arrai.length );
        }
    }


