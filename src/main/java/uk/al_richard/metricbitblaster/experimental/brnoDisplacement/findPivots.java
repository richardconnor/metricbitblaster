package uk.al_richard.metricbitblaster.experimental.brnoDisplacement;

import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;
import org.apache.commons.math3.linear.*;
import testloads.TestContext;
import testloads.TestContext.Context;
import uk.al_richard.metricbitblaster.experimental.alRichardLucia.Pair;
import uk.al_richard.metricbitblaster.experimental.alRichardLucia.ReverseOrderedList;
import util.OrderedList;

import java.io.DataInputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Attempts find pivots based on displacement - as per notebook p23-26
 */

public class findPivots {

	public static void main(String[] args) throws Exception {

		Context context = Context.euc20;

		int num_refPoints = 100;
		int witnessSize = 100;

		int required_refs = 20;

		TestContext tc = new TestContext(context);
		tc.setSizes(witnessSize, num_refPoints);
		List<CartesianPoint> witnesses = tc.getData();
		List<CartesianPoint> refPoints = tc.getRefPoints();
		Metric<CartesianPoint> metric = tc.metric();

		// double threshold = tc.getThresholds()[0];

		long t1 = System.currentTimeMillis();

		choosePivots(witnesses, refPoints, metric, required_refs);

		System.out.println( ( ( System.currentTimeMillis() - t1 ) / 1000 ) + " seconds to choose 20 pivots from 500 with 100 witnesses" );
	}

	public static List<Pair<Pair<Integer>>> choosePivots2(List<CartesianPoint> witnesses, List<CartesianPoint> refPoints,
														 Metric<CartesianPoint> metric, int required_refs) {
		List<Pair<Pair<Integer>>> pivots =  new ArrayList<>();
		int count = 0;
		for (int i = 0; i < 100 - 1; i++) {
			for (int j = i + 1; j < 100; j++) {
				pivots.add(new Pair<>(new Pair<Integer>(i, j), new Pair<Integer>(i + 1, j + 1)));
				if( pivots.size() >= required_refs ) {
					return pivots;
				}
			}
		}
		return pivots;
	}

	public static List<Pair<Pair<Integer>>> choosePivots(List<CartesianPoint> witnesses, List<CartesianPoint> refPoints,
														 Metric<CartesianPoint> metric, int required_refs) {

		long t1 = System.currentTimeMillis();

		System.out.println( "Getting pivot pairs"  );
		List<Pair<Integer>> pivot_pairs = getPivotPairs(refPoints);
		System.out.println( "Got " + pivot_pairs.size() + " pivot pairs in " +  ( ( System.currentTimeMillis() - t1 ) / 1000 ) + " seconds" );

		t1 = System.currentTimeMillis();

		System.out.println( "Finding pivot pairs with highest sd" );
		// displacement_map is a map from pivot pairs (sheets) to a map of x displacements from the pivot median line to the witnesses - both -ve and +ve values for right and left of line.
		ReverseOrderedList<Pair<Integer>, Double> pivotsWithHighestSDinXlist = pivotsWithHighestSDinX( pivot_pairs, witnesses, refPoints, metric, 100);
		System.out.println( "Got " + pivotsWithHighestSDinXlist.getList().size() + " high sd pivots in " +  ( ( System.currentTimeMillis() - t1 ) / 1000 ) + " seconds" );
		List<Double> sds = pivotsWithHighestSDinXlist.getComparators();
		System.out.println( "Max sd = " + sds.get(0) + " lowest sd = " + sds.get(sds.size()-1) );

		t1 = System.currentTimeMillis();

		System.out.println( "Finding uncorrelated pairs" );
		OrderedList<Pair<Pair<Integer>>, Double> un_correlated = mostUncorrelatedPivots(metric, pivotsWithHighestSDinXlist, required_refs, witnesses, refPoints);
		System.out.println( "Got " + un_correlated.getList().size() + " uncorrelated Pairs of pairs of pivots in " +  ( ( System.currentTimeMillis() - t1 ) / 1000 ) + " seconds" );


		List<Pair<Pair<Integer>>> uncorrelated_pivots = un_correlated.getList();
		List<Double> correlations = un_correlated.getComparators();
		for( int i = 0; i < uncorrelated_pivots.size(); i++ ) {
			Pair<Pair<Integer>> pair = uncorrelated_pivots.get(i);
			System.out.println( pair.getFirst().getFirst() + " " + pair.getFirst().getSecond() +
					" "  + pair.getSecond().getFirst() + " " + pair.getSecond().getSecond() + " " + correlations.get(i) );
		}
		return uncorrelated_pivots;
	}

	public static List<CartesianPoint> choosePivotsNChoose2(List<CartesianPoint> witnesses, List<CartesianPoint> refPoints,
														 Metric<CartesianPoint> metric, int required_refs) {

		long start_time = System.currentTimeMillis();

		System.out.println( "Getting pivot pairs"  );
		List<Pair<Integer>> pivot_pairs = getPivotPairs(refPoints);
		System.out.println( "Got " + pivot_pairs.size() + " pivot pairs in " +  ( ( System.currentTimeMillis() - start_time ) / 1000 ) + " seconds" );

		start_time = System.currentTimeMillis();

		System.out.println( "Finding pivot pairs with highest sd" );

		ReverseOrderedList<Pair<Integer>, Double> pivotsWithHighestSDinXlist = pivotsWithHighestSDinX( pivot_pairs, witnesses, refPoints, metric, required_refs);
		System.out.println( "Got " + pivotsWithHighestSDinXlist.getList().size() + " high sd pivots in " +  ( ( System.currentTimeMillis() - start_time ) / 1000 ) + " seconds" );
		List<Double> sds = pivotsWithHighestSDinXlist.getComparators();

		System.out.println( "Max sd = " + sds.get(0) + " lowest sd = " + sds.get(sds.size()-1) );

		start_time = System.currentTimeMillis();

		System.out.println( "Finding uncorrelated pairs" );

		OrderedList<Integer, Double> un_correlated = mostUncorrelatedPivotPairs(metric, pivotsWithHighestSDinXlist, required_refs, witnesses, refPoints);

		List<CartesianPoint> result = new ArrayList<>();
		for( Pair<Integer> pp : pivotsWithHighestSDinXlist.getList() ) {
			result.add( refPoints.get( pp.getFirst() ) );
			result.add( refPoints.get( pp.getSecond() ) );
			if( result.size() > required_refs ) {
			    break;
            }
		}

		return result;
	}

	public static List<Pair<CartesianPoint>> choosePivotsCoefDet(List<CartesianPoint> witnesses, List<CartesianPoint> refPoints,
                                                                 Metric<CartesianPoint> metric, int num_sample_refs, int required_refs) {

		long start_time = System.currentTimeMillis();

		System.out.println( "Getting pivot pairs"  );
		List<Pair<Integer>> pivot_pairs = getPivotPairs(refPoints);
		System.out.println( "Got " + pivot_pairs.size() + " pivot pairs" );

		start_time = System.currentTimeMillis();

		System.out.println( "Finding pivot pairs with highest sd" );

		ReverseOrderedList<Pair<Integer>, Double> pivotsWithHighestSDinXlist = pivotsWithHighestSDinX( pivot_pairs, witnesses, refPoints, metric, num_sample_refs);
		System.out.println( "Got " + pivotsWithHighestSDinXlist.getList().size() + " high sd pivots" );
		List<Double> sds = pivotsWithHighestSDinXlist.getComparators();

		System.out.println( "Max sd = " + sds.get(0) + " lowest sd = " + sds.get(sds.size()-1) );

		start_time = System.currentTimeMillis();

		System.out.println( "Finding uncorrelated pairs" );

        ReverseOrderedList<Integer, Double> un_correlated = cooefDetV2( pivotsWithHighestSDinXlist, required_refs, metric, witnesses, refPoints);

        List<Pair<CartesianPoint>> result = new ArrayList<>();

        System.out.println( "Comparitors: ");
        for( double d : un_correlated.getComparators() ) {
            System.out.println( d );
        }

        for( Integer i : un_correlated.getList() ) {
            Pair<Integer> pair = pivotsWithHighestSDinXlist.getList().get(i);
            result.add( new Pair<>( refPoints.get(pair.getFirst()), refPoints.get(pair.getSecond()) ) );
            if( result.size() > required_refs ) {
                break;
            }
        }

        return result;
    }


    private static ReverseOrderedList<Integer, Double> cooefDetV2(ReverseOrderedList<Pair<Integer>, Double> pivotsWithHighestSDinXlist, int required, Metric<CartesianPoint> metric, List<CartesianPoint> witnesses, List<CartesianPoint> refPoints) {

        // calculate the coefficient of determination
        // See:      https://www.datameer.com/blog/smart-data-discovery-identify-multiple-correlation-coefficients-datameer/
        // See also: https://en.wikipedia.org/wiki/Multiple_correlation
        
        List<Pair<Integer>> pivots = pivotsWithHighestSDinXlist.getList();

        for (int independent_pivot_index = 0; independent_pivot_index < pivots.size(); independent_pivot_index++) {

            RealMatrix RMatrix = correlationMatrix(pivots,independent_pivot_index, witnesses, refPoints, metric);
            RealMatrix c = dependentMatrix( pivots, independent_pivot_index, witnesses, refPoints, metric );
            RealMatrix ct = c.transpose();

//             RRQRDecomposition rrqr = new RRQRDecomposition( RMatrix );
//             System.out.println( "Ranks is: " + rrqr.getRank( 0.000000001 ) );
//             LUDecomposition decomposition = new LUDecomposition( RMatrix );

            SingularValueDecomposition decomposition = new SingularValueDecomposition( RMatrix );

//             System.out.println( "Det is " + decomposition.getDeterminant() ); // only for LUDecomposition

            DecompositionSolver solver = decomposition.getSolver();

//            RealMatrix rinverseC = solver.solve(c);
//            RealMatrix r_squared_matrix = ct.multiply(rinverseC);
//            System.out.println( "R2 = " + r_squared_matrix.getRow(0)[0] );

            RealMatrix RInverse = solver.getInverse(); // this is actually the pseudo inverse since may be (and probably is) singular

            showMatrix( RInverse, "R inverse", 10, 10 );

            showMatrix( RMatrix.multiply( RInverse ), "R * R-1", 10, 10 );

            // RealMatrix r_squared_matrix = ct.multiply(RInverse).multiply(c);

            // System.out.println( independent_pivot_index + " R2 = " + r_squared_matrix.getRow(0)[0] );
        }

            System.exit(1);

            return null;

    }



        /**
         * Returns a list of integers giving the indices into pivotsWithHighestSDinXlist of the most uncorrelated pairs.
         */
    private static ReverseOrderedList<Integer, Double> cooefDet(ReverseOrderedList<Pair<Integer>, Double> pivotsWithHighestSDinXlist, int required, Metric<CartesianPoint> metric, List<CartesianPoint> witnesses, List<CartesianPoint> refPoints) {

		// calculate the coefficient of determination
		// See:      https://www.datameer.com/blog/smart-data-discovery-identify-multiple-correlation-coefficients-datameer/
		// See also: https://en.wikipedia.org/wiki/Multiple_correlation

        long start_time = System.currentTimeMillis();

        ReverseOrderedList<Integer,Double> correlations = new ReverseOrderedList<>( required ); // pairs of pivot pairs and their correlations, least correlated first.
        List<Pair<Integer>> pivots = pivotsWithHighestSDinXlist.getList();

        for (int independent_pivot_index = 0; independent_pivot_index < pivots.size(); independent_pivot_index++) {

            RealMatrix RMatrix = correlationMatrix(pivots,independent_pivot_index, witnesses, refPoints, metric);
            showMatrix( RMatrix, "Rxx", 10, 10 );
            RealMatrix RInverse = MatrixUtils.inverse(RMatrix);

            System.out.println( "Det Rxx = " +  new LUDecomposition( RMatrix ).getDeterminant() );


            showMatrix( RInverse, "R inverse", 10, 10 );

            showMatrix( RMatrix.multiply( RInverse ), "R * R-1", 10, 10 );


            RealMatrix c = dependentMatrix( pivots, independent_pivot_index, witnesses, refPoints, metric );
            showMatrix( c, "c", 10, 1 );
            RealMatrix ct = c.transpose();
            showMatrix( ct, "ct", 1, 10 );

            RealMatrix r_squared_matrix = ct.multiply(RInverse).multiply(c);
            double r_squared_scalar = r_squared_matrix.getRow(0)[0];

            System.out.println(independent_pivot_index + " R2 = " + r_squared_scalar); // result is a scalar encoded in a matrix
            correlations.add(independent_pivot_index,Math.abs(r_squared_scalar)); // TODO This is supposed to be between zero and 1! - hack put abs in

        }
        List<Double> comparitors = correlations.getComparators();
        System.out.println( "Found " + comparitors.size() + " uncorrelated pairs,  min pearsonCorrelation = " + comparitors.get(0)  + " max pearsonCorrelation = " + comparitors.get(comparitors.size()-1)  + " in " + ( ( System.currentTimeMillis() - start_time ) / 1000 ) + " seconds" );

        return correlations;
	}

    private static RealMatrix dependentMatrix(List<Pair<Integer>> independent_variables, int dependent_index, List<CartesianPoint> witnesses, List<CartesianPoint> refPoints, Metric<CartesianPoint> metric) {

        double[] c_raw = new double[independent_variables.size() - 1];
        Pair<Integer> pair1 = independent_variables.get(dependent_index);

        for (int index = 0; index < independent_variables.size(); index++) {

            if( index != dependent_index ) {
                Pair<Integer> pair2 = independent_variables.get(index);
                int adjusted_index = index < dependent_index ? index : index - 1;
                c_raw[adjusted_index] = pearsonCorrelation(getDisplacements(witnesses, refPoints, metric, pair1), getDisplacements(witnesses, refPoints, metric, pair2));
            }
        }
        return new Array2DRowRealMatrix(c_raw); // creates a column matrix
    }

    /**
     *
     * Construct a pearsonCorrelation matrix, which is a symmetrical matrix of all (pair-wise) pearsonCorrelation coefficients between the independent variables
     * This does all the variables in the pivots list apart from ignore_index which is the dependent variable
     * @param independent_variables
     * @param dependent_index
     * @param witnesses
     * @param refPoints
     * @param metric
     * @return
     */
    private static RealMatrix correlationMatrix(List<Pair<Integer>> independent_variables, int dependent_index, List<CartesianPoint> witnesses, List<CartesianPoint> refPoints, Metric<CartesianPoint> metric) {

        double[][] Rraw = new double[independent_variables.size() - 1][independent_variables.size() - 1];   // columns then rows

        for (int col = 0; col < independent_variables.size(); col++) {

            if (col != dependent_index) {

                Pair<Integer> pair1 = independent_variables.get(col);

                double[] column = new double[independent_variables.size() - 1];
                int col_index = col < dependent_index ? col : col - 1;

                for (int row = 0; row < independent_variables.size(); row++) {

                    if (row != dependent_index) {

                        Pair<Integer> pair2 = independent_variables.get(row);
                        int row_index = row < dependent_index ? row : row - 1;
                        column[row_index] = pearsonCorrelation(getDisplacements(witnesses, refPoints, metric, pair1), getDisplacements(witnesses, refPoints, metric, pair2));
                    }
                    Rraw[col_index] = column;
                }
            }

        }
        return new Array2DRowRealMatrix(Rraw);
    }

    /**
     *
     * Construct a pearsonCorrelation matrix, which is a symmetrical matrix of all (pair-wise) pearsonCorrelation coefficients between the independent variables
     * This does all the variables in the pivots list
     * @param independent_variables
     * @param witnesses
     * @param refPoints
     * @param metric
     * @return
     */
    private static RealMatrix correlationMatrix(List<Pair<Integer>> independent_variables, List<CartesianPoint> witnesses, List<CartesianPoint> refPoints, Metric<CartesianPoint> metric) {

        double[][] Rraw = new double[ independent_variables.size() ][ independent_variables.size() ] ;   // columns then rows

        for (int col = 0; col < independent_variables.size(); col++) {

                Pair<Integer> pair1 = independent_variables.get(col);

                double[] column = new double[ independent_variables.size() ];

                for (int row = 0; row < independent_variables.size(); row++) {

                        Pair<Integer> pair2 = independent_variables.get(row);
                        column[row] = pearsonCorrelation(getDisplacements(witnesses, refPoints, metric, pair1), getDisplacements(witnesses, refPoints, metric, pair2));
                    }
                    Rraw[col] = column;

        }
        return new Array2DRowRealMatrix(Rraw);
    }

    public static void showMatrix(RealMatrix matrix, String label, int rows, int columns ) {
        System.out.println( label + ":" );
	    for( int i = 0; i < rows; i++ ) {
            double[] row = matrix.getRow(i);
            for( int j = 0; j < columns; j++ ) {
                System.out.print( row[j] + " " );
            }
            System.out.println();
        }
    }

    /**
     * Returns a list of integers giving the indices into pivotsWithHighestSDinXlist of the most uncorrelated pairs.
     * @param metric
     * @param pivotsWithHighestSDinXlist
     * @param required
     * @param witnesses
     * @param refPoints
     * @return
     */
	private static OrderedList<Integer, Double> mostUncorrelatedPivotPairs(Metric<CartesianPoint> metric, ReverseOrderedList<Pair<Integer>, Double> pivotsWithHighestSDinXlist, int required, List<CartesianPoint> witnesses, List<CartesianPoint> refPoints) {

		long start_time = System.currentTimeMillis();

		OrderedList<Integer, Double> correlations = new OrderedList<>( required ); // indices into pivotsWithHighestSDinXlist of pivot pairs and their sums of absolute correlations, least correlated first.

		List<Pair<Integer>> pivot_pairs = pivotsWithHighestSDinXlist.getList();

		for (int i = 0; i < pivot_pairs.size(); i++) {

			double sigma_correlation = 0;

			for (int j = 0; j < pivot_pairs.size(); j++) {

				if( i != j ) {

					Pair<Integer> pair1 = pivot_pairs.get(i);
					Pair<Integer> pair2 = pivot_pairs.get(j);

					double correlation = pearsonCorrelation(getDisplacements(witnesses, refPoints, metric, pair1), getDisplacements(witnesses, refPoints, metric, pair2));
//					System.out.println( pearsonCorrelation );
					sigma_correlation += Math.abs( correlation );
				}
			}
//			System.out.println( "Sigma = " + sigma_correlation );
//			System.out.println( "=============================================================" );
			correlations.add(i,sigma_correlation);
		}
		List<Double> comparitors = correlations.getComparators();
		System.out.println( "Found " + comparitors.size() + " uncorrelated pairs,  min normalised pearsonCorrelation = " + ( comparitors.get(0) / pivot_pairs.size() ) + " max normalised = " + (  comparitors.get(comparitors.size()-1) / pivot_pairs.size() ) + " in " + ( ( System.currentTimeMillis() - start_time ) / 1000 ) + " seconds" );

		// return the least correlated pairs
		return correlations;
	}


	private static OrderedList<Pair<Pair<Integer>>, Double> mostUncorrelatedPivots(Metric<CartesianPoint> metric, ReverseOrderedList<Pair<Integer>, Double> pivotsWithHighestSDinXlist, int required, List<CartesianPoint> witnesses, List<CartesianPoint> refPoints) {

		OrderedList<Pair<Pair<Integer>>,Double> correlations = new OrderedList<>( required ); // pairs of pivot pairs and their correlations, least correlated first.

		List<Pair<Integer>> pivots = pivotsWithHighestSDinXlist.getList();

		for (int i = 0; i < pivots.size() - 1; i++) {
			for (int j = i + 1; j < pivots.size(); j++) {

				Pair<Integer> pair1 = pivots.get( i );
				Pair<Integer> pair2 = pivots.get( j );

				double correlation = pearsonCorrelation( getDisplacements(witnesses, refPoints, metric, pair1),getDisplacements(witnesses, refPoints, metric, pair2) );

				correlations.add( new Pair<>(pair1,pair2), correlation );
			}
		}
		// return the least correlated pairs
		return correlations;
	}

	private static double pearsonCorrelation(double[] doubles1, double[] doubles2) {
		double mean1 = mean(doubles1);
		double mean2 = mean(doubles2);
        return covariance(doubles1, mean1, doubles2, mean2) / (stddev(doubles1, mean1) * stddev(doubles2, mean2));
	}


	private static double covariance(double[] doubles1, double mean1, double[] doubles2, double mean2) {
		double product = 0;
		int len =  doubles1.length;
		for( int i = 0; i < len; i++ ) {
			product += (doubles1[i] - mean1 ) * (doubles2[i] - mean2 );
		}
		return product / len;
	}


	private static ReverseOrderedList<Pair<Integer>, Double> pivotsWithHighestSDinX(List<Pair<Integer>> pivot_pairs, List<CartesianPoint> witnesses, List<CartesianPoint> refPoints, Metric<CartesianPoint> metric, int required) {

		ReverseOrderedList<Pair<Integer>,Double> standard_deviations = new ReverseOrderedList<>( required ); // list of pivot pairs and the std dev of x displacement from points, highest std dev first

		for( Pair<Integer> pair : pivot_pairs ) {

			double[] displacements = getDisplacements(witnesses, refPoints, metric, pair);
			double mean = mean(displacements);
			double sd = stddev(displacements,mean);
//			showDisplacements( displacements, sd, mean );
			standard_deviations.add( pair,sd );

		}
		return standard_deviations;
	}

	static DataInputStream in = new DataInputStream(System.in);

	private static void showDisplacements(double[] displacements, double sd, double mean) {
		System.out.println( "min = " + min(displacements) );
		System.out.println( "max = " + max(displacements) );
		System.out.println( "mean = " + mean );
		System.out.println( "sd = " + sd );

// 		to pause for next:
//		try {
//			String xx = in.readLine();
//		} catch (IOException e) {
//		}
	}

	private static double[] getDisplacements(List<CartesianPoint> witnesses, List<CartesianPoint> refPoints, Metric<CartesianPoint> metric, Pair<Integer> pair) {
		double[] displacements = new double[witnesses.size()];

		CartesianPoint p1 = refPoints.get( pair.getFirst() );
		CartesianPoint p2 = refPoints.get( pair.getSecond() );
		double base = metric.distance(  p1,p2 );

		for( int i = 0; i < witnesses.size(); i++ ) {

			CartesianPoint p = witnesses.get(i);

			double d_pivot1 = metric.distance( p, p1 );
			double d_pivot2 = metric.distance( p, p2 );

			displacements[i] = calculateXDisplacement( base, d_pivot1, d_pivot2 );
		}
		return displacements;
	}

	private static double calculateXDisplacement(double base, double d_pivot1, double d_pivot2) {
		// See diagram in Resources/triangles.png
		double half_perimeter = ( base + d_pivot1 + d_pivot2 ) / 2.0;
		double height = ( 2 * Math.sqrt( half_perimeter * ( half_perimeter - base ) * ( half_perimeter - d_pivot1 )  * ( half_perimeter - d_pivot2 ) ) ) / base; // Heron's formula
		// height is the height of the triangle formed between the three supplied sides.

		if( Double.isNaN( height ) ) {
			System.out.println( "nan1: " + base + " " + d_pivot1 + " " +  d_pivot2 );
		}
		double result;

		if( d_pivot1 < d_pivot2 ) { // point is closer to p1 than p2 - call that -ve
			double base_triangle_p1 = Math.sqrt( ( d_pivot1 * d_pivot1 )  - ( height * height )  );
			if( Double.isNaN( base_triangle_p1 ) ) { // can happen if height is very close to d_pivot1
				base_triangle_p1 = 0;
			}
			// base_triangle_p1 is the base of the right angled triangle formed with the d_pivot1 (hypotenuse) and the height
			// result is the x distance to midline of pivots: 1/2 base of interpivots - base_triangle_p1
			return -1 * (( base * 0.5 ) - base_triangle_p1);
		} else {
			double base_triangle_p2 = Math.sqrt( ( d_pivot2 * d_pivot2 )  - ( height * height )  );
			if( Double.isNaN( base_triangle_p2 ) ) { // can happen if height is very close to d_pivot2
				base_triangle_p2 = 0;
			}
			return 1 * (( base * 0.5 ) - base_triangle_p2);
		}
	}

	private static List<Pair<Integer>> getPivotPairs(List<CartesianPoint> refs) {
		List<Pair<Integer>> pivot_pairs = new ArrayList<>();

		for (int i = 0; i < refs.size() - 1; i++) {
			for (int j = i + 1; j < refs.size(); j++) {
				pivot_pairs.add(new Pair<Integer>(i,j));
			}
		}
		return pivot_pairs;
	}

	public static double mean( double[] arrai ) {
		double result = 0;
		for( double i : arrai ) {
			result = result + i;
		}
		return result / arrai.length;
	}

	public static double stddev( double[] arrai, double mean ) {
		double sd = 0;
		for( double i : arrai ) {
			sd += Math.pow(i - mean, 2);
		}
		return Math.sqrt( sd / arrai.length );
	}

	public static double stddev( double[] arrai ) {
		double mean = mean( arrai );
		double sd = 0;
		for( double i : arrai ) {
			sd += Math.pow(i - mean, 2);
		}
		return Math.sqrt( sd / arrai.length );
	}

	public static double min( double[] arrai ) {
		double min = Double.MAX_VALUE;
		for( double i : arrai ) {
			if( i < min ) {
				min = i;
			}
		}
		return min;
	}

	public static double max( double[] arrai ) {
		double max = Double.MIN_VALUE;
		for( double i : arrai ) {
			if( i > max ) {
				max = i;
			}
		}
		return max;
	}
}


