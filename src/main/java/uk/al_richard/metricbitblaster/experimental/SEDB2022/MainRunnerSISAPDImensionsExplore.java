package uk.al_richard.metricbitblaster.experimental.SEDB2022;

import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;
import dataPoints.cartesian.Euclidean;
import testloads.CartesianThresholds;
import testloads.TestLoad;
import uk.al_richard.metricbitblaster.experimental.al.MainRunnerFile;

import java.util.List;

import static uk.al_richard.metricbitblaster.experimental.SEDB2022.MainRunnerFileV9e.getNumParts;

public class MainRunnerSISAPDImensionsExplore extends MainRunnerFile {

    private static final Metric<CartesianPoint> metric = new Euclidean<>();

	public static void main(String[] args) throws Exception {

        boolean fourPoint = true;
        boolean balanced = true;

	    int a_hundred = 100;
        int two_hundred = 2 * a_hundred;
	    int one_thousand = 1000;
        int ten_thousand = 10 * one_thousand;
        int one_hundred_thousand = a_hundred * 1000;
        int a_million = 1 * one_thousand * one_thousand;
        int ten_million = 10 * a_million;

        int data_size = a_million; // ten_thousand; // a_million; // one_hundred_thousand; // ten_thousand;
        int number_of_queries = one_thousand;

        System.out.println( "Dimensions\tData size\tthreshold\tstatic selection\tdynamic selection\tno_selection_ros\tno ros\tno parts\tno. queries\tparts exluded\tdistances/q\tms/query\tphase1/q\tphase2/q\tphase3/q\tno unfiltered results\tno results" );

        for (int dimensions = 30; dimensions >= 4; dimensions -= 2) {

            for( int production_refs_size = 20; production_refs_size <= 100; production_refs_size+= 20 ) {

                for( int selection_mult = 2; selection_mult <= 64 ; selection_mult *= 4 ) {

                    int selection_refs_size = production_refs_size * selection_mult;

                    int parts_selected = 0; // not used unless dynamic selection is used.

                    boolean dynamic_selection = false;
                    boolean static_selection = false;
//                    doExperiment(true, dimensions, metric, data_size, number_of_queries, production_refs_size, production_refs_size, parts_selected, static_selection, dynamic_selection, balanced, fourPoint);

                    static_selection = true;
//                    doExperiment(false, dimensions, metric, data_size, number_of_queries, selection_refs_size, production_refs_size, parts_selected, static_selection, dynamic_selection, balanced, fourPoint);

                    dynamic_selection = true;
                    for( parts_selected = 300; parts_selected < Math.min(getNumParts(production_refs_size),8000); parts_selected += 500 ) {

                        doExperiment(false, dimensions, metric, data_size, number_of_queries, selection_refs_size, production_refs_size, parts_selected, static_selection, dynamic_selection, balanced, fourPoint);
                    }
                }
            }
        }
    }

    private static void doExperiment(boolean last_run, int dimensions, Metric<CartesianPoint> metric, int data_size, int number_of_queries, int selection_refs_size, int production_refs_size, int parts_selected, boolean static_selection, boolean dynamic_selection, boolean balanced, boolean fourPoint) {

        int refs_size = selection_refs_size + production_refs_size;

//        System.out.println( "Refs size = " + refs_size );
//        System.out.println( "selection refs size = " + selection_refs_size );
//        System.out.println( "production refs size = " + production_refs_size );

        TestLoad tl = new TestLoad(dimensions, data_size + number_of_queries + refs_size, false );

        List<CartesianPoint> refs = tl.getQueries(refs_size);
        List<CartesianPoint> queries = tl.getQueries( number_of_queries );
		List<CartesianPoint> data = tl.getDataCopy();

        double threshold = CartesianThresholds.getThreshold( metric.getMetricName(), dimensions, 1 ); // 1 is ppm

        try {
            MainRunnerFileV9e mrf = new MainRunnerFileV9e(last_run, dimensions, data, refs, metric, threshold, selection_refs_size, production_refs_size, parts_selected, static_selection, dynamic_selection); // AL HERE REFACTORING!!
            mrf.doQueries(queries, dynamic_selection);

        } catch ( Exception e ) {
            System.out.println( "Exception" );
            e.printStackTrace();
        }
	}
}
