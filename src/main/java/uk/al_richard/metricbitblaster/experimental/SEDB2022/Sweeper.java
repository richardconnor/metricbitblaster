package uk.al_richard.metricbitblaster.experimental.SEDB2022;

import coreConcepts.CountedMetric;
import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;
import dataPoints.cartesian.Euclidean;
import testloads.CartesianThresholds;
import testloads.TestLoad;
import uk.al_richard.metricbitblaster.experimental.pivotsAtQueryTime.*;
import uk.al_richard.metricbitblaster.util.OpenBitSet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static uk.al_richard.metricbitblaster.experimental.SEDB2022.MainRunnerFileV9e.RADIUS_INCREMENT;

/**
 * Based on Ninth version of MainRunnerFile V9c
 * Moves the offsets for both balls and sheets and measures the practical effect.
 */
public class Sweeper {

	/* Constants/config */

	public static final int WITNESS_SET_SIZE = 500; // confirmed as 99% the same by WitnessSetSize.
	public static final int NO_SAMPLES = 25;

	int SE_WITNESS_SET_SIZE = 5001;   // TODO why? Like this in ref impl
	int BALL_WITNESS_SET_SIZE = 1000; // TODO why? Like this in ref impl

	static final int dimensions = 20;
	static final double abs_max_sheet_20D_ipd = 2.7076; // from Matlab for 20 DIM
	static final double abs_max_ball_20D_radii = 1.8130; // from Matlab for 20 DIM
	static final int REFS_SIZE = 100;

	static final double abs_max_sheet_ipd = abs_max_sheet_20D_ipd; // abs_max_sheet_5D_ipd;
	static final double abs_max_ball_radii = abs_max_ball_20D_radii; // abs_max_ball_5D_radii;


//	static final int dimensions = 5;
//	static final double abs_max_sheet_5D_ipd = 2.8661;
//	static final double abs_max_ball_5D_radii = 1.7320; // from Matlab for 5 DIM
//	static final int REFS_SIZE = 20;
//
//	static final double abs_max_sheet_ipd = abs_max_sheet_5D_ipd; // abs_max_sheet_5D_ipd;
//	static final double abs_max_ball_radii = abs_max_ball_5D_radii; // abs_max_ball_5D_radii;

	private boolean fourPoint;
	private boolean balanced;
	private boolean rotationEnabled;
	private RefPointSet<CartesianPoint> rps;
	private List<ExclusionZone<CartesianPoint>> ezs;
	private int noOfBitSets;
	private OpenBitSet[] datarep;
	private List<CartesianPoint> dat;
	private int noOfRefPoints;
	private int partsSelected;
	private List<CartesianPoint> refs;
	private double threshold;
	private static Metric<CartesianPoint> metric = new Euclidean<>();
	private List<Integer> most_different;
	private double[] sheet_offsets;
	private double[] ball_radii;

	public static void main(String[] args) throws Exception {

		boolean fourPoint = false;
		boolean balanced = false;

		int a_hundred = 100;
		int two_hundred = 2 * a_hundred;
		int one_thousand = 1000;
		int ten_thousand = 10 * one_thousand;
		int one_hundred_thousand = a_hundred * 1000;
		int a_million = 1 * one_thousand * one_thousand;
		int ten_million = 10 * a_million;

		int data_size = a_million;
		int number_of_queries = one_thousand;

		System.out.println( "Sheet1\tSheet2\tBall1\tBall2\tDimensions\tData size\tthreshold\tstatic selection\tdynamic selection\tno_selection_ros\tno ros\tno parts\tno. queries\tparts exluded\tdistances/q\tms/query\tphase1/q\tphase2/q\tphase3/q\tno unfiltered results\tno results" );

		int selection_refs_size = REFS_SIZE;
		int production_refs_size = REFS_SIZE;
		int parts_selected = selection_refs_size;

		boolean dynamic_selection = false;
		boolean static_selection = false;

		for (int i = 0; i <= NO_SAMPLES; i++) {
			double fraction_abs_max_sheet_ipd = abs_max_sheet_ipd / (2 * NO_SAMPLES);
			double fraction_abs_max_ball_radii = abs_max_ball_radii / (2 * NO_SAMPLES); // balls starts at middle

			double sheet_inc = fraction_abs_max_sheet_ipd * i;
			double ball_inc = fraction_abs_max_ball_radii * i;

			double ball_mid = abs_max_ball_radii / 2;

			double[] sheet_offsets = new double[]{ -sheet_inc,sheet_inc };
			double[] ball_radii = new double[]{ ball_mid - ball_inc,ball_mid + ball_inc };

			System.out.print( (-sheet_inc) + "\t" + sheet_inc + "\t" + ( ball_mid - ball_inc ) + "\t" +  ( ball_mid + ball_inc ) + "\t");

			doExperiment(true, dimensions, metric, data_size, number_of_queries, selection_refs_size, production_refs_size, parts_selected, static_selection, dynamic_selection, balanced, fourPoint, sheet_offsets, ball_radii);
		}
	}

	private static void doExperiment(boolean last_run, int dimensions, Metric<CartesianPoint> metric, int data_size, int number_of_queries, int selection_refs_size, int production_refs_size, int parts_selected, boolean static_selection, boolean dynamic_selection, boolean balanced, boolean fourPoint, double[] sheet_offsets, double[] ball_radii) {

		int refs_size = selection_refs_size + production_refs_size;

//        System.out.println( "Refs size = " + refs_size );
//        System.out.println( "selection refs size = " + selection_refs_size );
//        System.out.println( "production refs size = " + production_refs_size );

		TestLoad tl = new TestLoad(dimensions, data_size + number_of_queries + refs_size, false );

		List<CartesianPoint> refs = tl.getQueries(refs_size);
		List<CartesianPoint> queries = tl.getQueries( number_of_queries );
		List<CartesianPoint> data = tl.getDataCopy();

		double threshold = CartesianThresholds.getThreshold( metric.getMetricName(), dimensions, 1 ); // 1 is ppm

		try {
			Sweeper mrf = new Sweeper(last_run, dimensions, data, refs, metric, threshold, selection_refs_size, production_refs_size, parts_selected, static_selection, dynamic_selection, sheet_offsets, ball_radii );
			mrf.doQueries(queries, dynamic_selection);

		} catch ( Exception e ) {
			System.out.println( "Exception" );
			e.printStackTrace();
		}
	}

	public Sweeper(boolean last_run, int dimensions, List<CartesianPoint> data, List<CartesianPoint> refs, Metric<CartesianPoint> metric, double threshold, int selection_refs_size, int production_refs_size, int parts_selected, boolean static_selection, boolean dynamic_selection, double[] sheet_offsets, double[] ball_radii) throws Exception {

		List<CartesianPoint> witness_set = data.subList(0, WITNESS_SET_SIZE);
		initialise_state(last_run, dimensions, data, refs, witness_set, metric, threshold, selection_refs_size, production_refs_size, parts_selected, static_selection, dynamic_selection, sheet_offsets, ball_radii);
	}

	public void initialise_state(boolean last_run, int dimensions, List<CartesianPoint> all_data, List<CartesianPoint> all_refs, List<CartesianPoint> witness_set, Metric<CartesianPoint> metric, double threshold, int selection_refs_size, int production_refs_size, int parts_selected, boolean static_selection, boolean dynamic_selection, double[] sheet_offsets, double[] ball_radii) throws Exception {

		this.fourPoint = true;
		this.balanced = false;
		this.rotationEnabled = false;

		this.partsSelected = parts_selected;
		
		this.sheet_offsets = sheet_offsets;
		this.ball_radii = ball_radii;

		this.most_different = null; // reset this to free up space - reinitialised below

		if( ! last_run && static_selection ) { // adjust the data and the witness sizes used for balancing.
			this.dat = witness_set;
			SE_WITNESS_SET_SIZE = witness_set.size() - 1;
			BALL_WITNESS_SET_SIZE = witness_set.size() - 1;
			this.refs = all_refs.subList(0,selection_refs_size);
			noOfRefPoints = selection_refs_size;
		} else {
			this.dat = all_data;
			this.refs = all_refs.subList(0,production_refs_size);
			noOfRefPoints = production_refs_size;
		}
		int nChoose2 = ((noOfRefPoints - 1) * noOfRefPoints) / 2;
		noOfBitSets = (( sheet_offsets.length ) * nChoose2) + (noOfRefPoints * ball_radii.length);
		this.threshold = threshold;
		this.metric = metric;
		this.rps = new RefPointSet<>(this.refs, this.metric);
		this.ezs = new ArrayList<>();

		if( last_run ) { // only do this the last time initialise_state is called
			System.out.print(dimensions + "\t" + dat.size() + "\t" + threshold + "\t" + static_selection + "\t" + dynamic_selection + "\t" + selection_refs_size + "\t" + refs.size() + "\t" + noOfBitSets);
		}

		ezs.addAll(getSheetExclusions(this.dat, this.refs, this.rps, this.balanced, this.fourPoint, this.rotationEnabled, sheet_offsets));
		ezs.addAll(getBallExclusions(this.dat, this.refs, this.rps, this.balanced));

		datarep = new OpenBitSet[this.noOfBitSets];
		long t_start_datasets = System.currentTimeMillis();
		buildBitSetData(this.dat, this.datarep, this.rps, this.ezs);

		long t_start_build_tfidf = System.currentTimeMillis();
		most_different = TFIDF.getMostDifferent(this.datarep, this.dat.size()); // the most different columns in the set

		if( static_selection && ! last_run ) {
			// only run this if it is the first time round
			// rather than go straight to performing queries get the best pivots and reinitialise BB.
			// This will give more columns that standard BB from which to pick using TFIDF but should be far fewer distance calculations.

			List<CartesianPoint> new_refs = findBestRefs2(this.most_different, production_refs_size, this.ezs, this.rps.refs);
			initialise_state(true, dimensions, all_data, new_refs, witness_set, this.metric, threshold, selection_refs_size, production_refs_size, parts_selected, static_selection, dynamic_selection, sheet_offsets, ball_radii);
		}
	}

	public void doQueries(List<CartesianPoint> queries, boolean dynamic_selection) {

		int noOfResults = 0;
		int noOfUnfliteredResults = 0;
		int partitionsExcluded = 0;
		long distance_calcs = 0;
		long t0 = System.currentTimeMillis();
		long phase1_time = 0;
		long phase2_time = 0;
		long phase3_time = 0;

		for( CartesianPoint query : queries) {

			CountedMetric<CartesianPoint> cm = new CountedMetric<>(metric);

			long start = System.currentTimeMillis();
			CantBeMustBes cbimbi = getEzs(query, threshold, rps, ezs);

			CantBeMustBes partitions_selected = cbimbi;
			if( dynamic_selection ) {
				partitions_selected = findNMostDifferentInQuery(cbimbi, most_different, partsSelected);
			}
			long end = System.currentTimeMillis();
			phase1_time += end - start;
			start = end;

			List<CartesianPoint> res = new ArrayList<>();

			List<Integer> mustBeIn = partitions_selected.must_bes;
			List<Integer> cantBeIn = partitions_selected.cant_bes;

			partitionsExcluded += cantBeIn.size() + mustBeIn.size();

			OpenBitSet inclusions = doExclusions(datarep, dat.size(), mustBeIn, cantBeIn);

			end = System.currentTimeMillis();
			phase2_time += end - start;
			start = end;

			if (inclusions == null) {
				noOfUnfliteredResults += dat.size();
				for (CartesianPoint d : dat) {
					if (cm.distance(query, d) <= threshold) {
						res.add(d);
					}
				}
			} else {
				noOfUnfliteredResults = inclusions.cardinality();
				filterContenders(dat, threshold, cm, query, res, dat.size(), inclusions);
			}

			noOfResults += res.size();
			distance_calcs += cm.reset();

			end = System.currentTimeMillis();
			phase3_time += end - start;
		}

		System.out.println( "\t" +
				+ queries.size() + "\t"
				+ ((double) partitionsExcluded / queries.size()) + "\t"
				+ (distance_calcs / (float) queries.size()) + "\t"
				+ ((System.currentTimeMillis() - t0) / (float) queries.size()) + "\t" +
				+ (phase1_time / (float) queries.size()) + "\t" +
				+ (phase2_time / (float) queries.size()) + "\t" +
				+ (phase3_time / (float) queries.size()) + "\t" +
				+ noOfUnfliteredResults + "\t"
				+ noOfResults );
	}

	/**
	 * Reverse engineer the best reference points from the most different columns
	 * Do this by assigning counts to the EZs based on the position of the contributing columns.
	 * This is an economic optimisation of best utility rather than first come first served as in the previous version.
	 * @param most_different
	 * @param number_required
	 * @param ezs
	 * @param rps
	 * @return
	 */
	private List<CartesianPoint> findBestRefs2(List<Integer> most_different, int number_required, List<ExclusionZone<CartesianPoint>> ezs, List<CartesianPoint> rps) {
		long[] ro_contributions = new long[rps.size()]; // keep track of all the contributions of each RO.

		for( Integer i : most_different ) {   // TODO could only do the first half or quarter etc.
			if( i != 0 ) { // columns that are all zero are at end but these do not contribute so miss out.
				ExclusionZone zone = ezs.get(i);
				if (zone instanceof SheetExclusion || zone instanceof SheetExclusion4p) {
					int ro1_index = ((SheetExclusion) zone).getRef1();
					ro_contributions[ro1_index] = ro_contributions[ro1_index] + i; // could add some difference measure here
					int ro2_index = ((SheetExclusion) zone).getRef2();
					ro_contributions[ro2_index] = ro_contributions[ro2_index] + i; // could add some difference measure here
				} else if (zone instanceof BallExclusion) {
					int ro_index = ((BallExclusion) zone).getRef();
					ro_contributions[ro_index] = ro_contributions[ro_index] + i; // could add some difference measure here - have column count
				}
			}
		}

		// after this loop ez_contributions contains the ordered total contribution of all the EZs (lower is better)
		return filterBestRefs( ro_contributions,number_required, rps );
	}

	/**
	 *
	 * @param counted_contributions is the contribution of the RO in this position
	 * @param number_required - the number of ROs that are required
	 * @param rps - all the reference objects in order
	 * @return a list of Reference objects according to their counted_contributions
	 */
	private List<CartesianPoint> filterBestRefs(long[] counted_contributions, int number_required, List<CartesianPoint> rps) {

		List<CartesianPoint> result = new ArrayList<>();
		List<Count<Long>> ros_by_utility = new ArrayList<>(); // the ros and their utility (count)

		for( int i = 0; i < counted_contributions.length; i++ ) {
			ros_by_utility.add( new Count<>(i, counted_contributions[i]));
		}

		Collections.sort(ros_by_utility); // sort the ros into count order smallest first - by contribution to most difference

		for( Count<Long> c : ros_by_utility ) {
			result.add( rps.get( c.position) );
			if( result.size() >= number_required ) {
				return result;
			}
		}
		return result;
	}

	private void showSelected(CantBeMustBes selected, int sheets_size, int balls_size ) {
//		System.out.println( "Can't bes size = " + selected.cant_bes.size() );
//		System.out.println( "Must bes size = " + selected.must_bes.size() );
		int sheets = 0;
		int balls = 0;
		for( int i : selected.cant_bes ) {
			if( i < sheets_size ) {
				sheets++;
			} else {
				balls++;
			}
		}
		for( int i : selected.must_bes ) {
			if( i < sheets_size ) {
				sheets++;
			} else {
				balls++;
			}
		}
//		System.out.println( "Most different includes " + sheets + " sheets from " + sheets_size + " and " + balls + " balls from " + balls_size );
	}

	/**
	 *
	 * @param query_selections - the can't beins and must be ins query_selections from the query
	 * @param most_different - the most different columns calcuated ahead of time
	 * @param partitions_required - the number of partitions required.
	 * @return the CantBeMustBes query_selections by filtering the most_different with the CantBeMustBes from the query
	 */
	private CantBeMustBes findNMostDifferentInQuery(CantBeMustBes query_selections, List<Integer> most_different, int partitions_required) {

		CantBeMustBes result = new CantBeMustBes(most_different);

		for( int column : most_different ) {
			if( query_selections.contains(column) ) {
				if(query_selections.cant_bes.contains(column)) {
					result.cant_bes.add(column);
				} else {
					result.must_bes.add(column);
				}
			}
			// be better to be able to work out when we have enough - could use different TFIDF interface for that? or measure somehow?
			if (result.size() >= partitions_required) break;
		}
		return result;
	}

	private List<CartesianPoint> querySubset(CartesianPoint query, List<CartesianPoint> dat, CountedMetric<CartesianPoint> metric, double threshold, OpenBitSet[] datarep, CantBeMustBes selected ) {

		List<Integer> mustBeIn = selected.must_bes;
		List<Integer> cantBeIn = selected.cant_bes;

		List<CartesianPoint> res = new ArrayList<>();

		OpenBitSet inclusions = doExclusions(datarep, dat.size(), mustBeIn, cantBeIn);

		if (inclusions == null) {
			for (CartesianPoint d : dat) {
				if (metric.distance(query, d) <= threshold) {
					res.add(d);
				}
			}
		} else {
			filterContenders(dat, threshold, metric, query, res, dat.size(), inclusions);
		}

		return res;
	}

	public List<ExclusionZone<CartesianPoint>> getBallExclusions(List<CartesianPoint> dat,
			List<CartesianPoint> refs, RefPointSet<CartesianPoint> rps, boolean balanced) {
		List<ExclusionZone<CartesianPoint>> res = new ArrayList<>();
		for (int i = 0; i < refs.size(); i++) {
			List<BallExclusion<CartesianPoint>> balls = new ArrayList<>();
			for (double radius : ball_radii) {
				BallExclusion<CartesianPoint> be = new BallExclusion<>(rps, i, radius);
				balls.add(be);
			}
			if (balanced) {
				// TODO this doesn't work!
				balls.get(0).setWitnesses(dat.subList(0, BALL_WITNESS_SET_SIZE));
				double midRadius = balls.get(0).getRadius();
				double thisRadius = midRadius - ((balls.size() / 2) * RADIUS_INCREMENT);
				for (int ball = 0; ball < balls.size(); ball++) {
					balls.get(ball).setRadius(thisRadius);
					thisRadius += RADIUS_INCREMENT;
				}
			}
			res.addAll(balls);
		}
		return res;
	}

	public List<ExclusionZone<CartesianPoint>> getSheetExclusions(List<CartesianPoint> dat,
			List<CartesianPoint> refs, RefPointSet<CartesianPoint> rps, boolean balanced, boolean fourPoint,
			boolean rotationEnabled, double[] sheet_offsets ) {
		List<ExclusionZone<CartesianPoint>> res = new ArrayList<>();
		for (int i = 0; i < refs.size() - 1; i++) {
			for (int j = i + 1; j < refs.size(); j++) {
				for( int z = 0; z < sheet_offsets.length; z++ ) {
					if (fourPoint) {
						SheetExclusion4p<CartesianPoint> se = new SheetExclusion4p<>(rps, i, j, sheet_offsets[z]);
						if (balanced) {
							se.setRotationEnabled(rotationEnabled);
							se.setWitnesses(dat.subList(0, SE_WITNESS_SET_SIZE));
						}
						res.add(se);
					} else {
						SheetExclusion3p<CartesianPoint> se = new SheetExclusion3p<>(rps, i, j, sheet_offsets[z]);
						if (balanced) {
							se.setWitnesses(dat.subList(0, SE_WITNESS_SET_SIZE));
						}
						res.add(se);
					}
				}
			}
		}
		return res;
	}

	public <T> void buildBitSetData(List<T> data, OpenBitSet[] datarep, RefPointSet<T> rps, List<ExclusionZone<T>> ezs) {
		int dataSize = data.size();
//		System.out.print( "Allocating " + datarep.length + " columns each of " + dataSize + " rows, size = ");
		double mbytes = (dataSize * 1.0) / (8 * 1024 * 1024);
//		System.out.println( new DecimalFormat("#.#").format(datarep.length * mbytes ) + " Mbytes " );
		for (int i = 0; i < datarep.length; i++) {
			datarep[i] = new OpenBitSet(dataSize);
		}
		for (int n = 0; n < dataSize; n++) {
			T p = data.get(n);
			double[] dists = rps.extDists(p);
			for (int x = 0; x < datarep.length; x++) {
				boolean isIn = ezs.get(x).isIn(dists);
				if (isIn) {
					datarep[x].set(n);
				}
			}
		}
	}

	protected OpenBitSet doExclusions(OpenBitSet[] datarep, final int dataSize, List<Integer> mustBeIn, List<Integer> cantBeIn) {
		if (mustBeIn.size() != 0) {
			OpenBitSet ands = getAndOpenBitSets(datarep, dataSize, mustBeIn);
			if (cantBeIn.size() != 0) {
				/*
				 * hopefully the normal situation or we're in trouble!
				 */
				OpenBitSet nots = getOrOpenBitSets(datarep, dataSize, cantBeIn);
				nots.flip(0, dataSize);
				ands.and(nots);
				return ands;
			} else {
				// there are no cantBeIn partitions
				return ands;
			}
		} else {
			// there are no mustBeIn partitions
			if (cantBeIn.size() != 0) {
				OpenBitSet nots = getOrOpenBitSets(datarep, dataSize, cantBeIn);
				nots.flip(0, dataSize);
				return nots;
			} else {
				// there are no exclusions at all...
				return null;
			}
		}
	}

	private <T> void filterContenders(List<CartesianPoint> dat, double t, Metric<CartesianPoint> cm, CartesianPoint q, List<CartesianPoint> res,
									  final int dataSize, OpenBitSet results) {
		for (int i = results.nextSetBit(0); i != -1 && i < dataSize; i = results.nextSetBit(i + 1)) {
			// added i < dataSize since Cuda code overruns datasize in the -
			// don't know case - must be conservative and include, easier than
			// complex bit masking and shifting.
			if (cm.distance(q, dat.get(i)) <= t) {
				res.add(dat.get(i));
			}
		}
	}

	private OpenBitSet getAndOpenBitSets(OpenBitSet[] datarep, final int dataSize, List<Integer> mustBeIn) {
		OpenBitSet ands = null;
		if (mustBeIn.size() != 0) {
			ands = datarep[mustBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < mustBeIn.size(); i++) {
				ands.and(datarep[mustBeIn.get(i)]);
			}
		}
		return ands;
	}

	private OpenBitSet getOrOpenBitSets(OpenBitSet[] datarep, final int dataSize, List<Integer> cantBeIn) {
		OpenBitSet nots = null;
		if (cantBeIn.size() != 0) {
			nots = datarep[cantBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < cantBeIn.size(); i++) {
				final OpenBitSet nextNot = datarep[cantBeIn.get(i)];
				nots.or(nextNot);
			}
		}
		return nots;
	}

	/**
	 * This is derived from first half of queryBitSetData
	 */
	private <T> CantBeMustBes getEzs(T query, double threshold, RefPointSet<T> rps, List<ExclusionZone<T>> ezs) {

		long t0 = System.currentTimeMillis();

		List<T> res = new ArrayList<>();
		double[] dists = rps.extDists(query, res, threshold);

		List<Integer> mustBeIn = new ArrayList<>();
		List<Integer> cantBeIn = new ArrayList<>();

		for (int i = 0; i < ezs.size(); i++) {
			ExclusionZone<T> ez = ezs.get(i);
			if (ez.mustBeIn(dists, threshold)) {
				mustBeIn.add(i);
			} else if (ez.mustBeOut(dists, threshold)) {
				cantBeIn.add(i);
			}
		}

		return new CantBeMustBes( cantBeIn, mustBeIn );
	}

	public String getFileName() {
		try {
			throw new RuntimeException();
		} catch (RuntimeException e) {
			return e.getStackTrace()[1].getClassName();
		}
	}

	public int getNumParts(int noOfRefPoints) {
		int nChoose2 = ((noOfRefPoints - 1) * noOfRefPoints) / 2;
		return (( sheet_offsets.length ) * nChoose2) + (noOfRefPoints * ball_radii.length);
	}
}
