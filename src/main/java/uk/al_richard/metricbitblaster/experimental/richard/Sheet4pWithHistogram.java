package uk.al_richard.metricbitblaster.experimental.richard;

import java.util.List;

import dataPoints.cartesian.CartesianPoint;
import searchStructures.ObjectWithDistance;
import searchStructures.Quicksort;
import testloads.TestContext;
import testloads.TestContext.Context;
import uk.al_richard.metricbitblaster.referenceImplementation.RefPointSet;
import uk.al_richard.metricbitblaster.referenceImplementation.SheetExclusion4p;

public class Sheet4pWithHistogram extends SheetExclusion4p<CartesianPoint> {

	private ObjectWithDistance<Object>[] owds;

	public Sheet4pWithHistogram(RefPointSet<CartesianPoint> pointSet, int ref1, int ref2) {
		super(pointSet, ref1, ref2);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setWitnesses(List<CartesianPoint> witnesses) {
		List<double[]> twoDpoints = getCoordinateSet(witnesses);

		if (this.rotationEnabled) {
			this.calculateTransform(twoDpoints);
		}
		/*
		 * now rotationAngle and x_offset are set, we need to get rotated xs
		 */
		this.owds = new ObjectWithDistance[witnesses.size()];
		for (int i = 0; i < owds.length; i++) {
			double[] pt = twoDpoints.get(i);
			double xOffset = this.rotationEnabled ? getNewX(pt[0], pt[1]) : pt[0];
			owds[i] = new ObjectWithDistance<>(null, xOffset);
		}
		Quicksort.sort(owds);
		// offset is now the median x value for the rotated pointset
		this.offset = this.owds[owds.length / 2].getDistance();
	}

	public double[][] valueHisto(double threshold) {
		int wSize = this.owds.length;
		double[] offsets = new double[wSize];
		int ptr = 0;
		for (ObjectWithDistance<Object> d : this.owds) {
			offsets[ptr++] = d.getDistance();
		}

		//pointer into the offsets available
		int ptr1 = 0;
		//pointer to first value where content is greater than distance at ptr1 + t, so everything to the right is excluded
		int ptr2 = 0;
		//pointer to first value where content is less than than distance at ptr1 - t, so everything to the left is excluded
		int ptr3 = 0;
		double[] lExcPowers = new double[wSize];
		double[] rExcPowers = new double[wSize];

		for (int i = 0; i < wSize; i++) {
			double t1 = offsets[ptr1++];
			double upb = t1 + threshold;
			while (ptr2 < wSize && offsets[ptr2] < upb) {
				ptr2++;
			}
			
			double lwb = t1 - threshold;
			while (ptr3 < wSize && offsets[ptr3] < lwb) {
				ptr3++;
			}
			
			lExcPowers[i] = (ptr1 / (float) wSize * ((wSize - ptr2) / (float) wSize));
			rExcPowers[i] = ((wSize - ptr1) / (float) wSize) * (ptr3 / (float) wSize);
		}

		double[][] res = { lExcPowers, rExcPowers };
		return res;
	}

	public static void main(String[] a) throws Exception {

		Context context = Context.euc20;

		boolean balanced = true;
		boolean rotationEnabled = true;
		int noOfRefPoints = 60;

		TestContext tc = new TestContext(context);
		int numberOfWitnesses = 10000;
		int querySize = (context == Context.colors || context == Context.nasa) ? tc.dataSize() / 10 : numberOfWitnesses;
		tc.setSizes(querySize, noOfRefPoints);
		List<CartesianPoint> dat = tc.getData();

		List<CartesianPoint> refs = tc.getRefPoints();
		double threshold = tc.getThresholds()[0];

		RefPointSet<CartesianPoint> rps = new RefPointSet<>(refs, tc.metric());

		Sheet4pWithHistogram se = new Sheet4pWithHistogram(rps, 0, 1);
		if (balanced) {
			se.setRotationEnabled(rotationEnabled);
			se.setWitnesses(dat.subList(0, numberOfWitnesses));
		}

		double[][] powers = se.valueHisto(threshold);
		for (int i = 0; i < numberOfWitnesses; i++) {
			System.out.print(powers[0][i]);
			System.out.print("\t" + powers[1][i]);
			System.out.println("\t" + (powers[0][i] + powers[1][i]));
		}
//		for (ObjectWithDistance<Object> d : se.owds) {
//			System.out.println(d.getDistance());
//		}

	}

}
