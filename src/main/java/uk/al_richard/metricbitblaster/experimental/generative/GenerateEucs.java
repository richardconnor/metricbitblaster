package uk.al_richard.metricbitblaster.experimental.generative;

import dataPoints.cartesian.CartesianPoint;
import testloads.TestContext;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

public class GenerateEucs {

    static final TestContext.Context context = TestContext.Context.euc20;;

    public static List<CartesianPoint> generate() throws Exception {

        TestContext tc = new TestContext(context);

        tc.setSizes(0, 0);
        int datasize = tc.dataSize();
        return tc.getData();
    }

    private static void writeEuc(CartesianPoint cartesianPoint, PrintWriter pw) {
        double[] points = cartesianPoint.getPoint();
        for( int i = 0; i < points.length; i++ ) {
            pw.print( points[i] );
            if( i == points.length - 1) {
                pw.print( "\n" );
            } else {
                pw.print(",");
            }
        }
    }

    private static void saveToFile(List<CartesianPoint> eucs, int eucs_per_file ) throws FileNotFoundException {
        for( int i = 0; i < 1000; i++ ) {
            String filename = "eucs" + i + ".csv";
            File f = new File( filename);
            PrintWriter pw = new PrintWriter(f);
            for( int j = 0; j < eucs_per_file; j++ ) {
                writeEuc( eucs.get( i + j ),pw );
            }
            pw.flush();
            pw.close();
            System.out.println( "wrote file: " + filename );
        }
        System.out.println( "finished" );
    }



    public static void main( String[] args ) throws Exception {

        List<CartesianPoint> eucs = generate();
        saveToFile( eucs, 1000);
    }


}
