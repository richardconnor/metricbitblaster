package uk.al_richard.metricbitblaster.experimental.fc6.decaf;

import javafx.util.Pair;
import uk.al_richard.metricbitblaster.experimental.fc6.decaf.metrics.FloatArrayPower;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SampleTriangles {

    public static final String INPUT_DIR = "/Users/al/Downloads/";
    private int seed = 9064573;
    private static Random random = new Random(9064573);

    private static double power = 2.5;
    private static FloatArrayPower metric = new FloatArrayPower( power );

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        List<Pair<Integer, float[]>> list = loadPoints();
        sample( list );
    }

    private static List<Pair<Integer,float[]>> loadPoints() throws IOException, ClassNotFoundException {
        List<Pair<Integer, float[]>> list = new ArrayList<>();

        File input_dir = new File( INPUT_DIR );
        if( ! input_dir.isDirectory() ) {
            System.err.println("Input dir is not a directory");
        }
        File[] listOfFiles = input_dir.listFiles();
        for( File input_file : listOfFiles ) {

            String input_filename = input_file.toString();
            if( input_filename.endsWith( ".obj" ) ) {

                Object data = ObjectReader.readObject(input_filename);
                List<Pair<Integer, float[]>> new_list = (List<Pair<Integer, float[]>>) data;
                list.addAll(new_list);
            }
        }
        return list;
    }

    private static void sample(List<Pair<Integer, float[]>> points ) {

        int len = points.size();
        long violations = 0;
        long tests = 0;

        while( true ) {
            float[] a = points.get( random.nextInt( len ) ).getValue();
            float[] b = points.get( random.nextInt( len ) ).getValue();
            float[] c = points.get( random.nextInt( len ) ).getValue();

            double A = metric.distance( b,c );
            double B = metric.distance( a,c );
            double C = metric.distance( a,b );

            tests++;
            if( A + B > C ) {
                violations++;
            }
            if( tests % 10 == 0 ) {
                System.out.println( "BallPower = " + power + " Tests = " + tests + " violations = " + violations );
            }

        }

    }
}
