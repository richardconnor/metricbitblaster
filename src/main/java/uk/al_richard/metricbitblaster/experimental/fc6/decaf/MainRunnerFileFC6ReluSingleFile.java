package uk.al_richard.metricbitblaster.experimental.fc6.decaf;

import coreConcepts.CountedMetric;
import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;
import searchStructures.VPTree;
import testloads.TestContext;
import testloads.TestContext.Context;
import uk.al_richard.metricbitblaster.experimental.fc6.decaf.metrics.EucPower;
import uk.al_richard.metricbitblaster.referenceImplementation.*;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class MainRunnerFileFC6ReluSingleFile {

	private final static String FILENAME = "/Users/al/Documents/MATLAB/fc6relu.csv";

	protected static final double RADIUS_INCREMENT = 10;
	private static double MEAN_DIST = 70;
	protected static double[] ball_radii = new double[] { MEAN_DIST - 2 * RADIUS_INCREMENT,
			MEAN_DIST - RADIUS_INCREMENT, MEAN_DIST, MEAN_DIST - RADIUS_INCREMENT, MEAN_DIST - 2 * RADIUS_INCREMENT };

	public static void main(String[] args) throws Exception {

		Context context = Context.euc20;

		boolean fourPoint = true;
		boolean balanced = false;
		boolean rotationEnabled = false;

		List<CartesianPoint> dat = getData( FILENAME );

		double power = 2.4;

		// For 40 pivots and 860 data points:
		// BallPower of 3 gives    2/5 results with 1408 triangle failures at build.
		// BallPower of 2.9 gives  3/5 results with 792 triangle failures at build.
		// BallPower of 2.8 gives  4/5 results with 409 triangle failures at build.
		// BallPower of 2.7 gives  4/5 results with 151 triangle failures at build.
		// BallPower of 2.6 gives  5/5 results with 52  triangle failures at build.
		// BallPower of 2.45 gives 5/5 results with 1 triangle failure at build.
		// BallPower of 2.4 gives  5/5 results with no failures at build.

		double threshold = Math.pow( 130, power);
		int noOfRefPoints = 40;
		int querySize = 100;
		int nChoose2 = ((noOfRefPoints - 1) * noOfRefPoints) / 2;
		int noOfBitSets = nChoose2 + (noOfRefPoints * ball_radii.length);

		Metric<CartesianPoint> metric = new EucPower( power );

		List<CartesianPoint> refs = dat.subList( 0,noOfRefPoints );
		List<CartesianPoint> queries = dat.subList( noOfRefPoints, noOfRefPoints + querySize );
		dat = dat.subList( noOfRefPoints + querySize, dat.size() );

		System.out.println("Date/time\t" + new Date().toString());
		System.out.println("Class\t" + getFileName());
		System.out.println("4point\t" + fourPoint);
		System.out.println("balanced\t" + balanced);
		System.out.println("power\t" + power);
		System.out.println("data size\t" + dat.size());
		System.out.println("query size\t" + queries.size());
		System.out.println("threshold\t" + threshold);
		System.out.println("refs size\t" + refs.size());
		System.out.println("no of bitsets\t" + noOfBitSets);

		// buildAndQueryVpt(tc);

		try {
			RefPointSet<CartesianPoint> rps = new RefPointSet<>(refs, metric);
			List<ExclusionZone<CartesianPoint>> ezs = new ArrayList<>();

			ezs.addAll(getSheetExclusions(dat, refs, rps, balanced, fourPoint, rotationEnabled));
			ezs.addAll(getBallExclusions(dat, refs, rps, balanced));

			System.out.println("ezs size:\t" + ezs.size());

			BitSet[] datarep = new BitSet[noOfBitSets];

			buildBitSetData(dat, datarep, rps, ezs, metric);

			queryBitSetData(queries, dat, metric, threshold, datarep, rps, ezs, noOfRefPoints);
		} catch( RuntimeException e ) {
			System.err.println( "Runtime exception - in all probability triangle inequality violated" );
		}
		bruteForce( queries, metric, querySize, dat, refs, threshold );
	}

	private static void bruteForce(List<CartesianPoint> queries, Metric<CartesianPoint> metric, int querySize, List<CartesianPoint> dat, List<CartesianPoint> refs, double threshold ) {

		int noOfResults = 0;
		int partitionsExcluded = 0;
		CountedMetric<CartesianPoint> cm = new CountedMetric<>(metric);
		long t0 = System.currentTimeMillis();

		System.out.println("----------- Brute Force ------------" );
		System.out.println("data size\t" + dat.size());
		System.out.println("query size\t" + querySize);
		System.out.println("threshold\t" + threshold);

		Collection<CartesianPoint> res = new ArrayList<>();

		for( CartesianPoint q : queries ) {

			for (CartesianPoint d : dat) {
				if (cm.distance(q, d) <= threshold) {
					res.add(d);
				}
			}
		}

		System.out.println( "results\t" + res.size() );
		System.out.println("dists per query\t"  + (cm.reset() / queries.size()));
		System.out.println( "time\t" + (System.currentTimeMillis() - t0) / (float) queries.size());
	}


	static List<CartesianPoint> getData(String filename) throws IOException {

		List<CartesianPoint> result = new ArrayList<>();

		BufferedReader in = new BufferedReader( new InputStreamReader( new FileInputStream( filename ) ) );
		String line = "";

		while((line = in.readLine()) != null) {
			CartesianPoint point = parse(line );
			result.add( point );
		}
		return result;
	}

	private static CartesianPoint parse(String line) {
		String[] line_as_strings = line.split(",");
		double[] doubles = new double[ line_as_strings.length ];
		for( int i = 0; i < line_as_strings.length; i++ ) {
			doubles[i] = Double.parseDouble( line_as_strings[i] );
		}
		return new CartesianPoint( doubles );
	}

	public static List<ExclusionZone<CartesianPoint>> getBallExclusions(List<CartesianPoint> dat,
			List<CartesianPoint> refs, RefPointSet<CartesianPoint> rps, boolean balanced) {
		List<ExclusionZone<CartesianPoint>> res = new ArrayList<>();
		for (int i = 0; i < refs.size(); i++) {
			List<BallExclusion<CartesianPoint>> balls = new ArrayList<>();
			for (double radius : ball_radii) {
				BallExclusion<CartesianPoint> be = new BallExclusion<>(rps, i, radius);
				balls.add(be);
			}
			if (balanced) {
				balls.get(0).setWitnesses(dat.subList(0, 1000));
				double midRadius = balls.get(0).getRadius();
				double thisRadius = midRadius - ((balls.size() / 2) * RADIUS_INCREMENT);
				for (int ball = 0; ball < balls.size(); ball++) {
					balls.get(ball).setRadius(thisRadius);
					thisRadius += RADIUS_INCREMENT;
				}
			}
			res.addAll(balls);
		}
		return res;
	}

	public static List<ExclusionZone<CartesianPoint>> getSheetExclusions(List<CartesianPoint> dat,
			List<CartesianPoint> refs, RefPointSet<CartesianPoint> rps, boolean balanced, boolean fourPoint,
			boolean rotationEnabled) {
		List<ExclusionZone<CartesianPoint>> res = new ArrayList<>();
		for (int i = 0; i < refs.size() - 1; i++) {
			for (int j = i + 1; j < refs.size(); j++) {
				if (fourPoint) {
					SheetExclusion4p<CartesianPoint> se = new SheetExclusion4p<>(rps, i, j);
					if (balanced) {
						se.setRotationEnabled(rotationEnabled);
						se.setWitnesses(dat.subList(0, 5001));
					}
					res.add(se);
				} else {
					SheetExclusion3p<CartesianPoint> se = new SheetExclusion3p<>(rps, i, j);
					if (balanced) {
						se.setWitnesses(dat.subList(0, 5001));
					}
					res.add(se);
				}
			}
		}
		return res;
	}

	@SuppressWarnings("unused")
	private static void buildAndQueryVpt(TestContext tc) {

		List<CartesianPoint> data = tc.getData();
		data.addAll(tc.getRefPoints());

		CountedMetric<CartesianPoint> cm = new CountedMetric<>(tc.metric());
		VPTree<CartesianPoint> vpt = new VPTree<>(data, cm);
		cm.reset();
		final List<CartesianPoint> queries = tc.getQueries();
		final double t = tc.getThreshold();

		long t0 = System.currentTimeMillis();
		int noOfRes = 0;
		for (CartesianPoint q : queries) {
			List<CartesianPoint> res = vpt.thresholdSearch(q, t);
			noOfRes += res.size();
		}

		System.out.println("vpt");
		System.out.println("dists per query\t" + cm.reset() / queries.size());
		System.out.println("results\t" + noOfRes);
		System.out.println("time\t" + (System.currentTimeMillis() - t0) / (float) queries.size());
	}

	public static <T> void buildBitSetData(List<T> data, BitSet[] datarep, RefPointSet<T> rps, List<ExclusionZone<T>> ezs, Metric<T> metric) {
		int dataSize = data.size();
		for (int i = 0; i < datarep.length; i++) {
			datarep[i] = new BitSet(dataSize);
		}
		for (int n = 0; n < dataSize; n++) {
			T p = data.get(n);
			double[] dists = rps.extDists(p);
			for (int x = 0; x < datarep.length; x++) {
				boolean isIn = ezs.get(x).isIn(dists);
				if( ezs.get(x) instanceof SheetExclusion ) {
					SheetExclusion<T> sheet = (SheetExclusion) ezs.get(x);
					List<T> points = sheet.getPointSet().refs;
					T pivot1 = points.get( sheet.getRef1() );
					T pivot2 = points.get( sheet.getRef2() );
					check_triangle( p, pivot1, pivot2, metric, sheet );

				}
				if (isIn) {
					datarep[x].set(n);
				}
			}
		}
	}

	static int count = 0;

	private static <T> void check_triangle(T p, T pivot1, T pivot2, Metric<T> metric, SheetExclusion<T> sheet) {
		double A = metric.distance(pivot1, pivot2);
		double C = metric.distance(p, pivot1);
		double B = metric.distance(p, pivot2);
		if( A + C <= B ) {
			System.out.println( "Triangle inequality violated triangle_violations_count=" + count++ + " for sheet " );
			printSides( A,B,C );
		}
	}

	private static void printSides(double a, double b, double c) {
		System.out.println( "\tSides = " + a + " (inter pivot) " + b + " " + c  );
	}

	private static void printAngles(double a, double b, double c) {

		double angle1 = 2 * Math.acos(Math.sqrt(((a + b + c) / 2) * (((a + b + c) / 2) - a) / (b * c)));
		double angle2 = 2 * Math.acos(Math.sqrt(((a + b + c) / 2) * (((a + b + c) / 2) - b) / (a * c)));
		double angle3 = Math.PI - angle1 - angle2;
		System.out.println( "\tAngles = " + angle1 + " " + angle2 + " " + angle3 );
	}

	protected static <T> BitSet doExclusions(List<T> dat, double t, BitSet[] datarep, CountedMetric<T> cm, T q,
			final int dataSize, List<Integer> mustBeIn, List<Integer> cantBeIn) {
		if (mustBeIn.size() != 0) {
			BitSet ands = getAndOpenBitSets(datarep, dataSize, mustBeIn);
			if (cantBeIn.size() != 0) {
				/*
				 * hopefully the normal situation or we're in trouble!
				 */
				BitSet nots = getOrOpenBitSets(datarep, dataSize, cantBeIn);
				nots.flip(0, dataSize);
				ands.and(nots);
				return ands;
				// filterContenders(dat, t, cm, q, res, dataSize, ands);
			} else {
				// there are no cantBeIn partitions
				return ands;
				// filterContenders(dat, t, cm, q, res, dataSize, ands);
			}
		} else {
			// there are no mustBeIn partitions
			if (cantBeIn.size() != 0) {
				BitSet nots = getOrOpenBitSets(datarep, dataSize, cantBeIn);
				nots.flip(0, dataSize);
				return nots;
				// filterContenders(dat, t, cm, q, res, dataSize, nots);
			} else {
				// there are no exclusions at all...
				return null;
				// for (T d : dat) {
				// if (cm.distance(q, d) < t) {
				// res.add(d);
				// }
				// }
			}
		}
	}

	static <T> void filterContenders(List<T> dat, double t, CountedMetric<T> cm, T q, Collection<T> res,
			final int dataSize, BitSet results) {
		for (int i = results.nextSetBit(0); i != -1 && i < dataSize; i = results.nextSetBit(i + 1)) {
			// added i < dataSize since Cuda code overruns datasize in the -
			// don't know case - must be conservative and include, easier than
			// complex bit masking and shifting.
			if (cm.distance(q, dat.get(i)) <= t) {
				res.add(dat.get(i));
			}
		}
	}

	static BitSet getAndOpenBitSets(BitSet[] datarep, final int dataSize, List<Integer> mustBeIn) {
		BitSet ands = null;
		if (mustBeIn.size() != 0) {
			ands = datarep[mustBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < mustBeIn.size(); i++) {
				ands.and(datarep[mustBeIn.get(i)]);
			}
		}
		return ands;
	}

	static BitSet getOrOpenBitSets(BitSet[] datarep, final int dataSize, List<Integer> cantBeIn) {
		BitSet nots = null;
		if (cantBeIn.size() != 0) {
			nots = datarep[cantBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < cantBeIn.size(); i++) {
				final BitSet nextNot = datarep[cantBeIn.get(i)];
				nots.or(nextNot);
			}
		}
		return nots;
	}

	static <T> void queryBitSetData(List<T> queries, List<T> dat, Metric<T> metric, double threshold, BitSet[] datarep,
			RefPointSet<T> rps, List<ExclusionZone<T>> ezs, int noOfRefPoints) {

		int noOfResults = 0;
		int partitionsExcluded = 0;
		CountedMetric<T> cm = new CountedMetric<>(metric);
		long t0 = System.currentTimeMillis();
		for (T q : queries) {
			List<T> res = new ArrayList<>();
			double[] dists = rps.extDists(q, res, threshold);

			List<Integer> mustBeIn = new ArrayList<>();
			List<Integer> cantBeIn = new ArrayList<>();

			for (int i = 0; i < ezs.size(); i++) {
				ExclusionZone<T> ez = ezs.get(i);
				if (ez.mustBeIn(dists, threshold)) {
					mustBeIn.add(i);
				} else if (ez.mustBeOut(dists, threshold)) {
					cantBeIn.add(i);
				}
			}

			partitionsExcluded += cantBeIn.size() + mustBeIn.size();

			BitSet inclusions = doExclusions(dat, threshold, datarep, cm, q, dat.size(), mustBeIn, cantBeIn);

			if (inclusions == null) {
				for (T d : dat) {
					if (cm.distance(q, d) <= threshold) {
						res.add(d);
					}
				}
			} else {
				filterContenders(dat, threshold, cm, q, res, dat.size(), inclusions);
			}

			noOfResults += res.size();
		}

		System.out.println("bitsets");
		System.out.println("dists per query\t" + (cm.reset() / queries.size() + noOfRefPoints));
		System.out.println("results\t" + noOfResults);
		System.out.println("time\t" + (System.currentTimeMillis() - t0) / (float) queries.size());
		System.out.println("partitions excluded\t" + ((double) partitionsExcluded / queries.size()));
	}

	public static String getFileName() {
		try {
			throw new RuntimeException();
		} catch (RuntimeException e) {
			return e.getStackTrace()[1].getClassName();
		}
	}
}
