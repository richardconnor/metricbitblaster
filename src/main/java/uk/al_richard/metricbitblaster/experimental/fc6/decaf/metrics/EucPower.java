package uk.al_richard.metricbitblaster.experimental.fc6.decaf.metrics;

import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;

public class EucPower<T extends CartesianPoint> implements Metric<T> {
    private final double power;
    private final double one_over_power;

    public EucPower(double power ) {
        this.power= power;
        this.one_over_power = 1 / power;
    }

    public double distance(T x, T y) {
        double[] xs = x.getPoint();
        double[] ys = y.getPoint();
        double acc = 0.0D;

        for(int i = 0; i < xs.length; i++) {
            double diff = xs[i] - ys[i];
            acc += diff * diff;
        }
        if( acc >= 0 ) {
            double srss = Math.sqrt(acc);
            return Math.pow(srss, power);
        } else {
            throw new RuntimeException( "Complex distance" );
        }
    }

    public String getMetricName() {
        return "euc-power-" + power ;
    }
}