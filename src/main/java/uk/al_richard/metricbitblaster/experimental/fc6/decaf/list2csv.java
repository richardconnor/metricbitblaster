package uk.al_richard.metricbitblaster.experimental.fc6.decaf;

import javafx.util.Pair;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class list2csv {

    public static final String INPUT_DIR = "/Users/al/Desktop/Profiset/";
    public static final String OUPUT_DIR = "/Users/al/Desktop/Profiset/";

    public static void main(String[] args) throws IOException, ClassNotFoundException {

        File input_dir = new File( INPUT_DIR );
        if( ! input_dir.isDirectory() ) {
            System.err.println("Input dir is not a directory");
        }
        File[] listOfFiles = input_dir.listFiles();
        for( File input_file : listOfFiles ) {

            String input_filename = input_file.toString();
            if( input_filename.endsWith( ".obj" ) ) {
                System.out.println( input_filename + " length = " + input_file.length() );
                int last_slash_pos = input_filename.lastIndexOf("/");
                String output_filename = input_filename.substring(last_slash_pos+1);
                int last_dot_pos = output_filename.lastIndexOf(".");
                output_filename = OUPUT_DIR + output_filename.substring(0,last_dot_pos) + ".csv";
                System.out.println("Converting from " + input_filename + " to " + output_filename );
                object2csv(input_filename, output_filename);
            }
        }
    }

    private static void object2csv(String input_filename, String outputfilename  ) throws IOException, ClassNotFoundException {
        Object data = null;
        try {
            data = ObjectReader.readObject(input_filename);
        } catch( Exception e ) {
            System.out.println("Exception reading object from file " + input_filename );
            return;
        }
        if( data == null ) {
            System.out.println("No data found in file" );
            return;
        }
        List<Pair<Integer, float[]>> list = (List<Pair<Integer, float[]>>) data;
        System.out.println("Read in " + list.size() + " id,float[] items");
        saveToFile(list, outputfilename);
    }


    private static void saveToFile(List<Pair<Integer, float[]>> data, String filename ) throws FileNotFoundException {
        File f = new File(filename);
        PrintWriter pw = new PrintWriter(f);
        int count = 0;
        for (Pair<Integer, float[]> entry : data) {
            writeFloats(entry, pw);
            count++;
        }
        pw.flush();
        pw.close();
        System.out.println("Wrote " + count + " id,float[] items to " + filename );
    }

    private static void writeFloats(Pair<Integer, float[]> entry, PrintWriter pw) {
        Integer id = entry.getKey();
        float[] points = entry.getValue();
        pw.print( id );
        pw.print(",");
        for( int i = 0; i < points.length; i++ ) {
            pw.print( points[i] );
            if( i == points.length - 1) {
                pw.print( "\n" );
            } else {
                pw.print(",");
            }
        }
    }
}
