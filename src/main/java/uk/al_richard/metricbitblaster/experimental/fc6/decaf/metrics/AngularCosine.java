package uk.al_richard.metricbitblaster.experimental.fc6.decaf.metrics;

import coreConcepts.Metric;

/**
 *
 * distance is the Euclidean distance between the end points of normalised
 * vectors
 *
 * a variant proposed by Lucia Vadicamo, documented in arXiv:1604.08640
 *
 * @author Richard Connor
 *
 */
public class AngularCosine implements Metric<float[]> {

    private static double oneOverRoot2 = 1 / Math.sqrt(2);

    @Override
    public double distance(float[] p1, float[] p2) {
        double p1mag = getMagnitude( p1 );
        double p2mag = getMagnitude( p2 );

        double magAcc = 0;
        for (int i = 0; i < p1.length; i++) {
            final double p1n = p1[i] / p1mag;
            final double p2n = p2[i] / p2mag;
            final double p12diff = p1n - p2n;
            magAcc += p12diff * p12diff;
        }

        return Math.sqrt(magAcc) * oneOverRoot2;
    }

    @Override
    public String getMetricName() {
        return "cos";
    }

    private double getMagnitude( float[] floats ) {

        return Math.sqrt(magnitudeSq(floats));
    }

    private double magnitudeSq( float[] floats ) {
        double sum = 0;
        for (float f : floats) {
            sum += f * f;
        }
        return sum;
    }
}