package uk.al_richard.metricbitblaster.experimental.fc6.decaf.metrics;

import coreConcepts.Metric;

public class Sqrt2MinusPiCosine implements Metric<float[]> {

    private static FloatArrayPower euc = new FloatArrayPower(1);

    @Override
    public double distance(float[] point1, float[] point2) {

        return Math.sqrt( 2d - ( Math.cos( Math.PI * euc.distance(  point1,point2 ) ) ) );     // normalise( point1 ),normalise( point2 ) ) ) ) );

    }

    @Override
    public String getMetricName() {
        return "Sqrt2MinusPiCosine";
    }


}