package uk.al_richard.metricbitblaster.experimental.fc6.decaf.metrics;

import coreConcepts.Metric;

public class ArcCos1MinusXSquared implements Metric<float[]> {

    private static FloatArrayPower euc = new FloatArrayPower(2);

    @Override
    public double distance(float[] point1, float[] point2) {

        double result = Math.acos(1 - euc.distance(normalise(point1), normalise(point2)));
        if( Double.isNaN(result)) {
            System.out.println( "nan: " + euc.distance(normalise(point1), normalise(point2)) );
        }
        return result;
    }

    @Override
    public String getMetricName() {
        return "ArcCos1MinusXSquared";
    }

    private float[] normalise( float[] floats ) {
        float mag = (float) getMagnitude(floats);
        float[] copy = new float[ floats.length ];
        for( int i = 0; i < floats.length; i++ ) {
            copy[i] = floats[i] / mag;
        }
        return copy;
    }

    private double getMagnitude( float[] floats ) {

        return Math.sqrt(magnitudeSq(floats));
    }

    private double magnitudeSq( float[] floats ) {
        double sum = 0;
        for (float f : floats) {
            sum += f * f;
        }
        return sum;
    }

}