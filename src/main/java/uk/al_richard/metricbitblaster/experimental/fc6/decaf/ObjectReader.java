package uk.al_richard.metricbitblaster.experimental.fc6.decaf;

import javafx.util.Pair;

import java.io.*;
import java.util.List;

public class ObjectReader {

    public static final String FILENAME = "/Users/al/Downloads/1.obj";

    public static void main(String[] args) throws IOException, ClassNotFoundException {

        Object data = readObject(FILENAME);
        List<Pair<Integer, float[]>> list = (List<Pair<Integer, float[]>>) data;
        System.out.println( "Read in " + list.size() + " id,float[] items" );
    }

    static void writeObject(Object data, String filename  ) throws IOException {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(filename);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(data);
        } finally {
            if (fos != null)
                fos.close();
        }
    }

    static Object readObject(String filename) throws IOException {

        Object data = null;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(filename);
            ObjectInputStream ois = new ObjectInputStream(fis);
            data = ois.readObject();
        } finally {
            if (fis != null)
                fis.close();
            return data;
        }
    }
}