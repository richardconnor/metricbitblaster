package uk.al_richard.metricbitblaster.experimental.fc6.decaf.metrics;

import coreConcepts.Metric;

/**
 * This is Euclidian distance raised to some power.
 */
public class FloatArrayPower implements Metric<float[]> {
    private final double power;
    private final double one_over_power;

    public FloatArrayPower(double power ) {
        this.power= power;
        this.one_over_power = 1 / power;
    }

    public double distance(float[] x, float[] y) {
        double acc = 0.0D;

        for(int i = 0; i < x.length; i++) {
            double diff = x[i] - y[i];
            acc += diff * diff;
        }
        if( acc >= 0 ) {
            double srss = Math.sqrt(acc);
            return Math.pow(srss, power);
        } else {
            throw new RuntimeException( "Complex distance" );
        }
    }

    public String getMetricName() {
        return "FloatArrayPower-" + power ;
    }
}