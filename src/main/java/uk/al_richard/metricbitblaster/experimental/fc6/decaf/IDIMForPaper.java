package uk.al_richard.metricbitblaster.experimental.fc6.decaf;

import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;
import javafx.util.Pair;
import testloads.TestContext;
import uk.al_richard.metricbitblaster.experimental.al.MainRunnerFilePavelPivot;
import uk.al_richard.metricbitblaster.experimental.fc6.decaf.metrics.ArcCos1MinusXSquared;
import uk.al_richard.metricbitblaster.experimental.fc6.decaf.metrics.FloatArrayPower;
import uk.al_richard.metricbitblaster.experimental.fc6.decaf.metrics.Sqrt2MinusPiCosine;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static uk.al_richard.metricbitblaster.experimental.fc6.decaf.MainRunnerFileFC6ReluMultipleFiles.getAllData;
import static uk.al_richard.metricbitblaster.experimental.fc6.decaf.MainRunnerFileFC6ReluMultipleFiles.stripIndices;
import static uk.al_richard.metricbitblaster.util.Filename.getFileName;

/**
 * Calculate IDIM for :
 *       euclidean distance
 *       square
 *       square root
 *       "angular" Cosine
 */
public class IDIMForPaper extends MainRunnerFilePavelPivot {


    private final static String DATA_DIRNAME = "/Users/al/Desktop/Profiset/data/";
    private static int num_files = 5;

    public static void main(String[] args) throws Exception {


        System.out.println("Date/time\t" + new Date().toString() );
        System.out.println("Class\t" + getFileName() );

        Metric<float[]>[] metrics = new Metric[]{ new FloatArrayPower(1),
                new FloatArrayPower(0.5),  new FloatArrayPower(2),
                new Sqrt2MinusPiCosine(), new ArcCos1MinusXSquared() };
        String[] names = new String[]{ "euc", "sqrt", "Square",
                "Sqrt2MinusCosineSquared", "ArcCos1MinusXSquared"  };

        nasaDimensions( metrics,names );
        fc6Dimensions( metrics,names );
        euc10Dimensions( metrics, names );
    }

    private static void fc6Dimensions(Metric<float[]>[] metrics, String[] names) throws Exception {
        List<Pair<Integer, float[]>> data_and_indices = getAllData(DATA_DIRNAME,num_files);

        List<float[]> dat = stripIndices( data_and_indices );
        System.out.println("Read " + dat.size() + " points " );

        report(dat, metrics, names, "fc6" );
    }

    private static void euc10Dimensions(Metric<float[]>[] metrics, String[] names) throws Exception {
        TestContext tc = new TestContext(TestContext.Context.euc10);
        tc.setSizes(0, 0);
        List<CartesianPoint> dat = tc.getData();
        List<float[]> float_data = toFloats( dat );

        report(float_data, metrics, names, "euc10" );
    }


    private static void nasaDimensions(Metric<float[]>[] metrics, String[] names) throws Exception {
        TestContext tc = new TestContext(TestContext.Context.nasa);
        tc.setSizes(0, 0);
        List<CartesianPoint> dat = tc.getData();
        List<float[]> float_data = toFloats( dat );

        report(float_data, metrics, names, "nasa" );
    }

    private static List<float[]> toFloats(List<CartesianPoint> dat) {
        List<float[]> result = new ArrayList<>();
        for( CartesianPoint point : dat ) {
            double[] doubles = point.getPoint();
            float[] floats = new float[ doubles.length ];
            for( int i = 0; i < doubles.length; i++ ) {
                floats[i] = (float) doubles[i];
            }
            result.add(floats);
        }
        return result;
    }

    private static void report(List<float[]> dat, Metric<float[]>[] metrics, String[] names, String dataset) throws Exception {
        for( int i = 0; i < metrics.length; i++ ) {

            double idim = ChavezIDIM(dat, metrics[i]);
            System.out.println(dataset + " IDIM of " + names[i] + " is " + idim);
            System.out.println();
        }
    }

    public static double ChavezIDIM(List<float[]> dat, Metric<float[]> metric) throws Exception {
        if( dat.size() > 5000 ) {
            dat = dat.subList(0,5000);
        }
        List<Double> dists = new ArrayList<>();
        for( float[] p1 : dat ) {
            for( float[] p2 : dat ) {
                if( ! p1.equals(p2) ) {
                    dists.add( metric.distance(p1,p2) );
                }
            }
        }
        Double[] array_dists = dists.toArray(new Double[dists.size()]);
        double mn = mean( array_dists );
        double sd = stddev( array_dists, mn );

        return (mn * mn) / ( 2 * sd * sd );
    }

    public static double mean( Double[] arrai ) {
        double result = 0;
        for( double i : arrai ) {
            result = result + i;
        }
        return result / arrai.length;
    }

    public static double stddev( Double[] arrai, double mean ) {
        double sd = 0;
        for( double i : arrai ) {
            sd += Math.pow(i - mean, 2);
        }
        return Math.sqrt( sd / arrai.length );
    }
}
