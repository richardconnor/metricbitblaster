package uk.al_richard.metricbitblaster.experimental.fc6.decaf;

import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Map;
import java.util.TreeMap;

public class GroundTruth {

    private final static String decafRoot = "/Users/al/Desktop/Profiset/";

    private static Map<Integer, Float> ground_truth = null;

    public static void main(String[] args) throws IOException {

        ground_truth = getQueryInfo( 2 );

        int count = 0;

        for(Map.Entry<Integer, Float> entry : ground_truth.entrySet() ) {
            System.out.println( entry.getKey() + " " + entry.getValue() );
            count++;
            if( count == 5 ) {
                break;
            }
        }
    }

    @SuppressWarnings("boxing")
    static Map<Integer, Float> getQueryInfo(int nn) throws IOException {
        Map<Integer, Float> queries = new TreeMap<>();
        String filename = decafRoot + "groundtruth-profineural-1M-q1000.txt";
        FileReader fr = new FileReader(filename);
        LineNumberReader lnr = new LineNumberReader(fr);

        for (int i = 0; i < 1000; i++) {
            String idLine = lnr.readLine();
            String[] sp = idLine.split("=");

            String nnLine = lnr.readLine();
            String[] spl = nnLine.split(",");
            String nnBit = spl[nn - 1]; // should look like: " 49.658: 0000927805"
            String[] flBit = nnBit.split(":");

            float f = Float.parseFloat(flBit[0]);

            queries.put(Integer.parseInt(sp[1]), f);
        }

        lnr.close();
        return queries;
    }



}
