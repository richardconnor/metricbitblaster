package uk.al_richard.metricbitblaster.experimental.fc6.decaf;

import coreConcepts.CountedMetric;
import coreConcepts.Metric;
import dataPoints.floatArray.Euclidean;
import javafx.util.Pair;
import searchStructures.SearchIndex;
import searchStructures.VPTree;

import java.io.*;
import java.util.*;

public class GetDecaf {
    public static String decafRoot = "/Volumes/Data/profiset/";
    static String dataFileName = "JSON_objects_with_sketches.data";
    static String queryFileName = "JSON_objects_with_sketches_queries.data";

    public static void main(String[] args) throws IOException, ClassNotFoundException {

//                            FileReader mainFile = new FileReader(decafRoot + queryFileName);
//                            LineNumberReader lnr = new LineNumberReader(mainFile);
//                            writeTextBatches(lnr);
//                            writeObjectBatches(lnr);
//                            writeQueryData(lnr, qs);
//                            lnr.close();

        Map<Integer, Float> queryToThresholdMap = getQueryInfo(5);
//                            for (int i : qs.keySet()) {
//                                            System.out.println(i + "\t" + qs.get(i));
//                            }

        queryAllData(queryToThresholdMap);
//                            testProfisetMissingResult();
//                            printNNdists();
    }

    @SuppressWarnings("boxing")
    private static void queryAllData(Map<Integer, Float> queryToThresholdMap)
            throws IOException, ClassNotFoundException {

        double POWER = 3.1;

        List<Pair<Integer, float[]>> queries = getProfisetQueries();
        List<Pair<Integer, float[]>> allData = getProfisetData();

        Metric<Pair<Integer, float[]>> euc = new Metric<Pair<Integer, float[]>>() {
            Metric<float[]> euc = new Euclidean();

            @Override
            public double distance(Pair<Integer, float[]> x, Pair<Integer, float[]> y) {
                final double distance = euc.distance(x.getValue(), y.getValue());
                return Math.pow(distance, POWER);
            }

            @Override
            public String getMetricName() {
                return euc.getMetricName();
            }
        };
        CountedMetric<Pair<Integer, float[]>> cm = new CountedMetric<>(euc);

        SearchIndex<Pair<Integer, float[]>> si = new VPTree<>(allData, cm);
//                            int noOfRefs = 100;
//                            List<Pair<Integer, float[]>> refs = allData.subList(0, noOfRefs);
//                            List<Pair<Integer, float[]>> dat = allData.subList(noOfRefs, allData.size());
//                            SearchIndex<Pair<Integer, float[]>> si = new BitBlaster<>(dat, refs, cm);
//                            SearchIndex<Pair<Integer, float[]>> si = new SerialSearch<>(allData, cm);

//                            SATExclusionFactory<Pair<Integer, float[]>> hpt = new HPT_random<>(cm, true, HPT_random.Arity.log, false);
//                            SearchIndex<Pair<Integer, float[]>> si = new NarySearchTree<>(allData, hpt);

        System.out.println("VPTree at power " + POWER);
        System.out.println(cm.reset() + " creation distances");

        for (Pair<Integer, float[]> q : queries) {
            int qid = q.getKey();
            double thresh = queryToThresholdMap.get(qid) + 0.0001;
            thresh = Math.pow(thresh, POWER);

            List<Pair<Integer, float[]>> res = si.thresholdSearch(q, thresh);
            System.out.print(qid + "\t" + thresh + "\t" + res.size() + "\t" + cm.reset());
//                                            if (res.size() > 0) {
//                                                            System.out.print("\t" + res.get(0).getKey());
//                                            }
            for (Pair<Integer, float[]> r : res) {
                System.out.print("\t" + r.getKey());
            }
            System.out.println();
        }

    }

    private static List<Pair<Integer, float[]>> getProfisetData()
            throws FileNotFoundException, IOException, ClassNotFoundException {
        List<Pair<Integer, float[]>> allData = new ArrayList<>();

        for (int batch = 0; batch < 1000; batch++) {
            FileInputStream fis = new FileInputStream(decafRoot + "data_relu_obj/" + batch + ".obj");
            ObjectInputStream ois = new ObjectInputStream(fis);
            @SuppressWarnings("unchecked")
            List<Pair<Integer, float[]>> next = (List<Pair<Integer, float[]>>) ois.readObject();
            allData.addAll(next);

            ois.close();
            if (batch % 100 == 0) {
                System.out.println(batch);
            }
        }
        System.out.println(allData.size());
        return allData;
    }

    private static void writeTextBatches(LineNumberReader lnr) throws IOException, FileNotFoundException {
        int batch = 0;
        int id = 0;
        PrintWriter pw = null;
        for (String line = lnr.readLine(); line != null; line = lnr.readLine()) {
            if (id % 1000 == 0) {
                if (batch != 0) {
                    pw.close();
                    System.out.println("done batch " + (batch - 1));
                }
                pw = new PrintWriter(decafRoot + "data_relu/" + batch + ".txt");
                batch++;
            }
            Pair<Integer, float[]> pair = getPair(line);
            id++;

            pw.print(pair.getKey());
            for (float f : pair.getValue()) {
                pw.print("\t" + f);
            }
            pw.println();
        }
        pw.close(); // only for the last ever printwriter
        System.out.println("done");
    }

    private static void writeObjectBatches(LineNumberReader lnr) throws IOException, FileNotFoundException {
        int batch = 0;
        int id = 0;
        ObjectOutputStream oos = null;
        List<Pair<Integer, float[]>> vals = null;
        for (String line = lnr.readLine(); line != null; line = lnr.readLine()) {
            if (id % 1000 == 0) {
                if (batch != 0) {
                    oos.writeObject(vals);
                    oos.close();
                    System.out.println("done batch " + (batch - 1));
                }
                FileOutputStream fos = new FileOutputStream(decafRoot + "data_relu_obj/" + batch + ".obj");
                oos = new ObjectOutputStream(fos);
                vals = new ArrayList<>();
                batch++;
            }
            Pair<Integer, float[]> pair = getPair(line);
            id++;
            vals.add(pair);
        }
        oos.writeObject(vals);
        oos.close(); // only for the last ever printwriter
        System.out.println("done");
    }

    private static void writeQueryData(LineNumberReader lnr, Set<Integer> queries)
            throws IOException, FileNotFoundException {

        FileOutputStream fos = new FileOutputStream(decafRoot + "queryData.obj");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        List<Pair<Integer, float[]>> vals = new ArrayList<>();
        int lineNo = 0;
        for (String line = lnr.readLine(); line != null; line = lnr.readLine()) {
            Pair<Integer, float[]> pair = getPair(line);
            if (queries.contains(pair.getKey())) {
                vals.add(pair);
            }
            if (lineNo++ % 1000 == 0) {
                System.out.println(lineNo);
            }
        }
        oos.writeObject(vals);
        oos.close();
        System.out.println("done");
    }

    private static List<Pair<Integer, float[]>> getProfisetQueries()
            throws IOException, FileNotFoundException, ClassNotFoundException {
        FileInputStream fis = new FileInputStream(decafRoot + "queries_relu/queryData.obj");
        ObjectInputStream ois = new ObjectInputStream(fis);

        @SuppressWarnings("unchecked")
        List<Pair<Integer, float[]>> res = (List<Pair<Integer, float[]>>) ois.readObject();
        ois.close();
        return res;
    }

    @SuppressWarnings("boxing")
    private static Pair<Integer, float[]> getPair(String a) {
        float[] vals = new float[4096];
        String[] b = a.split("decaf\\_float\\\"\\:\\[");
        String[] c = b[1].split("\\\"_id\\\"\\:\\\"");
        String[] d = c[1].split("\\\"");

        Scanner s = new Scanner(b[1]);
        s.useDelimiter(",");
        int dim = 0;
        while (s.hasNextFloat()) {
            vals[dim] = s.nextFloat();
            dim++;
        }
        String last = s.next();
        vals[dim] = Float.parseFloat(last.substring(0, last.length() - 1));

        Pair<Integer, float[]> pair = new Pair<>(Integer.parseInt(d[0]), vals);
        s.close();
        return pair;
    }

    @SuppressWarnings("boxing")
    static Map<Integer, Float> getQueryInfo(int nn) throws IOException {
        Map<Integer, Float> queries = new TreeMap<>();
        String filename = decafRoot + "groundtruth-profineural-1M-q1000.txt";
        FileReader fr = new FileReader(filename);
        LineNumberReader lnr = new LineNumberReader(fr);

        for (int i = 0; i < 1000; i++) {
            String idLine = lnr.readLine();
            String[] sp = idLine.split("=");

            String nnLine = lnr.readLine();
            String[] spl = nnLine.split(",");
            String nnBit = spl[nn - 1]; // should look like: " 49.658: 0000927805"
            String[] flBit = nnBit.split(":");

            float f = Float.parseFloat(flBit[0]);

            queries.put(Integer.parseInt(sp[1]), f);
        }

        lnr.close();
        return queries;
    }

    static void printNNdists() throws IOException {
        Map<Integer, Float> qi = getQueryInfo(2);
        for (float qdist : qi.values()) {
            System.out.println(qdist);
        }
    }

    static void testProfisetMissingResult() throws FileNotFoundException, ClassNotFoundException, IOException {
        int dataId = 1772361;
        int dataFile = 918;
        int queryId = 183919;

        List<Pair<Integer, float[]>> queries = getProfisetQueries();

        FileInputStream fis = new FileInputStream(decafRoot + "data_relu_obj/" + dataFile + ".obj");
        ObjectInputStream ois = new ObjectInputStream(fis);
        @SuppressWarnings("unchecked")

        List<Pair<Integer, float[]>> data = (List<Pair<Integer, float[]>>) ois.readObject();

        for (Pair<Integer, float[]> q : queries) {
            if (q.getKey() == queryId) {
                for (Pair<Integer, float[]> d : data) {
                    if (d.getKey() == dataId) {
                        Euclidean euc = new Euclidean();
                        System.out.println(euc.distance(q.getValue(), d.getValue()));
                    }
                }
            }
        }

    }
}