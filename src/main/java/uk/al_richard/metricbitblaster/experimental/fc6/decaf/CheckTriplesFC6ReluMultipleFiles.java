package uk.al_richard.metricbitblaster.experimental.fc6.decaf;

import coreConcepts.Metric;
import dataPoints.floatArray.Euclidean;
import javafx.util.Pair;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class CheckTriplesFC6ReluMultipleFiles {

	private final static String DATA_DIRNAME = "/Users/al/Desktop/Profiset/data/";
	private final static String QUERY_FILENAME = "/Users/al/Desktop/Profiset/queryData.obj";
	private static final int NN = 5;

	private static int num_files = 10;

	private static DecimalFormat df = new DecimalFormat("#.00");




	public static void main(String[] args)  {

		List<Pair<Integer, float[]>> dat = getAllData( DATA_DIRNAME,num_files );
		
		System.out.println("Date/time\t" + new Date().toString());
		System.out.println("num_files\t" + num_files );
		System.out.println("data points\t" + num_files * 1000 );

		System.out.println("Class\t" + getFileName());

		System.out.println("Injesting pivots from " + QUERY_FILENAME  );
		List<Pair<Integer, float[]>> pivots = getSerializedPairs(QUERY_FILENAME);
		pivots = pivots.subList(0,100);
		System.out.println( "Num pivots\t" + pivots.size() );

		for( int p = 16; p < 36; p++ ) {
			double power = p / 10;
			checkTriangles(dat, pivots, power);
		}

	}

	private static Metric<Pair<Integer, float[]>> getMetric( double power ) {
		return new Metric<Pair<Integer, float[]>>() {
			Metric<float[]> euc = new Euclidean();

			@Override
			public double distance(Pair<Integer, float[]> x, Pair<Integer, float[]> y) {
				final double distance = euc.distance(x.getValue(), y.getValue());
				return Math.pow(distance, power);
			}

			@Override
			public String getMetricName() {
				return euc.getMetricName();
			}
		};
	}

	public static void checkTriangles(List<Pair<Integer, float[]>> data, List<Pair<Integer, float[]>> pivots, double power) {

		System.out.println( "Checking triangles at power\t" + power );
		Metric<Pair<Integer, float[]>> metric = getMetric( power );

		int calculations = 0;
		int violations = 0;
		int pivot_violations = 0;

		for (int i = 0; i < pivots.size() - 1; i++) {

			Pair<Integer, float[]> pivot1 = pivots.get(i);

			for (int j = 1 + 1; j < pivots.size(); j++) {

				Pair<Integer, float[]> pivot2 = pivots.get(j);

				double interPivotDistance = metric.distance(pivot1, pivot2);

				int count = 0;

				for (int k = 0; k < data.size(); k++) {

					Pair<Integer, float[]> p = data.get(k);
					calculations++;

					if (triangleInequalityViolated(p, pivot1, pivot2, metric, interPivotDistance)) {
						count++;
						violations++;
					}

				}
				if (count > 0) {
					pivot_violations++;
					// System.out.println("BallPower\t" + power + " pivot index1\t" + i + " pivot index2\t" + j + " violations\t" + count);
				}
			}
		}
		// System.out.println( "=====" );
		System.out.println( "BallPower\t" + power +  " calculations\t" + calculations + " violations\t" + violations + " pivot pair violations\t" + pivot_violations );
	}

	private static double findMeanDistance(List<Pair<Integer, float[]>> dat, Metric<Pair<Integer, float[]>> metric) {

		List<Double> dists = new ArrayList<>();
		Random r = new Random();
		for( int i = 0; i < 1000; i++ ) {
			int rand1 = r.nextInt(dat.size());
			int rand2 = r.nextInt(dat.size());
			dists.add( metric.distance( dat.get(rand1),dat.get(rand2) ) );
		}
		return dists.get( 500 );
	}

	private static double findMedianDistance(List<Pair<Integer, float[]>> dat, Metric<Pair<Integer, float[]>> metric) {

		double dists = 0;
		Random r = new Random();
		for( int i = 0; i < 1000; i++ ) {
			int rand1 = r.nextInt(dat.size());
			int rand2 = r.nextInt(dat.size());
			dists += metric.distance( dat.get(rand1),dat.get(rand2) );
		}
		return dists / 1000;
	}

	private static List<Pair<Integer, float[]>> getAllData(String dirname, int num_files) {

		List<Pair<Integer, float[]>> result = new ArrayList<>();
		int obj_files_read = 0;
		int files_processed = 0;

		File input_dir = new File( dirname );
		if( ! input_dir.isDirectory() ) {
			System.err.println("Input dir is not a directory");
		}
		File[] listOfFiles = input_dir.listFiles();
		while( obj_files_read < num_files ) {

			File input_file = listOfFiles[ files_processed++ ];

			String input_filename = input_file.toString();
			if( input_filename.endsWith( ".obj" ) ) {
				System.out.println("Injesting from " + input_filename  );
				List<Pair<Integer, float[]>> next_batch = getSerializedPairs(input_filename);
				result.addAll( next_batch );
				obj_files_read++;
			}
		}
		return result;
	}

	private static List<Pair<Integer, float[]>> getSerializedPairs(String input_filename ) {
		Object data = null;
		try {
			data = ObjectReader.readObject(input_filename);
		} catch( Exception e ) {
			System.out.println("Exception reading object from file " + input_filename );
			return null;
		}
		if( data == null ) {
			System.out.println("No data found in file" );
			return null;
		}
		return (List<Pair<Integer, float[]>>) data;
	}


	private static <T> boolean triangleInequalityViolated(T p, T pivot1, T pivot2, Metric<T> metric, double inter_pivot_distance) {
		double side1 = metric.distance(p, pivot1);
		double side2 = metric.distance(p, pivot2);
		if( side1 + side2 <= inter_pivot_distance ) {
			// printSides( inter_pivot_distance, side1, side2 );
			return true;
		}
		return false;
	}

	private static void printSides(double inter_pivot_distance, double side1, double side2) {

		System.out.println( "\tinter pivot d\t" + df.format(inter_pivot_distance) + " side1\t" + df.format(side1) + "  side2\t" + df.format(side2) );
	}

	private static void printAngles(double a, double b, double c) {

		double angle1 = 2 * Math.acos(Math.sqrt(((a + b + c) / 2) * (((a + b + c) / 2) - a) / (b * c)));
		double angle2 = 2 * Math.acos(Math.sqrt(((a + b + c) / 2) * (((a + b + c) / 2) - b) / (a * c)));
		double angle3 = Math.PI - angle1 - angle2;
		System.out.println( "\tAngles = " + angle1 + " " + angle2 + " " + angle3 );
	}


	public static String getFileName() {
		try {
			throw new RuntimeException();
		} catch (RuntimeException e) {
			return e.getStackTrace()[1].getClassName();
		}
	}
}
