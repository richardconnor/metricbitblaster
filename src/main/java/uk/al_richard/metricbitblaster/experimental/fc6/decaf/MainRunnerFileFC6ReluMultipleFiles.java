package uk.al_richard.metricbitblaster.experimental.fc6.decaf;

import coreConcepts.CountedMetric;
import coreConcepts.Metric;
import dataPoints.floatArray.Euclidean;
import javafx.util.Pair;
import uk.al_richard.metricbitblaster.referenceImplementation.*;

import java.io.*;
import java.util.*;

public class MainRunnerFileFC6ReluMultipleFiles {

	private final static String DATA_DIRNAME = "/Users/al/Desktop/Profiset/data/";
	private final static String QUERY_FILENAME = "/Users/al/Desktop/Profiset/queryData.obj";
	private static final int NN = 5;

	private static double POWER = 1.9;

	private static int num_files = 20;

	private static HashSet<Integer> columns_that_violate_triangle = new HashSet<>();

	protected static double RADIUS_INCREMENT = 10;
	private static double MEAN_DIST = 70;
	protected static double[] ball_radii = new double[] { MEAN_DIST - 2 * RADIUS_INCREMENT,
			MEAN_DIST - RADIUS_INCREMENT, MEAN_DIST, MEAN_DIST - RADIUS_INCREMENT, MEAN_DIST - 2 * RADIUS_INCREMENT };

	static Metric<Pair<Integer, float[]>> euc = new Metric<Pair<Integer, float[]>>() {
		Metric<float[]> euc = new Euclidean();

		@Override
		public double distance(Pair<Integer, float[]> x, Pair<Integer, float[]> y) {
			final double distance = euc.distance(x.getValue(), y.getValue());
			return Math.pow(distance, POWER);
		}

		@Override
		public String getMetricName() {
			return euc.getMetricName();
		}
	};
	
	public static void main(String[] args)  {

		boolean fourPoint = false;
		boolean balanced = false;
		boolean rotationEnabled = false;

		List<Pair<Integer, float[]>> dat = getAllData( DATA_DIRNAME,num_files );
		
		System.out.println("Date/time\t" + new Date().toString());
		System.out.println("Class\t" + getFileName());

		Metric<Pair<Integer, float[]>> metric = euc;

		MEAN_DIST = findMeanDistance(dat.subList(0,1000), metric);

		System.out.println("Mean dist\t" + MEAN_DIST);

		RADIUS_INCREMENT = MEAN_DIST * 0.1;
		ball_radii = new double[] { MEAN_DIST - 2 * RADIUS_INCREMENT,
				MEAN_DIST - RADIUS_INCREMENT, MEAN_DIST, MEAN_DIST - RADIUS_INCREMENT, MEAN_DIST - 2 * RADIUS_INCREMENT };

		int noOfRefPoints = 200;

		int nChoose2 = ((noOfRefPoints - 1) * noOfRefPoints) / 2;
		int noOfBitSets = nChoose2 + (noOfRefPoints * ball_radii.length);

		List<Pair<Integer, float[]>> refs = dat.subList( 0,noOfRefPoints );
		dat = dat.subList( noOfRefPoints, dat.size() );

		System.out.println("Injesting queries from " + QUERY_FILENAME  );
		List<Pair<Integer, float[]>> queries = getSerializedQueries(QUERY_FILENAME);

	//	queries = queries.subList( 0, querySize );

	//	List<float[]> refs = pickPivots( dat,noOfRefPoints,metric );
	//	List<float[]> queries = dat.subList( 0, querySize );
	//	dat = dat.subList( querySize, dat.size() );

		System.out.println("4point\t" + fourPoint);
		System.out.println("balanced\t" + balanced);
		System.out.println("POWER\t" + POWER);
		System.out.println("data size\t" + num_files * 1000 ); //<<<<<<<<<< TODO HACKED
		System.out.println("query size\t" + queries.size());
		System.out.println("refs size\t" + refs.size());
		System.out.println("no of bitsets\t" + noOfBitSets);

		RefPointSet<Pair<Integer,float[]>> rps = new RefPointSet<>(refs, metric);
		List<ExclusionZone<Pair<Integer,float[]>>> ezs = new ArrayList<>();

		ezs.addAll(getSheetExclusions(refs, rps, balanced, fourPoint, rotationEnabled));
		ezs.addAll(getBallExclusions(refs, rps, balanced));

		System.out.println("ezs size:\t" + ezs.size());

		BitSet[] datarep = new BitSet[noOfBitSets];

		buildBitSetData(dat, datarep, rps, ezs, metric);

		System.out.println("column triangle violations\t" + columns_that_violate_triangle.size() );

		queryBitSetData(queries, dat, metric, datarep, rps, ezs, noOfRefPoints, POWER);

		bruteForce( queries, metric, dat, refs, POWER ); // <<<< COMMENTED SINCE PROBLEM WITH LOADING 1M Cartesians.
	}


//	private static void buildBitsFromData(String dirname, int num_files, int noOfRefPoints, BitSet[] datarep, RefPointSet<float[]> rps, List<ExclusionZone<float[]>> ezs, Metric<float[]> metric) {
//
//		boolean first = true;
//
//		int dataSize = ( num_files * 1000 ) - noOfRefPoints; //<<<<<<<<<<<<< FIXME TODO this is a huge hack
//		for (int i = 0; i < datarep.length; i++) {
//			datarep[i] = new BitSet(dataSize);
//		}
//
//		int obj_files_read = 0;
//		int files_processed = 0;
//		int start_row = 0;
//
//		File input_dir = new File( dirname );
//		if( ! input_dir.isDirectory() ) {
//			System.err.println("Input dir is not a directory");
//		}
//		File[] listOfFiles = input_dir.listFiles();
//		while( obj_files_read < num_files ) {
//
//			File input_file = listOfFiles[ files_processed++ ];
//
//			String input_filename = input_file.toString();
//			if( input_filename.endsWith( ".obj" ) ) {
//
//				List<Pair<Integer,float[]>> dat = getSerializedData(input_filename);
//				if( first ) {
//					// do not process the ref objects which we read already
//					// assumes that input_dir.listFiles() is always in the same order (is it?)
//					dat = dat.subList( noOfRefPoints,dat.size() );
//					first = false;
//				}
//
//				System.out.println("Injesting " + dat.size() + " objects into BB from " + input_filename  );
//				buildBitSetData(start_row, dat, datarep, rps, ezs, metric);
//				start_row += dat.size();
//
//				obj_files_read++;
//			}
//		}
//	}

	/**
	 * Tries to find points that are far apart - this is not the same as n choose 2 points far apart though
	 * @param dat
	 * @param noOfRefPoints
	 * @param metric
	 * @return
	 */
	private static List<Pair<Integer,float[]>> pickPivots(List<Pair<Integer,float[]>> dat, int noOfRefPoints, Metric<Pair<Integer,float[]>> metric) {

		dat.subList( 0,noOfRefPoints );

		List<Pair<Integer,float[]>> result = new ArrayList<>();
		Random r = new Random();

		double mean_dist = findMedianDistance( dat, metric );
		System.out.println( "Med dist = " + mean_dist );
		while( result.size() < noOfRefPoints ) {
			Pair<Integer,float[]> point1 = dat.get( r.nextInt(dat.size() ) );
			Pair<Integer,float[]> point2 = dat.get( r.nextInt(dat.size() ) );
			if( metric.distance( point1,point2 ) >= mean_dist * 2.1 ) { // 210 % of median
				result.add( point1 ); dat.remove( point1 );
				result.add( point2 ); dat.remove( point2 );
				System.out.println( "Added 2 pivots got " + result.size() + " / " + noOfRefPoints) ;
			}
		}
		return result;
	}

	private static double findMeanDistance(List<Pair<Integer, float[]>> dat, Metric<Pair<Integer, float[]>> metric) {

		List<Double> dists = new ArrayList<>();
		Random r = new Random();
		for( int i = 0; i < 1000; i++ ) {
			int rand1 = r.nextInt(dat.size());
			int rand2 = r.nextInt(dat.size());
			dists.add( metric.distance( dat.get(rand1),dat.get(rand2) ) );
		}
		return dists.get( 500 );
	}

	private static double findMedianDistance(List<Pair<Integer, float[]>> dat, Metric<Pair<Integer, float[]>> metric) {

		double dists = 0;
		Random r = new Random();
		for( int i = 0; i < 1000; i++ ) {
			int rand1 = r.nextInt(dat.size());
			int rand2 = r.nextInt(dat.size());
			dists += metric.distance( dat.get(rand1),dat.get(rand2) );
		}
		return dists / 1000;
	}

	static List<Pair<Integer, float[]>> getAllData(String dirname, int num_files) {

		List<Pair<Integer, float[]>> result = new ArrayList<>();
		int obj_files_read = 0;
		int files_processed = 0;

		File input_dir = new File( dirname );
		if( ! input_dir.isDirectory() ) {
			System.err.println("Input dir is not a directory");
		}
		File[] listOfFiles = input_dir.listFiles();
		while( obj_files_read < num_files ) {

			File input_file = listOfFiles[ files_processed++ ];

			String input_filename = input_file.toString();
			if( input_filename.endsWith( ".obj" ) ) {
				System.out.println("Injesting from " + input_filename  );
				List<Pair<Integer, float[]>> next_batch = getSerializedData(input_filename);
				result.addAll( next_batch );
				obj_files_read++;
			}
		}
		return result;
	}

	private static void bruteForce(List<Pair<Integer, float[]>> queries, Metric<Pair<Integer, float[]>> metric, List<Pair<Integer, float[]>> dat, List<Pair<Integer, float[]>> refs, double power ) {

		int noOfResults = 0;
		int partitionsExcluded = 0;

		Map<Integer, Float> ground_truth = null;
		try {
			ground_truth = GroundTruth.getQueryInfo(NN);
		} catch( IOException e ) {
			System.err.println( "IO exception - in getQueryInfo" );
		}

		CountedMetric<Pair<Integer, float[]>> cm = new CountedMetric<>(metric);
		long t0 = System.currentTimeMillis();

		System.out.println("----------- Brute Force ------------" );
		System.out.println("data size\t" + dat.size());
		System.out.println("query size\t" + queries.size());

		Collection<Pair<Integer, float[]>> all_results = new ArrayList<>();

		for( Pair<Integer, float[]> q : queries ) {

			Collection<float[]> res = new ArrayList<>();
			int id = q.getKey();
			double threshold = (double) ground_truth.get(id);
//			System.out.print( "GT Query at threshold\t" + threshold );
			threshold = Math.pow( (threshold + 0.0001), power);

			for (Pair<Integer, float[]> d : dat) {
				if (cm.distance(q, d) <= threshold) {
//					res.add(d);
					all_results.add(d);
				}
			}
//			System.out.println( " Results\t" + res.size() );
		}

		System.out.println( "results\t" + all_results.size() );
		System.out.println("dists per query\t"  + (cm.reset() / queries.size()));
		System.out.println( "time\t" + (System.currentTimeMillis() - t0) / (float) queries.size());
	}


	private static List<float[]> getCSVData(String filename) throws IOException {

		List<float[]> result = new ArrayList<>();

		BufferedReader in = new BufferedReader( new InputStreamReader( new FileInputStream( filename ) ) );
		String line = "";

		while((line = in.readLine()) != null) {
			float[] point = parse(line );
			result.add( point );
		}
		return result;
	}

	private static List<Pair<Integer, float[]>> getSerializedData(String input_filename ) {

		return getSerializedPairs( input_filename);
	}

	private static List<Pair<Integer, float[]>> getSerializedQueries(String input_filename ) {

		return getSerializedPairs( input_filename );
	}



	private static List<Pair<Integer, float[]>> getSerializedPairs(String input_filename ) {
		Object data = null;
		try {
			data = ObjectReader.readObject(input_filename);
		} catch( Exception e ) {
			System.out.println("Exception reading object from file " + input_filename );
			return null;
		}
		if( data == null ) {
			System.out.println("No data found in file" );
			return null;
		}
		return (List<Pair<Integer, float[]>>) data;
	}

	static List<float[]> stripIndices(List<Pair<Integer, float[]>> list) {
		List<float[]> result = new ArrayList<>();
		for( Pair<Integer,float[]> point : list ) {
			result.add( point.getValue() );
		}
		return result;
	}

	private static float[] parse(String line) {
		String[] line_as_strings = line.split(",");
		float[] floats = new float[ line_as_strings.length ];
		for( int i = 0; i < line_as_strings.length; i++ ) {
			floats[i] = Float.parseFloat( line_as_strings[i] );
		}
		return floats;
	}

	public static List<ExclusionZone<Pair<Integer,float[]>>> getBallExclusions(List<Pair<Integer, float[]>> refs, RefPointSet<Pair<Integer, float[]>> rps, boolean balanced) {
		List<ExclusionZone<Pair<Integer, float[]>>> res = new ArrayList<>();
		for (int i = 0; i < refs.size(); i++) {
			List<BallExclusion<Pair<Integer, float[]>>> balls = new ArrayList<>();
			for (double radius : ball_radii) {
				BallExclusion<Pair<Integer, float[]>> be = new BallExclusion<>(rps, i, radius);
				balls.add(be);
			}
			if (balanced) {
				System.err.println( "balancing code commented out");
//				balls.get(0).setWitnesses(dat.subList(0, 1000));
//				double midRadius = balls.get(0).getRadius();
//				double thisRadius = midRadius - ((balls.size() / 2) * RADIUS_INCREMENT);
//				for (int ball = 0; ball < balls.size(); ball++) {
//					balls.get(ball).setRadius(thisRadius);
//					thisRadius += RADIUS_INCREMENT;
//				}
			}
			res.addAll(balls);
		}
		return res;
	}

	public static List<ExclusionZone<Pair<Integer,float[]>>> getSheetExclusions(
			List<Pair<Integer, float[]>> refs, RefPointSet<Pair<Integer, float[]>> rps, boolean balanced, boolean fourPoint,
			boolean rotationEnabled) {
		List<ExclusionZone<Pair<Integer, float[]>>> res = new ArrayList<>();
		for (int i = 0; i < refs.size() - 1; i++) {
			for (int j = i + 1; j < refs.size(); j++) {
				if (fourPoint) {
					SheetExclusion4p<Pair<Integer, float[]>> se = new SheetExclusion4p<>(rps, i, j);
					if (balanced) {
						System.err.println( "balancing code commented out");
//						se.setRotationEnabled(rotationEnabled);
//						se.setWitnesses(dat.subList(0, 5001));
					}
					res.add(se);
				} else {
					SheetExclusion3p<Pair<Integer, float[]>> se = new SheetExclusion3p<>(rps, i, j);
					if (balanced) {
						System.err.println( "balancing code commented out");
//						se.setWitnesses(dat.subList(0, 5001));
					}
					res.add(se);
				}
			}
		}
		return res;
	}

	public static <T> void buildBitSetData(List<T> data, BitSet[] datarep, RefPointSet<T> rps, List<ExclusionZone<T>> ezs, Metric<T> metric) {
		int dataSize = data.size();
		for (int i = 0; i < datarep.length; i++) {
			datarep[i] = new BitSet(dataSize);
		}

		for (int row = 0; row < data.size(); row++) {
			T p = data.get(row);
			double[] dists = rps.extDists(p);
			for (int ez_index = 0; ez_index < datarep.length; ez_index++) {
				boolean isIn = ezs.get(ez_index).isIn(dists);
				if( ezs.get(ez_index) instanceof SheetExclusion ) {
					SheetExclusion<T> sheet = (SheetExclusion) ezs.get(ez_index);
					if( sheet.getInterPivotDistance() < MEAN_DIST * 0.5 ) {
						columns_that_violate_triangle.add(ez_index);
					}
//					List<T> points = sheet.getPointSet().refs;
//					T pivot1 = points.get( sheet.getRef1() );
//					T pivot2 = points.get( sheet.getRef2() );
//					if( triangleInequalityViolated( p, pivot1, pivot2, metric, sheet ) && ! columns_that_violate_triangle.contains(ez_index) ) {
//						columns_that_violate_triangle.add(ez_index);
//					}

				}
				if (isIn) {
					datarep[ez_index].set(row);
				}
			}
		}
	}


	private static <T> boolean triangleInequalityViolated(T p, T pivot1, T pivot2, Metric<T> metric, SheetExclusion<T> sheet) {
		double A = metric.distance(pivot1, pivot2);
		double C = metric.distance(p, pivot1);
		double B = metric.distance(p, pivot2);
		if( A + C <= B ) {
			return true;
			//System.out.println( "Triangle inequality violated triangle_violations_count = " + triangle_violations_count + " for sheet " );
			//printSides( A,B,C );
		}
		return false;
	}

	private static void printSides(double a, double b, double c) {
		System.out.println( "\tSides = " + a + " (inter pivot) " + b + " " + c  );
	}

	private static void printAngles(double a, double b, double c) {

		double angle1 = 2 * Math.acos(Math.sqrt(((a + b + c) / 2) * (((a + b + c) / 2) - a) / (b * c)));
		double angle2 = 2 * Math.acos(Math.sqrt(((a + b + c) / 2) * (((a + b + c) / 2) - b) / (a * c)));
		double angle3 = Math.PI - angle1 - angle2;
		System.out.println( "\tAngles = " + angle1 + " " + angle2 + " " + angle3 );
	}

	protected static <T> BitSet doExclusions(List<Pair<Integer, float[]>> dat, double t, BitSet[] datarep, CountedMetric<Pair<Integer, float[]>> cm, Pair<Integer, float[]> q,
											 final int dataSize, List<Integer> mustBeIn, List<Integer> cantBeIn) {
		if (mustBeIn.size() != 0) {
			BitSet ands = getAndOpenBitSets(datarep, dataSize, mustBeIn);
			if (cantBeIn.size() != 0) {
				/*
				 * hopefully the normal situation or we're in trouble!
				 */
				BitSet nots = getOrOpenBitSets(datarep, dataSize, cantBeIn);
				nots.flip(0, dataSize);
				ands.and(nots);
				return ands;
				// filterContenders(dat, t, cm, q, res, dataSize, ands);
			} else {
				// there are no cantBeIn partitions
				return ands;
				// filterContenders(dat, t, cm, q, res, dataSize, ands);
			}
		} else {
			// there are no mustBeIn partitions
			if (cantBeIn.size() != 0) {
				BitSet nots = getOrOpenBitSets(datarep, dataSize, cantBeIn);
				nots.flip(0, dataSize);
				return nots;
				// filterContenders(dat, t, cm, q, res, dataSize, nots);
			} else {
				// there are no exclusions at all...
				return null;
				// for (T d : dat) {
				// if (cm.distance(q, d) < t) {
				// res.add(d);
				// }
				// }
			}
		}
	}

	static <T> void filterContenders(List<T> dat, double t, CountedMetric<T> cm, T q, Collection<T> res,
			final int dataSize, BitSet results) {
		for (int i = results.nextSetBit(0); i != -1 && i < dataSize; i = results.nextSetBit(i + 1)) {
			// added i < dataSize since Cuda code overruns datasize in the -
			// don't know case - must be conservative and include, easier than
			// complex bit masking and shifting.
			if (cm.distance(q, dat.get(i)) <= t) {
				res.add(dat.get(i));
			}
		}
	}

	static BitSet getAndOpenBitSets(BitSet[] datarep, final int dataSize, List<Integer> mustBeIn) {
		BitSet ands = null;
		if (mustBeIn.size() != 0) {
			ands = datarep[mustBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < mustBeIn.size(); i++) {
				ands.and(datarep[mustBeIn.get(i)]);
			}
		}
		return ands;
	}

	static BitSet getOrOpenBitSets(BitSet[] datarep, final int dataSize, List<Integer> cantBeIn) {
		BitSet nots = null;
		if (cantBeIn.size() != 0) {
			nots = datarep[cantBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < cantBeIn.size(); i++) {
				final BitSet nextNot = datarep[cantBeIn.get(i)];
				nots.or(nextNot);
			}
		}
		return nots;
	}

	static <T> void queryBitSetData(List<Pair<Integer, float[]>> queries, List<Pair<Integer, float[]>> dat, Metric<Pair<Integer, float[]>> metric, BitSet[] datarep,
									RefPointSet<Pair<Integer, float[]>> rps, List<ExclusionZone<Pair<Integer, float[]>>> ezs, int noOfRefPoints, double power) {

		Map<Integer, Float> ground_truth = null;
		try {
			ground_truth = GroundTruth.getQueryInfo(NN);
		} catch( IOException e ) {
			System.err.println( "IO exception - in getQueryInfo" );
		}

		int noOfResults = 0;
		int partitionsExcluded = 0;
		CountedMetric<Pair<Integer, float[]>> cm = new CountedMetric<>(metric);
		long t0 = System.currentTimeMillis();
		for (Pair<Integer, float[]> q : queries) {

			int id = q.getKey();
			double threshold = (double) ground_truth.get(id);
//			System.out.print( "BB Query at threshold\t" + threshold );
			threshold = Math.pow( (threshold + 0.0001), power);

			List<Pair<Integer, float[]>> res = new ArrayList<>();
			double[] dists = rps.extDists(q, res, threshold);

			List<Integer> mustBeIn = new ArrayList<>();
			List<Integer> cantBeIn = new ArrayList<>();

			for (int i = 0; i < ezs.size(); i++) {
				if( ! columns_that_violate_triangle.contains( i ) ) {
					ExclusionZone<Pair<Integer, float[]>> ez = ezs.get(i);
					if (ez.mustBeIn(dists, threshold)) {
						mustBeIn.add(i);
					} else if (ez.mustBeOut(dists, threshold)) {
						cantBeIn.add(i);
					}
				}
			}

			partitionsExcluded += cantBeIn.size() + mustBeIn.size();

			BitSet inclusions = doExclusions(dat, threshold, datarep, cm, q, dat.size(), mustBeIn, cantBeIn);

			if (inclusions == null) {
				for (Pair<Integer, float[]> d : dat) {
					if (cm.distance(q, d) <= threshold) {
						res.add(d);
					}
				}
			} else {
				filterContenders(dat, threshold, cm, q, res, dat.size(), inclusions);
			}

			noOfResults += res.size();
//			System.out.println( " Results\t" + res.size() );
		}

		System.out.println("dists per query\t" + (cm.reset() / queries.size() + noOfRefPoints));
		System.out.println("results\t" + noOfResults);
		System.out.println("time\t" + (System.currentTimeMillis() - t0) / (float) queries.size());
		System.out.println("partitions excluded\t" + ((double) partitionsExcluded / queries.size()));
	}


	public static String getFileName() {
		try {
			throw new RuntimeException();
		} catch (RuntimeException e) {
			return e.getStackTrace()[1].getClassName();
		}
	}
}
