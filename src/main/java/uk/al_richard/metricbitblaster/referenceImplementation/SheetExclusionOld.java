package uk.al_richard.metricbitblaster.referenceImplementation;

import org.apache.commons.math3.stat.regression.SimpleRegression;
import searchStructures.ObjectWithDistance;
import searchStructures.Quicksort;

import java.util.ArrayList;
import java.util.List;

/**
 * @author newrichard
 * 
 *         so the intent is to store info about a pair of reference points and
 *         the partitions they can define but.. not so simple as the main point
 *         of this is to store distance computations at both build and query
 *         time
 * @param <T>
 *            the type of the values
 */
public class SheetExclusionOld<T> extends ExclusionZone<T> {

	private final RefPointSet<T> pointSet;
	private boolean rotationEnabled;

	private final int ref1;
	private final int ref2;
	private boolean fourPoint;
	private double distance;
	private double rotationAngle;
	private double x_offset_for_rotation;

    private double offset;


    @Deprecated
    SheetExclusionOld(RefPointSet<T> pointSet, int ref1, int ref2,
			boolean fourPoint) {
		this.pointSet = pointSet;
		this.ref1 = ref1;
		this.ref2 = ref2;
		this.fourPoint = fourPoint;
		if (fourPoint) {
			this.distance = pointSet.intDist(ref1, ref2);
		}
		this.offset = 0;
		this.setRotationEnabled(false);
	}

    /**
	 * inputs a list of (x,y) coordinates and calculates the gradient of the
	 * best-fit straight line
	 *
	 */
	private void calculateTransform(List<double[]> points) {

		SimpleRegression reg = new SimpleRegression(true);

		for (double[] p : points) {
			reg.addData(p[0], p[1]);
		}
		double grad = reg.getSlope();
		double y_int = reg.getIntercept();

		this.x_offset_for_rotation = -y_int / grad;
		this.rotationAngle = Math.atan(grad);

	}


    private List<double[]> getCoordinates(List<T> witnesses) {

		List<double[]> res = new ArrayList<>();

		for (int i = 0; i < witnesses.size(); i++) {
			double d1 = this.pointSet.extDist(witnesses.get(i), this.ref1);
			double d2 = this.pointSet.extDist(witnesses.get(i), this.ref2);
			// offset is x distance from ref1
			final double x_offset = (d1 * d1 - d2 * d2) / (2 * this.distance)
					+ this.distance / 2;
			final double y_offset = Math.sqrt(d1 * d1 - x_offset * x_offset);
			double[] p = { x_offset, y_offset };
			res.add(p);
		}
		return res;
	}

    private double getNewX(double x, double y) {
		double newX = x - this.x_offset_for_rotation;

		if (!Double.isNaN(this.rotationAngle)) {
			newX = Math.cos(this.rotationAngle) * newX
					+ Math.sin(this.rotationAngle) * y;
		}
		if (Double.isNaN(newX)) {
			throw new RuntimeException("newX is a nan!");
		}
		return newX;
	}

	private double[] getTwoDPoint(double d1, double d2) {
		final double x_offset = (d1 * d1 - d2 * d2) / (2 * this.distance)
				+ this.distance / 2;
		final double y_offset = d1 * d1 - x_offset * x_offset;
		double[] pt = { x_offset, y_offset };
		return pt;
	}



	private double getXOffset(double[] dists) {
		double d1 = dists[this.ref1];
		double d2 = dists[this.ref2];
		double[] pt = getTwoDPoint(d1, d2);
		double x_offset = getNewX(pt[0], pt[1]);
		return x_offset;
	}

	/**
	 * @return true if the point is strictly to the left of a defined partition
	 * 
	 *         this is a bit weird because of the single-distance calculation
	 *         optimisation...
	 */
	@Override
	public boolean isIn(double[] dists) {
		if (this.fourPoint) {
			double x_offset = getXOffset(dists);
			return x_offset < this.offset;
		} else {
			/*
			 * a positive offset means that most points are to the right of
			 * centre, ie d1 > d2 on the whole
			 */
			return (dists[this.ref1] - dists[this.ref2]) - this.offset < 0;
		}
	}

	public boolean isRotationEnabled() {
		return this.rotationEnabled;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ExclusionZone#mustBeIn(java.lang.Object, double[], double)
	 * 
	 * return true if the query is a long way to the left of the defined
	 * partition...
	 */
	@Override
	public boolean mustBeIn(double[] dists, double t) {
		if (this.fourPoint) {
			double x_offset = getXOffset(dists);
			return x_offset < this.offset - t;
		} else {
			return (dists[this.ref1] - dists[this.ref2]) - this.offset < -(2 * t);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ExclusionZone#mustBeIn(java.lang.Object, double[], double)
	 * 
	 * return true if the query is a long way to the right of the defined
	 * partition...
	 */
	@Override
	public boolean mustBeOut(double[] dists, double t) {
		if (this.fourPoint) {
			double x_offset = getXOffset(dists);
			return x_offset >= this.offset + t;
		} else {
			return (dists[this.ref1] - dists[this.ref2]) - this.offset >= (2 * t);
		}
	}

	public void setOffset( double offset ) {
        this.offset = offset;
    }

	public void setRotationEnabled(boolean rotationEnabled) {
		this.rotationEnabled = rotationEnabled;
	}

	public void setWitnesses(List<T> witnesses) {
        if (!this.fourPoint) {
            /*
             * set this.offset to the median value of d1 minus d2, which can be
             * negative; if it's positive, that means the centre line is to the
             * right of the Y-axis
             */
            ObjectWithDistance[] owds = new ObjectWithDistance[witnesses.size()];

            for (int i = 0; i < witnesses.size(); i++) {
                double d1 = this.pointSet.extDist(witnesses.get(i), this.ref1);
                double d2 = this.pointSet.extDist(witnesses.get(i), this.ref2);
                owds[i] = new ObjectWithDistance(null, d1 - d2);
            }
            Quicksort.placeMedian(owds);
            this.offset = owds[owds.length / 2].getDistance();
        } else {
            List<double[]> twoDpoints = new ArrayList<>();
            for (int i = 0; i < witnesses.size(); i++) {
                double d1 = this.pointSet.extDist(witnesses.get(i), this.ref1);
                double d2 = this.pointSet.extDist(witnesses.get(i), this.ref2);
                double[] pt = getTwoDPoint(d1, d2);
                twoDpoints.add(pt);
            }

            if (this.rotationEnabled) {
                this.calculateTransform(twoDpoints);
            }
            /*
             * now rotationAngle and x_offset are set, we need to get rotated xs
             */
            ObjectWithDistance[] owds = new ObjectWithDistance[witnesses.size()];
            for (int i = 0; i < owds.length; i++) {
                double[] pt = twoDpoints.get(i);
                double xOffset = this.rotationEnabled ? getNewX(pt[0], pt[1])
                        : pt[0];
                // double newX = getNewX(pt[0], pt[1]);
                owds[i] = new ObjectWithDistance(null, xOffset);
            }
            Quicksort.placeMedian(owds);
            // offset is now the median x value for the rotated pointset
            this.offset = owds[owds.length / 2].getDistance();
        }
    }


}
