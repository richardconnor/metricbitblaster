package uk.al_richard.metricbitblaster.LIDIMsEuc20;

import dataPoints.cartesian.CartesianPoint;
import testloads.TestContext;
import testloads.TestContext.Context;

import java.util.List;

/**
 * Prints Euc20 for use in Matlab
 */
public class PrintEuc20 {

	private static String TAB = "\t";

	public static void main(String[] args) throws Exception {

		// Only do this once or it goes wrong!
		Context context = Context.euc20;
		TestContext tc = new TestContext(context);
		tc.setSizes(0, 0);
		List<CartesianPoint> full_data = tc.getData();
		for( CartesianPoint point : full_data ) {
			printPoint(point);
		}
	}

	private static void printPoint(CartesianPoint point) {
		double[] doubles = point.getPoint();
		StringBuilder sb = new StringBuilder();
		for( double d : doubles ) {
			sb.append(d);
			sb.append(TAB);
		}
		sb.deleteCharAt(sb.lastIndexOf(TAB));
		System.out.println( sb );
		sb.setLength(0);
	}
}
