package uk.al_richard.metricbitblaster.LIDIMsEuc20;

import dataPoints.cartesian.CartesianPoint;
import uk.al_richard.metricbitblaster.experimental.pivotsAtQueryTime.LabelledCartesianPoint;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Reads in the previously generated LDIMS file for EUC20
 */
public class ReadEucs {

    public static final String filepath = "Euc20Data/Euc20.csv";

    /**
     *
     * @return a list of Eucs
     * @throws IOException if cannot read file, error occurs etc.
     */
    public static List<LabelledCartesianPoint> readEucs() throws IOException {
        List<LabelledCartesianPoint>  list = new ArrayList<>();

        BufferedReader reader = new BufferedReader(new FileReader(filepath));
        String line; // No header row in this file

        while ((line = reader.readLine()) != null) {
            StringTokenizer st = new StringTokenizer(line, "\t");
            double[] data = new double[20];
            for( int i = 0; i < 20; i++ ) { data[i] = Double.parseDouble( st.nextToken() ); }
            LabelledCartesianPoint labelled_point = new LabelledCartesianPoint(new CartesianPoint(data));
            list.add( labelled_point );
        }

        return list;
    }

}
