package uk.al_richard.metricbitblaster.LIDIMsEuc20;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Reads in the previously generated NNs file for EUC20
 */
public class ReadNNs {

    public static final String filepath = "Euc20Data/LIDMS_Euc20.csv";

    /* file format:
     *  qid,lidim,d0,..,d19, nn_id0,..,nn_id19
     */


    /**
     * @return a Map of NNInfo about all of the Euc20 points
     * @throws IOException if cannot read file, error occurs etc.
     */
    public static Map<Integer, NNInfo> readNNInfo() throws IOException {
        Map<Integer, NNInfo> map = new HashMap<>();

        BufferedReader reader = new BufferedReader(new FileReader(filepath));
        String line = reader.readLine(); // note that there is a header row in this file.

        while ((line = reader.readLine()) != null) {
            StringTokenizer st = new StringTokenizer(line, ",");
            int id = Integer.parseInt(st.nextToken());          // read the id
            double lidim = Double.parseDouble(st.nextToken());  // read the lidim

            double dists[] = new double[20];
            for (int i = 0; i < 20; i++) { // read the NN dists
                dists[i] = Double.parseDouble(st.nextToken());
            }

            int[] nn_ids = new int[20];
            for (int i = 0; i < 20; i++) { // read the NN dists
                nn_ids[i] = Integer.parseInt(st.nextToken());
            }
            NNInfo nnifo = new NNInfo(id, lidim, dists, nn_ids);
            map.put(id, nnifo);
        }
        return map;
    }


}
