package uk.al_richard.metricbitblaster.LIDIMsEuc20;

public class NNInfo {
    public final int id;
    public final double lidim;
    public final double[] dists;
    public final int[] nn_ids;

    public NNInfo(int id, double lidim, double[] dists, int[] nn_ids) {
        this.id = id;
        this.lidim = lidim;
        this.dists = dists;
        this.nn_ids = nn_ids;
    }
}
