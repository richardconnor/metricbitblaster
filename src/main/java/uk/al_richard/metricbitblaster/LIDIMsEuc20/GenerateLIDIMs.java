package uk.al_richard.metricbitblaster.LIDIMsEuc20;

import coreConcepts.CountedMetric;
import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;
import dataPoints.cartesian.Euclidean;
import testloads.TestContext;
import testloads.TestContext.Context;
import uk.al_richard.metricbitblaster.experimental.pivotsAtQueryTime.*;
import uk.al_richard.metricbitblaster.util.OpenBitSet;
import util.OrderedList;

import java.util.*;

/**
 *  Generates LIDMS (Levina Bickel) and NNs for all points in a context
 *      output format: qid,lidim,d0,..,d19, nn_id0,..,nn_id19
 */
public class GenerateLIDIMs {

	protected static final double RADIUS_INCREMENT = 0.3;
	private static double MEAN_DIST = 1.81;
	protected static double[] ball_radii = new double[] { MEAN_DIST - 2 * RADIUS_INCREMENT,
			MEAN_DIST - RADIUS_INCREMENT, MEAN_DIST, MEAN_DIST - RADIUS_INCREMENT, MEAN_DIST - 2 * RADIUS_INCREMENT };

	protected static Metric<LabelledCartesianPoint> metric = new Euclidean();

	public static void main(String[] args) throws Exception {

		Context context = Context.euc20;

		boolean fourPoint = true;
		boolean balanced = false;
		boolean rotationEnabled = true;
		int noOfRefPoints = 100; // 5X normal number for Euc20.

		int nChoose2 = ((noOfRefPoints - 1) * noOfRefPoints) / 2;
		int noOfBitSets = nChoose2 + (noOfRefPoints * ball_radii.length);

		TestContext tc = new TestContext(context);
		int querySize = (context == Context.colors || context == Context.nasa) ? tc.dataSize() / 10 : 1000;
		List<LabelledCartesianPoint> dat = applyLabels( tc.getData() );
		List<LabelledCartesianPoint> refs = getSubset( dat,noOfRefPoints );
		double threshold = tc.getThresholds()[0];

		int nns_required = 20;

		System.out.println("Date/time\t" + new Date().toString());
		System.out.println("Class\t" + getFileName());
		System.out.println("context\t" + context);
		System.out.println("nns required for LIDIM \t" + nns_required);
		System.out.print( "qid" + "\t" + "lidim" + "\t" );
		for( int i = 0; i < nns_required; i++ ) {
			System.out.print( "d" + i + "\t" );
		}
		for( int i = 0; i < nns_required; i++ ) {
			System.out.print( "nn_id" + i + "\t" );
		}
		System.out.println();

		RefPointSet<LabelledCartesianPoint> rps = new RefPointSet<LabelledCartesianPoint>(refs, metric);
		List<ExclusionZone<LabelledCartesianPoint>> ezs = new ArrayList<>();

		ezs.addAll(getSheetExclusions(dat, refs, rps, balanced, fourPoint, rotationEnabled));
		ezs.addAll(getBallExclusions(dat, refs, rps, balanced));

		OpenBitSet[] datarep = new OpenBitSet[noOfBitSets];

		buildBitSetData(dat, datarep, rps, ezs);

		List<LabelledCartesianPoint> all_points = new ArrayList<>();
		all_points.addAll( refs );
		all_points.addAll( dat );

		for( LabelledCartesianPoint query : all_points ) {
			List<LabelledCartesianPoint> res = new ArrayList<>();
			while( res.size() < nns_required ) {
				CantBeMustBes cbimbi = getEzs(query, threshold, rps, ezs);
				res = querySubset(query, dat, metric, threshold, datarep, cbimbi);
				threshold = threshold + ( threshold / 10 );
			}
			res = sortAndFilter( res,query,nns_required );
			printDiagnostics( query, metric, res );
		}
	}

	/**
	 * first sort into distances then filter out closest.
	 */
	private static List<LabelledCartesianPoint> sortAndFilter(List<LabelledCartesianPoint> res, LabelledCartesianPoint query, int nns_required) {

		OrderedList<LabelledCartesianPoint,Double> sorted = new OrderedList<>(nns_required);
		for( LabelledCartesianPoint pt : res ) {
			double distance = metric.distance( query, pt );
			sorted.add( pt,distance );
		}
		return sorted.getList();
	}


	private static List<LabelledCartesianPoint> getSubset(List<LabelledCartesianPoint> dat, int number) {
		List<LabelledCartesianPoint> res = new ArrayList();
		for(int i = 0; i < number; ++i) {
			res.add(dat.remove(0));
		}
		return res;
	}

	private static List<LabelledCartesianPoint> applyLabels(List<CartesianPoint> data) {
		List<LabelledCartesianPoint> result = new ArrayList<>();
		for( CartesianPoint cp : data ) {
			result.add( new LabelledCartesianPoint(cp));
		}
		return result;
	}

	private static void printDiagnostics(LabelledCartesianPoint query, Metric<LabelledCartesianPoint> metric, List<LabelledCartesianPoint> nns) throws Exception {

		List<Double> dists = new ArrayList<>();
		for( LabelledCartesianPoint pt : nns ) {
			dists.add( metric.distance( query,pt ) );
		}
		double lidim = LIDimLevinaBickel(dists);
		System.out.print( query.label + "\t" + lidim + "\t" );
		for( double dist : dists ) {
			System.out.print( dist + "\t" );
		}
		for( LabelledCartesianPoint nn : nns ) {
			System.out.print( nn.label + "\t" );
		}
		System.out.println();
	}

	private static <T> List<T> querySubset(T query, List<T> dat, Metric<T> metric, double threshold, OpenBitSet[] datarep, CantBeMustBes selected) {

		List<Integer> mustBeIn = selected.must_bes;
		List<Integer> cantBeIn = selected.cant_bes;

		List<T> res = new ArrayList<>();

		OpenBitSet inclusions = doExclusions(dat, threshold, datarep, metric, query, dat.size(), mustBeIn, cantBeIn);

		if (inclusions == null) {
			for (T d : dat) {
				double dist = metric.distance(query, d);
				if (dist <= threshold && dist != 0d ) {
					res.add(d);
				}
			}
		} else {
			filterContenders(dat, threshold, metric, query, res, dat.size(), inclusions);
		}

		return res;
	}

	/**
	 * MLE IDIM Estimator by Levina and Bickel in paper "Maximum Likelihood
	 * Estimation of Intrinsic Dimension".
	 * @param dists - a list of distances.
	 * @return the MLE IDIM of the list
	 * @throws Exception if a zero length list is supplied.
	 */
	public static double LIDimLevinaBickel(List<Double> dists) throws Exception {
		int count = dists.size();
		double one_over_k_minus_one = 1.0d / ((double) count - 1.0d);

		if (count > 0) {
			double max = max(dists);

			double sigma = 0; // sum

			for (int i = 0; i < count - 1; i++) {
				double d = dists.get(i);
				sigma += Math.log(max / d);
			}
			return 1.0d / (one_over_k_minus_one * sigma);
		} else {
			throw new Exception("zero length list");
		}
	}

	public static double max(List<Double> list) {
		if (list.size() == 0) {
			throw new RuntimeException("Empty list");
		}
		double max_so_far = Double.MIN_VALUE;
		for (double d : list) {

			max_so_far = Math.max( d,max_so_far );
		}
		return max_so_far;
	}

	static <T> void queryBitSetData(List<T> queries, List<T> dat, Metric<T> metric, double threshold, OpenBitSet[] datarep,
									RefPointSet<T> rps, List<ExclusionZone<T>> ezs, int noOfRefPoints) {

		int noOfResults = 0;
		int partitionsExcluded = 0;
		CountedMetric<T> cm = new CountedMetric<>(metric);
		long t0 = System.currentTimeMillis();
		for (T q : queries) {
			List<T> res = new ArrayList<>();
			double[] dists = rps.extDists(q, res, threshold);

			List<Integer> mustBeIn = new ArrayList<>();
			List<Integer> cantBeIn = new ArrayList<>();

			for (int i = 0; i < ezs.size(); i++) {
				ExclusionZone<T> ez = ezs.get(i);
				if (ez.mustBeIn(dists, threshold)) {
					mustBeIn.add(i);
				} else if (ez.mustBeOut(dists, threshold)) {
					cantBeIn.add(i);
				}
			}

			partitionsExcluded += cantBeIn.size() + mustBeIn.size();

			OpenBitSet inclusions = doExclusions(dat, threshold, datarep, cm, q, dat.size(), mustBeIn, cantBeIn);

			if (inclusions == null) {
				for (T d : dat) {
					if (cm.distance(q, d) <= threshold) {
						res.add(d);
					}
				}
			} else {
				filterContenders(dat, threshold, cm, q, res, dat.size(), inclusions);
			}

			noOfResults += res.size();
		}
	}

	public static List<ExclusionZone<LabelledCartesianPoint>> getBallExclusions(List<LabelledCartesianPoint> dat,
			List<LabelledCartesianPoint> refs, RefPointSet<LabelledCartesianPoint> rps, boolean balanced) {
		List<ExclusionZone<LabelledCartesianPoint>> res = new ArrayList<>();
		for (int i = 0; i < refs.size(); i++) {
			List<BallExclusion<LabelledCartesianPoint>> balls = new ArrayList<>();
			for (double radius : ball_radii) {
				BallExclusion<LabelledCartesianPoint> be = new BallExclusion<>(rps, i, radius);
				balls.add(be);
			}
			if (balanced) {
				balls.get(0).setWitnesses(dat.subList(0, 1000));
				double midRadius = balls.get(0).getRadius();
				double thisRadius = midRadius - ((balls.size() / 2) * RADIUS_INCREMENT);
				for (int ball = 0; ball < balls.size(); ball++) {
					balls.get(ball).setRadius(thisRadius);
					thisRadius += RADIUS_INCREMENT;
				}
			}
			res.addAll(balls);
		}
		return res;
	}

	public static List<ExclusionZone<LabelledCartesianPoint>> getSheetExclusions(List<LabelledCartesianPoint> dat,
			List<LabelledCartesianPoint> refs, RefPointSet<LabelledCartesianPoint> rps, boolean balanced, boolean fourPoint,
			boolean rotationEnabled) {
		List<ExclusionZone<LabelledCartesianPoint>> res = new ArrayList<>();
		for (int i = 0; i < refs.size() - 1; i++) {
			for (int j = i + 1; j < refs.size(); j++) {
				if (fourPoint) {
					SheetExclusion4p<LabelledCartesianPoint> se = new SheetExclusion4p<>(rps, i, j);
					if (balanced) {
						se.setRotationEnabled(rotationEnabled);
						se.setWitnesses(dat.subList(0, 5001));
					}
					res.add(se);
				} else {
					SheetExclusion3p<LabelledCartesianPoint> se = new SheetExclusion3p<>(rps, i, j);
					if (balanced) {
						se.setWitnesses(dat.subList(0, 5001));
					}
					res.add(se);
				}
			}
		}
		return res;
	}

	public static <T> void buildBitSetData(List<T> data, OpenBitSet[] datarep, RefPointSet<T> rps,
			List<ExclusionZone<T>> ezs) {
		int dataSize = data.size();
		for (int i = 0; i < datarep.length; i++) {
			datarep[i] = new OpenBitSet(dataSize);
		}
		for (int n = 0; n < dataSize; n++) {
			T p = data.get(n);
			double[] dists = rps.extDists(p);
			for (int x = 0; x < datarep.length; x++) {
				boolean isIn = ezs.get(x).isIn(dists);
				if (isIn) {
					datarep[x].set(n);
				}
			}
		}
	}

	protected static <T> OpenBitSet doExclusions(List<T> dat, double t, OpenBitSet[] datarep, Metric<T> cm, T q,
			final int dataSize, List<Integer> mustBeIn, List<Integer> cantBeIn) {
		if (mustBeIn.size() != 0) {
			OpenBitSet ands = getAndOpenBitSets(datarep, dataSize, mustBeIn);
			if (cantBeIn.size() != 0) {
				/*
				 * hopefully the normal situation or we're in trouble!
				 */
				OpenBitSet nots = getOrOpenBitSets(datarep, dataSize, cantBeIn);
				nots.flip(0, dataSize);
				ands.and(nots);
				return ands;
				// filterContenders(dat, t, cm, q, res, dataSize, ands);
			} else {
				// there are no cantBeIn partitions
				return ands;
				// filterContenders(dat, t, cm, q, res, dataSize, ands);
			}
		} else {
			// there are no mustBeIn partitions
			if (cantBeIn.size() != 0) {
				OpenBitSet nots = getOrOpenBitSets(datarep, dataSize, cantBeIn);
				nots.flip(0, dataSize);
				return nots;
				// filterContenders(dat, t, cm, q, res, dataSize, nots);
			} else {
				// there are no exclusions at all...
				return null;
				// for (T d : dat) {
				// if (cm.distance(q, d) < t) {
				// res.add(d);
				// }
				// }
			}
		}
	}

	static <T> void filterContenders(List<T> dat, double t, Metric<T> cm, T q, Collection<T> res,
			final int dataSize, OpenBitSet results) {
		for (int i = results.nextSetBit(0); i != -1 && i < dataSize; i = results.nextSetBit(i + 1)) {
			// added i < dataSize since Cuda code overruns datasize in the -
			// don't know case - must be conservative and include, easier than
			// complex bit masking and shifting.
			if (cm.distance(q, dat.get(i)) <= t && ! q.equals(dat.get(i))) { // always filter out the query point.
				res.add(dat.get(i));
			}
		}
	}

	static OpenBitSet getAndOpenBitSets(OpenBitSet[] datarep, final int dataSize, List<Integer> mustBeIn) {
		OpenBitSet ands = null;
		if (mustBeIn.size() != 0) {
			ands = datarep[mustBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < mustBeIn.size(); i++) {
				ands.and(datarep[mustBeIn.get(i)]);
			}
		}
		return ands;
	}

	static OpenBitSet getOrOpenBitSets(OpenBitSet[] datarep, final int dataSize, List<Integer> cantBeIn) {
		OpenBitSet nots = null;
		if (cantBeIn.size() != 0) {
			nots = datarep[cantBeIn.get(0)].get(0, dataSize);
			for (int i = 1; i < cantBeIn.size(); i++) {
				final OpenBitSet nextNot = datarep[cantBeIn.get(i)];
				nots.or(nextNot);
			}
		}
		return nots;
	}

	/**
	 * This is derived from first half of queryBitSetData
	 */
	static <T> CantBeMustBes getEzs(T query, double threshold, RefPointSet<T> rps, List<ExclusionZone<T>> ezs) {

		long t0 = System.currentTimeMillis();

		List<T> res = new ArrayList<>();
		double[] dists = rps.extDists(query, res, threshold);

		List<Integer> mustBeIn = new ArrayList<>();
		List<Integer> cantBeIn = new ArrayList<>();

		for (int i = 0; i < ezs.size(); i++) {
			ExclusionZone<T> ez = ezs.get(i);
			if (ez.mustBeIn(dists, threshold)) {
				mustBeIn.add(i);
			} else if (ez.mustBeOut(dists, threshold)) {
				cantBeIn.add(i);
			}
		}

		return new CantBeMustBes( cantBeIn, mustBeIn );
	}

	public static String getFileName() {
		try {
			throw new RuntimeException();
		} catch (RuntimeException e) {
			return e.getStackTrace()[1].getClassName();
		}
	}
}
