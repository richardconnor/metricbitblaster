package uk.al_richard.metricbitblaster.LIDIMsEuc20;

import java.io.*;
import java.util.*;

public class CSVReader {
    public static List<String> read(String filePath, Boolean has_header, Boolean keep_header) {

        List<String> result = new ArrayList<>();
        String line = null;

        try {
            BufferedReader reader = new BufferedReader(new FileReader(filePath));

            while ((line = reader.readLine()) != null) {
                result.add(line);
            }
        }
        catch (FileNotFoundException e ) {
            e.printStackTrace();
        }
        catch (IOException e ) {
            e.printStackTrace();
        }

        if(has_header && !keep_header) {
            result.remove(0);
        }

        return result;
    }
}