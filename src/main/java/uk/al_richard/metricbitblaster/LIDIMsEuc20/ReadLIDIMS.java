package uk.al_richard.metricbitblaster.LIDIMsEuc20;

import org.apache.commons.math3.util.Pair;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Reads in the previously generated LDIMS file for EUC20
 */
public class ReadLIDIMS {

    public static final String filepath = "Euc20Data/LIDMS_Euc20.csv";

    /* file format:
     *  qid,lidim,d0,..,d19, nn_id0,..,nn_id19
     */

    private static Comparator<? super Pair<Integer, Double>> comparator = new Comparator<Pair<Integer, Double>>() {
        @Override
        public int compare(Pair<Integer, Double> o1, Pair<Integer, Double> o2) {
            return o1.getSecond().compareTo(o2.getSecond());
        }
    };

    public static void main(String[] args) throws IOException {
        // Quick test
        List<Pair<Integer, Double>> list = getSortedIndexLIDIMPairs();
        for (int i = 1; i < 10; i++) {
            System.out.println(list.get(i).getFirst() + " " + list.get(i).getSecond());
        }
    }

    public static List<Pair<Integer, Double>> getSortedIndexLIDIMPairs() throws IOException {
        List<Pair<Integer, Double>> list = readIndexLIDIMPairs();
        Collections.sort(list, comparator);
        return list;
    }

    public static Map<Integer, Double> getLIDIMMap() throws IOException {
        Map<Integer, Double> map = new HashMap<>();
        List<Pair<Integer, Double>> list = readIndexLIDIMPairs();
        for( Pair<Integer,Double> pair : list ) {
            map.put( pair.getFirst(), pair.getSecond() );
        }
        return map;
    }

    /**
     * @return a list of index,LIDIm pairs
     * @throws IOException if cannot read file, error occurs etc.
     */
    public static List<Pair<Integer, Double>> readIndexLIDIMPairs() throws IOException {
        List<Pair<Integer, Double>> list = new ArrayList<>();

        BufferedReader reader = new BufferedReader(new FileReader(filepath));
        String line = reader.readLine(); // throw away the header row

        while ((line = reader.readLine()) != null) {
            StringTokenizer st = new StringTokenizer(line, "\t");
            int id = Integer.parseInt(st.nextToken());
            double lidim = Double.parseDouble(st.nextToken());
            list.add(new Pair<>(id, lidim));
        }
        return list;
    }
}
