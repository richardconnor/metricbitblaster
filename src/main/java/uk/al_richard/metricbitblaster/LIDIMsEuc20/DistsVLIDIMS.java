package uk.al_richard.metricbitblaster.LIDIMsEuc20;

import coreConcepts.Metric;
import dataPoints.cartesian.CartesianPoint;
import dataPoints.cartesian.Euclidean;
import uk.al_richard.metricbitblaster.experimental.pivotsAtQueryTime.LabelledCartesianPoint;

import java.util.List;
import java.util.Map;

/**
 * Prints Euc20 for use in Matlab
 */
public class DistsVLIDIMS {

	private static String TAB = "\t";

	static Metric<LabelledCartesianPoint>  metric = new Euclidean<>();

	static LabelledCartesianPoint middle = new LabelledCartesianPoint( -1,
			new CartesianPoint( new double[]{ 0.5d,0.5d,0.5d,0.5d,0.5d,
					0.5d,0.5d,0.5d,0.5d,0.5d,
					0.5d,0.5d,0.5d,0.5d,0.5d,
					0.5d,0.5d,0.5d,0.5d,0.5d } ) );

	public static void main(String[] args) throws Exception {

		Map<Integer, NNInfo> map = ReadNNs.readNNInfo();
		List<LabelledCartesianPoint> eucs = ReadEucs.readEucs();
		System.out.println( "index" + TAB + "mid_dist" + TAB + "lidim" + TAB + "d_0" + TAB + "d19"  );
		for( LabelledCartesianPoint euc : eucs ) {
			int index = euc.label;
			double mid_dist = metric.distance( euc,middle );
			System.out.println( index + TAB + mid_dist + TAB + map.get(index).lidim + TAB + map.get(index).dists[0] + TAB + map.get(index).dists[19] );
		}

	}

}
