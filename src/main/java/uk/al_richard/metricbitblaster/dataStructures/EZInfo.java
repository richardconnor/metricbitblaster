package uk.al_richard.metricbitblaster.dataStructures;

import dataPoints.cartesian.CartesianPoint;
import uk.al_richard.metricbitblaster.referenceImplementation.ExclusionZone;

public class EZInfo {
    public final double average_distance;
    public final ExclusionZone<CartesianPoint> ez;
    public final int position;

    public EZInfo(int position, double average_distance, ExclusionZone<CartesianPoint> ez) {
        this.position = position;
        this.average_distance = average_distance;
        this.ez = ez;
    }
}
