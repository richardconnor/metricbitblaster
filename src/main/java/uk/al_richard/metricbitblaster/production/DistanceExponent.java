package uk.al_richard.metricbitblaster.production;

import coreConcepts.Metric;
import uk.al_richard.metricbitblaster.referenceImplementation.BallExclusion;
import uk.al_richard.metricbitblaster.referenceImplementation.ExclusionZone;
import uk.al_richard.metricbitblaster.referenceImplementation.RefPointSet;
import uk.al_richard.metricbitblaster.referenceImplementation.SheetExclusion4p;
import uk.al_richard.metricbitblaster.util.DistanceHistogram;
import uk.al_richard.metricbitblaster.util.OpenBitSet;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.BiFunction;

/**
 * This code using the distance exponent technique to calculate IDM
 * Distance Exponent: A New Concept for Selectivity Estimation in Metric Trees
 * @article{traina_distance_nodate,
 * title = {Distance Exponent: A New Concept for Selectivity Estimators},
 * abstract = {We discuss the problem of selectivity estimation for range queries in real metric datasets, which include spatial, or dimensional, datasets as a special case. The main contribution of this paper is that, surprisingly, many diverse datasets follow a “power law”. This is the first analysis of distance distributions for metric datasets.},
 * pages = {14},
 * author = {Traina, Caetano and Traina, Agma and Faloutsos, Christos},
 * langid = {english},
 * file = {Distance Exponent A New Concept for Selectivity E.pdf:/Users/al/Zotero/storage/NKB3YGJF/Distance Exponent A New Concept for Selectivity E.pdf:application/pdf}
 * }
 */
public class DistanceExponent<T> {

	protected static final double RADIUS_INCREMENT = 0.3;

	private static double MEAN_DIST = 1.81;
	protected static double[] ball_radii = new double[] {
			MEAN_DIST - 2 * RADIUS_INCREMENT, MEAN_DIST - RADIUS_INCREMENT,
			MEAN_DIST, MEAN_DIST - RADIUS_INCREMENT,
			MEAN_DIST - 2 * RADIUS_INCREMENT };

	private DistanceHistogram dh;

	public DistanceExponent(BiFunction<T, T, Double> distfn, List<T> dat) throws Exception {

		int noOfRefPoints = 200;
		int nChoose2 = ((noOfRefPoints - 1) * noOfRefPoints) / 2;
		int noOfBitSets = nChoose2 + (noOfRefPoints * ball_radii.length);

		Metric<T> m = new Metric<T>() {

			@Override
			public double distance(T o1, T o2) {
				return distfn.apply(o1, o2);
			}

			@Override
			public String getMetricName() {
				return "supplied";
			}
		};

		List<T> refs = chooseRandomReferencePoints( dat,noOfRefPoints );

		RefPointSet<T> rps = new RefPointSet<>(refs, m);
		List<ExclusionZone<T>> ezs = new ArrayList<>();

		addSheetExclusions(dat, refs, rps, ezs, false, false);
		addBallExclusions(dat, refs, rps, ezs, false);

//		System.out.println("ezs size:\t" + ezs.size());

		OpenBitSet[] datarep = new OpenBitSet[noOfBitSets];
		dh = new DistanceHistogram();
		buildBitSetData(dat, datarep, dh, rps, ezs);

	}

	public double IDIM() {
		return trainaIDIM();
	}

	public double trainaIDIM() {
		dh.setMeasures();
		return dh.IDIM();
	}

	public double chavezIDIM() {
		dh.setMeasures();
		BigDecimal mean = new BigDecimal( dh.getMean());
		BigDecimal std_dev = new BigDecimal( dh.getStdDev());
		return mean.pow(2).divide(std_dev.pow(2).multiply(new BigDecimal(2)), RoundingMode.HALF_UP).doubleValue();
	}

	//--------------------------------- private methods ---------------------------------


	private void addBallExclusions(List<T> dat, List<T> refs,
								   RefPointSet<T> rps, List<ExclusionZone<T>> ezs, boolean balanced) {
		for (int i = 0; i < refs.size(); i++) {
			List<BallExclusion<T>> balls = new ArrayList<>();
			for (double radius : ball_radii) {
				BallExclusion<T> be = new BallExclusion<>(rps, i, radius);
				balls.add(be);
			}
			ezs.addAll(balls);
		}
	}

	private void addSheetExclusions(List<T> dat,
                                    List<T> refs, RefPointSet<T> rps,
                                    List<ExclusionZone<T>> ezs, boolean balanced,
                                    boolean fourPoint) {
		for (int i = 0; i < refs.size() - 1; i++) {
			for (int j = i + 1; j < refs.size(); j++) {
				SheetExclusion4p<T> se = new SheetExclusion4p<>(rps, i, j);
				ezs.add(se);
			}
		}
	}

	private void buildBitSetData(List<T> data, OpenBitSet[] datarep, DistanceHistogram dh,
								 RefPointSet<T> rps, List<ExclusionZone<T>> ezs) {
		int dataSize = data.size();
		for (int i = 0; i < datarep.length; i++) {
			datarep[i] = new OpenBitSet(dataSize);
		}
		for (int n = 0; n < dataSize; n++) {
			T p = data.get(n);
			double[] dists = rps.extDists(p);
			dh.add( dists );
			for (int x = 0; x < datarep.length; x++) {
				boolean isIn = ezs.get(x).isIn(dists);
				if (isIn) {
					datarep[x].set(n);
				}
			}
		}
	}

	/**
	 *
	 * @param data - the dataset from which Reference objects are chosen
	 * @param number_of_reference_points the number of points to choose
	 * @param <X> the type of the data
	 * @return a list of randomly chosen points from data
	 */
	private static <X> List<X> chooseRandomReferencePoints(final List<X> data, int number_of_reference_points) {

		if (number_of_reference_points >= data.size()) {
			return data;
		}

		List<X> reference_points = new ArrayList<>();
		Random random = new Random();

		while (reference_points.size() < number_of_reference_points) {
			X item = data.get(random.nextInt(data.size()));
			if (!reference_points.contains(item)) {
				reference_points.add(item);
			}
		}

		return reference_points;
	}
}
