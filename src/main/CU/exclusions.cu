#include <cuda_runtime.h>
#include <cuda.h>
#include "math.h"
#include <stdio.h>

// Return the number of elements in an array
#define NELEMS(x)  (sizeof(x) / sizeof((x)[0])) // doesn't work with dynamic memory in cuda!

#define LONGSIZE 8

// setBit(A, k): set the kth bit in the bit array of longs A:
#define setBit(A,k)     ( A[(k/LONGSIZE)] |= (1 << (k%LONGSIZE)) )

// clearBit(A, k): clear the kth bit in the bit array of longs A:
#define clearBit(A,k)   ( A[(k/LONGSIZE)] &= ~(1 << (k%LONGSIZE)) )

// testBit(A, k): test the kth bit in the bit array of longs A:
#define testBit(A,k)    ( A[(k/LONGSIZE)] & (1 << (k%LONGSIZE)) )

#define TRUE 1
#define FALSE 0

__device__ int* getNthPair(int i) {

        int* result = (int*) malloc( sizeof(int) * 2 );

        int guess = (int) floor( sqrt( (float) 2 * i ) );
        int ch3 = ((guess + 1) * guess) / 2;

        if (ch3 == i) {
            result[0] = 0;
            result[1] = guess + 1;
            return result;
        } else if (ch3 < i) {
            result[0] = i - ch3;
            result[1] = guess + 1;
            return result;
        } else {
             result[0] = guess - (ch3 - i);
             result[1] = guess;
             return result;
        }
}
	
/*
  Cuda implementation of:
    private static void ball_mustBeInOrOut(int ref_point_index, int num_sheets, double threshold, double[] ball_radii, double[] dists, List<Integer> mustBeIn, List<Integer> cantBeIn) {


        int num_radii = ball_radii.length;

        for ( int radius_index = 0; radius_index < num_radii; radius_index++ ) {
            double radius = ball_radii[radius_index];

            int dists_index =   radius_index + // get us past previous radii in this ball
                                num_sheets +  // get past the sheets
                                (ref_point_index * num_radii); // get us past the previous balls

            if (dists[ref_point_index] < radius - threshold) {   // was ball mustBeIn - return dists[this.ref] < this.radius - t;
                mustBeIn.add(dists_index);
            } else if (dists[ref_point_index] >= radius + threshold) { // was ball mustBeOut - return dists[this.ref] >= this.radius + t;
                cantBeIn.add(dists_index);
            }
        }
    }
 */
extern "C"
__global__ void ball_mustBeInOrOut( int n_ros, int num_sheets, int num_radii, double *threshold, double *ball_radii, double *dists, int *mustBeIn, int *cantBeIn )
{
  // threadIdx.x contains the index of the current thread within its block
  // blockDim.x contains the number of threads in the block
  // blockIdx.x, which contains the index of the current thread block in the grid.
  // gridDim.x, which contains the number of blocks in the grid

    int thread_offset = blockIdx.x * blockDim.x + threadIdx.x;
    int stride = blockDim.x * gridDim.x;

    for (int index = thread_offset; index < n_ros; index += stride) {

        for( int radius_index = 0; radius_index < num_radii ; radius_index++ ) {

            double radius = ball_radii[radius_index];

            int dists_index =   radius_index + // get us past previous radii in this ball
                                num_sheets +  // get past the sheets
                                (index * num_radii); // get us past the previous balls

            if (dists[index] <  radius - threshold[0]) {
                    mustBeIn[dists_index] = 1;
            } else if (dists[index] >= radius + threshold[0]) {
                    cantBeIn[dists_index] = 1;
            }
        }
			
    }
}


/*
  Cuda implementation of:
     private static void sheet_mustBeInOrOut(int index, double threshold, double[] dists, OpenBitSet mustBeIn, OpenBitSet cantBeIn ) {

        int[] coords = FastPerms.getNthPair(index);
        int i = coords[0];
        int j = coords[1];
        if (dists[i] - (dists[j]) < -(2 * threshold)) {  // was sheet mustBeIn - only 3 point - return  (dists[this.ref1] - dists[this.ref2]) - this.offset < -(2 * t);
            mustBeIn.set(index);
        } else if (dists[i] - (dists[j]) >= 2 * threshold) { // was sheet mustBeOut - only 3 point - return (dists[this.ref1] - dists[this.ref2]) - this.offset >= (2 * t);
            cantBeIn.set(index);
        }
    }
 */
extern "C"
__global__ void sheet_mustBeInOrOut( int n_sheets, double *threshold, double *dists, int *mustBeIn, int *cantBeIn )
{

  // threadIdx.x contains the index of the current thread within its block
  // blockDim.x contains the number of threads in the block
  // blockIdx.x, which contains the index of the current thread block in the grid.
  // gridDim.x, which contains the number of blocks in the grid

  int thread_offset = blockIdx.x * blockDim.x + threadIdx.x;
  int stride = blockDim.x * gridDim.x;
  
  for (int index = thread_offset; index < n_sheets; index += stride) {

         int *coords = getNthPair(index);
         int i = coords[0];
         int j = coords[1];
		 free( coords );

         double difference = dists[i] - dists[j];
         double two_t = 2 * threshold[0];
         if( difference < -two_t ) { // was sheet mustBeIn - only 3 point
              mustBeIn[index] = 1;
         } else if( difference >= two_t ) { // was sheet mustBeOut - only 3 point
              cantBeIn[index] = 1;
	     }
  }
}

/*
Cuda impl of:
    private static void mustBeInOrOut(int index, double threshold, int number_sheets, double[] ball_radii, double[] dists, List<Integer> mustBeIn, List<Integer> cantBeIn) {

        if( index < number_sheets ) { // sheet exclusion
            int[] coords = FastPerms.getNthPair(index);
            int i = coords[0];
            int j = coords[1];
            if (dists[i] - (dists[j]) < -(2 * threshold)) {  // was sheet mustBeIn - only 3 point - return  (dists[this.ref1] - dists[this.ref2]) - this.offset < -(2 * t);
                mustBeIn.add(index);
            } else if (dists[i] - (dists[j]) >= 2 * threshold) { // was sheet mustBeOut - only 3 point - return (dists[this.ref1] - dists[this.ref2]) - this.offset >= (2 * t);
                cantBeIn.add(index);
            }
        } else { // ball exclusion

            int ro_index = (index - number_sheets) / ball_radii.length;         // which ro we are looking at
            int radius_index = (index - number_sheets) % ball_radii.length;     // index of the radius relative to the current ro.

            double radius = ball_radii[radius_index];

            if (dists[ro_index] < radius - threshold) {   // was ball mustBeIn - return dists[this.ref] < this.radius - t;
                mustBeIn.add(index);
            } else if (dists[ro_index] >= radius + threshold) { // was ball mustBeOut - return dists[this.ref] >= this.radius + t;
                cantBeIn.add(index);
            }
        }
    }
*/
extern "C"
__global__ void mustBeInOrOut( int n_ezs, double *threshold, int number_sheets, int num_radii, double *ball_radii, double *dists, int *mustBeIn, int *cantBeIn )
{

  // threadIdx.x contains the index of the current thread within its block
  // blockDim.x contains the number of threads in the block
  // blockIdx.x, which contains the index of the current thread block in the grid.
  // gridDim.x, which contains the number of blocks in the grid

  int thread_offset = blockIdx.x * blockDim.x + threadIdx.x;
  int stride = blockDim.x * gridDim.x;

  for (int index = thread_offset; index < n_ezs; index += stride) {

    if( index < number_sheets ) { // sheet exclusion
         // code body same as sheet_mustBeInOrOut
         int *coords = getNthPair(index);
         int i = coords[0];
         int j = coords[1];
         free( coords );

         double difference = dists[i] - dists[j];
         double two_t = 2 * threshold[0];
         if( difference < -two_t ) { // was sheet mustBeIn - only 3 point
              mustBeIn[index] = 1;
         } else if( difference >= two_t ) { // was sheet mustBeOut - only 3 point
              cantBeIn[index] = 1;
    	 }
    }
    else { // ball exclusion
         int ro_index = (index - number_sheets) / num_radii;         // which ro we are looking at
         int radius_index = (index - number_sheets) % num_radii;     // index of the radius relative to the current ro.

         double radius = ball_radii[radius_index];

          if (dists[ro_index] < radius - threshold[0]) {   // was ball mustBeIn - return dists[this.ref] < this.radius - t;
               mustBeIn[index] = 1;
          } else if (dists[ro_index] >= radius + threshold[0]) { // was ball mustBeOut - return dists[this.ref] >= this.radius + t;
               cantBeIn[index] = 1;
          }

    }
  }
}

extern "C"
__global__ void do_exclusions( int datasize_in_longs, int n_ezs, long **data, int *mustBeIn, int *cantBeIn, long *results )
{
  // threadIdx.x contains the index of the current thread within its block
  // blockDim.x contains the number of threads in the block
  // blockIdx.x, which contains the index of the current thread block in the grid.
  // gridDim.x, which contains the number of blocks in the grid

  int thread_offset = blockIdx.x * blockDim.x + threadIdx.x;
  int stride = blockDim.x * gridDim.x;


  for (int row = thread_offset; row < datasize_in_longs; row += stride) {

        int first = TRUE;

        for( int column_id = 0; column_id < n_ezs ; column_id++ ) {

            if( mustBeIn[column_id] == TRUE ) {
                if( first ) {
                    results[row] = data[column_id][row];
                    first = FALSE;
                } else {
                    results[row] &= data[column_id][row];
                }
            }
            if( cantBeIn[column_id] == TRUE ) {
                if( first ) {
                    results[row] = ~ data[column_id][row];
                    first = FALSE;
                } else {
                     // and not:
                     results[row] &= ~ data[column_id][row];
                }
            }
        }
        if( first ) {  //  not excluded or included - don't know case - must be conservative and include.
            results[row] = -1l;  // was ~ (results[row] & 0); // set all bits to 1 - honestly!
        }
  }
}

